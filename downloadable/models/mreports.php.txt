<?php
class mReports extends Model {


	function mReports()
    {
        // Call the Model constructor
        parent::Model();
    }

	function member_report( $mem_key, $beg_date = 0, $end_date = 'today', $include_services = 1, $include_offerings = 1, $include_stats=1, $tnServices = 0, $include_family_info = 1 )
	{ 
		$beg_date_str = strtotime ( $beg_date );
		$beg_date_f = date( 'Y-m-d', $beg_date_str );
		
		if( $end_date == 'today' ){ 
			$end_date_str = time();
		} else {
			$end_date_str = strtotime( $end_date );
		}
				
		$this->load->database();
		
			
		$this->db->where( 'key', $mem_key );
		$res0 = $this->db->get( 'members' );
		$mInfo = $res0->row_array();

		if( $include_family_info ){

			$this->db->where( 'member', $mem_key );
			$res1 = $this->db->get( 'familytomember' );
			if( $res1->num_rows() > 0 ){
				$ftm_info = $res1->row_array();
				$fam_num = $ftm_info['family'];
				
				$this->db->where( 'key', $fam_num );
				$res2 = $this->db->get( 'families' );
				$mFamily = $res2->row_array();
				$mFamily['family_num'] = $fam_num;
			}else{
			
				$mFamily = array(
					'key' => null,
					'order' => null,
					'family_name' => null,
					'family_num' => null,
					'address' => null,
					'city' => null,
					'state' => null,
					'zip' => null,
					'hphone' => null,
					'ophone' => null,
					'notes' => null,
					'mod_date' => null,
					);
			}
		}
	
		$this->db->order_by('date', 'asc'); 		
		$this->db->where( 'member', $mem_key );
		$res3 = $this->db->get('membertoservice');
		if( $res3->num_rows() > 0 ){
		
			$first_service = $res3->row_array();
			
			$fdss = strtotime ( $first_service['date'] );
			
			// Set $tnServies to zero if $beg_date is changed to make sure total number of services is recalculated
			if( $fdss > $beg_date_str ){ $beg_date_f = $first_service['date']; $beg_date_str = $fdss; $tnServices = 0; }
			
			if( $tnServices == 0 ){
				$mtCount = 0; $mwtCount = 0;
				$res = $this->db->get('service');
				$tServices = $res->result();
				foreach( $tServices as $serv ){
					$sDate = strtotime( $serv->date ); 
					if( $sDate >=  $beg_date_str && $sDate <= $end_date_str ){
						$mtCount++;
						if( $serv->type == 'Worship Service' ){
							$mwtCount++;
						}
					}
				}
			} else { $mtCount = $tnServices['service']; $mwtCount = $tnServices['worship']; }
			
			$mem_services = $res3->result_array();
			$msCount=0;
			$mwsCount=0;
			$mcCount=0;
			$mbCount=0;
			$mssCount=0;
			//set $mServices in case no services are found.
			$mServices = array();
			foreach( $mem_services as $mbs ){

				$sDate = strtotime( $mbs['date'] ); 
				if( $sDate >=  $beg_date_str && $sDate <= $end_date_str ){
					$mServices[$mbs['key']] =	$mbs ;
					
					$this->db->where('key', $mbs['service_key']);
					$srResults = $this->db->get('service');
					$srResult = $srResults->row();
					
					$mServices[$mbs['key']]['type'] =	$srResult->type ;
					$mServices[$mbs['key']]['name'] =	$srResult->name ;
					
					
					if( $mbs['service'] ){
						$msCount++;
						if( $srResult->type == 'Worship Service' ){ $mwsCount++; }
					}
					if( $mbs['communion'] ){ $mcCount++; }
					if( $mbs['bibleclass'] ){ $mbCount++; }
					if( $mbs['sundayschool'] ){ $mssCount++; }
					
					unset( $srResults, $srResult );
					
				} 
			
			}
			if( $mwtCount>0 ){	$awCount = round( $mwsCount/$mwtCount*100 ); }else{ $awCount = 0; }
		
		} else { $mServices = array(); 	$msCount=0; $mwsCount=0; $mcCount=0; $mbCount=0; $mssCount=0; $awCount = 0; $mtCount = $tnServices['service']; $mwtCount = $tnServices['worship']; }

		
		$this->db->order_by('date', 'asc'); 		
		$this->db->where( 'member', $mem_key );
		$res4 = $this->db->get('offerings');
		if( $res4->num_rows() > 0 ){
			$moInfos = $res4->result();
			foreach( $moInfos as $mfs ){
				$sDate = strtotime( $mfs->date );
				if( $sDate >=  $beg_date_str && $sDate <= $end_date_str ){
				
				$mOfferings[$mfs->key] = $mfs;
				if( empty($mOfferings['totals'][$mfs->fund]) ){ $mOfferings['totals'][$mfs->fund] = $mfs->amount ; }
				else { $mOfferings['totals'][$mfs->fund] = $mOfferings['totals'][$mfs->fund] + $mfs->amount ; }
				if( empty($mOfferings['totals']['total_offerings']) ){ $mOfferings['totals']['total_offerings'] = $mfs->amount ; }
				else { $mOfferings['totals']['total_offerings'] = $mOfferings['totals']['total_offerings'] + $mfs->amount ; }
				
				}
			}
		} else { $mOfferings = array(); }
		
		$tInfo = array_merge( $mFamily, $mInfo );
		if( $include_offerings ){ $tInfo['offerings']= $mOfferings; }
		if( $include_services ){ $tInfo['services']= $mServices; }
		if( $include_stats ){			
			$tInfo['stats']['total_services'] = $mtCount;			
			$tInfo['stats']['total_worship_services'] = $mwtCount;			
			$tInfo['stats']['services_attended'] = $msCount;			
			$tInfo['stats']['worship_services'] = $mwsCount;			
			$tInfo['stats']['avg_attendance'] = $awCount;			
			$tInfo['stats']['communion'] = $mcCount;			
			$tInfo['stats']['bible_class'] = $mbCount;			
			$tInfo['stats']['sunday_school'] = $mssCount;
		}
		
		return $tInfo;
		
	}
	
}	
?>