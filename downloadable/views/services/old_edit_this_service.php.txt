<?php
$this->load->helper('form');
$this->load->helper('array');
date_default_timezone_set( $timezone );
$servicet_ar = $service_type->result();
$offeringt_ar = $offering_type->result();
$this_service = $this_service_obj->row_array();
$visitors = $service_visitors->result_array();
$offerings = $offerings->result();
$mem_att = $mem_att->result();
// Build array for use in other offerings

$ct=0;
$offering = array();
foreach( $offerings as $off ):
	if( $off->fund == 'Gen' ){
		$offering['Gen_' . $off->member ] = $off->amount ;
	} elseif  ( $off->amount > 0 ){ 
		if( isset( $lmember ) && $off->member == $lmember ){ $ct++; } else { $ct=1; }
		$offering[$off->member]['sp'][$ct] = array( 'amount' => $off->amount, 'fund' => $off->fund, 'num' => $ct );
		if( $off->member == 0 ){ $qwe = 'other'; } else { $qwe = $off->member; }
		$value = set_value( $qwe.'_num_sp_offerings', $ct );
 		$offering[$off->member]['num'] = $value; 
		$lmember = $off->member;
	}
endforeach;

//Build an array for use in member attendance
$attendance = array( );
foreach( $mem_att as $ma ): 
	$attendance[ 'ws_' . $ma->member ] = $ma->service ;
	$attendance[ 'ls_' . $ma->member ] = $ma->communion ;
	$attendance[ 'bc_' . $ma->member ] = $ma->bibleclass ;
	$attendance[ 'ss_' . $ma->member ] = $ma->sundayschool ;
endforeach; 
		
// Set display of various elements from the site options table

?> <style type="text/css">
<!-- 
<?php if( !$sp_view_totals ): ?>
#totals {
	display:none!important;
	}
<?php endif; ?>	
<?php if( !$sp_view_members ): ?>
#members {
	display:none!important;
	}
<?php endif; ?>	
<?php if( !$sp_view_visitors ): ?>
#visitor {
	display:none!important;
	}
#add-visitor {
	display:none!important;
	}
<?php endif; ?>	
<?php if( !$sp_view_attendance ): ?>
.attendance {
	display:none!important;
	}
#members, #visitor {
	width:auto!important;
	}
<?php endif; ?>	
<?php if( !$sp_view_offerings ): ?>
.offering {
	display:none!important;
	}
#members, #visitor {
	width:auto!important;
	}
<?php endif; ?>	
<?php if( !$sp_view_notes ): ?>
#notes {
	display:none!important;
	}
<?php endif; ?>	
<?php if( !$sp_view_home_church ): ?>
.home_church {
	display:none!important;
	}
<?php endif; ?>	
<?php if( !$sp_view_contact_info ): ?>
.contact_info {
	display:none!important;
	}
<?php endif; ?>	
<?php if( !$sp_view_contact_info && !$sp_view_home_church): ?>
.home_church_row {
	display:none!important;
	}
<?php endif; ?>	
						
?> -->
</style> 

<div id="view_this_service">
<?php echo $this->Forms->view_this_service( element( 'key', $this_service ) ); ?>
</div>
<button id="view_this_service_button">View Service Report</button>

<?php 

    $inclass = 'class="ui-widget input ui-state-default ui-corner-all"';
    $inclassv = 'class="ui-widget input ui-state-default ui-corner-all vnames"';
    $inclassmoney = 'class="money ui-widget input ui-state-default ui-corner-all"';
    $inclassatt = 'class="attendance ui-widget input ui-state-default ui-corner-all"';
    $inclasslong = 'class="long ui-widget input ui-state-default ui-corner-all"';
    $inclassdate = 'class="datepicker ui-widget input ui-state-default ui-corner-all"';
    $inclasstype = 'class="type ui-widget input ui-state-default ui-corner-all"';
    $inclassotype = 'class="otype ui-widget input ui-state-default ui-corner-all"';
	$inclasscheck = 'id="checkbox"';
    $lbclass = array( 'class' => 'ui-widget ui-state-default ui-corner-all' );
    echo form_open('sub/edit_edit_this_service/' . element( 'key', $this_service )  );
	echo form_hidden('key', element( 'key', $this_service ) );
	?>
<div class="form">
  <?php echo validation_errors(); ?>
   
    <?php
    echo '<span class="label">' . form_label('Type', 'type') . '</span>';
    echo '<span id="type" class="input">' . form_input('type',  set_value('type', element( 'type', $this_service )), $inclasstype ) . '</span>';
	
    echo '<span class="label">' . form_label('Date', 'date') . '</span>';
    echo '<span id="date" class="input">' . form_input('date', set_value('date', date( 'm/d/Y', strtotime(element( 'date', $this_service )))), $inclassdate ) . '</span>';
    echo '<span class="label">' . form_label('Name', 'name') . '</span>';
    echo '<span id="name" class="input">' . form_input('name', set_value( 'name', element( 'name', $this_service )), $inclass ) . '</span>'; ?>

	<div id="totals">
    
    <?php
	$totals_array = explode( ':', element( 'attendance', $this_service ));
	foreach( $totals_array as $total ){
		$which_totals[ $total ] = element( $total, $this_service);
	}
	 
	
	echo '<span class="label">' . form_label('Total Offerings:', 'offering') . '</span>';
    echo '<span id="offering" class="input"><img class="money_sign" src="' . base_url() . '/system/money_sign.png" alt="$" />' . form_input('offering', set_value( 'offering', element( 'offering', $which_totals )), $inclassmoney ) . '</span>';
    
    echo '<span class="label"> Attendance Totals: </span>';
    echo '<span id="total_attendance" class="input"> Service: ';
	echo form_input( 'total_attendance', set_value( 'total_attendance', element( 'total_attendance', $which_totals )), $inclassatt ) ; 
	echo '</span>';
    echo "<span id='communion_attendance' class='input'> Lord's Supper: ";
	echo form_input( 'communion_attendance', set_value( 'communion_attendance', element( 'communion_attendance', $which_totals )), $inclassatt ) ; 
	echo '</span>';
    echo '<span id="bc_attendance" class="input"> Bible Class: ';
	echo form_input( 'bc_attendance', set_value( 'bc_attendance', element( 'bc_attendance', $which_totals )), $inclassatt ) ; 
	echo '</span>';
    echo '<span id="ss_attendance" class="input"> Sundy School: ';
	echo form_input( 'ss_attendance', set_value( 'ss_attendance', element( 'ss_attendance', $which_totals )), $inclassatt ) ; 
	echo '</span>';
	
    echo '<span class="label"> Total Members: </span>';
    echo '<span id="member_attendance" class="input">';
	echo form_input( 'member_attendance', set_value( 'member_attendance', element( 'member_attendance', $which_totals )), $inclassatt ) ; 
	echo '</span>';
	
    echo '<span class="label"> Total Visitors: </span>';
    echo '<span id="visitor_attendance" class="input">';
	echo form_input( 'visitor_attendance', set_value( 'visitor_attendance', element( 'visitor_attendance', $which_totals )), $inclassatt ) ; 
	echo '</span>';
	
   
   ?> </div>
 
</div>
   
<div class="table">
   <table id="members" ><h3>Members</h3>
        <tr>
            <th class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active" >Family Name</th>
            <th class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active" >Last Name</th>
            <th class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active" >First Name</th>
            <th class="attendance ui-state-default ui-corner-top ui-tabs-selected ui-state-active" colspan="4" >Attendance</th>
            <th class="offering ui-state-default ui-corner-top ui-tabs-selected ui-state-active" >Offering, General</th>
            <th class="offering ui-state-default ui-corner-top ui-tabs-selected ui-state-active" colspan="2">Offering, Special</th>
       </tr>
       <tr class="second offering">
       		<th colspan="3"></th>
       		<th colspan="4" class="attendance"></th>
       		<th class="offering"></th>
            <th class="offering">Amount</th>
            <th class="offering">Fund</th>
       </tr>
<?php if( !empty($grp_by_fam) ): foreach(  $grp_by_fam as $gp ): if( !empty($gp['members'][0]['key']) ):?>
    <tr class="ui-state-highlight ui-corner-all"><td class="family_name" rowspan="2"><?php  echo $gp['family_name'] ; ?> </td><td colspan="2"></td><td colspan="4" class="attendance"></td><td colspan="3" class="offering"></td></tr>
<?php $this->load->model('Forms'); ?>

		<?php $ct = 0; ?>
        <?php foreach ( $gp['members'] as $mem ): ?>
            <tr><?php if( $ct>0 ){ ?><td></td><?php } ?>
            <td> <?php echo $mem['lname']; ?></td>
            <td> <?php echo $mem['fname'] ; ?> </td>
            <td  class="center attendance"> 
				<?php echo form_checkbox( $mem['key'] . '_service', 1,  set_checkbox($mem['key'] . '_service', 1, element( 'ws_' . $mem['key'], $attendance)), 'id="' . $mem['key'] . '_service" class="checkbox"') . form_label('WS', $mem['key'] . '_service'); ?> 
            </td>
			<td  class="center attendance"> 
				<?php echo form_checkbox( $mem['key'] . '_communion', 1,  set_checkbox($mem['key'] . '_communion', 1, element( 'ls_' . $mem['key'], $attendance)), 'id="' . $mem['key'] . '_communion" class="checkbox"') . form_label('LS', $mem['key'] . '_communion'); ?> 
            </td>
			<td  class="center attendance"> 
				<?php echo form_checkbox( $mem['key'] . '_bc', 1,  set_checkbox($mem['key'] . '_bc', 1, element( 'bc_' . $mem['key'], $attendance)), 'id="' . $mem['key'] . '_bc" class="checkbox"') . form_label('BC', $mem['key'] . '_bc'); ?> 
            </td>
			<td  class="center attendance"> 
				<?php echo form_checkbox( $mem['key'] . '_ss', 1,  set_checkbox($mem['key'] . '_ss', 1, element( 'ss_' . $mem['key'], $attendance)), 'id="' . $mem['key'] . '_ss" class="checkbox"') . form_label('SS', $mem['key'] . '_ss'); ?> 
            </td>
            <td class="offering"> 
            	<span id="<?php echo $mem['key'] . '_gen_offering' ; ?>" >
            	<img class="money_sign" src="<?php echo base_url() . '/system/money_sign.png' ; ?>" alt="$" />
				<?php echo form_input( $mem['key'] . '_gen_offering', set_value( $mem['key'] . '_gen_offering', element(  'Gen_' . $mem['key'], $offering )),  $inclassmoney ); ?>
                </span>
            </td>
            <?php if( !empty( $offering[$mem['key']]['sp'] ) ){ ?>            
            <td id="tdspoffering_<?php echo $mem['key'] ?>" class="offering">
            <?php foreach( $offering[ $mem['key'] ]['sp'] as $sp ): ?>
            <?php $name = $mem['key'] . '_sp_offering_' . $sp['num'] . '_am' ; ?>
            <span id="<?php echo $name; ?>" >
            <img class="money_sign" src="<?php echo base_url() . '/system/money_sign.png' ; ?>" alt="$" />
			<?php echo form_input( $name, set_value( $name, $sp['amount']), $inclassmoney ); ?>
            </span><br />
            <?php endforeach; ?>
            </td>
            <td id="tdspofferingty_<?php echo $mem['key'] ?>" class="offering"> 
			<?php foreach( $offering[ $mem['key'] ]['sp'] as $sp ): ?>
            <?php $name = $mem['key'] . '_sp_offering_' . $sp['num'] . '_ty' ; ?>
            <span id="<?php echo $name; ?>" >
			<?php echo form_input( $name, set_value( $name, $sp['fund']), $inclassmoney ); ?>
            </span>
			<?php echo form_hidden( $mem['key'] . '_num_sp_offerings', set_value( $mem['key'] . '_num_sp_offerings', $sp['num'] )); ?><br />            
            <?php endforeach; ?>            
            </td>
            <?php } else { ?>
            <td id="tdspoffering_<?php echo $mem['key'] ?>" class="offering"> 
            <img class="money_sign" src="<?php echo base_url() . '/system/money_sign.png' ; ?>" alt="$" />
			<?php echo form_input( $mem['key'] . '_sp_offering_1_am', '', $inclassmoney ); ?><br />
            </td>
            <td id="tdspofferingty_<?php echo $mem['key'] ?>" class="offering"> 
			<?php echo form_input( $mem['key'] . '_sp_offering_1_ty', '', $inclassotype ); ?> 
			<?php echo form_hidden( $mem['key'] . '_num_sp_offerings', 1 ); ?><br />
            </td>
            <?php } ?>
            <td class="offering bottom"><span id="spoffering_<?php echo $mem['key'] ?>">Add Offering</span> </td>            
           </tr>
        <?php $ct++; endforeach; ?>

<?php endif; endforeach; endif;?>
     	<tr class="offering ui-state-highlight ui-corner-all"><td style="margin:2px;" colspan="3"></td><td colspan="4" class="attendance"></td><td colspan="3" class="offering"></td></tr>
        <tr class="offering"><td></td>
            <td>OTHER</td>
            <td> OFFERINGS: 
            </td>
            <td class="attendance"></td>
            <td class="attendance"></td>
            <td class="attendance"></td>
            <td class="attendance"></td>
            <td class="offering"> <img class="money_sign" src="<?php echo base_url() . '/system/money_sign.png' ; ?>" alt="$" /><?php echo form_input( 'other_gen_offering', set_value( 'other_gen_offering', element( 'Gen_0' , $offering )),  $inclassmoney ); ?></td>
            <?php if( !empty( $offering[0]['sp'] ) ){ ?>            
            <td id="tdspoffering_other" class="offering">
            <?php foreach( $offering[0]['sp'] as $sp ): ?>
            <?php $name = 'other_sp_offering_' . $sp['num'] . '_am' ; ?>
            <span id="<?php echo $name; ?>" >
            <img class="money_sign" src="<?php echo base_url() . '/system/money_sign.png' ; ?>" alt="$" />
			<?php echo form_input(  $name, set_value( $name, $sp['amount']), $inclassmoney ); ?>
            </span><br />
            <?php endforeach; ?>
            </td>
            <td id="tdspofferingty_other" class="offering"> 
			<?php foreach( $offering[0]['sp'] as $sp ): ?>
            <?php $name = 'other_sp_offering_' . $sp['num'] . '_ty' ; ?>
            <span id="<?php echo $name ; ?>" >
			<?php echo form_input( $name, set_value( $name, $sp['fund']), $inclassotype ); ?>
            <?php echo form_hidden( 'other_num_sp_offerings', set_value( 'other_num_sp_offerings', $sp['num'] )); ?>
            </span><br />
            <?php endforeach; ?>            
            </td>
            <?php } else { ?>
            <td id="tdspoffering_other" class="offering"> <img class="money_sign" src="<?php echo base_url() . '/system/money_sign.png' ; ?>" alt="$" /><?php echo form_input( 'other_sp_offering_1_am', '', $inclassmoney ); ?><br /></td>
            <td id="tdspofferingty_other" class="offering"> <?php echo form_input( 'other_sp_offering_1_ty', '', $inclassotype ); ?> <?php echo form_hidden( 'other_num_sp_offerings', 1 ); ?><br /></td>            
            <?php } ?>
            <td class="offering bottom"> <span id="spoffering_other">Add Offering</span> </td>            
      	</tr>
	</table>
</div>
<div class="table">    
    <table id="visitor"><h3>Visitors</h3>
        <tr>
            <th class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">Names</th>
            <th class="attendance ui-state-default ui-corner-top ui-tabs-selected ui-state-active" colspan="4">Attendance</th>
       </tr>
       <tr class="second">
       		<th><?php echo form_hidden( 'visitor_form_num', $service_visitors->num_rows() ); ?> </th>
            <th class="attendance">WS</th>
            <th class="attendance">LS</th>
            <th class="attendance">BC</th>
            <th class="attendance">SS</th>
       </tr>
       <?php $ct=1;
	   foreach( $visitors as $visitor ): ?>       
        <tr><td id="<?php echo 'visitors_names_' . $ct ; ?>" > <?php echo form_hidden( 'visitors_key_' . $ct, $visitor['key'] ); ?>
			<?php echo form_input('visitors_names_' . $ct, $visitor['names'], $inclassv ) ; ?> </td>
            <td id="<?php echo 'visitor_service_' . $ct ; ?>" class="center attendance"> <?php echo form_input( 'visitor_service_' . $ct, set_value( 'visitor_service_' . $ct, $visitor['ws']), $inclassatt ) ; ?></td>
			<td id="<?php echo 'visitor_communion_' . $ct ; ?>" class="center attendance"> <?php echo form_input( 'visitor_communion_' . $ct, set_value( 'visitor_communion_' . $ct, $visitor['ls']), $inclassatt ) ; ?></td>
			<td id="<?php echo 'visitor_bc_' . $ct ; ?>" class="center attendance"> <?php echo form_input( 'visitor_bc_' . $ct, set_value( 'visitor_bc_' . $ct, $visitor['bc']), $inclassatt ) ; ?> </td>
			<td id="<?php echo 'visitor_ss_' . $ct ; ?>" class="center attendance"> <?php echo form_input( 'visitor_ss_' . $ct, set_value( 'visitor_ss_' . $ct, $visitor['ss']), $inclassatt ) ; ?> </td>
        </tr>
        <tr class="home_church_row">
            <td id="<?php echo 'visitors_home_church_' . $ct ; ?>" class="home_church" colspan="5"> Home Church: <?php echo form_input('visitors_home_church_' . $ct, set_value( 'visitors_home_church_' . $ct, $visitor['home_church']), $inclasslong ) ; ?> </td>
		</tr>
        <tr>
            <td id="<?php echo 'visitors_contact_info_' . $ct ; ?>" class="contact_info" colspan="5"> Contact Info: <?php echo form_input('visitors_contact_info_' . $ct, set_value( 'visitors_contact_info_' . $ct, $visitor['contact_info']), $inclasslong ) ; ?> </td>
        </tr>
       <tr >
       		<th> </th>
       		<th colspan="4" class="attendance"></th>
       </tr>
        
      <?php $ct++; endforeach; ?>
    </table>
    <br />
    <span id="add-visitor" >More Visitor Fields </span>
    <br /><br />
</div>
    
<div class="form">
	<div id="notes">
    <?php
 
    echo '<span class="label">' . form_label('Notes', 'notes') . '</span>';
    echo '<span class="input">' . form_textarea('notes', set_value( 'notes', element( 'notes', $this_service ) ) , $inclass  ) . '</span>'; ?>
	</div>    
	<span class="label"></span><span class="input"><?php echo form_submit('mysubmit', 'Edit Service', 'class="button"'); ?></span>

</div>

<script type="text/javascript">

	$(function() {
		$(".datepicker").datepicker({
		changeYear: true,
		changeMonth: true,
		yearRange: '-100:+1',
		dateFormat: 'mm/dd/yy'
		});
	});
	$(function() {
		var availableTags = [<?php foreach( $servicet_ar as $type ){ echo '"' . $type->type . '", '; } ?>];
		$(".type").autocomplete({
			source: availableTags,
			minLength: 0
		});
	});
	$(function() {
		var availableTags = [<?php foreach( $offeringt_ar as $type ){ echo '"' . $type->type . '", '; } ?>];
		$(".otype").autocomplete({
			source: availableTags,
			minLength: 0
		});
	});
	$(function () {
		$("input, .button, #view_this_service_button").button();
	});
	$(function() {
		$('#view_this_service').dialog({
				autoOpen: false,
				buttons: {
					Ok: function() {
					$(this).dialog('close');
				}
			}

		});
		
		$('#view_this_service_button').click(function() {
			$('#view_this_service').dialog('open');
			return false;
		});
	});
	
	$(function() {
		var availableTags = [<?php foreach( $visitor_names as $visitor ){ echo '"' . $visitor->names . '", '; } ?>];
		$(".vnames").autocomplete({
			source: availableTags,
			minLength: 0
		});
	});	

<?php if( $sp_view_visitors ): ?>		
	visitor_count = <?php echo $ct; ?> ;
	$(function() {	
		$('#add-visitor')
			.button({ icons: {primary:'ui-icon-plus'} })
			.click(function() {

				$('#visitor').append(
					'<tr><td> <input type="hidden" value="' + visitor_count + '" name="visitor_form_num"> <input type="text" name="visitors_names_' + visitor_count + '" value="" class="ui-widget input ui-state-default ui-corner-all vnames" /></td>' +
					'	<td class="center attendance"> <input type="text" name="visitor_service_' + visitor_count + '" value="" class="attendance ui-widget input ui-state-default ui-corner-all" /></td>' +
					'	<td class="center attendance"> <input type="text" name="visitor_communion_' + visitor_count + '" value="" class="attendance ui-widget input ui-state-default ui-corner-all" /></td>' +
					'	<td class="center attendance"> <input type="text" name="visitor_bc_' + visitor_count + '" value="" class="attendance ui-widget input ui-state-default ui-corner-all" /> </td>' +
					'	<td class="center attendance"> <input type="text" name="visitor_ss_' + visitor_count + '" value="" class="attendance ui-widget input ui-state-default ui-corner-all" /> </td>' +
					'<tr class="home_church_row">' +
					'	<td class="home_church" colspan="5"> Home Church: <input type="text" name="visitors_home_church_' + visitor_count + '" value="" class="long ui-widget input ui-state-default ui-corner-all" /> </td></tr><tr>' +
					'	<td class="contact_info" colspan="5"> Contact Info: <input type="text" name="visitors_contact_info_' + visitor_count + '" value="" class="long ui-widget input ui-state-default ui-corner-all" /> </td>' +
					'</tr>'
				);
						
				visitor_count = visitor_count + 1;
				
				$("input, .button, #view_this_service_button").button();
				
				$(function() {
					var availableTags = [<?php foreach( $visitor_names as $visitor ){ echo '"' . $visitor->names . '", '; } ?>];
					$(".vnames").autocomplete({
						source: availableTags,
						minLength: 0
					});
				});	
			});
	});	
<?php endif; ?>

	
<?php if( $sp_view_offerings ): ?>	
<?php if( !empty($grp_by_fam) ): foreach(  $grp_by_fam as $gp ): if( !empty($gp['members'][0]['key']) ):?>		
		<?php foreach( $gp['members'] as $mem ): ?>
		num_of_offerings_<?php echo $mem['key'] ?> = <?php if( isset( $offering[$mem['key']]['num'] ) ) { echo $offering[$mem['key']]['num'] ; } else { echo '1'; } ?> + 1 ;
	$(function() {
		$('#spoffering_<?php echo $mem['key'] ?>')
			.button({ icons: {primary:'ui-icon-plus'}, text: false })
			.click(function() {
						$('#tdspoffering_<?php echo $mem['key'] ?>').append(
            '<img class="money_sign" src="http://ethnis/lwm//system/money_sign.png" alt="$" /><input type="text" name="<?php echo $mem['key'] ?>_sp_offering_' + num_of_offerings_<?php echo $mem['key'] ?> + '_am" value="" class="money ui-widget input ui-state-default ui-corner-all" /><br />'
						);
						$('#tdspofferingty_<?php echo $mem['key'] ?>').append(
            '<input type="text" name="<?php echo $mem['key'] ?>_sp_offering_' + num_of_offerings_<?php echo $mem['key'] ?> + '_ty" value="" class="otype ui-widget input ui-state-default ui-corner-all" /><input type="hidden" name="<?php echo $mem['key'] ?>_num_sp_offerings" value="' + num_of_offerings_<?php echo $mem['key'] ?> + '" /><br />'
						);
						num_of_offerings_<?php echo $mem['key'] ?> = num_of_offerings_<?php echo $mem['key'] ?> + 1 ;
						$(function() {
							var availableTags = [<?php foreach( $offeringt_ar as $type ){ echo '"' . $type->type . '", '; } ?>];
							$(".otype").autocomplete({
								source: availableTags,
								minLength: 0
							});
						});
				$("input, .button, #view_this_service_button").button();
			});
	});
		<?php endforeach; ?>
<?php endif; endforeach; endif; ?>
		num_of_offerings_other = <?php if( isset( $offering[0]['num'] ) ) { echo $offering[0]['num'] ; } else { echo '1'; } ?> + 1 ;
	$(function() {
		$('#spoffering_other')
			.button({ icons: {primary:'ui-icon-plus'}, text: false })
			.click(function() {
						$('#tdspoffering_other').append(
           '<img class="money_sign" src="http://ethnis/lwm//system/money_sign.png" alt="$" /><input type="text" name="other_sp_offering_' + num_of_offerings_other + '_am" value="" class="money ui-widget input ui-state-default ui-corner-all" /><br />'

						);
						$('#tdspofferingty_other').append(
            '<input type="text" name="other_sp_offering_' + num_of_offerings_other + '_ty" value="" class="otype ui-widget input ui-state-default ui-corner-all" /><input type="hidden" name="other_num_sp_offerings" value="' + num_of_offerings_other + '" /><br />'
						);
						num_of_offerings_other = num_of_offerings_other + 1 ;			
						$(function() {
							var availableTags = [<?php foreach( $offeringt_ar as $type ){ echo '"' . $type->type . '", '; } ?>];
							$(".otype").autocomplete({
								source: availableTags,
								minLength: 0
							});
						});
				$("input, .button, #view_this_service_button").button();
			});
	});
<?php endif; ?>	
$(function() {
 <?php  foreach( $_POST as $k => $v ): 
 			$set = form_error($k);
 			if( !empty( $set ) ): ?>
			$('#<?php echo $k; ?> input').switchClass('ui-state-default', 'ui-state-error' );
			<?php endif;
		endforeach;
?>
		return false;
});

</script>