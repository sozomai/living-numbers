<?php $service_ar = $services->result(); ?>

<div id="sdate_year">
	<ul id="sdate_year_list"><h3>Choose Year: </h3>
		<?php foreach( $service_date_array as $year => $months ): ?>
        	<li><a href="#sdate_year-<?php echo $year; ?>"><?php echo $year; ?></a></li>
        <?php endforeach; ?>
    </ul>
    <?php foreach( $service_date_array as $year => $months ): ?>
    <div id="#sdate_year-<?php echo $year; ?>" >
    
    
        <div id="sdate_month">
            <ul id="sdate_month_list"><h3>Choose Month: </h3>
                <?php foreach( $months as $month => $days ): ?>
                    <li><a href="#sdate_month-<?php echo $month; ?>"><?php echo $month; ?></a></li>
                <?php endforeach; ?>
            </ul>
  			<?php foreach( $service_date_array as $year => $months ): ?>
  			<div id="#sdate_month-<?php echo $month; ?>" >
            
            
                 <div id="sdate_days">
                    <ul id="sdate_day_list"><h3>Choose Service: </h3>
                        <?php foreach( $days as $day => $services ): ?>
                        	<li><h3><?php echo $month . ' ' . $day ?> </h3>
                            	<div class="services">
                                	<ul>
                            		<?php foreach( $services as $serv ): ?>
                           			 <li><?php echo anchor( 'services/view_this_service/' . $serv['key'], $serv['date'] . ' ' . $serv['type'] . ' ' . $serv['name'], 'title="services-1"'  ); ?></li>         
                            		<?php endforeach; ?>
                                    </ul>
                                </div>
                           </li>
           
                        <?php endforeach; ?>
                    </ul>
                </div>
            
            </div>
        </div>
    </div>  
</div> 	
	


<div id="services-1">
</div>

<div class='page_br'></div>




<script type="text/javascript">
	$(function() {
		$(".services").tabs({
			ajaxOptions: {
				error: function(xhr, status, index, anchor) {
					$(anchor.hash).html("Couldn't load this tab. We'll try to fix this as soon as possible. If this wouldn't be a demo.");
				}
			}
		});
	});
</script>
