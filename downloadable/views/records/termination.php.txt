<div class="ui-widget-header ui-corner-all center">
<h2>Add Termination to Church Records</h2>
</div>

<div class="ui-tabs-panel ui-widget-content ui-corner-bottom">

<?php if( !$help_menu ){ ?><button id="help_button">Help</button><?php } ?>
<div class="page_help ui-state-default ui-widget ui-widget-content ui-corner-all">
<small>This help menu my be turned off in "Site" -> "Options" -> "Help Menus"</small>
<div id="page_help"  title="Adding Services Help">

<h3 class="ui-widget-header ui-corner-all help-title">Help on Adding Services </h3>


</div>
</div>

</style> <?php 

    $this->load->helper('form');
    $inclass = 'class="ui-widget input ui-state-default ui-corner-all"';
    $inclassdate = 'class="datepicker ui-widget input ui-state-default ui-corner-all"';
    $inclassmember = 'class="member ui-widget input ui-state-default ui-corner-all"';
    $inclassfamily = 'class="family ui-widget input ui-state-default ui-corner-all"';
    $inclassstatus = 'class="status ui-widget input ui-state-default ui-corner-all"';
	echo form_open_multipart('records/add_record' );
	?>
    
 <?php echo validation_errors(); ?>
 <?php if( isset($upload_error)){ echo $upload_error ; } ?>
                	
<div class="form">
    
    <?php
	
	echo form_hidden('page', 'termination');
	echo form_hidden('type', 'termination');

    echo '<span class="label">Date</span>';
    echo '<span id="date" class="input">' . form_input('date', set_value('date', date( 'm/d/Y' )), $inclassdate ) . '</span>';

    echo '<span class="label">Name</span>';
    echo '<span id="name" class="input">' . form_input('name', set_value('name' ), $inclassmember ) . '</span>';
	
    echo '<span class="label"></span>';
    echo '<span class="input buttonset">' . form_radio('status', 'change_to', false, "id='change_to'" );
	echo form_label( 'Change',"change_to" ) . form_radio('status', 'skip', true, "id='none'" );
	echo form_label( 'Skip',"none" ) .  '</span>';
    echo '<div id="mem_update"><span class="label"></span>';
	echo '<span class="input mem_update"> Status: ' . form_input('mstatus', set_value('mstatus', 'Ex-member' ), $inclassstatus ) . '</span>';	
    echo '<span class="label"></span></div>';	

	echo '<span class="label">Reason</span>';
    echo '<span id="reason" class="input">' . form_textarea('reason', set_value('reason' ), $inclass ) . '</span>';

	echo '<span class="label">Notes</span>';
    echo '<span id="notes" class="input">' . form_textarea('notes', set_value('notes' ), $inclass ) . '</span>';
		
	echo '<span class="label"></span>';
    echo '<span class="input">' . form_submit('Submit', 'Add Record' ) . form_close() . '</span>';
		
?>		
</div>
</div>
<script type="text/javascript">
	$(function() {
		$(".buttonset").buttonset();
		$("#add_to").button({ icons: {primary:'ui-icon-plus'}, text:false });
		$("#change_to").button({ icons: {primary:'ui-icon-arrowreturnthick-1-n'}, text:false });
		$("#none").button({ icons: {primary:'ui-icon-cancel'}, text:false });
		$("#rto").button({ icons: {primary:'ui-icon-arrowthick-1-s'}, text:false });
		$("#rfrom").button({ icons: {primary:'ui-icon-arrowthick-1-n'}, text:false });
		$( ".mem_update input" ).button({ disabled: true });
	});
	$(function() {	
		$('#none')
			.click(function() {
				$( ".mem_update input" ).button({ disabled: true });
			});
	});
	$(function() {	
		$('#change_to, #add_to')
			.click(function() {
				$( ".mem_update input" ).button({ disabled: false });
			});
	});
	
	$(function() {
		var availableTags = [<?php foreach( $mstatus as $status ) { echo '"'.$status->status.'", '; } ?>];
		$(".status").autocomplete({
			source: availableTags
		});
	});
	
	$(function() {
		var availableTags = [<?php foreach( $families as $fam ) { echo '"'.$fam->family_name.'", '; } ?>];
		$(".family").autocomplete({
			source: availableTags
		});
	});
	
	$(function() {
		var availableTags = [<?php foreach( $members as $mem ) { echo '"'.$mem->lname.', '.$mem->fname.'", '; } ?>];
		$(".member").autocomplete({
			source: availableTags
		});
	});
	$(function() {
		$(".datepicker").datepicker({
		changeYear: true,
		changeMonth: true,
		yearRange: '-100:+1',
		dateFormat: 'mm/dd/yy'
		});
	});
	$(function() {
	 <?php  foreach( $_POST as $k => $v ): 
				$set = form_error($k);
				if( !empty( $set ) ): ?>
				$('#<?php echo $k; ?> input').switchClass('ui-state-default', 'ui-state-error' );
				<?php endif;
			endforeach;
	?>
		return false;
	});

</script>