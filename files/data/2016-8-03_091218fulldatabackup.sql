#
# TABLE STRUCTURE FOR: families
#

DROP TABLE IF EXISTS families;

CREATE TABLE `families` (
  `key` int(11) NOT NULL AUTO_INCREMENT,
  `order` int(5) NOT NULL,
  `family_name` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `zip` varchar(25) NOT NULL,
  `hphone` varchar(25) NOT NULL,
  `ophone` varchar(25) NOT NULL,
  `notes` longtext NOT NULL,
  `mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: familytomember
#

DROP TABLE IF EXISTS familytomember;

CREATE TABLE `familytomember` (
  `key` int(11) NOT NULL AUTO_INCREMENT,
  `family` int(11) NOT NULL,
  `member` int(11) NOT NULL,
  `fam_order` int(5) NOT NULL,
  `mem_order` int(5) NOT NULL,
  `mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: frontpage_stats
#

DROP TABLE IF EXISTS frontpage_stats;

CREATE TABLE `frontpage_stats` (
  `key` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `data` varchar(100) NOT NULL,
  `order` smallint(6) NOT NULL,
  `visible` varchar(5) NOT NULL,
  `other_data` varchar(100) NOT NULL,
  `mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO frontpage_stats (`key`, `name`, `data`, `order`, `visible`, `other_data`, `mod_date`) VALUES (1, 'least_sacramental_members', 'members', 3, '1', '15', '2014-11-14 09:31:43');
INSERT INTO frontpage_stats (`key`, `name`, `data`, `order`, `visible`, `other_data`, `mod_date`) VALUES (2, 'deliquent_members_by_last_service', 'members', 1, '1', '3', '2014-11-14 09:31:43');
INSERT INTO frontpage_stats (`key`, `name`, `data`, `order`, `visible`, `other_data`, `mod_date`) VALUES (3, 'visitors_to_contact', 'visitors', 4, '1', '0', '2014-11-14 09:43:35');
INSERT INTO frontpage_stats (`key`, `name`, `data`, `order`, `visible`, `other_data`, `mod_date`) VALUES (4, 'yearly_stats', 'year_stats', 6, '1', '0', '2014-11-14 09:43:36');
INSERT INTO frontpage_stats (`key`, `name`, `data`, `order`, `visible`, `other_data`, `mod_date`) VALUES (5, 'last_service_stats', 'last_service', 5, '1', '0', '2014-11-14 09:43:36');
INSERT INTO frontpage_stats (`key`, `name`, `data`, `order`, `visible`, `other_data`, `mod_date`) VALUES (6, 'least_attendant_members', 'members', 2, '1', '15', '2014-11-14 09:31:44');


#
# TABLE STRUCTURE FOR: links
#

DROP TABLE IF EXISTS links;

CREATE TABLE `links` (
  `key` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `desc` varchar(100) NOT NULL,
  `url` varchar(255) NOT NULL,
  `cat` varchar(100) NOT NULL,
  `mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

INSERT INTO links (`key`, `name`, `desc`, `url`, `cat`, `mod_date`) VALUES (1, 'New Service', 'Record service, attendance, offerings, and communion attendance', 'service', 'Services', '2014-11-14 09:31:40');
INSERT INTO links (`key`, `name`, `desc`, `url`, `cat`, `mod_date`) VALUES (2, 'Edit/View Service', 'view details of previously enetered services', 'view_service', 'Services', '2014-11-14 09:31:40');
INSERT INTO links (`key`, `name`, `desc`, `url`, `cat`, `mod_date`) VALUES (3, 'Delete Services', 'delete a service and all data associated with it', 'delete_services', 'Services', '2014-11-14 09:31:40');
INSERT INTO links (`key`, `name`, `desc`, `url`, `cat`, `mod_date`) VALUES (4, 'Records', 'View Church Records', 'view', 'Records', '2014-11-14 09:31:40');
INSERT INTO links (`key`, `name`, `desc`, `url`, `cat`, `mod_date`) VALUES (5, 'Baptism', 'Add baptism informatoin to Church Records', 'baptism', 'Records', '2014-11-14 09:31:40');
INSERT INTO links (`key`, `name`, `desc`, `url`, `cat`, `mod_date`) VALUES (6, 'Confirmation', 'Add Confirmation informatoin to Church Records', 'confirmation', 'Records', '2014-11-14 09:31:40');
INSERT INTO links (`key`, `name`, `desc`, `url`, `cat`, `mod_date`) VALUES (7, 'Adult Confirmation', 'Add Confirmation informatoin to Church Records', 'adult_conf', 'Records', '2014-11-14 09:31:40');
INSERT INTO links (`key`, `name`, `desc`, `url`, `cat`, `mod_date`) VALUES (8, 'Wedding', 'Add Wedding informatoin to Church Records', 'wedding', 'Records', '2014-11-14 09:31:40');
INSERT INTO links (`key`, `name`, `desc`, `url`, `cat`, `mod_date`) VALUES (9, 'Funeral', 'Add Funeral informatoin to Church Records', 'funeral', 'Records', '2014-11-14 09:31:40');
INSERT INTO links (`key`, `name`, `desc`, `url`, `cat`, `mod_date`) VALUES (10, 'Transfer', 'Add Member Transfer informatoin to Church Records', 'transfer', 'Records', '2014-11-14 09:31:40');
INSERT INTO links (`key`, `name`, `desc`, `url`, `cat`, `mod_date`) VALUES (11, 'Termination', 'Add Member Termination informatoin to Church Records', 'termination', 'Records', '2014-11-14 09:31:41');
INSERT INTO links (`key`, `name`, `desc`, `url`, `cat`, `mod_date`) VALUES (12, 'Families', 'Add/Edit/View Family Groups', 'families', 'Members', '2014-11-14 09:31:41');
INSERT INTO links (`key`, `name`, `desc`, `url`, `cat`, `mod_date`) VALUES (13, 'Members', 'Add/Edit/View Members', 'member', 'Members', '2014-11-14 09:31:41');
INSERT INTO links (`key`, `name`, `desc`, `url`, `cat`, `mod_date`) VALUES (14, 'Family Groups', 'Visual interface for editing family groups and order of display.', 'familygroups', 'Members', '2014-11-14 09:31:41');
INSERT INTO links (`key`, `name`, `desc`, `url`, `cat`, `mod_date`) VALUES (15, 'Delete Members', 'delete a member or a family group', 'delete_members', 'Members', '2014-11-14 09:31:41');
INSERT INTO links (`key`, `name`, `desc`, `url`, `cat`, `mod_date`) VALUES (16, 'Reports', 'view attendance records for members', '', 'Reports', '2014-11-14 09:31:41');
INSERT INTO links (`key`, `name`, `desc`, `url`, `cat`, `mod_date`) VALUES (17, 'Update', 'check for updates to the software', 'check_update', 'Options', '2014-11-14 09:31:41');
INSERT INTO links (`key`, `name`, `desc`, `url`, `cat`, `mod_date`) VALUES (18, 'Options', 'change options and values throughout the site', 'site_options', 'Options', '2014-11-14 09:31:41');
INSERT INTO links (`key`, `name`, `desc`, `url`, `cat`, `mod_date`) VALUES (19, 'Database', 'update, download and backup the databse', 'edit_database', 'Options', '2014-11-14 09:31:41');


#
# TABLE STRUCTURE FOR: member_status
#

DROP TABLE IF EXISTS member_status;

CREATE TABLE `member_status` (
  `key` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(100) NOT NULL,
  `description` longtext NOT NULL,
  `member` varchar(5) NOT NULL DEFAULT '1',
  `communicate` varchar(5) NOT NULL DEFAULT '0',
  `list` varchar(5) NOT NULL DEFAULT '1',
  `mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

INSERT INTO member_status (`key`, `status`, `description`, `member`, `communicate`, `list`, `mod_date`) VALUES (1, 'Member', '', '1', '1', '1', '2014-11-14 09:31:41');
INSERT INTO member_status (`key`, `status`, `description`, `member`, `communicate`, `list`, `mod_date`) VALUES (2, 'Voter', '', '1', '1', '1', '2014-11-14 09:31:41');
INSERT INTO member_status (`key`, `status`, `description`, `member`, `communicate`, `list`, `mod_date`) VALUES (3, 'Baptized', '', '1', '0', '1', '2014-11-14 09:31:41');
INSERT INTO member_status (`key`, `status`, `description`, `member`, `communicate`, `list`, `mod_date`) VALUES (4, 'Visitor', '', '0', '0', '1', '2014-11-14 09:31:41');
INSERT INTO member_status (`key`, `status`, `description`, `member`, `communicate`, `list`, `mod_date`) VALUES (5, 'Victorious', '', '0', '0', '1', '2014-11-14 09:31:41');
INSERT INTO member_status (`key`, `status`, `description`, `member`, `communicate`, `list`, `mod_date`) VALUES (6, 'Ex-member', '', '0', '0', '1', '2014-11-14 09:31:41');
INSERT INTO member_status (`key`, `status`, `description`, `member`, `communicate`, `list`, `mod_date`) VALUES (7, 'Transferred ', '', '0', '0', '1', '2014-11-14 09:31:41');


#
# TABLE STRUCTURE FOR: members
#

DROP TABLE IF EXISTS members;

CREATE TABLE `members` (
  `key` int(11) NOT NULL AUTO_INCREMENT,
  `order` int(5) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `dob` varchar(25) NOT NULL,
  `occupation` varchar(100) NOT NULL,
  `cphone` varchar(25) NOT NULL,
  `wphone` varchar(25) NOT NULL,
  `email` varchar(50) NOT NULL,
  `baptism` varchar(25) NOT NULL,
  `confirmation` varchar(25) NOT NULL,
  `anniversary` varchar(25) NOT NULL,
  `death` varchar(25) NOT NULL,
  `status` varchar(100) NOT NULL,
  `notes` longtext NOT NULL,
  `mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: membertoservice
#

DROP TABLE IF EXISTS membertoservice;

CREATE TABLE `membertoservice` (
  `key` int(11) NOT NULL AUTO_INCREMENT,
  `service_key` int(11) NOT NULL,
  `date` date NOT NULL,
  `member` int(11) NOT NULL,
  `service` varchar(5) NOT NULL DEFAULT 'FALSE',
  `communion` varchar(5) NOT NULL DEFAULT 'FALSE',
  `bibleclass` varchar(5) NOT NULL DEFAULT 'FALSE',
  `sundayschool` varchar(5) NOT NULL DEFAULT 'FALSE',
  `notes` longtext NOT NULL,
  `mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: offering_type
#

DROP TABLE IF EXISTS offering_type;

CREATE TABLE `offering_type` (
  `key` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `desc` varchar(255) NOT NULL,
  `mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: offerings
#

DROP TABLE IF EXISTS offerings;

CREATE TABLE `offerings` (
  `key` int(11) NOT NULL AUTO_INCREMENT,
  `service` int(11) NOT NULL,
  `date` date NOT NULL,
  `member` int(11) NOT NULL,
  `fund` varchar(100) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: records
#

DROP TABLE IF EXISTS records;

CREATE TABLE `records` (
  `key` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(25) NOT NULL,
  `church` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `born` varchar(25) NOT NULL,
  `died` varchar(25) NOT NULL,
  `name` varchar(100) NOT NULL,
  `bride` varchar(100) NOT NULL,
  `groom` varchar(100) NOT NULL,
  `parents` varchar(200) NOT NULL,
  `brides_parents` varchar(200) NOT NULL,
  `grooms_parents` varchar(200) NOT NULL,
  `witness_one` varchar(100) NOT NULL,
  `witness_two` varchar(100) NOT NULL,
  `official` varchar(100) NOT NULL,
  `passage` text NOT NULL,
  `text` varchar(75) NOT NULL,
  `sermon` longtext NOT NULL,
  `asermon` varchar(255) NOT NULL,
  `reason` longtext NOT NULL,
  `direction` varchar(5) NOT NULL,
  `notes` longtext NOT NULL,
  `mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO records (`key`, `type`, `church`, `date`, `born`, `died`, `name`, `bride`, `groom`, `parents`, `brides_parents`, `grooms_parents`, `witness_one`, `witness_two`, `official`, `passage`, `text`, `sermon`, `asermon`, `reason`, `direction`, `notes`, `mod_date`) VALUES (1, 'baptism', 'Church', '2014-11-15', '11/15/2014', '0', 'test, test', '0', '0', '', '0', '0', '', '', '', '', '', '', '', '0', '0', '', '0000-00-00 00:00:00');
INSERT INTO records (`key`, `type`, `church`, `date`, `born`, `died`, `name`, `bride`, `groom`, `parents`, `brides_parents`, `grooms_parents`, `witness_one`, `witness_two`, `official`, `passage`, `text`, `sermon`, `asermon`, `reason`, `direction`, `notes`, `mod_date`) VALUES (2, 'confirmation', '0', '2014-11-15', '0', '0', 'test, test', '0', '0', '0', '0', '0', '0', '0', 'Pastor Me', '', '', '', '', '0', '0', '', '0000-00-00 00:00:00');
INSERT INTO records (`key`, `type`, `church`, `date`, `born`, `died`, `name`, `bride`, `groom`, `parents`, `brides_parents`, `grooms_parents`, `witness_one`, `witness_two`, `official`, `passage`, `text`, `sermon`, `asermon`, `reason`, `direction`, `notes`, `mod_date`) VALUES (3, 'adult_conf', '0', '2014-11-15', '0', '0', 'test2, test', '0', '0', '0', '0', '0', '0', '0', 'Pastor ME', '', '', '', '', '0', '0', '', '0000-00-00 00:00:00');
INSERT INTO records (`key`, `type`, `church`, `date`, `born`, `died`, `name`, `bride`, `groom`, `parents`, `brides_parents`, `grooms_parents`, `witness_one`, `witness_two`, `official`, `passage`, `text`, `sermon`, `asermon`, `reason`, `direction`, `notes`, `mod_date`) VALUES (4, 'wedding', 'Church', '2014-11-15', '0', '0', '0', 'test2, test', 'test, test', '0', '', '', '', '', '', '0', '', '', '', '0', '0', '', '0000-00-00 00:00:00');
INSERT INTO records (`key`, `type`, `church`, `date`, `born`, `died`, `name`, `bride`, `groom`, `parents`, `brides_parents`, `grooms_parents`, `witness_one`, `witness_two`, `official`, `passage`, `text`, `sermon`, `asermon`, `reason`, `direction`, `notes`, `mod_date`) VALUES (5, 'funeral', '', '2014-11-15', '0', '11/15/2014', 'test2, test', '0', '0', '0', '0', '0', '0', '0', '', '0', '', '', '', '0', '0', '', '0000-00-00 00:00:00');


#
# TABLE STRUCTURE FOR: service
#

DROP TABLE IF EXISTS service;

CREATE TABLE `service` (
  `key` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `name` varchar(50) NOT NULL,
  `offering` decimal(15,2) NOT NULL,
  `attendance` varchar(500) NOT NULL,
  `offering_details` varchar(500) NOT NULL,
  `member_attendance` mediumint(9) NOT NULL,
  `visitor_attendance` mediumint(9) NOT NULL,
  `total_attendance` mediumint(9) NOT NULL,
  `ss_attendance` mediumint(9) NOT NULL,
  `bc_attendance` mediumint(9) NOT NULL,
  `communion_attendance` mediumint(9) NOT NULL,
  `visitors` longtext NOT NULL,
  `visitors_num` int(10) NOT NULL,
  `notes` longtext NOT NULL,
  `mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: service_type
#

DROP TABLE IF EXISTS service_type;

CREATE TABLE `service_type` (
  `key` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `desc` varchar(255) NOT NULL,
  `mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO service_type (`key`, `type`, `desc`, `mod_date`) VALUES (1, 'Worship Service', 'normal weekly worship service', '2014-11-14 09:31:43');


#
# TABLE STRUCTURE FOR: service_visitors
#

DROP TABLE IF EXISTS service_visitors;

CREATE TABLE `service_visitors` (
  `key` int(11) NOT NULL AUTO_INCREMENT,
  `service` int(11) NOT NULL,
  `date` date NOT NULL,
  `names` mediumtext NOT NULL,
  `home_church` mediumtext NOT NULL,
  `contact_info` mediumtext NOT NULL,
  `mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ws` mediumint(9) NOT NULL,
  `ls` mediumint(9) NOT NULL,
  `bc` mediumint(9) NOT NULL,
  `ss` mediumint(9) NOT NULL,
  `visited` mediumint(9) NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# END OF BACKUP!