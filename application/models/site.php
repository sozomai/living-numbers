<?php 
class Site extends CI_Model {


	function Site()
    {
        // Call the Model constructor
		parent::__construct();
    }
	
	
	
	function post_message ( $message ) {
	
		$this->load->helper('form');
	
	
	}
	
	
	function site_options () {

	$this->load->database();
	
	$site_options_object = $this->db->get('site_options');
	$site_options_array = $site_options_object->result();
	$data = array();
	foreach( $site_options_array as $option ){
		$data[$option->name] = $option->value;
		}

	return $data;
	
	}
	
	function check_table ( $table, $data ){
		$this->load->database();
		$check_data = $this->db->get_where( $table, $data );
		$data = $check_data->num_rows();
		if(  $data > 0 ){ return true; } else { return false; }
	}
	
	
	function return_family_groups () {
	
	$this->load->database();
	
	$site_options_object = $this->db->get('site_options');
	$site_options_array = $site_options_object->result();	
	foreach( $site_options_array as $option ){
		$data[$option->name] = $option->value;
		}

	if( $data['sort_members'] == 'manual' ){ $this->db->order_by( 'order' ); }
	$this->db->order_by( 'lname' );
	$this->db->order_by( 'fname' );
	$allmembers = $this->db->get('members');
	$data['members'] = $allmembers->result_array();
	if( $data['sort_members'] == 'manual' ){ $this->db->order_by( 'order' ); }
	$this->db->order_by( 'family_name' );
	$families = $this->db->get('families');
	$data['families'] = $families->result();
		
	$ct=0;
	foreach( $data['families'] as $fam ):
	
		if( $data['sort_members'] == 'manual' ){ 
			$this->db->order_by( 'mem_order' );
		}
		$this->db->where( 'family', $fam->key );
		$familygroups_obj = $this->db->get('familytomember');
		$familygroup = $familygroups_obj->result();
		
		$members[0] = array( );
		$ctm=0;
		foreach( $familygroup as $gp ):
						 
			$this->db->where( 'key', $gp->member );
			$member_obj = $this->db->get('members');
			$member = $member_obj->row_array();
			
			$this->db->where( 'status', $member['status'] );
			$mso = $this->db->get( 'member_status' );
			$msoa = $mso->row();
			if( empty( $msoa->list ) || $msoa->list == 0 ){ continue; }
			
			$this->db->where( 'status', $member['status'] );
			$statuses = $this->db->get('member_status');
			$stat = $statuses->row_array();
			
			$minus_year = time()-31556926;
			
			$this->db->where( 'member', $member['key'] );
			$this->db->order_by( 'date', 'asc' );
			$mem_attenso = $this->db->get('membertoservice');
			$mem_attens = $mem_attenso->result_array();
			
			$latt = array( 'type' => 'NEVER', 'date' => '', 'name' => '' );
			$att_list = array();
			$tatt = 0;
			$avg_att = 0;
			$lcomm = array( 'type' => 'NEVER', 'date' => '', 'name' => '' );
			$comm_list = array();
			$tcomm = 0;
			$avg_comm = 0;
			
			if(!empty($mem_attens)){
				$cta=0;
				foreach( $mem_attens as $att ):
				
					$this->db->where('key', $att['service_key']);
					$serv_o = $this->db->get('service');
					$serv = $serv_o->row_array();
				
					if( $att['service'] ){
						$latt = array( 'type' => $serv['type'], 'date' => $serv['date'], 'name' => $serv['name'] );
						$att_list[$cta] = $latt ;
						if( strtotime($serv['date']) > $minus_year ):
						$tatt++;
						$avg_att = $tatt/24;
						endif;
					}
					
					if( $att['communion'] ){
						$lcomm = array( 'type' => $serv['type'], 'date' => $serv['date'], 'name' => $serv['name'] );
						$comm_list[$cta] = $lcomm ;
						if( strtotime($serv['date']) > $minus_year ):
						$tcomm++;
						$avg_comm = $tcomm/24;
						endif;
					}
					
					$cta++;
				endforeach;
			}
						 
			$members[ $ctm ] = array( 
										'lname' => $member['lname'],
										'fname' => $member['fname'],
										'key' => $member['key'],
										'order' => $member['order'],
										'dob' => $member['dob'],
										'occupation' => $member['occupation'],
										'cphone' => $member['cphone'],
										'wphone' => $member['wphone'],
										'email' => $member['email'],
										'baptism' => $member['baptism'],
										'confirmation' => $member['confirmation'],
										'anniversary' => $member['anniversary'],
										'death' => $member['death'],
										'status' => $member['status'],
										'notes' => $member['notes'],
										'member' => $stat['member'],
										'communicate' => $stat['communicate'],
										'mod_date' => $member['mod_date'],
										'last_service' => $latt,
										'service_list' => $att_list,
										'total_services' => $tatt,
										'avg_service' => $avg_att,
										'last_communion' => $lcomm,
										'communion_list' => $comm_list,
										'total_communion' => $tcomm,
										'avg_communion' => $avg_comm,
										);
		
		$ctm++;
		endforeach;
		if( $data['sort_members'] == 'alphabetical' ){ 		
			asort( $members );
		}
		
		
		$data['grp_by_fam'][ $ct ] = array( 
											'key' => $fam->key,
											'family_name' => $fam->family_name,
											'order' => $fam->order,
											'address' => $fam->address,
											'city' => $fam->city,
											'state' => $fam->state,
											'zip' => $fam->zip,
											'hphone' => $fam->hphone,
											'ophone' => $fam->ophone,
											'notes' => $fam->notes,
											'mod_date' => $fam->mod_date,
											'members' => $members,
											);
	unset( $members );
	
	$ct++;	
	endforeach;
	
	$ctm=0;
	foreach( $data['members'] as $mem ):
	
		$this->db->where( 'status', $mem['status'] );
		$mso = $this->db->get( 'member_status' );
		$msoa = $mso->row();
		if( empty( $msoa->list ) || $msoa->list == 0 ){ continue; }
	
		$this->db->where( 'member', $mem['key'] );
		$query = $this->db->get( 'familytomember' );
		if( $query->num_rows() > 0 ){
		
			continue;
			
		} else {
					
		 $members[ $ctm ] = $mem ;
		 $ctm++;
		}
		
		
	endforeach;
	if( empty( $members ) ){ $members = array() ; }
	
		$data['grp_by_fam'][ $ct ] = array( 
											'key' => 0,
											'family_name' => 'No Family',
											'order' => 1000000000,
											'members' => $members,
											);
	

	return $data ;
	
	}
	
	
	
	function return_member_attendance () {
	
		$this->db->order_by( 'lname' );
		$this->db->order_by( 'fname' );
	
		$mem_ob = $this->db->get('members');
		$member_array = $mem_ob->result_array();
		
		$members[0] = array( );
		$ctm=0;
		$minus_year = time()-31556926;

		$this->db->where('type', 'Worship Service');
		$tsty_obj = $this->db->get('service'); 
		$tsty_arr = $tsty_obj->result_array();
		$tsty = 0; 
			foreach( $tsty_arr as $tstys ):
				if( strtotime( $tstys['date'] ) > $minus_year ){ $tsty++; }
			endforeach;
		if( $tsty == 0 ){ $tsty = 1; }
		
		if(!empty($members)): foreach( $member_array as $member ):
						 
			
			$this->db->where( 'status', $member['status'] );
			$statuses = $this->db->get('member_status');
			$stat = $statuses->row_array();
			
			
			$this->db->where( 'member', $member['key'] );
			$this->db->order_by( 'date', 'asc' );
			$mem_attenso = $this->db->get('membertoservice');
			$mem_attens = $mem_attenso->result_array();
			
			$latt = array( 'type' => 'NEVER', 'date' => '', 'name' => '' );
			$att_list = array();
			$tatt = 0;
			$avg_att = 0;
			$lcomm = array( 'type' => 'NEVER', 'date' => '', 'name' => '' );
			$comm_list = array();
			$tcomm = 0;
			$avg_comm = 0;
			
			if(!empty($mem_attens)){
				$cta=0;
				foreach( $mem_attens as $att ):
				
					$this->db->where('key', $att['service_key']);
					$serv_o = $this->db->get('service');
					$serv = $serv_o->row_array();
				
					if( $att['service'] ){
						$latt = array( 'type' => $serv['type'], 'date' => $serv['date'], 'name' => $serv['name'] );
						$att_list[$cta] = $latt ;
						if( strtotime($serv['date']) > $minus_year ):
						$tatt++;
						$avg_att = round($tatt/$tsty*100) ;
						endif;
					}
					
					if( $att['communion'] ){
						$lcomm = array( 'type' => $serv['type'], 'date' => $serv['date'], 'name' => $serv['name'] );
						$comm_list[$cta] = $lcomm ;
						if( strtotime($serv['date']) > $minus_year ):
						$tcomm++;
						$avg_comm = $tcomm;
						endif;
					}
					
					$cta++;
				endforeach;
			}
						 
			$members['by_name'][ $ctm ] = array( 
										'lname' => $member['lname'],
										'fname' => $member['fname'],
										'key' => $member['key'],
										'order' => $member['order'],
										'dob' => $member['dob'],
										'occupation' => $member['occupation'],
										'cphone' => $member['cphone'],
										'wphone' => $member['wphone'],
										'email' => $member['email'],
										'baptism' => $member['baptism'],
										'confirmation' => $member['confirmation'],
										'anniversary' => $member['anniversary'],
										'death' => $member['death'],
										'status' => $member['status'],
										'notes' => $member['notes'],
										'member' => $stat['member'],
										'communicate' => $stat['communicate'],
										'mod_date' => $member['mod_date'],
										'last_service' => $latt,
										'service_list' => $att_list,
										'total_services' => $tatt,
										'avg_service' => $avg_att,
										'last_communion' => $lcomm,
										'communion_list' => $comm_list,
										'total_communion' => $tcomm,
										'avg_communion' => $avg_comm,
										);


			$members['by_order'][ $ctm ] = array( 
										'order' => $member['order'],
										'lname' => $member['lname'],
										'fname' => $member['fname'],
										'key' => $member['key'],
										'dob' => $member['dob'],
										'occupation' => $member['occupation'],
										'cphone' => $member['cphone'],
										'wphone' => $member['wphone'],
										'email' => $member['email'],
										'baptism' => $member['baptism'],
										'confirmation' => $member['confirmation'],
										'anniversary' => $member['anniversary'],
										'death' => $member['death'],
										'status' => $member['status'],
										'notes' => $member['notes'],
										'member' => $stat['member'],
										'communicate' => $stat['communicate'],
										'mod_date' => $member['mod_date'],
										'last_service' => $latt,
										'service_list' => $att_list,
										'total_services' => $tatt,
										'avg_service' => $avg_att,
										'last_communion' => $lcomm,
										'communion_list' => $comm_list,
										'total_communion' => $tcomm,
										'avg_communion' => $avg_comm,
										);

			$members['by_last_service'][ $ctm ] = array( 
										'last_service_date' => $latt['date'],
										'lname' => $member['lname'],
										'fname' => $member['fname'],
										'key' => $member['key'],
										'order' => $member['order'],
										'dob' => $member['dob'],
										'occupation' => $member['occupation'],
										'cphone' => $member['cphone'],
										'wphone' => $member['wphone'],
										'email' => $member['email'],
										'baptism' => $member['baptism'],
										'confirmation' => $member['confirmation'],
										'anniversary' => $member['anniversary'],
										'death' => $member['death'],
										'status' => $member['status'],
										'notes' => $member['notes'],
										'member' => $stat['member'],
										'communicate' => $stat['communicate'],
										'mod_date' => $member['mod_date'],
										'last_service' => $latt,
										'service_list' => $att_list,
										'total_services' => $tatt,
										'avg_service' => $avg_att,
										'last_communion' => $lcomm,
										'communion_list' => $comm_list,
										'total_communion' => $tcomm,
										'avg_communion' => $avg_comm,
										);

			$members['by_last_communion'][ $ctm ] = array( 
										'last_communion_date' => $lcomm['date'],
										'lname' => $member['lname'],
										'fname' => $member['fname'],
										'key' => $member['key'],
										'order' => $member['order'],
										'dob' => $member['dob'],
										'occupation' => $member['occupation'],
										'cphone' => $member['cphone'],
										'wphone' => $member['wphone'],
										'email' => $member['email'],
										'baptism' => $member['baptism'],
										'confirmation' => $member['confirmation'],
										'anniversary' => $member['anniversary'],
										'death' => $member['death'],
										'status' => $member['status'],
										'notes' => $member['notes'],
										'member' => $stat['member'],
										'communicate' => $stat['communicate'],
										'mod_date' => $member['mod_date'],
										'last_service' => $latt,
										'service_list' => $att_list,
										'total_services' => $tatt,
										'avg_service' => $avg_att,
										'last_communion' => $lcomm,
										'communion_list' => $comm_list,
										'total_communion' => $tcomm,
										'avg_communion' => $avg_comm,
										);

			$members['by_avg_service'][ $ctm ] = array( 
										'avg_service' => $avg_att,
										'lname' => $member['lname'],
										'fname' => $member['fname'],
										'key' => $member['key'],
										'order' => $member['order'],
										'dob' => $member['dob'],
										'occupation' => $member['occupation'],
										'cphone' => $member['cphone'],
										'wphone' => $member['wphone'],
										'email' => $member['email'],
										'baptism' => $member['baptism'],
										'confirmation' => $member['confirmation'],
										'anniversary' => $member['anniversary'],
										'death' => $member['death'],
										'status' => $member['status'],
										'notes' => $member['notes'],
										'member' => $stat['member'],
										'communicate' => $stat['communicate'],
										'mod_date' => $member['mod_date'],
										'last_service' => $latt,
										'service_list' => $att_list,
										'total_services' => $tatt,
										'last_communion' => $lcomm,
										'communion_list' => $comm_list,
										'total_communion' => $tcomm,
										'avg_communion' => $avg_comm,
										);

			$members['by_avg_communion'][ $ctm ] = array( 
										'avg_communion' => $avg_comm,
										'lname' => $member['lname'],
										'fname' => $member['fname'],
										'key' => $member['key'],
										'order' => $member['order'],
										'dob' => $member['dob'],
										'occupation' => $member['occupation'],
										'cphone' => $member['cphone'],
										'wphone' => $member['wphone'],
										'email' => $member['email'],
										'baptism' => $member['baptism'],
										'confirmation' => $member['confirmation'],
										'anniversary' => $member['anniversary'],
										'death' => $member['death'],
										'status' => $member['status'],
										'notes' => $member['notes'],
										'member' => $stat['member'],
										'communicate' => $stat['communicate'],
										'mod_date' => $member['mod_date'],
										'last_service' => $latt,
										'service_list' => $att_list,
										'total_services' => $tatt,
										'avg_service' => $avg_att,
										'last_communion' => $lcomm,
										'communion_list' => $comm_list,
										'total_communion' => $tcomm,
										);

										
		$ctm++;
		endforeach; endif;
		
		foreach( $members as $k => $v ):
			if( !empty( $members[$k] )){ asort( $members[$k] ); }
		endforeach;
		
		return $members;
	}
	
	
	
	
	
	function backup_all_data ( $filename = '' ) {
	
	$this->db->where('name', 'timezone' );
	$timeza = $this->db->get('site_options');
	$timez = $timeza->result();
	$timezone = $timez[0]->value;
	
	date_default_timezone_set($timezone);
	
	if( !isset( $filename ) ){ 	
		$filename = date('Y-j-m_his') . 'fulldatabackup.sql';
	}
		
	$prefs = array(
                'tables'      => array(),  // Array of tables to backup.
                'ignore'      => array('site_options'),           // List of tables to omit from the backup
                'format'      => 'txt',        // gzip, zip, txt
                'filename'    => $filename,    // File name - NEEDED ONLY WITH ZIP FILES
                'add_drop'    => TRUE,         // Whether to add DROP TABLE statements to backup file
                'add_insert'  => TRUE,         // Whether to add INSERT data to backup file
              );

	
	// Load the DB utility class
	$this->load->dbutil();
	
	// Backup your entire database and assign it to a variable
	$backup = $this->dbutil->backup($prefs);
	$backup .= '# END OF BACKUP!';
	
	// Load the file helper and write the file to your server
	$this->load->helper('file');
	write_file('./files/' . $filename, $backup );
	
	// Load the download helper and send the file to your desktop
	$this->load->helper('download');
	force_download($filename, $backup); 

	}
	
	
	
	function members_att_off ( $sort = 'lname asc, fname asc' ) {
	
	$this->db->order_by( $sort );
	$mem_obj = $this->db->get( 'members' );
	$mems = $mem_obj->result();
	foreach( $mems as $mem ):
		$this->db->where( 'member', $mem->key );
		$serv_obj = $this->db->get( 'membertoservice' );
		$servs = $serv_obj->result();
		$cts=0;
		foreach( $servs as $serv ):
			$this->db->order_by( 'date' );
			$this->db->where( 'key', $serv->service_key );
			$serv_info_obj = $this->db->get( 'service' );
			$service[$cts] = $serv_info_obj->row_array();
			$service[$cts]['ws'] = $serv->service;
			$service[$cts]['bc'] = $serv->bibleclass;
			$service[$cts]['ss'] = $serv->sundayschool;
			$service[$cts]['ls'] = $serv->communion;
			$cts++;
		endforeach;
		
		
		$this->db->where( 'member', $mem->key );
		$off_obj = $this->db->get( 'offerings' );
		$offs = $off_obj->result();
		$cto=0;
		foreach( $offs as $off ):
			$offerings[$off->date][$cto] = array( 'amount' => $off->amount, 'fund' => $off->fund );
		$cto++;
		endforeach;
		
	
	endforeach;
	
	
	}
	
	


}