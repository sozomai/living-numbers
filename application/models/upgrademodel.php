<?php
class Upgrademodel extends CI_Model {

	function Upgrademodel()
    {
        // Call the Model constructor
		parent::__construct();
    }

	function html_output ( $location, $output ) {
	
		?>
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<TITLE>Living Numbers: Updating Files . . . </TITLE> 
		<SCRIPT LANGUAGE="JavaScript">
		  function redireccionar() {
			setTimeout("location.href='<?php echo $location; ?>'", 5000);
		  }
		</SCRIPT>
		<style type="text/css">
		body{ background-color:#000; padding:0px; margin:0px; color:fff; font:Calibri, candara; }
		#upgrade_bod{ background: url('../files/img/install.bmp') no-repeat; width:800px; height:600px; margin:0px auto; }
		#info{ margin:0px auto; position:relative; top:200px; width:364px;}
		#links{ margin:0px auto; position:relative; top:200px; width:182px;}
		#links a { width:182px; height:29px; margin:5px 0px; color:#75abff; text-align:center; text-decoration:none; float:left; background:url('../../files/img/fButton.bmp') no-repeat; padding:4px 0 0; font-weight:bold;}
		#links a:hover { color:#fff; }
		p{ float:left; color:#fff; text-align:left; }
		h2{ float:left; color:#fff; text-align:left; }
		</style>		
		</HEAD>
		<BODY onLoad='redireccionar()' >
			<div id="upgrade_bod" >
				<div id="info" >
					<h2>Updating Files . . . </h2>
				</div>
				<div id="links" >
					<p><?php echo $output; ?></p>
				</div>
			</div>
		</BODY>
		</HTML>	
		<?php						
	
	}


}	
?>