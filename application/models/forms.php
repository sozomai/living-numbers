<?php
class Forms extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function Forms()
    {
        // Call the Model constructor
		parent::__construct();
    }
	
	function cktbl_data ( $table, $row, $data) {
		
	$this->load->database();
	
	$this->db->where( $row, $data );
	$res = $this->db->get( $table );
	$result = $res->result();
	if( empty( $result )){ return 'true'; } else { return 'false'; } 
	
	}
	

	function rtn_mem_attendance( $servicekey, $memkey, $service ){
		$this->load->database();
		$this->db->where( 'service_key', $servicekey );
		$this->db->where( 'member', $memkey );
		$memdata_obj = $this->db->get('membertoservice');
		$memdata_array = $memdata_obj->result();
		if( !empty($memdata_array ) ){
			return $memdata_array[0]->$service ;
		}
		else { return false; }
	}
	
		
	function rtn_mem_offering ( $servicekey, $memkey, $offering, $type = 'amount' ){
		$this->load->database();
		$this->db->where( 'service', $servicekey );
		$this->db->where( 'member', $memkey );
		$memdata_obj = $this->db->get('offerings');
		$memdata_its = $memdata_obj->num_rows();
		$memdata_array = $memdata_obj->result();
		if( $memdata_its > 0 ){
			if( $offering == 'Gen' ){
				foreach( $memdata_array as $memd ){
					if( $memd->fund == 'Gen' ){
						return $memd->amount;
						break;
					}
				}
			}
			if( $offering == 'spec_1' ){
				foreach( $memdata_array as $k => $memd ){
					if( $memd->fund == 'Gen' ){
						$skip_num = $k ;
						break;
					}
				}
				foreach( $memdata_array as $k => $memd ) {
					if( isset($skip_num) ) {
						if( $k != $skip_num ) {
							if( $type == 'fund' ){ return $memd->fund; }
							else{ return $memd->amount; }
							break;
						}
					} else {
						if( $type == 'fund' ){ return $memd->fund; }
						else{ return $memd->amount; }
						break;
					}
				}
			}
			if( $offering == 'spec_2' && $memdata_its > 1 ){
				foreach( $memdata_array as $k => $memd ){
					if( $memd->fund == 'Gen' ){
						$skip_num = $k ;
						break;
					}
				}
				foreach( $memdata_array as $k => $memd ) {
					if( isset($skip_num) ) {
						if( $k != $skip_num ) {
							$skip_num_2 = $k;
							break;
						}
					} else {
							$skip_num_2 = $k;
						break;
					}
				}
				foreach ($memdata_array as $k => $memd ){
					if( isset( $skip_num ) && $k == $skip_num ){ continue; }
					elseif( isset( $skip_num_2 ) && $k == $skip_num_2 ){ continue; }
					else{ 
						if( $type == 'fund' ){ return $memd->fund; }
						else{ return $memd->amount; }
					}
				}
			}
		}
	}

	
	
	function view_this_service ( $serv_num ) { 
    
		$this->load->helper('cookie');
		
		$wr_data = '';
		
		$this->load->database();
	
		
		$this->db->where( 'key', $serv_num );
		$data_service = $this->db->get('service');
		
		$ct = 0;
		$this->db->where( 'service', $serv_num );
		$data_offerings = $this->db->get('offerings');
			$off_members = $data_offerings->result();
			foreach( $off_members as $mem ){
				if( $mem->member == 0 ){
							$offerings[ $mem->member ][$ct] = array (
											'lname' => 'other',
											'fname' => '',
											'fund' => $mem->fund,
											'amount' => $mem->amount,
											'key' => $mem->key
											);
				}
				else {
					$this->db->where( 'key', $mem->member );
					$this_member_ob = $this->db->get( 'members' );
					$this_member_ar = $this_member_ob->result();
					$this_member = $this_member_ar[0];
				
				$offerings[ $mem->member ][$ct] = array (
											'lname' => $this_member->lname,
											'fname' => $this_member->fname,
											'fund' => $mem->fund,
											'amount' => $mem->amount,
											'key' => $mem->key
											);
				}
				$ct++;
			}
			
			if( isset( $offerings )){ $offer_mem = $offerings; }
			
			
	
		$this->db->where( 'service_key', $serv_num );
		$attendance_obj = $this->db->get( 'membertoservice' );
				$attendance_array = $attendance_obj->result();
				if( !empty($attendance_array)){
					$ct=0;
					foreach( $attendance_array as $attender ){
						$this->db->where( 'key', $attender->member );
						$mem_obj = $this->db->get( 'members');
						$mem_array = $mem_obj->result();
						$mem = $mem_array[0];
						
						$attenders[$attender->member] = array(
															'lname' => $mem->lname,
															'fname' => $mem->fname,
															'mem_key' => $mem->key,
															'service' => $attender->service,
															'communion' => $attender->communion,
															'bibleclass' => $attender->bibleclass,
															'sundayschool' => $attender->sundayschool,
															);
						
						if( $attender->service ){ 
							$service['service'][$ct] = array( 
															'lname' => $mem->lname,
															'fname' => $mem->fname,
															'mem_key' => $mem->key
															);
						}
						if( $attender->communion ){ 
							$service['communion'][$ct] = array( 
															'lname' => $mem->lname,
															'fname' => $mem->fname,
															'mem_key' => $mem->key
															);
						}
						if( $attender->bibleclass ){ 
							$service['bibleclass'][$ct] = array( 
															'lname' => $mem->lname,
															'fname' => $mem->fname,
															'mem_key' => $mem->key
															);
						}
						if( $attender->sundayschool ){ 
							$service['sundayschool'][$ct] = array( 
															'lname' => $mem->lname,
															'fname' => $mem->fname,
															'mem_key' => $mem->key
															);
						}
						
						$ct++;
					}
					$attendersva=$attenders;
					$attendersvs=$service;
				}
		
		 $service_gh = $data_service->result(); 
	
		$wr_data .= '<h3 id="service_head">';
		$wr_data .= $service_gh[0]->date;  
		$wr_data .= $service_gh[0]->type;  
		$wr_data .= $service_gh[0]->name;  
		$wr_data .= '</h3>';
		
		$wr_data .= '<div id="attendance" class="left">';
		$wr_data .= '<ul id="ul_attendance">';
		$wr_data .=  '<li>Members: ' . $service_gh[0]->member_attendance .'</li>';
		$wr_data .= '<li>Visitors: ' . $service_gh[0]->visitor_attendance . '</li>';
		$wr_data .= '<li>Total: ' . $service_gh[0]->total_attendance . '</li>';
		$wr_data .= '<li>Bible Class: ' . $service_gh[0]->bc_attendance . '</li>';
		$wr_data .= '<li>Sunday School: ' . $service_gh[0]->ss_attendance . '</li>';
		$wr_data .= '<li>Communion: ' . $service_gh[0]->communion_attendance . '</li>';
		$wr_data .= '</ul>';
		if( $service_gh[0]->offering > 0 ){ 
			$wr_data .=  'Total Monies Recieved: ' . $service_gh[0]->offering . '</h3>';
		 } 
		$wr_data .= '</div>';
		
		if( !empty($offer_mem) ): 
			$wr_data .= '<div class="left">';
			$wr_data .= '<h4> Money Recieved: by fund </h4>';
			$wr_data .= '<table>';
			asort( $offer_mem );
			$ct = 0;
			foreach( $offer_mem as $mem ){
				foreach( $mem as $mem2 ){
				$funds_ar[$mem2['fund']][$ct] = $mem2;
				$ct++;
				}
			}
			asort( $funds_ar);
				$ntotals = 0;
			foreach( $funds_ar as $k => $array ){ 
				$wr_data .= '<tr><th>' . $k . '</th></tr>';
				 asort( $array );
				$totals = 0;
				foreach( $array as $mo ){ 
					$wr_data .= '<tr><td>' . $mo['lname'] . ', ' . $mo['fname'] . '</td>';
						$wr_data .= '<td>$' . $mo['amount'] . '</td></tr>'; 
					$totals = $totals + $mo['amount'];
					$ntotals = $ntotals + $mo['amount'];
				} 
				$wr_data .= '<tr><td><b>TOTAL:</td><td>' . $totals . '</b></td></tr>';
			}      
			
				$wr_data .= '<tr></tr>';
				$wr_data .= '<tr><td><b>FINAL TOTAL:</td><td>$' . $ntotals . '</b></td></tr>';
			$wr_data .= '</table>';
			$wr_data .= '</div>';
			$wr_data .= '<div class="left">';
		
			$wr_data .= '<h4> Money Recieved: by member </h4>';
			
			$wr_data .= '<table>';
			
				$totals = 0;
				$ntotals = 0;
				foreach( $offer_mem as $mem2 ) { 
					$wr_data .= '<tr><td></td><td></td><td></td><tr></tr>';
					
					foreach( $mem2 as $mem ){  	
						$wr_data .= '<tr><td>' . $mem['lname'] .', '. $mem['fname'] .'</td>';
							$wr_data .= '<td>' . $mem['fund'] . '</td>';
							$wr_data .= '<td>$' . $mem['amount'] . '</td></tr>';
						$totals = $totals + $mem['amount'];
						$ntotals = $ntotals + $mem['amount'];
					} 
					 $wr_data .= '<tr><td><b>TOTAL:</td><td></td><td>$' . $totals . '</b></td><tr></tr>';
					 $totals = 0;
				} 
				$wr_data .= '<tr><td><b>FINAL TOTAL:</td><td></td><td>$' . $ntotals . '</b></td></tr><tr></tr>';
			$wr_data .= '</table>';
			$wr_data .= '</div>';
		endif;
		
		if( isset( $attendersvs ) ){ 
			$wr_data .= '<div class="left">';
			$wr_data .= '<h4> Attendance: by service </h4>';
			
				foreach( $attendersvs as $k => $att ) { 
					$wr_data .= '<ul><h4>' . $k . '</h4>';
					asort( $att ); 
					foreach( $att as $member ){ 
						$wr_data .= '<li>' . $member['lname'] . ', ' . $member['fname'] . '</li>';
					 } 
					 $wr_data .= '</ul>';
						
				} 
			$wr_data .= '</div>';
			$wr_data .= '<div class="left">';
		  $wr_data .= '<h4> Attendance: by member </h4>';
		  
			
			foreach( $attendersva as $member ){ 
				$wr_data .= '<ul><h4>' . $member['lname'] . ', ' . $member['fname'] . '</h4>';
				 if( $member['service'] ){ $wr_data .= '<li>Worship Service</li>'; }
				 if( $member['communion'] ){ $wr_data .= '<li>Communion</li>'; }
				 if( $member['bibleclass'] ){ $wr_data .= '<li>Bible Class</li>'; }
				 if( $member['sundayschool'] ){ $wr_data .= '<li>Sunday School</li>'; } 
				 $wr_data .= '</ul>';
			}
		$wr_data .= '</div>';
	 } 
		
	
		return $wr_data;
		
	}

}
?>
