<?php
class Index_reports extends CI_Controller {
	
	
	function index_reports()
	{
		parent::__construct();
		
		$this->load->library('session');
		
		$this->load->helper('form');

		$this->load->helper('inflector');
		
		$this->load->database();
		$data = $this->Site->site_options();

		date_default_timezone_set($data['timezone']);
		
	}

	function deliquent_members_by_last_service() { ?>

		<table class="ui-state-default ui-widget-content ui-corner-all left" >
			<tr><th colspan="7" ><div class="ui-widget-header ui-corner-all center"><h3>Deliquent Members By Last Service </h3></div></th></tr>
			<tr>	
				<th class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active" >Name</th>
				<th class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">Last WS</th>
				<th class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">Avg. WS</th>
				<th class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">Total WS</th>
				<th class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">Total LS</th>
			</tr>
		<?php if(!empty( $members['by_last_service'])): 
				foreach( $members['by_last_service'] as $mem ): 
					if( strtotime($mem['last_service']['date']) < $neg_3_mo && $mem['member'] ): ?>
						<tr class="ui-widget-content ui-corner-all" >
							<td><?php echo $mem['lname'].', '.$mem['fname']; ?></td>
							<td><?php echo date( 'M, jS Y' , strtotime($mem['last_service']['date'])); ?></td>
							<td><?php echo round($mem['avg_service'], 2 ) ; ?></td>
							<td><?php echo $mem['total_services'] ; ?></td>
							<td><?php echo $mem['total_communion'] ; ?></td>
						</tr>
		<?php
					endif;
				endforeach;
			endif; ?>
		</table>

		<?php

	}
	
	function least_attendant_members () { ?>


		<table class="ui-state-default ui-widget-content ui-corner-all left" >
			<tr><th colspan="7" ><div class="ui-widget-header ui-corner-all center"><h3> Least Attendant Members </h3></div></th></tr>
			<tr>	
				<th class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active" >Name</th>
				<th class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">Last WS</th>
				<th class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">Avg. WS</th>
				<th class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">Total WS</th>
				<th class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">Total LS</th>
			</tr>
		<?php if(!empty( $members['by_avg_service'])): 
				$ct=0;
				foreach( $members['by_avg_service'] as $mem ):
					if( $mem['member'] ):
						if( $ct < 15 ): ?>
							<tr class="ui-widget-content ui-corner-all" >
								<td><?php echo $mem['lname'].', '.$mem['fname']; ?></td>
								<td><?php echo date( 'M, jS Y' , strtotime($mem['last_service']['date'])); ?></td>
								<td><?php echo round($mem['avg_service'], 2 ) ; ?></td>
								<td><?php echo $mem['total_services'] ; ?></td>
								<td><?php echo $mem['total_communion'] ; ?></td>
							</tr>
			<?php		endif;
						$ct++;
					endif;
				endforeach;
			endif; ?>
		</table>
		<?php
	}
	
	function least_sacramental_members() { ?>


		<table class="ui-state-default ui-widget-content ui-corner-all left" >
			<tr><th colspan="7" ><div class="ui-widget-header ui-corner-all center"><h3> Least Sacramental Members </h3></div></th></tr>
			<tr>	
				<th class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active" >Name</th>
				<th class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">Last LS</th>
				<th class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">Total LS</th>
				<th class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">Total WS</th>
			</tr>
		<?php if(!empty( $members['by_avg_communion'])): 
				$ct=0; 
				foreach( $members['by_avg_communion'] as $mem ):
					if( $mem['communicate'] ):
						if( $ct < 15 ): ?>
							<tr class="ui-widget-content ui-corner-all" >
								<td><?php echo $mem['lname'].', '.$mem['fname']; ?></td>
								<td><?php echo date( 'M, jS Y' , strtotime($mem['last_communion']['date'])); ?></td>
								<td><?php echo $mem['total_communion'] ; ?></td>
								<td><?php echo $mem['total_services'] ; ?></td>
							</tr>
			<?php		endif;
						$ct++; 
					endif;
				endforeach;
			endif; ?>
		</table>
		<?php
	}
	
	function visitors_to_contact() { ?>
	
		<table class="ui-state-default ui-widget-content ui-corner-all left" >
			<tr><th colspan="4" ><div class="ui-widget-header ui-corner-all center"><h3> Visitors to Contact </h3></div></th></tr>
			<tr>
				<th><?php echo form_open('main/visited_visitor');?> </th>
				<th class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active" >Name</th>
				<th class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">Contact Info</th>
				<th class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">Visit Date</th>
			</tr>
		<?php if(!empty( $visitors)): 
				$names_array = array();
				$ct=0; 
				foreach( $visitors as $visitor ):
					$x=$visitor['visited'];
					foreach( $names_array as $name):
						if( $name == $visitor['names']){ $x=false; break; } 
					endforeach;
					if($x): ?>
					<tr class="ui-widget-content ui-corner-all" >
						<td><input type="checkbox" class="visited" value="1" name="visited<?php echo $visitor['key']; ?>" id="visited<?php echo $visitor['key']; ?>"><label for="visited<?php echo $visitor['key']; ?>" >Visited</label></td>
						<td><?php echo $visitor['names']; ?></td>
						<td><?php echo $visitor['contact_info']; ; ?></td>
						<td><?php echo date( 'M, jS Y' , strtotime($visitor['date'])); ?></td>
					</tr>
			<?php	
					endif;
				$names_array[$ct]= $visitor['names'];
				$ct++;
				endforeach;
			endif; ?>
			<td colspan="3"></td><td><?php echo form_submit( 'My Submit', 'Submit' ) . form_close() ; ?></td>
		</table>
		<?php
	}
	
	function last_service_stats() { ?>
	
		<table class="ui-state-default ui-widget-content ui-corner-all left" >
			<tr><th colspan="2" ><div class="ui-widget-header ui-corner-all center"><h3> Last Service Stats </h3></div></th></tr>
		<?php if(!empty( $last_service)): 
				foreach( $last_service as $k => $v ): if(!empty($v)): ?>
					<tr class="ui-widget-content ui-corner-all" >
						<td class="ui-state-default ui-corner-left ui-tabs-selected ui-state-active" ><?php echo humanize($k); ?>: </td>
						<td> <?php echo $v; ?> </td>
					</tr>
			<?php
				endif; endforeach;
			endif; ?>
		</table>
		<?php
	}
	
	function yearly_stats() { ?>


		<table class="ui-state-default ui-widget-content ui-corner-all left" >
			<tr><th colspan="2" ><div class="ui-widget-header ui-corner-all center"><h3> Yearly Stats </h3></div></th></tr>
		<?php if(!empty( $year_stats)): 
				foreach( $year_stats as $k => $v ): if(!empty($v)): ?>
					<tr class="ui-widget-content ui-corner-all" >
						<td class="ui-state-default ui-corner-left ui-tabs-selected ui-state-active" ><?php echo humanize($k); ?>: </td>
						<td> <?php echo $v; ?> </td>
					</tr>
			<?php
				endif; endforeach;
			endif; ?>
		</table>
		<?php
	}
	
	
}
