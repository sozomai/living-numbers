<div class="center">
<h2>Check for Update?</h2>
</div>

<div class="data">

<?php		

$yversion = $version ;
$cversion = file_get_contents('http://members.clclutheran.net/index.php/main/current_version');

if( $cversion == FALSE ){ echo "<h2>There seems to be a problem contacting the server please try again later.</h2>"; }
else {

 ?>
 <h3>Your Version is <?php echo $yversion; ?></h3>
 <h3>Newest Version is <?php echo $cversion; ?></h3>
 <?php if( $yversion < $cversion ): ?>
	<p> <?php echo anchor( 'fileupgrade', 'Click Here to upgrade to the newest version.' ) ?> </p>
 <?php else: ?>
	<p> Congratulations! You have the latest version. There is nothing to upgrade. </p>
	<p> <?php echo anchor( 'fileupgrade', 'Click Here if you want to reinstall the latest version.' ) ?> </p>
	
 <?php endif; 
 }?>

</div>