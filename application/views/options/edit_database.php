<div class="  center">
<h2>Database Backup/Manipulation</h2>
</div>

<div class="data">

<?php date_default_timezone_set( $timezone ); $this->load->helper('url'); $this->load->helper('number');?>
<div class="form">
<h2>Backup Full Database</h2>
<?php
	echo anchor('options/backup_database', 'Backup Database', 'class="button" title="Backup Database"' );
?>
</div>

<div class="form">
<h2>Restore Full Database </h2>
<?php	
    $this->load->helper('form');
    $inclass = 'class=" input  "';
    $inclassdate = 'class="datepicker  input   "';
	
	echo form_open_multipart('options/restore_database');
	
	echo '<span class="label">' . form_label( 'Choose File:' , 'userfile' ); ?></span>
    <span class="input"><input type="file" name="userfile" value="" class=" input  " /></span>
	<span class="label"><?php echo form_submit('mysubmit', 'Restore Database' ); ?> </span>
    <?php echo form_close(); ?>
    <span class="page_br"></span>
</div>


<div class="form">
<h2>Sync Data</h2>
<?php

	echo anchor('options/save_data', 'Save Data for Syncing', 'class="button disabled" title="Save Data"' ); ?>
    
</div>	
<div class="form disabled">
<h2> Sync Saved Database </h2>
<?php	
    $this->load->helper('form');
    $inclass = 'class=" input  "';
    $inclassdate = 'class="datepicker  input   "';
	
	echo form_open_multipart('options/restore_save_data');
	
	echo '<span class="label">' . form_label( 'Choose File:' , 'userfile' ); ?></span>
    <span class="input"><input type="file" name="userfile" value="" class=" input  " /></span>
	<span class="label"><?php echo form_submit('mysubmit', 'Upload and Sync Data' ); ?> </span>
    <?php echo form_close(); ?>
    <span class="page_br"></span>
</div>


<div class="form disabled">
<h2> Files </h2>
<?php
    $this->load->helper('form');
	echo form_open('options/database_files');
	$ct=0;
?>

<?php if( isset( $file_array['text/sql']  ) ): ?>
<h3>Full Database Backup</h3>
<table class="table">
<?php foreach( $file_array['text/sql'] as $file ): ?>
<?php $sFile =  str_replace('(', '.9.', $file['name'] ) ?>
<?php $sFile =  str_replace(')', '.0.', $sFile ) ?>
<tr>
<td><?php echo form_checkbox( 'd_' . $ct, $file['name'], false, 'id="d_'.$ct.'" class="delete"' ) . form_label('delete', 'd_'.$ct ) ; ?></td>
<td><?php echo form_checkbox( 's_' . $ct, $file['name'], false, 'id="s_'.$ct.'" class="save"' ) . form_label('save', 's_'.$ct ) ; ?></td>
<td><?php echo anchor('options/download_data_file/' . $sFile, $file['name'] );  ?></td>
<td><?php echo date( 'l  M, jS Y', $file['date']); ?></td>
<td><?php echo byte_format($file['size']); ?></td>
</tr>
<?php $ct++; endforeach; ?>
</table>
<?php endif; ?>

<?php if( isset( $file_array['text/plain']  ) ): ?>
<h3>Saved Data Files</h3>
<table class="table">
<?php foreach( $file_array['text/plain'] as $file ): ?>
<?php $sFile =  str_replace('(', '.9.', $file['name'] ) ?>
<?php $sFile =  str_replace(')', '.0.', $sFile ) ?>
<tr>
<td><?php echo form_checkbox( 'd_' . $ct, $file['name'], false, 'id="d_'.$ct.'" class="delete"' ) . form_label('delete', 'd_'.$ct ) ; ?></td>
<td><?php echo form_checkbox( 's_' . $ct, $file['name'], false, 'id="s_'.$ct.'" class="save"' ) . form_label('save', 's_'.$ct ) ; ?></td>
<td><?php echo anchor('options/download_data_file/'.$sFile,  $file['name']  );  ?></td>
<td><?php echo date( 'l  M, jS Y', $file['date']); ?></td>
<td><?php echo byte_format($file['size']); ?></td>
</tr>

<?php $ct++; endforeach; ?>
</table>
<?php endif; ?>


<table class="table">
<?php $ctr=0; foreach( $file_array as $mime => $filez ): ?>
<?php if( $mime != 'text/sql' && $mime != 'text/plain' ): ?>
<?php if( $ct == 0 ){ ?><h3>Other Files</h3> <?php } ?>
<?php $sFile =  str_replace('(', '.9.', $file['name'] ) ?>
<?php $sFile =  str_replace(')', '.0.', $sFile ) ?>
<tr>
<td><?php echo form_checkbox( 'd_' . $ct, $file['name'], false, 'id="d_'.$ct.'" class="delete"' ) . form_label('delete', 'd_'.$ct ) ; ?></td>
<td><?php echo form_checkbox( 's_' . $ct, $file['name'], false, 'id="s_'.$ct.'" class="save"' ) . form_label('save', 's_'.$ct ) ; ?></td>
<td><?php echo anchor('options/download_data_file/' .$sFile , $file['name'] );  ?></td>
<td><?php echo date( 'l  M, jS Y', $file['date']); ?></td>
<td><?php echo byte_format($file['size']); ?></td>
</tr>

<?php $ctr++; $ct++; endif; endforeach; ?>
</table>
<br /><br />
<?php echo form_hidden( 'num_rows', $ct ); ?>
<?php echo form_submit('mysubmit', 'Delete/Download Checked Files'); ?>
<?php echo form_close(); ?>

</div>

</div>
<script type="text/javascript">

	$(function () {
		$( ".disabled input, a.disabled" ).button({ disabled: true });
	});
	$(function () {
		$(".delete").button({ 
            icons: {
                primary: 'ui-icon-trash'
            },
            text: false
		});
	});	
	$(function () {
		$(".save").button({ 
            icons: {
                primary: 'ui-icon-disk'
            },
            text: false
		});
	});	
	$(function() {
		$(".datepicker").datepicker({
		yearRange: '-2:+1',
		dateFormat: 'yy-mm-dd'
		});
	});	
	
</script>