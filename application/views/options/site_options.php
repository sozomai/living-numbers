<div class="  center">
<h2>Site Options</h2>
</div>

<div class="  ">

<div class="page_help    ">

<h3>Help on this topic </h3>

</div>
<?php
$this->load->helper('array');
$this->load->helper('form');
$this->load->library('form_validation');
$options_array = $options_obj->result(); 
foreach( $options_array as $option ){
	$options[$option->name] = $option->value;
}

$theme_options[ '' ] = '--select a theme --';

foreach( $css_files as $file ){
	if( strchr($file['name'],'.css')  ){ 
		$path_ar = strchr($file['relative_path'],'\css');
		// in case on webserver
		if( empty( $path_ar ) ){ $path_ar = strchr($file['relative_path'],'/css'); }
		$stri_len = strlen($path_ar)-6;
		$theme_name = substr( $path_ar, 5, $stri_len );
		$theme_path =  $theme_name . '/' . $file['name'] ;
		$theme_array[ $theme_name ] = array( 'name' => $theme_name, 'path' => $theme_path, 'file_name' => $file['name'] );
		$theme_options[ $theme_path ] = $theme_name;
	}
}

foreach( $functions as $funk ):
	$$funk['name'] = $funk;
endforeach

?>
 <?php echo validation_errors(); ?>

<div class="form data">
<div id="tabs">
	<ul>
		<li><a href="#site_options">Site Options</a></li>
		<li><a href="#front_page_options">Front Page Options</a></li>
		<li><a href="#service_page_options">Service Page Options</a></li>
		<li><a href="#member_statuses">Member Statuses</a></li>
		<li><a href="#ftp_options">Update Options</a></li>
	</ul>
	
	<div id="site_options">
		<h3> Site Options </h3> 

		<?php 
			$inclass = 'class=" input  "';
			$lbclass = array( 'class' => '  ' );
			$inclassmoney = 'class="money  input  "';
			echo form_open('options/edit_site_options' );


			echo '<span class="label">' . form_label( 'Site Name' , 'site_name' ) . '</span>';
			echo '<span id="site_name" class="input">' . form_input( 'site_name' , set_value( 'site_name',  element('site_name', $options)), $inclass ) . '</span>';

			echo '<span class="label">' . form_label( 'Site Title' , 'site_title' ) . '</span>';
			echo '<span id="site_title" class="input">' . form_input( 'site_title' , set_value( 'site_title', element('site_title', $options) ), $inclass ) . '</span>';

			echo '<span class="label">' . form_label( 'Site Plug' , 'site_plug' ) . '</span>';
			echo '<span id="site_plug" class="input">' . form_input( 'site_plug' , set_value( 'site_plug', element('site_plug', $options)), $inclass ) . '</span>';

			echo '<span class="label">' . form_label( 'Site Theme' , 'site_theme' ) . '</span>';
			echo '<span id="site_theme" class="input">' . form_dropdown( 'site_theme' ,  $theme_options, set_value( 'site_theme', element('site_theme', $options) ), $inclass ) . '</span>';

			$help_menu = array( "Hide", "Show" );
			echo '<span class="label">' . form_label( 'Help Menus' , 'help_menu' ) . '</span>';
			echo '<span id="help_menu" class="input">' . form_dropdown( 'help_menu' ,  $help_menu,  set_value( 'help_menu', element('help_menu', $options) ), $inclass ) . '</span>';

			$fy_year = array( 
							'01'  => 'January',
							'02'  => 'February',
							'03'  => 'March',
							'04'  => 'April',
							'05'  => 'May',
							'06'  => 'June',
							'07'  => 'July',
							'08'  => 'August',
							'09'  => 'September',
							'10'  => 'October',
							'11'  => 'November',
							'12'  => 'December',
							);
			echo '<span class="label">' . form_label( 'FY End Month' , 'last_month' ) . '</span>';
			echo '<span id="last_month" class="input">' . form_dropdown( 'last_month' ,  $fy_year,  set_value( 'last_month', element('last_month', $options)), $inclass ) . '</span>';

			echo '<span class="label">' . form_label( 'Your Timezone:' , 'timezone' ) . '</span>';
			echo '<span id="timezone" class="input">' . form_dropdown( 'timezone' ,  $timezone_list,  set_value( 'timezone', element('timezone', $options)), $inclass ) . '</span>';
			echo '<span class="label">' . form_label( 'Server Timezone:' , 'timezone' ) . '</span>';
			echo '<span id="server_timezone" class="input">' . form_dropdown( 'server_timezone' ,  $timezone_list,  set_value( 'server_timezone', element('server_timezone', $options)), $inclass ) . '</span>';
			
		?>
		<span class="page_br"></span>
		<?php $tzv =  strtotime($chk_timezone->value); $tzm = strtotime($chk_timezone->mod_date);

			if( $tzv+60 < $tzm || $tzv-60 > $tzm ): ?>
		<div class="form   ui-state-error" >
		<div style="margin:25px;">
		<h2>ALERT: VERY IMPORTANT</h2>

			<p class="" >
			Check to make sure the following two dates are identical:</p>
			<p class="" >
			<?php echo date( 'Y-m-d h:i', $tzv ); ?><br />
			<?php echo date( 'Y-m-d h:i', $tzm ); ?><br />
			</p>
			
			<p class="" >
		 If they are off by a few seconds don't worry. If they are off by minutes your server timezone settings are incorrect. Change your server timezone settings above to match the given timezone printed below.</p>
			
			<div style="display:none;">
			<?php $server = date('e'); ?>
			</div>
			
			<p class="" >
			Your Server timezone is: <span style="font-size:1em; font-weight:bold;"> <?php echo $server; ?></span></p>
			
		</div>
		</div>
		<?php endif; ?>
		<span class="page_br"></span>
		<hr />
			<span class="label"></span><span class="input"><?php echo form_submit('mysubmit', 'Save Options', 'class="button" title="Change Options"'); ?></span>

		<span class="page_br"></span>
		<hr />

		<span class="page_br"></span>

	</div>

	<div id="front_page_options">
		<div id="front_page">
			<h3>Front Page Options</h3>
			<span class="desc"> Here you can choose what elements appear on the front page.  </span>

		<?php

			echo '<span class="label">Deliquent Members By Last Service</span><span class="input">'; 
			echo form_checkbox( 'deliquent_members_by_last_service_visible' , '1', element('visible', $deliquent_members_by_last_service), 'id="deliquent_members_by_last_service_visible"');
			echo form_label('View', 'deliquent_members_by_last_service_visible') . '</span>';
			echo '<span id="deliquent_members_by_last_service_order" class="input"> Order: ' . form_input( 'deliquent_members_by_last_service_order' , set_value( 'deliquent_members_by_last_service_order', element('order', $deliquent_members_by_last_service)), $inclassmoney ) . '</span>';
			echo '<span id="deliquent_members_by_last_service_other_data" class="input"> #Months: ' . form_input( 'deliquent_members_by_last_service_other_data' , set_value( 'deliquent_members_by_last_service_other_data', element('other_data', $deliquent_members_by_last_service)), $inclassmoney ) . '</span>';

			echo '<span class="label">Least Attendant Members</span><span class="input">'; 
			echo form_checkbox( 'least_attendant_members_visible' , '1', element('visible', $least_attendant_members), 'id="least_attendant_members_visible"');
			echo form_label('View', 'least_attendant_members_visible') . '</span>';
			echo '<span id="least_attendant_members_order" class="input"> Order: ' . form_input( 'least_attendant_members_order' , set_value( 'least_attendant_members_order', element('order', $least_attendant_members)), $inclassmoney ) . '</span>';
			echo '<span id="least_attendant_members_other_data" class="input"> #Members: ' . form_input( 'least_attendant_members_other_data' , set_value( 'least_attendant_members_other_data', element('other_data', $least_attendant_members)), $inclassmoney ) . '</span>';

			echo '<span class="label">Least Sacramental Members</span><span class="input">'; 
			echo form_checkbox( 'least_sacramental_members_visible' , '1', element('visible', $least_sacramental_members), 'id="least_sacramental_members_visible"');
			echo form_label('View', 'least_sacramental_members_visible') . '</span>';
			echo '<span id="least_sacramental_members_order" class="input"> Order: ' . form_input( 'least_sacramental_members_order' , set_value( 'least_sacramental_members_order', element('order', $least_sacramental_members)), $inclassmoney ) . '</span>';
			echo '<span id="least_sacramental_members_other_data" class="input"> #Members: ' . form_input( 'least_sacramental_members_other_data' , set_value( 'least_sacramental_members_other_data', element('other_data', $least_sacramental_members)), $inclassmoney ) . '</span>';

			echo '<span class="label">Visitors to Contact</span><span class="input">'; 
			echo form_checkbox( 'visitors_to_contact_visible' , '1', element('visible', $visitors_to_contact), 'id="visitors_to_contact_visible"');
			echo form_label('View', 'visitors_to_contact_visible') . '</span>';
			echo '<span id="visitors_to_contact_order" class="input"> Order: ' . form_input( 'visitors_to_contact_order' , set_value( 'visitors_to_contact_order', element('order', $visitors_to_contact)), $inclassmoney ) . '</span>';

			echo '<span class="label">Last Service Stats</span><span class="input">'; 
			echo form_checkbox( 'last_service_stats_visible' , '1', element('visible', $last_service_stats), 'id="last_service_stats_visible"');
			echo form_label('View', 'last_service_stats_visible') . '</span>';
			echo '<span id="last_service_stats_order" class="input"> Order: ' . form_input( 'last_service_stats_order' , set_value( 'last_service_stats_order', element('order', $last_service_stats)), $inclassmoney ) . '</span>';

			echo '<span class="label">Annual Stats</span><span class="input">'; 
			echo form_checkbox( 'yearly_stats_visible' , '1', element('visible', $yearly_stats), 'id="yearly_stats_visible"');
			echo form_label('View', 'yearly_stats_visible') . '</span>';
			echo '<span id="yearly_stats_order" class="input"> Order: ' . form_input( 'yearly_stats_order' , set_value( 'yearly_stats_order', element('order', $yearly_stats)), $inclassmoney ) . '</span>';

			
		?> 

		<span class="page_br"></span>
		<hr />
		</div>

			<span class="label"></span><span class="input"><?php echo form_submit('mysubmit', 'Save Options', 'class="button" title="Change Options"'); ?></span>
			
		<span class="page_br"></span>
		<hr />

		<span class="page_br"></span>
	</div>

	<div id="member_statuses">
		<div id="member_statuses">
			<h3>Member Statuses</h3>
			<?php echo form_hidden( 'num_new_statuses', set_value( 'num_new_statuses', '0') ); ?>
			<span class="desc"> Below you can edit the name of member statuses as well as choose whether certain statuses indicate church membership and or communicate membership. New statuses can be added in the empty boxes. Statuses can be delete by emptying their name box. </span>

			<?php	
			$num_statuses = 0;
			foreach( $mstatus as $status ):
			
			echo '<span class="label">' . form_hidden( 'member_key' . $status->key , $status->key ) . '</span>';
			echo '<span id="member_status' . $status->key .'" class="input">' . form_input( 'member_status' . $status->key , set_value( 'member_status'.$status->key, $status->status ) , $inclass ) . '</span>';
			$name = 'member_member' . $status->key;
			echo '<span class="input">' . form_checkbox( $name , 1,  $status->member, 'id="mem' . $status->key . '"') . form_label('Member', 'mem' . $status->key ) . '</span>'; 
			$name = 'member_communicate' . $status->key;
			echo '<span class="input">' . form_checkbox( $name , 1, $status->communicate, 'id="com' . $status->key . '"') . form_label('Communicate', 'com' . $status->key ) .'</span>'; 
			$name = 'member_list' . $status->key;
			echo '<span class="input">' . form_checkbox( $name , 1, $status->list, 'id="list' . $status->key . '"') . form_label('List', 'list' . $status->key ) . '</span>'; 
				$num_statuses++;
			
			endforeach; ?>
			<?php
			$ctvalue = set_value( 'num_new_statuses');
			$ct=1;
			if( $ctvalue > 0 ): while( $ctvalue > 0 ) : ?>
			
			<span class="label"></span>
			<span class="input"><input type="text" name="member_status_new_<?php echo $ct; ?>" value="<?php echo set_value('member_status_new_' . $ct ) ; ?>" class=" input  " /></span>
			<span class="input"><input type="checkbox" name="member_member_new_<?php echo $ct; ?>"  value="1" <?php echo set_checkbox( 'member_member_new_'.$ct, 1 ); ?> id="mem_new_<?php echo $ct; ?>" /><label for="mem_new_<?php echo $ct; ?>">Member</label></span>
			<span class="input"><input type="checkbox" name="member_communicate_new_<?php echo $ct; ?>" value="1"  <?php echo set_checkbox( 'member_communicate_new_'.$ct, 1 ); ?> id="com_new_<?php echo $ct; ?>" /><label for="com_new_<?php echo $ct; ?>">Communicate</label></span>
			
			<?php $ctvalue--;$ct++; endwhile; endif; ?>   
		</div>
			<span class="label"><span id="add-status">Add Status</span></span>
			<span class="page_br"></span>
		<hr  />
				<span class="page_br"></span>
			<span class="label"></span><span class="input"><?php echo form_submit('mysubmit', 'Save Options', 'class="button" title="Change Options"'); ?></span>

			<span class="page_br"></span>
		<hr />

		<span class="page_br"></span>
	</div>

	<div id="service_page_options">
		<div id="service_page">
			<h3>Service Page Options</h3>
			<span class="desc"> Here you can choose what elements appear on the "service" page. All highlighted elements below will be seen on the "service" and "edit service" pages. All others will be hidden. See the help menu for more information. </span>

		<span class="label">Choose Visisble Elements:</span>
		<?php

			echo '<span class="label"></span><span class="input">' . form_checkbox( 'sp_view_totals' , '1', element('sp_view_totals', $options), 'id="sp_view_totals"') . form_label('Enter Totals', 'sp_view_totals') . '</span>'; 
			echo '<span class="label"></span><span class="input">' . form_checkbox( 'sp_view_members' , '1', element('sp_view_members', $options), 'id="sp_view_members"') . form_label('Members Table', 'sp_view_members') .'</span>'; 
			echo '<span class="label"></span><span class="input">' . form_checkbox( 'sp_view_visitors' , '1', element('sp_view_visitors', $options), 'id="sp_view_visitors"') . form_label('Visitors Table', 'sp_view_visitors') . '</span>'; 
			echo '<span class="label"></span><span class="input">' . form_checkbox( 'sp_view_attendance' , '1', element('sp_view_attendance', $options), 'id="sp_view_attendance"') . form_label('Attendance Columns', 'sp_view_attendance') . '</span>'; 
			echo '<span class="label"></span><span class="input">' . form_checkbox( 'sp_view_offerings' , '1', element('sp_view_offerings', $options), 'id="sp_view_offerings"') . form_label('Offering Columns', 'sp_view_offerings') . '</span>'; 
			echo '<span class="label"></span><span class="input">' . form_checkbox( 'sp_view_notes' , '1', element('sp_view_notes', $options), 'id="sp_view_notes"') . form_label('Notes', 'sp_view_notes') . '</span>'; 
			echo '<span class="label"></span><span class="input">' . form_checkbox( 'sp_view_home_church' , '1', element('sp_view_home_church', $options), 'id="sp_view_home_church"') . form_label('Visitors: Home Church', 'sp_view_home_church') . '</span>'; 
			echo '<span class="label"></span><span class="input">' . form_checkbox( 'sp_view_contact_info' , '1', element('sp_view_contact_info', $options), 'id="sp_view_contact_info"') . form_label('Visitors: Contact Info', 'sp_view_contact_info') . '</span>'; 
			echo '<span class="label"></span><span class="input">' . form_checkbox( 'sp_view_merge' , '1', element('sp_view_merge', $options), 'id="sp_view_merge"') . form_label('Merge Services', 'sp_view_merge') . '</span>'; 


		?> 
			<span class="page_br"></span>
		   <?php $sort_members = array( 'alphabetical' => 'Alphabetical', 'manual' => 'Manual' ); ?>
		<?php	echo '<span class="label">' . form_label( 'Sort Members:' , 'sort_members' ) . '</span>';
			echo '<span id="sort_members" class="input">' . form_dropdown( 'sort_members' ,  $sort_members, set_value( 'sort_members', element('sort_members', $options) ), $inclass ) . '</span>';
		?>
		<?php	echo '<span class="label">' . form_label( 'Visitor Fields' , 'visitor_fields' ) . '</span>';
			echo '<span id="visitor_fields" class="input">' . form_input( 'visitor_fields' , set_value( 'visitor_fields', element('visitor_fields', $options) ) , $inclass ) . '</span>';
		?>


		<span class="page_br"></span>
		<hr />
		</div>

		<span class="label"></span><span class="input"><?php echo form_submit('mysubmit', 'Save Options', 'class="button" title="Change Options"'); ?></span>

		<span class="page_br"></span>
		<hr />

		<span class="page_br"></span>
    </div>
	<div id="ftp_options">
		<div id="ftp_page">
			<h3>Update Options</h3>
			<span class="desc"> This option allows LN to automatically change the permissions on files and folders when it is attempting to update the software. After completing software update LN switches all files back to read only. You should use this option if LN is hosted on a webserver, but not if LN is hosted on your personal computer using a program like XAMPP. If LN is hosted on a webserver but you DON'T want to use the FTP option make sure the permissions on the following folders ( and files contained therein ) are set to 777:  /system/application, /system/application/views, /system/application/controllers, /system/application/models.  </span>

		<span class="label"></span>
		<?php

			echo '<span class="label"></span><span class="input" id="ftp_check">' . form_checkbox( 'up_use_ftp' , '1', element('up_use_ftp', $options), 'id="up_use_ftp"') . form_label('Allow FTP Use', 'up_use_ftp') . '<label>Allow LN to change file/folder permissions</label></span>'; 
			echo '<div id="ftp_form"><span class="label"></span><span class="input">' . form_input( 'up_user_name' , set_value( 'up_user_name', element('up_user_name', $options)), 'id="up_user_name"') . form_label('FTP User Name', 'up_user_name') .'</span>'; 
			echo '<span class="label"></span><span class="input">' . form_password( 'up_password' , set_value( 'up_password', element('up_password', $options)), 'id="up_password"') . form_label('FTP Password', 'up_password') . '</span>'; 
			echo '<span class="label"></span><span class="input">' . form_password( 'up_password2' , set_value( 'up_password', element('up_password', $options)), 'id="up_password2"') . form_label('Repeat Password', 'up_password2') . '</span>'; 
			echo '<span class="label"></span><span class="input">' . form_input( 'up_hostname' , set_value( 'up_hostname', element('up_hostname', $options)), 'id="up_hostname"') . form_label('Hostname ( eg. livingword.org)', 'up_hostname') . '</span>'; 
			echo '<span class="label"></span><span class="input">' . form_input( 'up_servername' , set_value( 'up_servername', element('up_servername', $options)), 'id="up_servername"') . form_label('Root Path to LN directory ( normally "/public_html" )', 'up_servername') . '</span></div>'; 

		?> 

		<span class="page_br"></span>
		<hr />
		</div>

		<span class="label"></span><span class="input"><?php echo form_submit('mysubmit', 'Save Options', 'class="button" title="Change Options"'); ?></span>

		<span class="page_br"></span>
		<hr />

		<span class="page_br"></span>
    </div>	
</div>
<span class="page_br"><?php echo form_close(); ?></span>
  
</div>

</div>
<script type="text/javascript">
	num_statuses = <?php echo set_value( 'num_new_statuses', 0); ?> + 1;
	$(function() {	
		$('#add-status').button().click(function() {
			$('#member_statuses').append(
				'<span class="label"></span><span class="input"><input type="text" name="member_status_new_' + num_statuses + '" class=" input  " /></span><span class="input"><input type="checkbox" name="member_member_new_' + num_statuses + '" value="1" id="mem_new_' + num_statuses + '" /><label for="mem_new_' + num_statuses + '">Member</label></span><span class="input"><input type="checkbox" name="member_communicate_new_' + num_statuses + '" value="1"  id="com_new_' + num_statuses + '" /><label for="com_new_' + num_statuses + '">Communicate</label></span><span class="input"><input type="checkbox" name="member_list_new_' + num_statuses + '" value="1"  id="list_new_' + num_statuses + '" /><label for="list_new_' + num_statuses + '">List</label></span><input type="hidden" name="num_new_statuses" value="' + num_statuses + '" />'
			);
			num_statuses = num_statuses + 1;
			$("input").button();
		});
	});
	<?php if ( element('up_use_ftp', $options) ){ ?>
	$('#up_use_ftp').button({
		text: false,
		icons: {primary:'ui-icon-check'}
	});
	hide=false;
	<?php } else { ?>
	$('#up_use_ftp').button({
		text: false,
		icons: {primary:'ui-icon-close'}
	});
	$( "#ftp_form" ).addClass('hidden');
	hide=true;
	<?php } ?>
	$(function() {	
		$('#up_use_ftp').click(function() {
			if(hide){
				$( "#ftp_form" ).removeClass('hidden');
				$( "#up_use_ftp" ).button( "option", "icons", {primary:'ui-icon-check'} );
				hide=false;
			} else {
				$( "#ftp_form" ).addClass('hidden');
				$( "#up_use_ftp" ).button( "option", "icons", {primary:'ui-icon-close'} );
				hide=true;
			}			
		});
	});
$(function() {
 <?php  foreach( $_POST as $k => $v ): 
 			$set = form_error($k);
 			if( !empty( $set ) ): ?>
			$('#<?php echo $k; ?> input').switchClass('', 'ui-state-error' );
			<?php endif;
		endforeach;
?>
		return false;
});

	
</script>