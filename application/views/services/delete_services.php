<div class="center">
<h2>Delete Services</h2>
</div>



<?php if( !$help_menu ){ ?><button id="help_button">Help</button><?php } ?>
<div class="page_help">
<div id="page_help"  title="Delete Services Help">

<h3 class="  help-title">Help on Delete Services </h3>

<p>Deleting a service is simply enough, you simply click on the delete, ie <?php echo anchor('', 'Delete', 'class="deletebutton"' ); ?>, button next to the service you wish to delete. </p>
<p>WARNING: This will destory ALL data pertaining. Hence any visitors information entered in that service will be delete, although member information itself will not be destoryed their attendance and offering records for that service will be. Once deleted this infomation is not recoverable, unless you have already saved a backup copy of the database.</p>

</div>
</div>

<div class="data">
<?php

$services_array = $services_obj->result(); ?>

<h2> Services Table </h2> 
<div class="table">
<table id="results"> <?php
foreach( $services_array as $services ){ ?>
		<tr><th></th> <?php
	foreach( $services as $k => $v ){
			if( $k != 'attendance' && $k != 'visitors_num' && $k != 'visitors' && $k != 'notes'){?>
		<th class=" ui-corner-top ui-tabs-selected ui-state-active"><?php echo $k; ?></th> <?php
	}} ?>
    	</tr> <?php
		break;
} 
foreach( $services_array as $services ){ ?>
		<tr><td> <?php echo anchor('services/delete_delete_services/' . $services->key, 'Delete', 'class="deletebutton"' ); ?></td> <?php
	foreach( $services as $k => $v ){ 
			if(  $k != 'attendance' && $k != 'visitors_num' && $k != 'visitors' && $k != 'notes'){?>
		<td><?php echo $v; ?></td> <?php
	}} ?>
    	</tr> <?php
} ?>
</table>
</div>

</div>
<script type="text/javascript">
	$(function () {
		$(".deletebutton").button({
				icons: {
					primary: 'ui-icon-close'
				},
				text: false
		});
	});
</script>  