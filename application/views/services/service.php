<div class="  center">
<h2>Add Service</h2>
</div>

<div>

<?php if( !$help_menu ){ ?><button id="help_button">Help</button><?php } ?>
<div class="page_help    ">
<small>This help menu my be turned off in "Site" -> "Options" -> "Help Menus"</small>
<div id="page_help"  title="Adding Services Help">

<h3 class="  help-title">Help on Adding Services </h3>

<h3 id="toc" class="  help-title">Table of Contents</h3>
<ul>
	<li><a href="#purpose">Purpose</a></li>
	<li><a href="#merge">Merge</a></li>
	<li><a href="#type">Type</a></li>
	<li><a href="#date">Date</a></li>
	<li><a href="#name">Name</a></li>
	<li><a href="#totals">Totals</a></li>
	<li><a href="#members">Members</a><ul>
	<li><a href="#member_attendance">Attendance</a></li>
	<li><a href="#member_offerings">Offerings</a></li>
	<li><a href="#other_offerings">Other Offerings</a></li></ul></li>
	<li><a href="#visitors">Visitors</a><ul>
	<li><a href="#visitor_attendance">Attendance</a></li>
	<li><a href="#visitor_offerings">Offerings</a></li></ul></li>
	<li><a href="#notes">Notes</a></li>
</ul>

<h3 id="purpose" class="  help-title">PURPOSES:</h3>
<ul>
<li>MAIN PURPOSES:
	<li>tracking memeber attendance</li>
    <li>tracking member communion attendance</li>
    <li>overall church statistics</li></li>
<li>Other uses:
	<li>tracking member offering</li>
    <li>tracking general and specific funds</li>
    <li>tracking sunday school and bible class attendance</li>
    <li>tracking visitor attendance</li></li>
</ul>

<h3 id="merge" class="  help-title" >MERGE: OPTIONAL</h3>
<p>WARNING: Unfortunately there is currently a glitch which currently does not allow this option to be used when the help menus are turned on. 
To use this option turn the help menus off first.</p>
<p>This option may be turned on or off in the Optoions->Options page.
This gives you the option of merging information with another service. 
Services merged will be recorded as a single service. 
Simply click the "Merge" button and then choose the service you wish to merge with. 
It will only give you the option of merging with a service recorded within the last six days.
After six days you will have to create a service and then merge them in the Edit/View service page.</p>
<p>This allows you to add numbers and information of different aspect or from different services as a single services. 
This merge service should  be used if for example you regularly have two services on the same weekend. 
Another example would be if the pastor is entering information about members, the elders are entering financial information, and the teachers are entering sunday school numbers. 
It is also possible to merge services after they are created. </p>

<h3 id="type" class="  help-title" >TYPE: REQUIRED</h3>
<p>DEFAULT: Worship Service </p>
<p>The type is meant to indicate a specific kind of church service, eg Worship Service, Wednesday Bible Class, Lent Service, etc. If you type at least one letter in the box a list of possible options will be displayed. If you type in a &quot;type&quot; not in the list it will be added to the list and be available in the future. </p>
<p>By default end of the year statistics such as average attendance are calculated only on the basis of service  type "Worship Service". This however can be changed on the site options page, and individual member statistics such as how often they come to church, the last time they were at church, and communion participation take all services into account.</p>
<p><b>DO NOT</b> use this box to differentiate between services that are of the same type but slightly different in some way. Example you have a 8am and 10am worship service on Sunday, both should be designated just "Worship Service" you can differentiate between the two in the "Name" box.</p>

<h3 id="date" class="  help-title">DATE: REQUIRED</h3>
<p>DEFAULT: Today's date</p>
<p> You may enter the date manual or just pick the day from the calendar. </p>
<p>You can enter the date in just about any format you wish, eg 08/15/2010, Aug-15, 2010, 08/15, 8/15, aug-15, etc, as long as you specify at least the month and the day. If you do not specify the year, eg 08/15, the current year will be assumed. </p>
<p>The default format is mm/dd/yyyy, eg 08/15/2010</p>
<h3 id="name" class="  help-title">NAME: OPTIONAL</h3>
<p> This box is optional, unless you have two services on the same day of the same type. Then you must enter a name for at least one of them, the name may be watever you choose it to be. e.g. You have two worship services every Sunday, one at 8am and one at 10am. You may wish to designte the first "8am" or "early" and the second "10am" or "late". </p>

<h3 id="totals" class="  help-title">TOTALS: OPTIONAL</h3>
<p>Normally the totals ( total offering, total attendance, total communion etc ) are calculated based on the data entered in the members and visitors tables. However in the site options page you have the option to display the &quot;totals&quot; forms before the members table. Even if you have the &quot;totals&quot; forms turned on, this will not change anything unless you actually enter numbers into the totals forms. For example if you turn totals on and enter 3 in the &quot;ss&quot; box, it doesn't matter how many members you click as attending &quot;ss&quot; the total for that sunday will be recorded as 3.</p>
<p>By default these total forms are hidden. However if you want to record certain service totals without clicking through the members table, this may be a helpful way to do it. For example you want to know which of your members were at service and which took communion, but you don't really care WHO was at sunday school only how many. Turn the totals on in the site options page. After &quot;Attendance Totals&quot; in the &quot;ss&quot; box record the sunday school attendance. Leave the rest of the boxes blank and check the appropriate &quot;ws&quot; and &quot;ls&quot; boxes for each member. Don't bother checking whether they were in sunday school or not. </p>
<h3 id="members" class="  help-title">Members</h3>

<p>This table is where you can record information about both the attendnace and/or offerings for this serivce of your members. Each part of this table may be turned on or off in the site options page. For example you want to record their attendance, but don't care about their offering. On the &quot;site options&quot; page, under &quot;service page options&quot; make sure the &quot;Attendance Columns&quot; option is selected and the &quot;Offering Columns&quot; is deselected. </p>
<p>Furthermore by default the members are listed in the their family groups and both the fmaily groups and the members of the family are listed in alphabetical order. If you rpefer to manually set the order the ought to appear in go to the site options page,and under &quot;Service Page Options&quot; select &quot;manual&quot;. Now go to &quot;Members&quot; &gt;&gt; &quot;Family Groups&quot; here you can drag the family groups into the order you want. You can also drag the members within the family groups into the order you want. This is useful because it will allow you to group regular attenders near the top of the table, and non regular attenders near the bottom, making it easier to find those who are there regularly and often need to be checked. </p>

<h4 id="member_attendance" class="  help-title">Member ATTENDANCE</h4>
<p>Here you may keep track of member attendance. Just click the appropriate box for each member. If it's higlighted that means they have attended. An explanation of the four different boxes follows.</p>
<ul>
<li>WS: Worship Service - Techincally it stands for worship service, but it just means the attended the main service even if it isn't a worship service.</li>
<li>LS: Lords Supper </li>
<li>BC: Bible Class </li>
<li>SS: Sunday School</li>
</ul>

<h4 id="member_offerings" class="  help-title">Member OFFERINGS</h4>
<p>If you wish to keep track of what individual members give record it here. The benefit is that a report can be generated at the end of the year for each member as to how much they gave. You can also generate reports specifying how much the church has recieved and in which funds.</p>

<p>Usually you would simply fill in the amount each member gave after their name under the <b>"Offering, General"</b> column, however if they give to special funds and you wish to keep track of the that you can fill it in under the first and/or second <b>"Offering, Special"</b> columns. This will give you the advantage of recieving a report of how much your church has given to each fund on a per day, per month, or per year basis. In the first box under each "Offering, Special" column type the amount given, in the second box type the name of the fund it was given to. Any offering fund names entered will be saved and given as possible options next time you use the form.</p>

<h4  id="other_offerings" class="  help-title">OTHER OFFERINGS</h4>
<p>All offerings given by visitors or of unkown origins can be entered here.</p>

<h3 id="visitors" class="  help-title">VISITORS</h3>
<p> Here you can keep track of visitors to your church. Not only does this record the number of visitors to each service, but it also allows for a place to keep their contact info which can then be used to follow up on their visit, further it allows you to keep records of visitors from sister congregations who may have recieved communion at your church. Later in the year end report, you will be told which visitors had commnunion at your church on which days, where there home church is and reminded to contact their pastor with this information.</p>
<p>It is suggest that visitors who come to the church on a recurring basis be entered as members with the status &quot;visitor&quot;. Thus you won't have to enter their names everytime.</p>
<h3 id="visitor_attendance" class="  help-title">VISITOR Attendance</h3>
<p>Unlike member attendnace this isn't a simply click box. That is because you may wish to enter a family name rather than each individual visitors names:</p>
<p>For Example</p>
<p>Name: &quot;John Smith Family&quot; ws: &quot;4&quot; ls:&quot; &quot; bc:&quot;2&quot; ss:&quot;2&quot;</p>
<h3 id="visitor_offering" class="  help-title">VISITOR Offering</h3>
<p>Enter any offerings the visitors gave in the same manner you would the members. This part of the visitor's table may be turned off or on in the site options be deselecting/selecting &quot;Visitor: Offerings&quot;. By default it is turned off.</p>
<h3 id="notes" class="  help-title">NOTES</h3>
<p>Enter any other details you wish to note about the service here</p>

</div>
</div>
<?php
date_default_timezone_set( $timezone );
$servicet_ar = $service_type->result();
$offeringt_ar = $offering_type->result();

// Set display of various elements from the site options table

?> <style type="text/css">
<!-- 
<?php if( !$sp_view_totals ): ?>
#totals {
	display:none!important;
	}
<?php endif; ?>	
<?php if( !$sp_view_members ): ?>
#members {
	display:none!important;
	}
<?php endif; ?>	
<?php if( !$sp_view_visitors ): ?>
#visitors {
	display:none!important;
	}
#add-visitors {
	display:none!important;
	}
<?php endif; ?>	
<?php if( !$sp_view_attendance ): ?>
.attendance {
	display:none!important;
	}
#members, #visitors {
	width:auto!important;
	}
<?php endif; ?>	
<?php if( !$sp_view_offerings ): ?>
.offering {
	display:none!important;
	}
#members, #visitors {
	width:auto!important;
	}
<?php endif; ?>	
<?php if( !$sp_view_notes ): ?>
#notes {
	display:none!important;
	}
<?php endif; ?>	
<?php if( !$sp_view_home_church ): ?>
.home_church {
	display:none!important;
	}
<?php endif; ?>	
<?php if( !$sp_view_contact_info ): ?>
.contact_info {
	display:none!important;
	}
<?php endif; ?>	
<?php if( !$sp_view_contact_info && !$sp_view_home_church): ?>
.home_church_row {
	display:none!important;
	}
<?php endif; ?>	
						
?> -->
</style> <?php 

    $this->load->helper('form');
    $inclass = 'class=" input  "';
	$vnames = 'class=" input   vnames"';
    $inclassmoney = 'class="money  input  "';
    $inclassatt = 'class="attendance  input  "';
    $inclasslong = 'class="long  input  "';
    $inclassdate = 'class="datepicker  input  "';
    $inclasstype = 'class="type  input  "';
    $inclassotype = 'class="otype  input  "';
	$inclasscheck = 'id="check"';
    $lbclass = array( 'class' => '  ' );
    echo form_open('services/add_service' );
	?>
    
 <?php echo validation_errors(); if( isset($name_error) ){ echo $name_error; } ?>

<div class="data">                	
<div class="form">
    
    <?php
	if( $sp_view_merge ):
    echo '<span class="label"></span>';
    echo '<span class="input">' . form_checkbox('merge', true, set_checkbox('merge', true ), 'id="merge" class="check"' ) . form_label('Merge', 'merge') . '</span><span id="merge_with" class="input hidden">';
	echo form_dropdown('merge_with', $serv_opt, set_value('merge_with'), $inclass ). '</span>';
		
	endif;
	
    echo '<span class="label">' . form_label('Type', 'type') . '</span>';
    echo '<span id="type" class="input">' . form_input('type', set_value('type', 'Worship Service'), $inclasstype ) . '</span>';

    echo '<span class="label">' . form_label('Date', 'date') . '</span>';
    echo '<span id="date" class="input">' . form_input('date', set_value('date', date('m/d/Y')), $inclassdate ) . '</span>';
	
    echo '<span class="label">' . form_label('Name', 'name') . '</span>';
    echo '<span id="name" class="input">' . form_input('name', set_value('name'), $inclass ) . '</span>';?>

	<div id="totals">
    
    <?php
	echo '<span class="label">' . form_label('Total Offerings:', 'offering') . '</span>';
    echo '<span id="offering" class="input"><img class="money_sign" src="' . base_url() . '/system/money_sign.png" alt="$" />' . form_input('offering', set_value('offering'), $inclassmoney ) . '</span>';
    
    echo '<span class="label"> Attendance Totals: </span>';
    echo '<span id="total_attendance" class="input"> Service: ';
	echo form_input( 'total_attendance', set_value('total_attendance'), $inclassatt ) ; 
	echo '</span>';
    echo "<span id='communion_attendance' class='input'> Lord's Supper: ";
	echo form_input( 'communion_attendance', set_value('communion_attendance'), $inclassatt ) ; 
	echo '</span>';
    echo '<span id="bc_attendance" class="input"> Bible Class: ';
	echo form_input( 'bc_attendance', set_value('bc_attendance'), $inclassatt ) ; 
	echo '</span>';
    echo '<span id="ss_attendance" class="input"> Sundy School: ';
	echo form_input( 'ss_attendance', set_value('ss_attendance'), $inclassatt ) ; 
	echo '</span>';
	
    echo '<span class="label"> Total Members: </span>';
    echo '<span id="member_attendance" class="input">';
	echo form_input( 'member_attendance', set_value('member_attendance'), $inclassatt ) ; 
	echo '</span>';
	
    echo '<span class="label"> Total Visitors: </span>';
    echo '<span id="visitor_attendance" class="input">';
	echo form_input( 'visitor_attendance', set_value('visitor_attendance'), $inclassatt ) ; 
	echo '</span>';
	
   
   ?> </div>
 
</div>
   
<div class="table">
   <table id="members" ><h3>Members</h3>
        <tr>
            <th class="   " >Family Name</th>
            <th class="   " >Last Name</th>
            <th class="   " >First Name</th>
            <th class="attendance    " colspan="4" >Attendance</th>
            <th class="offering    " >Offering, General</th>
            <th class="offering    " colspan="2">Offering, Special</th>
            <th class="offering" ></th>
       </tr>
       <tr class="second offering">
       		<th colspan="3"></th>
       		<th colspan="4" class="attendance"></th>
       		<th class="offering"></th>
            <th class="offering">Amount</th>
            <th class="offering">Fund</th>
       </tr>
<?php if( !empty($grp_by_fam) ): foreach(  $grp_by_fam as $gp ): if( !empty($gp['members'][0]['key']) ):?>
    <tr class=" "><td class="family_name" rowspan="2"><?php  echo $gp['family_name'] ; ?> </td><td colspan="2"></td><td colspan="4" class="attendance"></td><td colspan="3" class="offering"></td></tr>
		<?php $ct = 0; ?>
        <?php foreach ( $gp['members'] as $mem ): ?>
            <tr><?php if( $ct>0 ){ ?><td></td><?php } ?>
            <td> <?php echo $mem['lname']; ?></td>
            <td> <?php echo $mem['fname'] ; ?> </td>
            <td class="center attendance"> <?php echo form_checkbox( $mem['key'] . '_service', 1, set_checkbox($mem['key'] . '_service', 1 ), 'id="' . $mem['key'] . '_service" class="check"') . form_label('WS', $mem['key'] . '_service'); ?> </td>
			<td class="center attendance"> <?php echo form_checkbox( $mem['key'] . '_communion', 1, set_checkbox($mem['key'] . '_communion', 1 ), 'id="' . $mem['key'] . '_communion" class="check"') . form_label('LS', $mem['key'] . '_communion'); ?> </td>
			<td class="center attendance"> <?php echo form_checkbox( $mem['key'] . '_bc', 1, set_checkbox($mem['key'] . '_bc', 1 ), 'id="' . $mem['key'] . '_bc" class="check"') . form_label('BC', $mem['key'] . '_bc'); ?> </td>
			<td class="center attendance"> <?php echo form_checkbox( $mem['key'] . '_ss', 1, set_checkbox($mem['key'] . '_ss', 1 ), 'id="' . $mem['key'] . '_ss" class="check"') . form_label('SS', $mem['key'] . '_ss'); ?> </td>
            <td id="<?php echo $mem['key'] . '_gen_offering'; ?>" class="offering"> 
				<?php echo form_input( $mem['key'] . '_gen_offering', set_value($mem['key'] . '_gen_offering'), $inclassmoney ); ?>
            </td>
            <td id="tdspoffering_<?php echo $mem['key'] ?>" class="offering"> 
            	<?php $mno[$mem['key']] = set_value( $mem['key'] . '_num_sp_offerings' ) ; ?>
                <?php $ct = 1; ?>
                <?php if( $ct > $mno[$mem['key']] ){ $mno[$mem['key']] = 1; }
				while( $ct <= $mno[$mem['key']] ): ?>
                <?php if( $ct > 1 ){ echo '<br />'; } ?>
                <?php $name = $mem['key'] . '_sp_offering_' . $ct . '_am' ; ?>
               <span id="<?php echo $name ; ?>" >
				<?php echo form_input( $name, set_value( $name ), $inclassmoney ); ?>
                </span>
                <?php  $ct++; endwhile; ?>
            </td>
            <td id="tdspofferingty_<?php echo $mem['key'] ?>" class="offering"> 
                <?php $ct = 1; 
				while( $ct <= $mno[$mem['key']] ): ?>
                <?php $name = $mem['key'] . '_sp_offering_'.$ct.'_ty' ; ?>
                <?php if( $ct > 1 ){ echo '<br />'; } ?>
               <span id="<?php echo $name; ?>" >
				<?php echo form_input( $name, set_value($name), $inclassotype ); ?> 
                </span>
                <?php  $ct++; endwhile; ?>
 				<?php echo form_hidden( $mem['key'] . '_num_sp_offerings', $mno[$mem['key']] ); ?>
           </td>
            <td class="offering bottom"><span id="spoffering_<?php echo $mem['key'] ?>">Add Offering</span> </td>
           </tr>
        <?php $ct++; endforeach; ?>

<?php endif; endforeach; endif;?>
     	<tr class="offering  "><td style="margin:2px;" colspan="3"></td><td colspan="4" class="attendance"></td><td colspan="3" class="offering"></td></tr>
        <tr class="offering"><td></td>
            <td>OTHER</td>
            <td> OFFERINGS:</td>
            <td class="attendance"></td>
            <td class="attendance"></td>
            <td class="attendance"></td>
            <td class="attendance"></td>
            <td id="other_gen_offering" class="offering"> <?php echo form_input( 'other_gen_offering', set_value('other_gen_offering'), $inclassmoney ); ?></td>
            <td id="tdspoffering_other" class="offering"> 
            	<?php $ono = set_value( 'other_num_sp_offerings', 1 ) ; ?>
                <?php $ct = 1; ?>
                <?php if( $ct > $ono ){ $ono = 1; }
				while( $ct <= $ono ): ?>
                <?php if( $ct > 1 ){ echo '<br />'; } ?>
                <span id="<?php echo 'other_sp_offering_'.$ct.'_am'; ?>" >
				<?php echo form_input( 'other_sp_offering_'.$ct.'_am', set_value('other_sp_offering_'.$ct.'_am'), $inclassmoney ); ?>
                </span>
                <?php $ct++; endwhile; ?>
                </td>
            <td id="tdspofferingty_other" class="offering"> 
                <?php $ct = 1; 
				while( $ct <= $ono ): ?>
                <?php if( $ct > 1 ){ echo '<br />'; } ?>
                <span id="<?php echo 'other_sp_offering_'.$ct.'_ty'; ?>" >
				<?php echo form_input( 'other_sp_offering_'.$ct.'_ty', set_value('other_sp_offering_'.$ct.'_ty'), $inclassotype ); ?> 
                </span>
                <?php $ct++; endwhile; ?>
				<?php echo form_hidden( 'other_num_sp_offerings', $ono ); ?>
             </td>
            <td class="offering bottom"> <span id="spoffering_other">Add Offering</span> </td>
      	</tr>
	</table>
</div>    
<div class="table">
    <table id="visitors"><h3>Visitors</h3>
        <tr>
            <th class="   ">Names</th>
            <th class="attendance    " colspan="4">Attendance</th>
       </tr>
       <tr class="second">
       		<?php $nvf = set_value( 'visitor_form_num' );
			if( empty( $nvf ) ) { $nvf = $visitor_fields ; } ?>
       		<th><?php echo form_hidden( 'visitor_form_num', $nvf ); ?> </th>
            <th class="attendance">WS</th>
            <th class="attendance">LS</th>
            <th class="attendance">BC</th>
            <th class="attendance">SS</th>
       </tr>
       <?php $dvfn=1; 
	   if( !( $dvfn <= $nvf ) ){ $nvf = 3; } ?>
       <?php while( $dvfn <= $nvf ): ?>       
        <tr><td> <?php echo form_input('visitors_names_' . $dvfn, set_value('visitors_names_' . $dvfn), $vnames ) ; ?> </td>
            <td id="<?php echo 'visitor_service_' . $dvfn ; ?>" class="center attendance"> <?php echo form_input( 'visitor_service_' . $dvfn, set_value('visitor_service_' . $dvfn), $inclassatt ) ; ?></td>
			<td id="<?php echo 'visitor_communion_' . $dvfn ; ?>" class="center attendance"> <?php echo form_input( 'visitor_communion_' . $dvfn, set_value('visitor_communion_' . $dvfn), $inclassatt ) ; ?></td>
			<td id="<?php echo 'visitor_bc_' . $dvfn ; ?>" class="center attendance"> <?php echo form_input( 'visitor_bc_' . $dvfn, set_value('visitor_bc_' . $dvfn), $inclassatt ) ; ?> </td>
			<td id="<?php echo 'visitor_ss_' . $dvfn ; ?>" class="center attendance"> <?php echo form_input( 'visitor_ss_' . $dvfn, set_value('visitor_ss_' . $dvfn), $inclassatt ) ; ?> </td>
        </tr>
        <tr class="home_church_row">
            <td id="<?php echo 'visitors_home_church_' . $dvfn ; ?>" class="home_church" colspan="5"> Home Church: <?php echo form_input('visitors_home_church_' . $dvfn, set_value('visitors_home_church_' . $dvfn), $inclasslong ) ; ?> </td>
       </tr>
       <tr>
            <td id="<?php echo 'visitors_contact_info_' . $dvfn ; ?>" class="contact_info" colspan="5"> Contact Info: <?php echo form_input('visitors_contact_info_' . $dvfn, set_value('visitors_contact_info_' . $dvfn), $inclasslong ) ; ?> </td>
        </tr>
       <tr >
       		<th> </th>
       		<th colspan="4" class="attendance"></th>
       </tr>
        
        <?php $dvfn++; ?>
      <?php endwhile; ?>
    </table>
    <br />
    <span id="add-visitors" >More Visitor Fields </span>
    <br /><br />
</div>
    
<div class="form">
	<div id="notes">
    <?php
 
    echo '<span class="label">' . form_label('Notes', 'notes') . '</span>';
    echo '<span class="input">' . form_textarea('notes', set_value('notes'), $inclass  ) . '</span>'; ?>
	</div>    
	<span class="label"></span><span class="input"><?php echo form_submit('mysubmit', 'Add Service', 'class="button"'); ?></span>

</div>

</div>
</div><!-- End data div -->

<script type="text/javascript">

	$(function() {
		$(".datepicker").datepicker({
		changeYear: true,
		changeMonth: true,
		yearRange: '-100:+1',
		dateFormat: 'mm/dd/yy'
		});
	});
	$(function() {
		var availableTags = [<?php foreach( $servicet_ar as $type ){ echo '"' . $type->type . '", '; } ?>];
		$(".type").autocomplete({
			source: availableTags,
			minLength: 0
		});
	});
	$(function() {
		var availableTags = [<?php foreach( $offeringt_ar as $type ){ echo '"' . $type->type . '", '; } ?>];
		$(".otype").autocomplete({
			source: availableTags,
			minLength: 0
		});
	});
	
	$(function() {
		var availableTags = [<?php foreach( $visitor_names as $visitor ){ echo '"' . $visitor->names . '", '; } ?>];
		$(".vnames").autocomplete({
			source: availableTags,
			minLength: 0
		});
	});	

<?php if( $sp_view_visitors ): ?>	
	visitor_count = <?php echo $dvfn; ?>;
	$(function() {	
		$('#add-visitors')
			.button({ icons: {primary:'ui-icon-plus'} })
			.click(function() {

						$('#visitors').append(
        					'<tr><td> <input type="hidden" value="' + visitor_count + '" name="visitor_form_num"> <input type="text" name="visitors_names_' + visitor_count + '" value="" class=" input   vnames" /></td>' +
            				'	<td class="center attendance"> <input type="text" name="visitor_service_' + visitor_count + '" value="" class="attendance  input  " /></td>' +
							'	<td class="center attendance"> <input type="text" name="visitor_communion_' + visitor_count + '" value="" class="attendance  input  " /></td>' +
							'	<td class="center attendance"> <input type="text" name="visitor_bc_' + visitor_count + '" value="" class="attendance  input  " /> </td>' +
							'	<td class="center attendance"> <input type="text" name="visitor_ss_' + visitor_count + '" value="" class="attendance  input  " /> </td>' +
							'<tr class="home_church_row">' +
							'	<td class="home_church" colspan="5"> Home Church: <input type="text" name="visitors_home_church_' + visitor_count + '" value="" class="long  input  " /> </td></tr>' +
            				'	<tr><td class="contact_info" colspan="5"> Contact Info: <input type="text" name="visitors_contact_info_' + visitor_count + '" value="" class="long  input  " /> </td>' +
							'</tr>'
						);
					visitor_count = visitor_count + 1;				
				$(function() {
					var availableTags = [<?php foreach( $visitor_names as $visitor ){ echo '"' . $visitor->names . '", '; } ?>];
					$(".vnames").autocomplete({
						source: availableTags,
						minLength: 0
					});
				});	

			});
	});
<?php endif; ?>

	
<?php if( $sp_view_offerings ): ?>
<?php if( !empty($grp_by_fam) ): foreach(  $grp_by_fam as $gp ): if( !empty($gp['members'][0]['key']) ):?>		
		<?php foreach( $gp['members'] as $mem ): ?>
		num_of_offerings_<?php echo $mem['key'] ?> = <?php echo $mno[$mem['key']] ?> + 1;
	$(function() {
		$('#spoffering_<?php echo $mem['key'] ?>')
			.button({ icons: {primary:'ui-icon-plus'}, text: false })
			.click(function() {
						$('#tdspoffering_<?php echo $mem['key'] ?>').append(
            '<br /><input type="text" name="<?php echo $mem['key'] ?>_sp_offering_' + num_of_offerings_<?php echo $mem['key'] ?> + '_am" value="" class="money  input  " />'
						);
						$('#tdspofferingty_<?php echo $mem['key'] ?>').append(
            '<br /><input type="text" name="<?php echo $mem['key'] ?>_sp_offering_' + num_of_offerings_<?php echo $mem['key'] ?> + '_ty" value="" class="otype  input  " /><input type="hidden" name="<?php echo $mem['key'] ?>_num_sp_offerings" value="' + num_of_offerings_<?php echo $mem['key'] ?> + '" />'
						);
						num_of_offerings_<?php echo $mem['key'] ?> = num_of_offerings_<?php echo $mem['key'] ?> + 1 ;			
						$(function() {
							var availableTags = [<?php foreach( $offeringt_ar as $type ){ echo '"' . $type->type . '", '; } ?>];
							$(".otype").autocomplete({
								source: availableTags,
								minLength: 0
							});
						});
			});
	});
		<?php endforeach; ?>
<?php endif; endforeach; endif; ?>
		num_of_offerings_other = <?php echo $ono ; ?> + 1 ;
	$(function() {
		$('#spoffering_other')
			.button({ icons: {primary:'ui-icon-plus'}, text: false })
			.click(function() {
						$('#tdspoffering_other').append(
           '<br /><input type="text" name="other_sp_offering_' + num_of_offerings_other + '_am" value="" class="money  input  " />'

						);
						$('#tdspofferingty_other').append(
            '<br /><input type="text" name="other_sp_offering_' + num_of_offerings_other + '_ty" value="" class="otype  input  " /><input type="hidden" name="other_num_sp_offerings" value="' + num_of_offerings_other + '" />'
						);
						num_of_offerings_other = num_of_offerings_other + 1 ;			
						$(function() {
							var availableTags = [<?php foreach( $offeringt_ar as $type ){ echo '"' . $type->type . '", '; } ?>];
							$(".otype").autocomplete({
								source: availableTags,
								minLength: 0
							});
						});
				$(".button").button();
			});
	});
<?php endif; ?>	
$(function() {
 <?php  foreach( $_POST as $k => $v ): 
 			$set = form_error($k);
 			if( !empty( $set ) ): ?>
			$('#<?php echo $k; ?> input').switchClass('', 'ui-state-error' );
			<?php endif;
		endforeach;
?>
 <?php if( isset($name_error) ){ ?>
 			$('#name input, #date input, #type input').switchClass('', 'ui-state-error' );
<?php } ?>
		return false;
});
			off=true;
	$(function() {	
		$('#merge')
			.click(function() {
			if(off){
				$( "#type input" ).button({ disabled: true });
				$( "#date input" ).button({ disabled: true });
				$( "#name input" ).button({ disabled: true });
				$( "#merge_with" ).switchClass('hidden', 'display');
				off=false;
			} else {
				$( "#type input" ).button({ disabled: false });
				$( "#date input" ).button({ disabled: false });
				$( "#name input" ).button({ disabled: false });
				$( "#merge_with" ).switchClass('display', 'hidden');
				off=true;
			}			
			});
	});

</script>