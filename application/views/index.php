<?php
/* members listed by:
		names: $members['by_name'] 
		order: $members['by_order'] 
		last service attended: $members['by_last_service'] 
		last communion recieved: $members['by_last_communion'] 
		avg service attendance: $members['by_avg_service'] 
		avg communion attendance: $members['by_avg_communion'] 

		
*/	
	$this->load->helper('form');
	$this->load->helper('inflector');

	function deliquent_members_by_last_service( $data, $months=3 ) {
		$mons = 60*60*24*30*$months;
		$neg = time()-$mons;
		?>
		<table class="left table front-page" >
			<tr><th colspan="7" ><div class="center"><h3>Deliquent Members By Last Service </h3></div></th></tr>
			<tr>	
				<th>Name</th>
				<th>Last WS</th>
				<th>Avg. WS</th>
				<th>Total WS</th>
				<th>Total LS</th>
			</tr>
		<?php if(!empty( $data['by_last_service'] )): 
				foreach( $data['by_last_service'] as $mem ): 
					if( strtotime($mem['last_service']['date']) < $neg && $mem['member'] ): ?>
						<tr >
							<td><?php echo $mem['lname'].', '.$mem['fname']; ?></td>
							<td><?php echo date( 'M, jS Y' , strtotime($mem['last_service']['date'])); ?></td>
							<td><?php echo round($mem['avg_service'], 2 ) ; ?></td>
							<td><?php echo $mem['total_services'] ; ?></td>
							<td><?php echo $mem['total_communion'] ; ?></td>
						</tr>
		<?php
					endif;
				endforeach;
			endif; ?>
		</table>

		<?php

	}
	
	function least_attendant_members ( $data, $num=15 ) { ?>


		<table class="left table front-page" >
			<tr><th colspan="7" ><div class="center"><h3> Least Attendant Members </h3></div></th></tr>
			<tr>	
				<th class="" >Name</th>
				<th class="">Last WS</th>
				<th class="">Avg. WS</th>
				<th class="">Total WS</th>
				<th class="">Total LS</th>
			</tr>
		<?php if(!empty( $data['by_avg_service'] )): 
				if( $num == 0){ $ct=-1; } else { $ct=0; }
				foreach( $data['by_avg_service'] as $mem ):
					if( $mem['member'] ):
						if( $ct < $num ): ?>
							<tr class="" >
								<td><?php echo $mem['lname'].', '.$mem['fname']; ?></td>
								<td><?php echo date( 'M, jS Y' , strtotime($mem['last_service']['date'])); ?></td>
								<td><?php echo round($mem['avg_service'], 2 ) ; ?></td>
								<td><?php echo $mem['total_services'] ; ?></td>
								<td><?php echo $mem['total_communion'] ; ?></td>
							</tr>
			<?php		endif;
						if( $num == 0){ $ct--; }else{ $ct++; }
					endif;
				endforeach;
			endif; ?>
		</table>
		<?php
	}
	
	function least_sacramental_members( $data, $num=15 ) { ?>


		<table class="left table front-page" >
			<tr><th colspan="7" ><div class="center"><h3> Least Sacramental Members </h3></div></th></tr>
			<tr>	
				<th class="" >Name</th>
				<th class="">Last LS</th>
				<th class="">Total LS</th>
				<th class="">Total WS</th>
			</tr>
		<?php if(!empty( $data['by_avg_communion'])): 
				if( $num == 0){ $ct=-1; } else { $ct=0; }
				foreach( $data['by_avg_communion'] as $mem ):
					if( $mem['communicate'] ):
						if( $ct < $num ): ?>
							<tr class="" >
								<td><?php echo $mem['lname'].', '.$mem['fname']; ?></td>
								<td><?php echo date( 'M, jS Y' , strtotime($mem['last_communion']['date'])); ?></td>
								<td><?php echo $mem['total_communion'] ; ?></td>
								<td><?php echo $mem['total_services'] ; ?></td>
							</tr>
			<?php		endif;
						if( $num == 0){ $ct--; }else{ $ct++; }
					endif;
				endforeach;
			endif; ?>
		</table>
		<?php
	}
	
	function visitors_to_contact( $visitors ) { ?>
	
		<table class="left table front-page" >
			<tr><th colspan="4" ><div class="center"><h3> Visitors to Contact </h3></div></th></tr>
			<tr>
				<th><?php echo form_open('main/visited_visitor');?> </th>
				<th class="" >Name</th>
				<th class="">Contact Info</th>
				<th class="">Visit Date</th>
			</tr>
		<?php if(!empty( $visitors)): 
				$names_array = array();
				$ct=0; 
				foreach( $visitors as $visitor ):
					$x=$visitor['visited'];
					foreach( $names_array as $name):
						if( $name == $visitor['names']){ $x=false; break; } 
					endforeach;
					if($x): ?>
					<tr class="" >
						<td><input type="checkbox" class="visited" value="1" name="visited<?php echo $visitor['key']; ?>" id="visited<?php echo $visitor['key']; ?>"><label for="visited<?php echo $visitor['key']; ?>" >Visited</label></td>
						<td><?php echo $visitor['names']; ?></td>
						<td><?php echo $visitor['contact_info']; ; ?></td>
						<td><?php echo date( 'M, jS Y' , strtotime($visitor['date'])); ?></td>
					</tr>
			<?php	
					endif;
				$names_array[$ct]= $visitor['names'];
				$ct++;
				endforeach;
			endif; ?>
			<td colspan="3"></td><td><?php echo form_submit( 'My Submit', 'Submit' ) . form_close() ; ?></td>
		</table>
		<?php
	}
	
	function last_service_stats( $last_service ) { ?>
	
		<table class="left table front-page" >
			<tr><th colspan="2" ><div class="center"><h3> Last Service Stats </h3></div></th></tr>
		<?php if(!empty( $last_service)): 
				foreach( $last_service as $k => $v ): if(!empty($v)): ?>
					<tr class="" >
						<td class="" ><?php echo humanize($k); ?>: </td>
						<td> <?php echo $v; ?> </td>
					</tr>
			<?php
				endif; endforeach;
			endif; ?>
		</table>
		<?php
	}
	
	function yearly_stats( $year_stats ) { ?>


		<table class="left table front-page" >
			<tr><th colspan="2" ><div class="center"><h3> Annual Stats </h3></div></th></tr>
		<?php if(!empty( $year_stats)): 
				foreach( $year_stats as $k => $v ): if(!empty($v)): ?>
					<tr class="" >
						<td class="" ><?php echo humanize($k); ?>: </td>
						<td> <?php echo round($v); ?> </td>
					</tr>
			<?php
				endif; endforeach;
			endif; ?>
		</table>
		<?php
	}





?>



<?php if( !$help_menu ){ ?><button id="help_button">Help</button><?php } ?>
<div class="page_help">
<small>This help menu my be turned off in "Site" -> "Options" -> "Help Menus"</small>
<div id="page_help"  title="Adding Services Help">

<h3 class="help-title">Help on Front Page </h3>

<h3 id="toc" class="help-title">Table of Contents</h3>
<ul>
	<li><a href="#overview">Overview</a></li>
	<li><a href="#members_last_service">Deliquent Members By Last Service</a></li>
	<li><a href="#members_least_attendant">Least Attendant Members</a></li>
	<li><a href="#members_least_sacramental">Least Sacramental Members</a></li>
	<li><a href="#visitors_to_contact">Visitors to Contact</a></li>
	<li><a href="#service_stats">Last Serivce Stats</a></li>
	<li><a href="#annual_stats">Annual Stats</a></li>
</ul>

<h3 id="overview" class="help-title">Overview:</h3>
<p>This page gives basic stats on the members information that you have entered.
Most of the stats are based on attendance to "Worship Services", if services are listed as another type these services are ignored for the purpose of statistics. 
Which stats you can see as well as which order they appear in can be configured on the Options->options page. 
Click options, then options again, scroll down till you see 'Front Page options'. 
In the first column you can decide which stats to view.
In the second column you can decide the order of the stats.
In the third column for certain stats you can decide how many members to view in that category. </p>

<h3 id="members_last_service" class="help-title" >Delinquent Members by Last Service</h3>
<p> These stats list the members in order of who has been missing from service for the longest time. 
The first person listed is the member who has been missing from all services for the longest amount of time.
Choose the number of members included in the list on the Options->Options page. </p>

<h3 id="members_least_attendant" class="help-title">Least Attendant Members</h3>
<p>This stats list, list members in order of their average attendance. 
Whoever has been delinquent from church the most often in the last year will  be listed first.
The number of members listed can be changed in the Options->Options page.</p>

<h3 id="members_least_sacramental" class="help-title">Least Sacramental Members</h3>
<p>This stats list, list members in order of their average use of communion. 
Whoever has recieved the Lord's Supper least in the last year will  be listed first.
The number of members listed can be changed in the Options->Options page.</p>

<h3 id="visitors_to_contact" class="help-title">Visitors to Contact</h3>
<p> This list lists all visitors to your church along with their contact information. 
Once you have contacted the visitor check their in this list and hit the submit button. 
They will no longer be listed.</p>


<h3 id="service_stats" class="help-title">Last Service Stats</h3>
<p> This gives you the quick stats from the last service entered. </p>

<h3 id="annual_stats" class="help-title">Annual Stats</h3>
<p> This list gives some quick stats for the last year. </p>


</div>
</div>

<div class="data">

<?php foreach ( $functions as $function ):
if( $function['visible'] ):
echo $function['name']( $$function['data'], $function['other_data'] );
endif;
endforeach;
?>


</div>