<?php $this->load->helper('url'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title> Living Numbers : Upgrade </title>

<style type="text/css">
body{ background-color:#000; padding:0px; margin:0px; }
#upgrade_bod{ background: url('../../files/img/install.bmp') no-repeat; width:800px; height:600px; margin:0px auto; }
#info{ margin:0px auto; position:relative; top:200px; width:364px;}
#links{ margin:0px auto; position:relative; top:200px; width:182px;}
#links a { width:182px; height:29px; margin:5px 0px; color:#75abff; text-align:center; text-decoration:none; float:left; background:url('../../files/img/fButton.bmp') no-repeat; padding:4px 0 0; font-weight:bold;}
#links a:hover { color:#fff; }
p{ float:left; color:#fff; text-align:left; }
h2{ float:left; color:#fff; text-align:left; }
</style>
</head>

<body>
<div id="upgrade_bod" >

<div id="info" >
<h2>You database needs to be updated! Please backup your database then update your database!</h2>
</div>
<div id="links" >

<a href="<?php echo site_url("options/save_data"); ?>" title="upgrade">Backup Database</a>

<a href="<?php echo site_url("install/installer"); ?>" title="upgrade">Upgrade Database</a>
</div>

</div>
