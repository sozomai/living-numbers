<div class="  center">
<h2>Print Reports</h2>
</div>

<div class="  ">

<?php if( !$help_menu ){ ?><button id="help_button">Help</button><?php } ?>
<div class="page_help    ">
<small>This help menu my be turned off in "Site" -> "Options" -> "Help Menus"</small>
<div id="page_help"  title="Adding Services Help">

</div>
</div>




<?php

$this->load->helper('url');
$this->load->helper('form'); 
echo form_open('reports/index');
echo validation_errors(); if( isset($name_error) ){ echo $name_error; }
echo form_hidden('print_reports', 'yes' );
    $inclassdate = 'class="datepicker  input  "';

?>
                	
<div class="form data">
<span class="label">Include: </span>
<?php
	echo '<span class="input">'.form_checkbox( 'inc_service' , 1, set_checkbox('inc_service' , 1, TRUE ), 'id="inc_service" class="check"') . form_label('Service Information', 'inc_service' ).' Service Information</span>'; 
	echo '<span class="label"> </span><span class="input">'.form_checkbox( 'inc_offering' , 1, set_checkbox('inc_offering' , 1, TRUE ), 'id="inc_offering" class="check"') . form_label('Offering Information', 'inc_offering' ).' Offering Information</span>'; 
 	echo '<span class="label"> </span><span class="input">'.form_checkbox( 'inc_stats' , 1, set_checkbox('inc_stats' , 1, TRUE ), 'id="inc_stats" class="check"') . form_label('Statistics Information', 'inc_stats' ).' Statistics Information</span>'; 
 	echo '<span class="label"> </span><span class="input">'.form_checkbox( 'inc_chstats' , 1, set_checkbox('inc_chstats' , 1, TRUE ), 'id="inc_chstats" class="check"') . form_label('Church Statistics', 'inc_chstats' ).' Church Statistics</span>'; 
	echo '<span class="label">' . form_label('Begin Date', 'beg_date') . '</span>';
    echo '<span class="input">' . form_input('beg_date', set_value('beg_date', date('m/d/Y', mktime( date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')-1))), $inclassdate ) . '</span>';
    echo '<span class="label">' . form_label('End Date', 'end_date') . '</span>';
    echo '<span class="input">' . form_input('end_date', set_value('beg_date', date('m/d/Y')), $inclassdate ) . '</span>';
    echo '<span class="label"></span>';
 	echo '<span class="label"> </span><span class="input">'.form_checkbox( 'rev_checks' , 1, set_checkbox('rev_checks' , '1' ), 'id="rev_checks" class="check"') . form_label('Ignore Marked Members', 'rev_checks' ).' Ignore Marked Members</span>'; 
?>
	<div class="table">
	   <table id="members" >
	<?php if( !empty($grp_by_fam) ): foreach(  $grp_by_fam as $gp ): if( !empty($gp['members'][0]['key']) ):?>
		<tr class="ui-state-highlight "><td class="family_name" rowspan="2"><?php  echo $gp['family_name'] ; ?> </td><td colspan="2"></td><td colspan="4" class="attendance"></td><td colspan="3" class="offering"></td></tr>
			<?php $ct = 0; ?>
			<?php foreach ( $gp['members'] as $mem ): ?>
				<tr><?php if( $ct>0 ){ ?><td></td><?php } ?>
				<td class="center attendance"> <?php echo form_checkbox( $mem['key'] , $mem['key'], set_checkbox($mem['key'] , 1, TRUE ), 'id="' . $mem['key'] . '" class="check"') . form_label('X', $mem['key'] ); ?> </td>
				<td> <?php echo $mem['lname']; ?></td>
				<td> <?php echo $mem['fname'] ; ?> </td>
			<?php $ct++; endforeach; ?>


	<?php endif; endforeach; endif; ?>
		</table>
	<span class="label"></span><span class="input"><?php echo form_submit('mysubmit', 'Get Report', 'class="button"'); ?></span>
	</div>
</div>

</div>
</div>
<script type="text/javascript" >
	$(function() {
		$(".datepicker").datepicker({
		changeYear: true,
		changeMonth: true,
		yearRange: '-100:+1',
		dateFormat: 'mm/dd/yy'
		});
	});
	$(function () {
		$(".check").button({
			icons: {primary:'ui-icon-check'},
			text: false,
		});
	});
	

</script>