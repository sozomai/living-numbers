<div class="  center">
<h2>Add Baptism to Church Records</h2>
</div>

<div class="  ">

<?php if( !$help_menu ){ ?><button id="help_button">Help</button><?php } ?>

<div class="page_help    ">
<small>This help menu my be turned off in "Site" -> "Options" -> "Help Menus"</small>
<div id="page_help"  title="Adding Services Help">

<h3 class="  help-title">Help on Adding Bapotism </h3>

<p> This page allows you to add baptisms to your church records. </p>
<p>"Date" indicates date of baptism, while "Born" indicates the day of brith for the baptized.</p>
<p>Updating status: After the name box you will see three small icons, a plus, an arrow, and a "do not" circle. 
These allow you you to update the baptized status in the church records. The plus sign will add the child to family name given below with the given status. 
The arrow will first try to update the status if the child is already listed, if the program can not find the child the update button will then add the child.
The "do not" circle will skip this process completely and will make no changes to the member database.</p>
<p> A file of the sermon either audio or written can be uploaded and stored for viewing later.</p>

</div>
</div>

</style> <?php 

    $this->load->helper('form');
    $inclass = 'class=" input  "';
    $inclassdate = 'class="datepicker  input  "';
    $inclassmember = 'class="member  input  "';
    $inclassfamily = 'class="family  input  "';
    $inclassstatus = 'class="status  input  "';
	echo form_open_multipart('records/add_record' );
	?>
    
 <?php echo validation_errors(); ?>
 <?php if( isset($upload_error)){ echo $upload_error ; } ?>
                	
<div class="form data">
    
    <?php
	
	echo form_hidden('page', 'baptism');
	echo form_hidden('type', 'baptism');
		
	echo '<span class="label">Location</span>';
    echo '<span id="church" class="input">' . form_input('church', set_value('church', 'Church'), $inclass ) . '</span>';
	
    echo '<span class="label">Date</span>';
    echo '<span id="date" class="input">' . form_input('date', set_value('date', date( 'm/d/Y' )), $inclassdate ) . '</span>';

    echo '<span class="label">Born</span>';
    echo '<span id="born" class="input">' . form_input('born', set_value('born', date( 'm/d/Y' )), $inclassdate ) . '</span>';

    echo '<span class="label">Name</span>';
    echo '<span id="name" class="input">' . form_input('name', set_value('name' ), $inclassmember ) . '</span>';
	
    echo '<span class="label"></span>';
    echo '<span class="input buttonset">' . form_radio('status', 'add_to', false, "id='add_to'" );
	echo form_label( 'Add',"add_to" ) . form_radio('status', 'change_to', false, "id='change_to'" );
	echo form_label( 'Change',"change_to" ) . form_radio('status', 'skip', true, "id='none'" );
	echo form_label( 'Skip',"none" ) .  '</span>';
    echo '<div id="mem_update"><span class="label"></span>';
	echo '<span class="input mem_update"> To Family Group: ' . form_input('fgroup', set_value('fgroup' ), $inclassfamily ) . '</span>';
    echo '<span class="label"></span>';
	echo '<span class="input mem_update"> As Status: ' . form_input('mstatus', set_value('mstatus', 'Baptized' ), $inclassstatus ) . '</span>';	
    echo '<span class="label"></div>';	

	echo '<span class="label">Parents</span>';
    echo '<span id="parents" class="input">' . form_input('parents', set_value('parents' ), $inclass ) . '</span>';

	echo '<span class="label">First Witness</span>';
    echo '<span id="witness_one" class="input">' . form_input('witness_one', set_value('witness_one' ), $inclassmember ) . '</span>';

	echo '<span class="label">Second Witness</span>';
    echo '<span id="witness_two" class="input">' . form_input('witness_two', set_value('witness_two' ), $inclassmember ) . '</span>';

	echo '<span class="label">Official</span>';
    echo '<span id="official" class="input">' . form_input('official', set_value('official' ), $inclassmember ) . '</span>';

	echo '<span class="label">Passage</span>';
    echo '<span id="passage" class="input">' . form_input('passage', set_value('passage' ), $inclass ) . '</span>';
	
	echo '<span class="label">Sermon Text/Theme</span>';
    echo '<span id="text" class="input">' . form_input('text', set_value('text' ), $inclass ) . '</span>';

	echo '<span class="label">Written Sermon</span>';
    echo '<span id="sermon" class="input"><input type="file" name="sermon" '.set_value('sermon').' '. $inclass .' /></span>';

	echo '<span class="label">Audio Sermon</span>';
    echo '<span id="asermon" class="input"><input type="file" name="asermon" '.set_value('asermon').' '. $inclass .' /></span>';

	echo '<span class="label">Notes</span>';
    echo '<span id="notes" class="input">' . form_textarea('notes', set_value('notes' ), $inclass ) . '</span>';
		
	echo '<span class="label"></span>';
    echo '<span class="input">' . form_submit('Submit', 'Add Record' ) . form_close() . '</span>';
		
?>		
</div>
</div>
<script type="text/javascript">
	$(function() {
		$(".buttonset").buttonset();
		$("#add_to").button({ icons: {primary:'ui-icon-plus'}, text:false });
		$("#change_to").button({ icons: {primary:'ui-icon-arrowreturnthick-1-n'}, text:false });
		$("#none").button({ icons: {primary:'ui-icon-cancel'}, text:false });
		$("#rto").button({ icons: {primary:'ui-icon-arrowthick-1-s'}, text:false });
		$("#rfrom").button({ icons: {primary:'ui-icon-arrowthick-1-n'}, text:false });
		$( ".mem_update input" ).button({ disabled: true });
	});
	$(function() {	
		$('#none')
			.click(function() {
				$( ".mem_update input" ).button({ disabled: true });
			});
	});
	$(function() {	
		$('#change_to, #add_to')
			.click(function() {
				$( ".mem_update input" ).button({ disabled: false });
			});
	});
	
	$(function() {
		var availableTags = [<?php foreach( $mstatus as $status ) { echo '"'.$status->status.'", '; } ?>];
		$(".status").autocomplete({
			source: availableTags
		});
	});
	
	$(function() {
		var availableTags = [<?php foreach( $families as $fam ) { echo '"'.$fam->family_name.'", '; } ?>];
		$(".family").autocomplete({
			source: availableTags
		});
	});
	
	$(function() {
		var availableTags = [<?php foreach( $members as $mem ) { echo '"'.$mem->lname.', '.$mem->fname.'", '; } ?>];
		$(".member").autocomplete({
			source: availableTags
		});
	});
	$(function() {
		$(".datepicker").datepicker({
		changeYear: true,
		changeMonth: true,
		yearRange: '-100:+1',
		dateFormat: 'mm/dd/yy'
		});
	});
	$(function() {
	 <?php  foreach( $_POST as $k => $v ): 
				$set = form_error($k);
				if( !empty( $set ) ): ?>
				$('#<?php echo $k; ?> input').switchClass('', 'ui-state-error' );
				<?php endif;
			endforeach;
	?>
		return false;
	});

</script>