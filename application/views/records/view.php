<?php

$this->load->helper('form');
$this->load->helper('inflector');
$this->load->helper('text');
$this->load->helper('file');
$this->load->helper('url');
$this->load->helper('number');
$this->load->helper('array');

$ct=0;
$recordz=array();
if(!empty($records)): foreach( $records as $record ):
	$recordz[$record['type']][$ct] = $record ;
	$ct++;
endforeach; endif;

?>

<?php if( !$help_menu ){ ?><button id="help_button">Help</button><?php } ?>

<div class="page_help">
<small>This help menu my be turned off in "Site" -> "Options" -> "Help Menus"</small>
<div id="page_help"  title="Adding Services Help">

<h3 class="help-title">Help on Viewing Records </h3>
<p> This page allows you to view records previously entered. You can choose to view all records for a given year or choose to view all records of a certain type.</p>

</div>
</div>

<?php if( isset( $years )): ?>
	<div class="sidebar">
		<h2>Choose List for Display</h2>
		<div id="list_records">
			<h3><a href="#">Pick a Year</a></h3>
				<div class="menu" id="pick-a-year" > 
					<ul class="ui-menu">
						<?php foreach( $years as $year ){ echo '<li class="ui-menu-item  ui-selectee" >'.anchor('records/view/'.$year, $year, 'rel="services"' ).'</li>'; } ?>
					</ul>
				</div>
			<h3><a href="#">Pick a Record Type</a></h3>
				<div class="menu" id="pick-a-record" > 
					<ul class="ui-menu">
						<?php  foreach( $types as $type ){ echo '<li class="ui-menu-item  ui-selectee" >'.anchor('records/view/type/'.$type, $type, 'title="services-1"' ).'</li>'; } ?>
					</ul>
				</div>		
		</div>
	
	</div>
<?php endif; ?>

<div class="data">




<?php foreach( $recordz as $type => $records ): ?>
	<?php if( $type == 'baptism' ){ ?>
		<table class="table left" >
			<tr><th colspan="12" ><div class="  center"><h3><?php echo humanize( $type ); ?>s</h3></div></th></tr>
			<tr>	
				<th class="   ">Date</th>
				<th class="   ">Location</th>
				<th class="   ">Name</th>
				<th class="   ">Born</th>
				<th class="   ">Parents</th>
				<th class="   ">Witness</th>
				<th class="   ">Witness</th>
				<th class="   ">Official</th>
				<th class="   ">Text/Theme</th>
				<th class="   ">Audio Sermon</th>
				<th class="   ">Written Sermon</th>
				<th class="   ">Notes</th>
			</tr>
			<?php foreach( $records as $record ): ?>
						<tr class=" " >
							<td><?php echo date( 'M, jS Y' , strtotime($record['date'])); ?></td>
							<td><?php echo element('church', $record); ?></td>
							<td><?php echo element('name', $record); ?></td>
							<td><?php echo date( 'M, jS Y' , strtotime($record['born'])); ?></td>
							<td><?php echo element('parents', $record); ?></td>
							<td><?php echo element('witness_one', $record); ?></td>
							<td><?php echo element('witness_two', $record); ?></td>
							<td><?php echo element('official', $record); ?></td>
							<td><?php echo element('text', $record); ?></td>
							<?php $asermon = get_file_info($record['asermon']); ?>
							<td><?php if( !empty($asermon)): ?>
								<a href="<?php echo base_url().$asermon['server_path']; ?>" rel="<?php echo $asermon['name']; ?>"><?php echo humanize($asermon['name']); ?>, size: <?php echo byte_format($asermon['size']);?> </a>
								<?php endif; ?>
							</td>
							<td><?php echo word_wrap( word_limiter($record['sermon'], 25), 25); ?></td>
							<td><?php  echo word_wrap( word_limiter($record['notes'], 25), 25); ?></td>
						</tr>
			<?php endforeach; ?>
		</table>
	<?php } elseif( $type == 'confirmation' || $type == 'adult_conf' ){ ?>
		<table class="table left" >
			<tr><th colspan="12" ><div class="  center"><h3><?php echo humanize( $type ); ?>s</h3></div></th></tr>
			<tr>	
				<th class="   ">Date</th>
				<th class="   ">Name</th>
				<th class="   ">Official</th>
				<th class="   ">Text/Theme</th>
				<th class="   ">Audio Sermon</th>
				<th class="   ">Written Sermon</th>
				<th class="   ">Notes</th>
			</tr>
			<?php foreach( $records as $record ): ?>
						<tr class=" " >
							<td><?php echo date( 'M, jS Y' , strtotime($record['date'])); ?></td>
							<td><?php echo element('name', $record); ?></td>
							<td><?php echo element('official', $record); ?></td>
							<td><?php echo element('text', $record); ?></td>
							<?php $asermon = get_file_info($record['asermon']); ?>
							<td><?php if( !empty($asermon)): ?>
								<a href="<?php echo base_url().$asermon['server_path']; ?>" rel="<?php echo $asermon['name']; ?>"><?php echo humanize($asermon['name']); ?>, size: <?php echo byte_format($asermon['size']);?> </a>
								<?php endif; ?>
							</td>
							<td><?php echo word_wrap( word_limiter($record['sermon'], 25), 25); ?></td>
							<td><?php  echo word_wrap( word_limiter($record['notes'], 25), 25); ?></td>
						</tr>
			<?php endforeach; ?>
		</table>
	<?php } elseif( $type == 'wedding' ){ ?>
		<table class="table left" >
			<tr><th colspan="13" ><div class="  center"><h3><?php echo humanize( $type ); ?>s</h3></div></th></tr>
			<tr>	
				<th class="   ">Date</th>
				<th class="   ">Location</th>
				<th class="   ">Bride</th>
				<th class="   ">Groom</th>
				<th class="   ">Bride's Parents</th>
				<th class="   ">Groom's Parents</th>
				<th class="   ">Witness</th>
				<th class="   ">Witness</th>
				<th class="   ">Official</th>
				<th class="   ">Text/Theme</th>
				<th class="   ">Audio Sermon</th>
				<th class="   ">Written Sermon</th>
				<th class="   ">Notes</th>
			</tr>
			<?php foreach( $records as $record ): ?>
						<tr class=" " >
							<td><?php echo date( 'M, jS Y' , strtotime($record['date'])); ?></td>
							<td><?php echo element('church', $record); ?></td>
							<td><?php echo element('bride', $record); ?></td>
							<td><?php echo element('groom', $record); ?></td>
							<td><?php echo element('brides_parents', $record); ?></td>
							<td><?php echo element('grooms_parents', $record); ?></td>
							<td><?php echo element('witness_one', $record); ?></td>
							<td><?php echo element('witness_two', $record); ?></td>
							<td><?php echo element('official', $record); ?></td>
							<td><?php echo element('text', $record); ?></td>
							<?php $asermon = get_file_info($record['asermon']); ?>
							<td><?php if( !empty($asermon)): ?>
								<a href="<?php echo base_url().$asermon['server_path']; ?>" rel="<?php echo $asermon['name']; ?>"><?php echo humanize($asermon['name']); ?>, size: <?php echo byte_format($asermon['size']);?> </a>
								<?php endif; ?>
							</td>
							<td><?php echo word_wrap( word_limiter($record['sermon'], 25), 25); ?></td>
							<td><?php  echo word_wrap( word_limiter($record['notes'], 25), 25); ?></td>
						</tr>
			<?php endforeach; ?>
		</table>
	<?php } elseif( $type == 'funeral' ){ ?>
		<table class="table left" >
			<tr><th colspan="13" ><div class="  center"><h3><?php echo humanize( $type ); ?>s</h3></div></th></tr>
			<tr>	
				<th class="   ">Date</th>
				<th class="   ">Buried At</th>
				<th class="   ">Died</th>
				<th class="   ">Official</th>
				<th class="   ">Text/Theme</th>
				<th class="   ">Audio Sermon</th>
				<th class="   ">Written Sermon</th>
				<th class="   ">Notes</th>
			</tr>
			<?php foreach( $records as $record ): ?>
						<tr class=" " >
							<td><?php echo date( 'M, jS Y' , strtotime($record['date'])); ?></td>
							<td><?php echo element('church', $record); ?></td>
							<td><?php echo element('died', $record); ?></td>
							<td><?php echo element('official', $record); ?></td>
							<td><?php echo element('text', $record); ?></td>
							<?php $asermon = get_file_info($record['asermon']); ?>
							<td><?php if( !empty($asermon)): ?>
								<a href="<?php echo base_url().$asermon['server_path']; ?>" rel="<?php echo $asermon['name']; ?>"><?php echo humanize($asermon['name']); ?>, size: <?php echo byte_format($asermon['size']);?> </a>
								<?php endif; ?>
							</td>
							<td><?php echo word_wrap( word_limiter($record['sermon'], 25), 25); ?></td>
							<td><?php  echo word_wrap( word_limiter($record['notes'], 25), 25); ?></td>
						</tr>
			<?php endforeach; ?>
		</table>
	<?php } elseif( $type == 'transfer' ){ ?>
		<table class="table left" >
			<tr><th colspan="13" ><div class="  center"><h3><?php echo humanize( $type ); ?>s</h3></div></th></tr>
			<tr>	
				<th class="   ">Date</th>
				<th class="   ">Direction</th>
				<th class="   ">From/To Church</th>
				<th class="   ">Name</th>
				<th class="   ">Notes</th>
			</tr>
			<?php foreach( $records as $record ): ?>
						<tr class=" " >
							<td><?php echo date( 'M, jS Y' , strtotime($record['date'])); ?></td>
							<td><?php echo element('direction', $record); ?></td>
							<td><?php echo element('church', $record); ?></td>
							<td><?php echo element('name', $record); ?></td>
							<td><?php  echo word_wrap( word_limiter($record['notes'], 25), 25); ?></td>
						</tr>
			<?php endforeach; ?>
		</table>
	<?php } elseif( $type == 'termination' ){ ?>
		<table class="table left" >
			<tr><th colspan="13" ><div class="  center"><h3><?php echo humanize( $type ); ?>s</h3></div></th></tr>
			<tr>	
				<th class="   ">Date</th>
				<th class="   ">Name</th>
				<th class="   ">Reason</th>
				<th class="   ">Notes</th>
			</tr>
			<?php foreach( $records as $record ): ?>
						<tr class=" " >
							<td><?php echo date( 'M, jS Y' , strtotime($record['date'])); ?></td>
							<td><?php echo element('name', $record); ?></td>
							<td><?php  echo word_wrap( word_limiter($record['reason'], 25), 25); ?></td>
							<td><?php  echo word_wrap( word_limiter($record['notes'], 25), 25); ?></td>
						</tr>
			<?php endforeach; ?>
		</table>
	<?php } else { ?>
		<table class="table left" >
			<tr><th colspan="13" ><div class="  center"><h3>Other</h3></div></th></tr>
			<tr>	
				<th class="   ">Date</th>
				<th class="   ">Type</th>
				<th class="   ">Name</th>
				<th class="   ">Location</th>
				<th class="   ">Born</th>
				<th class="   ">Died</th>
				<th class="   ">Bride</th>
				<th class="   ">Groom</th>
				<th class="   ">Bride's Parents</th>
				<th class="   ">Groom's Parents</th>
				<th class="   ">Witness</th>
				<th class="   ">Witness</th>
				<th class="   ">Official</th>
				<th class="   ">Passage</th>
				<th class="   ">Text/Theme</th>
				<th class="   ">Audio Sermon</th>
				<th class="   ">Written Sermon</th>
				<th class="   ">Reason</th>
				<th class="   ">Notes</th>
			</tr>
			<?php foreach( $records as $record ): ?>
						<tr class=" " >
							<td><?php echo date( 'M, jS Y' , strtotime($record['date'])); ?></td>
							<td><?php echo element('type', $record); ?></td>
							<td><?php echo element('name', $record); ?></td>
							<td><?php echo element('church', $record); ?></td>
							<td><?php echo date( 'M, jS Y' , strtotime($record['born'])); ?></td>
							<td><?php echo date( 'M, jS Y' , strtotime($record['died'])); ?></td>
							<td><?php echo element('bride', $record); ?></td>
							<td><?php echo element('groom', $record); ?></td>
							<td><?php echo element('brides_parents', $record); ?></td>
							<td><?php echo element('grooms_parents', $record); ?></td>
							<td><?php echo element('witness_one', $record); ?></td>
							<td><?php echo element('witness_two', $record); ?></td>
							<td><?php echo element('official', $record); ?></td>
							<td><?php echo element('passage', $record); ?></td>
							<td><?php echo element('text', $record); ?></td>
							<?php $asermon = get_file_info($record['asermon']); ?>
							<td><?php if( !empty($asermon)): ?>
								<a href="<?php echo base_url().$asermon['server_path']; ?>" rel="<?php echo $asermon['name']; ?>"><?php echo humanize($asermon['name']); ?>, size: <?php echo byte_format($asermon['size']);?> </a>
								<?php endif; ?>
							</td>
							<td><?php echo word_wrap( word_limiter($record['sermon'], 25), 25); ?></td>
							<td><?php  echo word_wrap( word_limiter($record['reason'], 25), 25); ?></td>
							<td><?php  echo word_wrap( word_limiter($record['notes'], 25), 25); ?></td>
						</tr>
			<?php endforeach; ?>
		</table>
	<?php } ?>
<?php endforeach; ?>



</div>

<script type="text/javascript">
	$(function() {
		var icons = {
			header: "ui-icon-carat-1-e",
			headerSelected: "ui-icon-carat-1-s"
		};

		$("#list_records").accordion({
			autoHeight: false,
			navigation: true,
			icons: icons,
		});
		$('.ui-menu-item').hover(function() {
			$('.ui-menu-item').removeClass( 'ui-state-hover' );
			$(this).addClass( 'ui-state-hover' );
		});
		$('h3').hover(function() {
			$('.ui-menu-item').removeClass( 'ui-state-hover' );
		});				
						
	});
	$(function() {	
		$( "ul.dates_list li a" ).button();
	});

	$(function() {
		//scrollpane parts
		var scrollPane = $('.dates');
		var scrollContent = $('.dates_list');
		
		//build slider
		var scrollbar = $(".scroll-bar").slider({
			slide:function(e, ui){
				if( scrollContent.width() > scrollPane.width() ){ scrollContent.css('margin-left', Math.round( ui.value / 100 * ( scrollPane.width() - scrollContent.width() )) + 'px'); }
				else { scrollContent.css('margin-left', 0); }
			}
		});
		
		//append icon to handle
		var handleHelper = scrollbar.find('.ui-slider-handle')
		.mousedown(function(){
			scrollbar.width( handleHelper.width() );
		})
		.mouseup(function(){
			scrollbar.width( '100%' );
		})
		.append('<span class="ui-icon ui-icon-grip-dotted-vertical"></span>')
		.wrap('<div class="ui-handle-helper-parent"></div>').parent();
		
		//change overflow to hidden now that slider handles the scrolling
		scrollPane.css('overflow','hidden');
		
		//size scrollbar and handle proportionally to scroll distance
		function sizeScrollbar(){
			var remainder = scrollContent.width() - scrollPane.width();
			var proportion = remainder / scrollContent.width();
			var handleSize = scrollPane.width() - (proportion * scrollPane.width());
			scrollbar.find('.ui-slider-handle').css({
				width: handleSize,
				'margin-left': -handleSize/2
			});
			handleHelper.width('').width( scrollbar.width() - handleSize);
		}
		
		//reset slider value based on scroll content position
		function resetValue(){
			var remainder = scrollPane.width() - scrollContent.width();
			var leftVal = scrollContent.css('margin-left') == 'auto' ? 0 : parseInt(scrollContent.css('margin-left'));
			var percentage = Math.round(leftVal / remainder * 100);
			scrollbar.slider("value", percentage);
		}
		//if the slider is 100% and window gets larger, reveal content
		function reflowContent(){
				var showing = scrollContent.width() + parseInt( scrollContent.css('margin-left') );
				var gap = scrollPane.width() - showing;
				if(gap > 0){
					scrollContent.css('margin-left', parseInt( scrollContent.css('margin-left') ) + gap);
				}
		}
		
		//change handle position on window resize
		$(window)
		.resize(function(){
				resetValue();
				sizeScrollbar();
				reflowContent();
		});
		//init scrollbar size
		setTimeout(sizeScrollbar,10);//safari wants a timeout
	});
	
</script>
	
