<div class="  center">
<h2>Add Transfer to Church Records</h2>
</div>

<div class="  ">

<?php if( !$help_menu ){ ?><button id="help_button">Help</button><?php } ?>
<div class="page_help    ">
<small>This help menu my be turned off in "Site" -> "Options" -> "Help Menus"</small>
<div id="page_help"  title="Adding Services Help">

<h3 class="  help-title">Help on Transfering Members </h3>

<p>The arrows indicate whether the member is being transfered "to" or "from" another church. 
The down arrow is someone being transfered out of your church family TO a different church. 
The up arrow indicates someone being transfered into your church family FROM a different church.</p>
<p>The "TO" box indicates the other church involved whether the member is being transfered from or to.</p>
<p>Family Group: If you are transfering a family OUT OF your church TO another church, simply type in the family group and ALL the members of that family group will be transfered.
If you are transfering someone INTO your church FROM another church, type in a new family group name which will be created, AND the names of those being transfered. Those names entered will be added as members of the given family group.</p>
<p>Updating status: After the name boxes you will see three small icons, a plus, an arrow, and a "do not" circle. 
These allow you you to update the status of the transfered members in the church records. 
The plus sign will add the members to the family name given with the given status. 
The arrow will first try to update the status if the member is already listed, if the program can not find the member the update button will then add the member.
The "do not" circle will skip this process completely and will make no changes to the member database.
The "add" button should normally be used if you are transfering people INTO your church.
The "update" button should normally be used if you are transfering people AWAY FROM your church.</p>



</div>
</div>

</style> <?php 

    $this->load->helper('form');
    $inclass = 'class=" input  "';
    $inclassdate = 'class="datepicker  input  "';
    $inclassmember = 'class="member  input  "';
    $inclassfamily = 'class="family  input  "';
    $inclassstatus = 'class="status  input  "';
	echo form_open_multipart('records/transfer_members' );
	?>
    
 <?php echo validation_errors(); ?>
 <?php if( isset($upload_error)){ echo $upload_error ; } ?>
                	
<div class="form data">
    
    <?php
	
	echo form_hidden('page', 'transfer');
	echo form_hidden('type', 'transfer');
	

	echo '<span class="label">Direction</span>';
    echo '<span id="direction" class="input buttonset">' . form_radio('direction', 'to', true, 'id="rto"' );
	echo form_label( 'To', 'rto' );
	echo form_radio('direction', 'from', false, 'id="rfrom"' );
	echo form_label( 'From', 'rfrom' ) . '</span>';
	
	echo '<span id="tto" class="label">Transfer To </span>';
 	echo '<span id="tfrom" class="label hidden">Transfer From </span>';
   echo '<span id="church" class="input">' . form_input('church', set_value('church'), $inclass ) . '</span>';
	
    echo '<span class="label">Date</span>';
    echo '<span id="date" class="input">' . form_input('date', set_value('date', date( 'm/d/Y' )), $inclassdate ) . '</span>';

    echo '<span class="label">Family Group</span>';
    echo '<span id="family_group" class="input">' . form_input('family_group', set_value('family_group' ), $inclassfamily ) . '</span>';

	echo '<div id="names">';

    echo '<span class="label">Names:</span>';
    echo '<span class="input names">' . form_input('names[]', set_value('names[]' ), $inclassmember ) . '</span>';

	echo '</div>';
	echo '<span class="label"></span><span class="input" id="add_names">Add</span>';

	
    echo '<span class="label"></span>';
    echo '<span class="input buttonset">' . form_radio('status', 'add_to', false, "id='add_to'" );
	echo form_label( 'Add',"add_to" ) . form_radio('status', 'change_to', true, "id='change_to'" );
	echo form_label( 'Change',"change_to" ) . form_radio('status', 'skip', false, "id='none'" );
	echo form_label( 'Skip',"none" ) .  '</span>';
    echo '<span class="label"></span>';
	echo '<span class="input mem_update"> As Status: ' . form_input('mstatus', set_value('mstatus', 'Ex-member' ), $inclassstatus ) . '</span>';	
    echo '<span class="label"></span></div>';	
	
	echo '<span class="label">Notes</span>';
    echo '<span id="notes" class="input">' . form_textarea('notes', set_value('notes' ), $inclass ) . '</span>';
		
	echo '<span class="label"></span>';
    echo '<span class="input">' . form_submit('Submit', 'Add Record' ) . form_close() . '</span>';
		
?>		
</div>
</div>
<script type="text/javascript">
	$(function() {
		$(".buttonset").buttonset();
		$("#add_to").button({ icons: {primary:'ui-icon-plus'}, text:false });
		$("#change_to").button({ icons: {primary:'ui-icon-arrowreturnthick-1-n'}, text:false });
		$("#none").button({ icons: {primary:'ui-icon-cancel'}, text:false });
		$("#rto").button({ icons: {primary:'ui-icon-arrowthick-1-s'}, text:false });
		$("#rfrom").button({ icons: {primary:'ui-icon-arrowthick-1-n'}, text:false });
		$( ".names input" ).button({ disabled: true });
	});
	$(function() {	
		$('#rfrom')
			.click(function() {
				$( ".names input" ).button({ disabled: false });
				$( "#tfrom" ).removeClass('hidden');
				$( "#tto" ).addClass('hidden');
			});
	});
	$(function() {	
		$('#rto')
			.click(function() {
				$( ".names input" ).button({ disabled: true });
				$( "#tfrom" ).addClass('hidden');
				$( "#tto" ).removeClass('hidden');
			});
	});
	$(function() {	
		$('#none')
			.click(function() {
				$( ".mem_update input" ).button({ disabled: true });
			});
	});
	$(function() {	
		$('#change_to, #add_to')
			.click(function() {
				$( ".mem_update input" ).button({ disabled: false });
			});
	});
	
	$(function() {
		var availableTags = [<?php foreach( $mstatus as $status ) { echo '"'.$status->status.'", '; } ?>];
		$(".status").autocomplete({
			source: availableTags
		});
	});
	
	$(function() {
		var availableTags = [<?php foreach( $families as $fam ) { echo '"'.$fam->family_name.'", '; } ?>];
		$(".family").autocomplete({
			source: availableTags
		});
	});
	
	$(function() {
		var availableTags = [<?php foreach( $members as $mem ) { echo '"'.$mem->lname.', '.$mem->fname.'", '; } ?>];
		$(".member").autocomplete({
			source: availableTags
		});
	});
	$(function() {
		$(".datepicker").datepicker({
		changeYear: true,
		changeMonth: true,
		yearRange: '-100:+1',
		dateFormat: 'mm/dd/yy'
		});
	});
	$(function() {
	 <?php  foreach( $_POST as $k => $v ): 
				$set = form_error($k);
				if( !empty( $set ) ): ?>
				$('#<?php echo $k; ?> input').switchClass('', 'ui-state-error' );
				<?php endif;
			endforeach;
	?>
		return false;
	});
	$(function() {
		$('#add_names')
			.button({ icons: {primary:'ui-icon-plus'}, text: false })
			.click(function() {
				$('#names').append(
					'<span class="label"></span><span class="input names"><input type="text" name="names[]" value="" class="member  input  " /></span>'
				)
				$(function() {
					var availableTags = [<?php foreach( $members as $mem ) { echo '"'.$mem->lname.', '.$mem->fname.'", '; } ?>];
					$(".member").autocomplete({
						source: availableTags
					});
				});
				$("input").button();
			});
	});

</script>