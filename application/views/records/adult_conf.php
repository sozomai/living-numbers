<div class="  center">
<h2>Add Adult Confirmation to Church Records</h2>
</div>

<div class="  ">

<?php if( !$help_menu ){ ?><button id="help_button">Help</button><?php } ?>
<div class="page_help    ">
<small>This help menu my be turned off in "Site" -> "Options" -> "Help Menus"</small>
<div id="page_help"  title="Adding Services Help">

<h3 class="  help-title">Help on Adding Adult Confirmations </h3>

<p>There is no difference between an adult confirmation and a regular confirmation except it is recorded as an "Adult Confirmation".</p>
<p>Updating status: After the date box you will see two small icons an arrow, and a "do not" circle. 
These allow you you to update the status of the confirmands in the church records.  
The arrow will first try to update the status if the confirmand is already listed, if the program can not find the confirmand the update button will then add the confirmand.
The "do not" circle will skip this process completely and will make no changes to the member database.</p>
<p> A file of the sermon either audio or written can be uploaded and stored for viewing later.</p>


</div>
</div>

</style> <?php 

    $this->load->helper('form');
    $inclass = 'class=" input  "';
    $inclassdate = 'class="datepicker  input  "';
    $inclassmember = 'class="member  input  "';
    $inclassfamily = 'class="family  input  "';
    $inclassstatus = 'class="status  input  "';
	echo form_open_multipart('records/add_conf' );
	?>
    
 <?php echo validation_errors(); ?>
 <?php if( isset($upload_error)){ echo $upload_error ; } ?>
                	
<div class="form data">
    
    <?php
	
	echo form_hidden('page', 'adult_conf');
	echo form_hidden('type', 'adult_conf');
			
    echo '<span class="label">Date</span>';
    echo '<span id="date" class="input">' . form_input('date', set_value('date', date( 'm/d/Y' )), $inclassdate ) . '</span>';
	
    echo '<span class="label"></span>';
    echo '<span class="input buttonset">' . form_radio('status', 'change_to', true, "id='change_to'" );
	echo form_label( 'Change',"change_to" ) . form_radio('status', 'skip', false, "id='none'" );
	echo form_label( 'Skip',"none" ) .  '</span>';
    echo '<div id="mem_update"><span class="label"></span>';
	echo '<span class="input mem_update"> Status: ' . form_input('mstatus', set_value('mstatus', 'Member' ), $inclassstatus ) . '</span>';	
    echo '<span class="label"></span></div>';	
	echo '<span class="label">Official</span>';
    echo '<span id="official" class="input">' . form_input('official', set_value('official' ), $inclassmember ) . '</span>';

	echo '<div id="confirmands">';
    echo '<span class="label">Confirmands:</span>';
	echo form_hidden('num_conf', set_value('num_conf', 5));	
	
	$x=set_value('num_conf', 5);
	$ct=0;
	while( $ct < $x ):
    echo '<span class="label">Name</span>';
    echo '<span id="name'.$ct.'" class="input">' . form_input('name'.$ct, set_value('name'.$ct ), $inclassmember ) . '</span>';
	echo '<span class="input">Passage: </span>';
    echo '<span id="passage'.$ct.'" class="input">' . form_input('passage'.$ct, set_value('passage'.$ct ), $inclass ) . '</span>';
	$ct++;
	endwhile;

	echo '</div>';
	echo '<span class="label"></span><span class="input" id="add_conf">Add</span>';
	
	echo '<span class="label">Sermon Text/Theme</span>';
    echo '<span id="text" class="input">' . form_input('text', set_value('text' ), $inclass ) . '</span>';
	
	echo '<span class="label">Written Sermon</span>';
    echo '<span id="sermon" class="input"><input type="file" name="sermon" '.set_value('sermon').' '. $inclass .' /></span>';

	echo '<span class="label">Audio Sermon</span>';
    echo '<span id="asermon" class="input"><input type="file" name="asermon" '.set_value('asermon').' '. $inclass .' /></span>';

	echo '<span class="label">Notes</span>';
    echo '<span id="notes" class="input">' . form_textarea('notes', set_value('notes' ), $inclass ) . '</span>';
		
	echo '<span class="label"></span>';
    echo '<span class="input">' . form_submit('Submit', 'Add Record' ) . form_close() . '</span>';
		
?>		
</div>
</div>
<script type="text/javascript">
	$(function() {
		$(".buttonset").buttonset();
		$("#add_to").button({ icons: {primary:'ui-icon-plus'}, text:false });
		$("#change_to").button({ icons: {primary:'ui-icon-arrowreturnthick-1-n'}, text:false });
		$("#none").button({ icons: {primary:'ui-icon-cancel'}, text:false });
		$("#rto").button({ icons: {primary:'ui-icon-arrowthick-1-s'}, text:false });
		$("#rfrom").button({ icons: {primary:'ui-icon-arrowthick-1-n'}, text:false });
	});
	$(function() {	
		$('#none')
			.click(function() {
				$( ".mem_update input" ).button({ disabled: true });
			});
	});
	$(function() {	
		$('#change_to, #add_to')
			.click(function() {
				$( ".mem_update input" ).button({ disabled: false });
			});
	});
	
	$(function() {
		var availableTags = [<?php foreach( $mstatus as $status ) { echo '"'.$status->status.'", '; } ?>];
		$(".status").autocomplete({
			source: availableTags
		});
	});
	
	$(function() {
		var availableTags = [<?php foreach( $families as $fam ) { echo '"'.$fam->family_name.'", '; } ?>];
		$(".family").autocomplete({
			source: availableTags
		});
	});
	
	$(function() {
		var availableTags = [<?php foreach( $members as $mem ) { echo '"'.$mem->lname.', '.$mem->fname.'", '; } ?>];
		$(".member").autocomplete({
			source: availableTags
		});
	});
	$(function() {
		$(".datepicker").datepicker({
		changeYear: true,
		changeMonth: true,
		yearRange: '-100:+1',
		dateFormat: 'mm/dd/yy'
		});
	});
	$(function() {
	 <?php  foreach( $_POST as $k => $v ): 
				$set = form_error($k);
				if( !empty( $set ) ): ?>
				$('#<?php echo $k; ?> input').switchClass('', 'ui-state-error' );
				<?php endif;
			endforeach;
	?>
		return false;
	});
	num_conf = <?php echo set_value('num_conf', 5) ; ?> ;
	$(function() {
		$('#add_conf')
			.button({ icons: {primary:'ui-icon-plus'}, text: false })
			.click(function() {
				x=0;
				while(x<5){
				$('#confirmands').append(
					'<span class="label">Name</span><span id="name'+num_conf+'" class="input"><input type="text" name="name'+num_conf+'" value="" class="member  input  " /></span><span class="input">Passage: </span><span id="passage'+num_conf+'" class="input"><input type="text" name="passage'+num_conf+'" value="" class=" input  " /><input type="hidden" name="num_conf" value="'+num_conf+'" /></span>'
				);
				num_conf = num_conf + 1 ;
				x=x+1;
				}
				$(function() {
					var availableTags = [<?php foreach( $members as $mem ) { echo '"'.$mem->lname.', '.$mem->fname.'", '; } ?>];
					$(".member").autocomplete({
						source: availableTags
					});
				});
				$("input").button();
			});
	});


</script>