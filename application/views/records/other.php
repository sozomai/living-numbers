<div class="  center">
<h2>Add Other Church Records</h2>
</div>

<div class="ui-tabs-panel  ">

<?php if( !$help_menu ){ ?><button id="help_button">Help</button><?php } ?>
<div class="page_help    ">
<small>This help menu my be turned off in "Site" -> "Options" -> "Help Menus"</small>
<div id="page_help"  title="Adding Services Help">

<h3 class="  help-title">Help on Adding Services </h3>


</div>
</div>

</style> <?php 

    $this->load->helper('form');
    $inclass = 'class=" input  "';
    $inclassdate = 'class="datepicker  input  "';
    $inclassmember = 'class="member  input  "';
    $inclassfamily = 'class="family  input  "';
    $inclassstatus = 'class="status  input  "';
	echo form_open_multipart('records/add_record' );
	?>
    
 <?php echo validation_errors(); ?>
 <?php if( isset($upload_error)){ echo $upload_error ; } ?>
                	
<div class="form data">
    
    <?php
	
	echo form_hidden('page', 'other');
	
	
	echo '<span class="label">Church</span>';
    echo '<span id="church" class="input">' . form_input('church', set_value('church'), $inclass ) . '</span>';

	echo '<span class="label">Type</span>';
    echo '<span id="type" class="input">' . form_input('type', set_value('type'), $inclass ) . '</span>';
	
    echo '<span class="label">Date</span>';
    echo '<span id="date" class="input">' . form_input('date', set_value('date', date( 'm/d/Y' )), $inclassdate ) . '</span>';

    echo '<span class="label">Born</span>';
    echo '<span id="born" class="input">' . form_input('born', set_value('born', date( 'm/d/Y' )), $inclassdate ) . '</span>';

    echo '<span class="label">Died</span>';
    echo '<span id="died" class="input">' . form_input('died', set_value('died', date( 'm/d/Y' )), $inclassdate ) . '</span>';

    echo '<span class="label">Name</span>';
    echo '<span id="name" class="input">' . form_input('name', set_value('name' ), $inclassmember ) . '</span>';
	
    echo '<span class="label">Bride</span>';
    echo '<span id="bride" class="input">' . form_input('bride', set_value('bride' ), $inclassmember ) . '</span>';

	echo '<span class="label">Groom</span>';
    echo '<span id="groom" class="input">' . form_input('groom', set_value('groom' ), $inclassmember ) . '</span>';

	echo '<span class="label">Parents</span>';
    echo '<span id="parents" class="input">' . form_input('parents', set_value('parents' ), $inclass ) . '</span>';

	echo '<span class="label">Parents of the Bride</span>';
    echo '<span id="brides_parents" class="input">' . form_input('brides_parents', set_value('brides_parents' ), $inclass ) . '</span>';

	echo '<span class="label">Parents of the Groom</span>';
    echo '<span id="grooms_parents" class="input">' . form_input('grooms_parents', set_value('grooms_parents' ), $inclass ) . '</span>';

	echo '<span class="label">First Witness</span>';
    echo '<span id="witness_one" class="input">' . form_input('witness_one', set_value('witness_one' ), $inclassmember ) . '</span>';

	echo '<span class="label">Second Witness</span>';
    echo '<span id="witness_two" class="input">' . form_input('witness_two', set_value('witness_two' ), $inclassmember ) . '</span>';

	echo '<span class="label">Official</span>';
    echo '<span id="official" class="input">' . form_input('official', set_value('official' ), $inclassmember ) . '</span>';

	echo '<span class="label">Passage</span>';
    echo '<span id="passage" class="input">' . form_input('passage', set_value('passage' ), $inclass ) . '</span>';
	
	echo '<span class="label">Sermon Text/Theme</span>';
    echo '<span id="text" class="input">' . form_input('text', set_value('text' ), $inclass ) . '</span>';

	echo '<span class="label">Written Sermon</span>';
    echo '<span id="sermon" class="input"><input type="file" name="sermon" '.set_value('sermon').' '. $inclass .' /></span>';

	echo '<span class="label">Audio Sermon</span>';
    echo '<span id="asermon" class="input"><input type="file" name="asermon" '.set_value('asermon').' '. $inclass .' /></span>';

	echo '<span class="label">Reason</span>';
    echo '<span id="reason" class="input">' . form_textarea('reason', set_value('reason' ), $inclass ) . '</span>';

	echo '<span class="label">Notes</span>';
    echo '<span id="notes" class="input">' . form_textarea('notes', set_value('notes' ), $inclass ) . '</span>';
		
	echo '<span class="label"></span>';
    echo '<span class="input">' . form_submit('Submit', 'Add Record' ) . form_close() . '</span>';
		
?>		
</div>
</div>
<script type="text/javascript">
	$(function() {
		$(".buttonset").buttonset();
		$("#add_to").button({ icons: {primary:'ui-icon-plus'}, text:false });
		$("#change_to").button({ icons: {primary:'ui-icon-arrowreturnthick-1-n'}, text:false });
		$("#none").button({ icons: {primary:'ui-icon-cancel'}, text:false });
		$("#rto").button({ icons: {primary:'ui-icon-arrowthick-1-s'}, text:false });
		$("#rfrom").button({ icons: {primary:'ui-icon-arrowthick-1-n'}, text:false });
		$( ".mem_update input" ).button({ disabled: true });
	});
	$(function() {	
		$('#none')
			.click(function() {
				$( ".mem_update input" ).button({ disabled: true });
			});
	});
	$(function() {	
		$('#change_to, #add_to')
			.click(function() {
				$( ".mem_update input" ).button({ disabled: false });
			});
	});
	
	$(function() {
		var availableTags = [<?php foreach( $mstatus as $status ) { echo '"'.$status->status.'", '; } ?>];
		$(".status").autocomplete({
			source: availableTags
		});
	});
	
	$(function() {
		var availableTags = [<?php foreach( $families as $fam ) { echo '"'.$fam->family_name.'", '; } ?>];
		$(".family").autocomplete({
			source: availableTags
		});
	});
	
	$(function() {
		var availableTags = [<?php foreach( $members as $mem ) { echo '"'.$mem->lname.', '.$mem->fname.'", '; } ?>];
		$(".member").autocomplete({
			source: availableTags
		});
	});
	$(function() {
		$(".datepicker").datepicker({
		changeYear: true,
		changeMonth: true,
		yearRange: '-100:+1',
		dateFormat: 'mm/dd/yy'
		});
	});
	$(function() {
	 <?php  foreach( $_POST as $k => $v ): 
				$set = form_error($k);
				if( !empty( $set ) ): ?>
				$('#<?php echo $k; ?> input').switchClass('', 'ui-state-error' );
				<?php endif;
			endforeach;
	?>
		return false;
	});

</script>