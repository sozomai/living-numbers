<div class="center">
<h2>Delete Members and Families</h2>
</div>

<div class="data">
<?php

$members_array = $members_obj->result(); ?>

<h2> Members Table </h2> 
<table id="results"> <?php
foreach( $members_array as $member ){ ?>
		<tr><th></th> <?php
	foreach( $member as $k => $v ){ ?>
		<th class=""><?php echo $k; ?></th> <?php
	} ?>
    	</tr> <?php
		break;
} 
foreach( $members_array as $member ){ ?>
		<tr><td> <?php echo anchor('members/delete_delete_member/' . $member->key, 'Delete', 'class="deletebutton"' ); ?></td> <?php
	foreach( $member as $k => $v ){ ?>
		<td><?php echo $v; ?></td> <?php
	} ?>
    	</tr> <?php
} ?>
</table>
  
<?php

$families_array = $families_obj->result(); ?>

<h2> Families Table </h2> 
<table id="results"> <?php
foreach( $families_array as $family ){ ?>
		<tr><th></th> <?php
	foreach( $family as $k => $v ){ ?>
		<th class=""><?php echo $k; ?></th> <?php
	} ?>
    	</tr> <?php
		break;
} 
foreach( $families_array as $family ){ ?>
		<tr><td> <?php echo anchor('members/delete_delete_family/' . $family->key, 'delete', 'class="deletebutton"' ); ?></td> <?php
	foreach( $family as $k => $v ){ ?>
		<td><?php echo $v; ?></td> <?php
	} ?>
    	</tr> <?php
} ?>
</table>

</div>
	
<script type="text/javascript">
	$(function () {
		$(".deletebutton").button({
				icons: {
					primary: 'ui-icon-close'
				},
				text: false
		});
	});
</script>        