<div class="  center">
<h2>Sort Families and Members</h2>
</div>

<div class="data">

<div id="familygroups">
    
	<?php 
	if( isset( $grp_by_fam )): 
	foreach( $grp_by_fam as $family ): ?>
        <ul id="ul_<?php echo $family['key'] ; ?>" class="connectedSortable sortable ">
        	<h3 class=""> <?php echo $family['family_name']; ?> </h3><li id="fam_<?php echo $family['key'] ; ?>" >
				<?php foreach( $family['members'] as $mema ): if( !empty( $mema['key'] )): ?>
                    <li id="mem_<?php echo $mema['key']; ?>" class="sorts" ><?php echo $mema['lname'] . ', ' . $mema['fname'] ; ?></li>
                <?php endif; endforeach; ?>
				<span class="page_br"></span>
        	</li></ul>
    <?php endforeach; endif;
       
  ?> <span class="page_br"></span> <?php
    
   echo anchor( 'members/edit_familygroups', 'Save Changes', 'class="button"' ); ?>
    
</div>

</div>
		

	<script type="text/javascript">
	function setCookie(c_name,value,expiredays)
	{
		var exdate=new Date();
		exdate.setDate(exdate.getDate()+expiredays);
		document.cookie=c_name+ "=" +escape(value)+
		((expiredays==null) ? "" : ";expires="+exdate.toUTCString());
	}
	$(function() {
		$(".sortable").sortable(
		{
			connectWith: '.connectedSortable',
			items: 'li',
			placeholder:  'ui-state-highlight  ui-button placeholder',
			forcePlaceholderSize: true,
			update: function() {
					var result= new Array();
					<?php foreach( $grp_by_fam as $fam ){ echo "result[" . $fam['key'] . "] = $('#ul_" . $fam['key'] . "').sortable('serialize');"; }  ?>
					setCookie( 'familygroups', result );
					}

		}).disableSelection();
	});
	<?php if( $sort_members == 'manual' ): ?>
	$(function () {
		$("#familygroups").sortable({ 
				axis: 'x',
				containment: 'parent',
				helper: 'clone',
				opacity: 0.6,
				placeholder:  'ui-state-highlight  ui-button placeholder',
				forcePlaceholderSize: true,
				forceHelperSize: true,
				items: 'ul',
				update: function() {
						result = $('#familygroups').sortable('serialize');
						setCookie( 'familyorder', result );
						}
		 });
	});
	<?php endif; ?>
	</script>


