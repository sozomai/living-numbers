<div class="center">
<h2>Edit Families</h2>
</div>

<div class="">
<?php 
$this->load->helper('form');
echo validation_errors(); ?>


	<div id="family-form" class="form sidebar">
		<?php
		$this->load->helper('form');
		$inclass = 'class="input  "';
		$inclassstates = 'class="states  input  "';
		$lbclass = array( 'class' => '  ' );
		if( !empty( $fam['key'] )){
			echo '<h3>Edit Family: '.$fam['family_name'].'</h3> '. anchor("members/families", "Add New Family", "class='button'");  
			echo form_open('members/edit_edit_family/' . $fam['key'] );
			echo form_hidden('key', $fam['key'] );
		} else {
			echo '<h3>Add New Family</h3>';  
			echo form_open('members/add_new_family');
		}
		echo '<span class="input-span">' . form_label('Family Name', 'family_name') . '';
		echo '<span id="family_name" class="input">' . form_input('family_name', set_value( 'family_name', $fam['family_name']), $inclass ) . '</span></span>';
		echo '<span class="input-span">' . form_label('Address', 'address') . '';
		echo '<span id="address" class="input">' . form_input('address', set_value( 'address', $fam['address']), $inclass ) . '</span></span>';
		echo '<span class="input-span">' . form_label('City', 'city') . '';
		echo '<span id="city" class="input">' . form_input('city', set_value( 'city', $fam['city']), $inclass ) . '</span></span>';
		echo '<span class="input-span">' . form_label('State', 'state') . '';
		echo '<span id="state" class="input">' . form_input('state', set_value( 'state', $fam['state']), $inclassstates ) . '</span></span>';
		echo '<span class="input-span">' . form_label('Zip', 'zip') . '';
		echo '<span id="zip" class="input">' . form_input('zip', set_value( 'zip', $fam['zip']), $inclass ) . '</span></span>';
		echo '<span class="input-span">' . form_label('Home Phone', 'hphone' ) . '';
		echo '<span id="hphone" class="input">' . form_input('hphone', set_value( 'hphone', $fam['hphone']), $inclass ) . '</span></span>';
		echo '<span class="input-span">' . form_label('Other Phone', 'ophone') . '';
		echo '<span id="ophone" class="input">' . form_input('ophone', set_value( 'ophone', $fam['ophone']), $inclass ) . '</span></span>';
		echo '<span class="input-span"><span class="input"><button id="notes_button">Write Notes</button></span></span>';
		echo '<div id="notes_div" title="Write Notes">' . form_textarea('notes', set_value('notes', $fam['notes']), 'class="input  " id="notes"' ) . '</div>';
		echo '<div id="notes_holder" style="display:none;" ></div>';
		?>
		<span class="label"></span><span class="input"><?php echo form_submit('mysubmit', 'Save', 'class="button"'); ?></span>
		</form>
		
	</div>
	
	<div id="list" class="data">
	<table id="results">

	<?php
	$family_ar = $familylist->result();

	if( !empty( $family_ar )):

		$ct=0;
		foreach( $family_ar as $fam ){
			$fam_array[$ct] = array( 
							 'family_name' => $fam->family_name,
							 'key' => $fam->key,
							 'address' => $fam->address,
							 'city' => $fam->city,
							 'state' => $fam->state,
							 'zip' => $fam->zip,
							 'hphone' => $fam->hphone,
							 'ophone' => $fam->ophone,
							 'notes' => $fam->notes,
							 );
		$ct++;
		}
		?>
		<tr >
		<th></th>
		<th class=""> Family </th>
		<th class=""> Address </th>
		<th class=""> City </th>
		<th class=""> State </th>
		<th class=""> Zip Code </th>
		<th class=""> Home Phone </th>
		<th class=""> Other Phone </th>
		<th class=""> Notes </th>
		</tr>  
		<?php
		foreach( $fam_array as $fam ):
		?>
		<tr>
		<td><?php echo anchor('members/families/' . $fam['key'] , 'Edit', 'class="editbutton" title="Edit"'); ?></td>   
		<td><?php echo $fam['family_name']; ?></td>
		<td><?php echo $fam['address']; ?></td>
		<td><?php echo $fam['city']; ?></td>
		<td><?php echo $fam['state']; ?></td>
		<td><?php echo $fam['zip']; ?></td>
		<td><?php echo $fam['hphone']; ?></td>
		<td><?php echo $fam['ophone']; ?></td>
		<td><?php echo $fam['notes']; ?></td>
		</tr>    
		<?php
		endforeach;
		?>
		<?php
	endif;
	?>
	</table>
	</div>

</div>
<script type="text/javascript">
	$(function() {
		var availableTags = ["AL", "AK", "AS", "AZ", "AR", "CA", "CO", "CT", "DE", "DC", "FL", "GA", "GU", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MH", "MA", "MI", "FM", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "MP", "OH", "OK", "OR", "PW", "PA", "PR", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "VI", "WA", "WV", "WI", "WY "];
		$(".states").autocomplete({
			source: availableTags
		});
	});
	$(function() {
		var notes  = $( "#notes" ),
			allFields = $( [] ).add( notes );		
		$('#notes_div').dialog({
			autoOpen: false,
				width: 610,
				buttons: {
					"Ok": function() { 
							$( "#notes_holder" ).append( " <input type='hidden' name='notes' value='" + notes.val() + "'> " ); 
							$( this ).dialog( "close" );
						 
					}, 
					"Cancel": function() { 
							$( this ).dialog( "close" );
						 
					}
				}
		});
		$('#notes_button').click(function(){
			$('#notes_div').dialog('open');
			return false;
		});
	});
	$(function () {
		$(".editbutton").button({
				icons: {
					primary: 'ui-icon-pencil'
				},
				text: false,
		});
	});
	$(function() {
	 <?php  foreach( $_POST as $k => $v ): 
				$set = form_error($k);
				if( !empty( $set ) ): ?>
				$('#<?php echo $k; ?> input').switchClass('', 'ui-state-error' );
				<?php endif;
			endforeach;
	?>
			return false;
	});
	

</script>
