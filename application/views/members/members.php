<div class="  center">
<h2>Edit Members</h2>
</div>

<div class="  ">
<?php 
$member_ar = $memberlist->result();
$familytomember_ar = $familytomember->result();
$family_ar = $familylist->result();
$mem =  $memberinfo ;
    $this->load->helper('form');
echo validation_errors(); 
?>

	<div id="form" class="form sidebar" >
		<?php


			if( !empty( $familytomember_ar )){
				foreach( $familytomember_ar as $fam ){
					if( $fam->member == $mem['key'] ){ $fnum = $fam->family ; break; } else { $fnum = 0; }
				}
				foreach( $family_ar as $fam ){ 
					if( $fam->key == $fnum ){ $fgroup = $fam->family_name ; break; } else { $fgroup = ''; }
				}
			} else { $fgroup = ''; }
		$inclass = 'class=" input "';
		$inclassdate = 'class="datepicker  input "';
		$inclassfam = 'class="fam  input "';
		$inclassstatus = 'class="status  input "';
		$lbclass = array( 'class' => ' ' );
		
		if( !empty( $mem['key'] )){
			echo '<h3>Edit Member: '.$mem['lname'].', '.$mem['fname'].'</h3> '.anchor("members/member", "Add New Member", "class='button'").'';  
			echo form_open('members/edit_edit_member/'  . $mem['key'] );
			echo form_hidden('key', $mem['key'] );
		} else {
			echo '<h3>Add New Member</h3>';  
			echo form_open('members/add_new_member');
		}
		
		
		echo '<span class="input-span">' . form_label('Last Name', 'lname') ;
		echo '<span id="lname" class="input">' . form_input('lname', set_value( 'lname', $mem['lname']), $inclass ) . '</span></span>';
		echo '<span class="input-span">' . form_label('First Name', 'fname') ;
		echo '<span id="fname" class="input">' . form_input('fname', set_value( 'fname', $mem['fname']), $inclass ) . '</span></span>';
		echo '<span class="input-span">' . form_label('Family Group', 'fgroup') ;
		echo '<span id="fgroup" class="input">' . form_input('fgroup', set_value( 'fgroup', $fgroup), $inclassfam ) . '</span></span>';
		echo '<span class="input-span">' . form_label('Birth', 'dob') ;
		echo '<span id="dob" class="input">' . form_input('dob', set_value( 'dob', $mem['dob']), $inclassdate ) . '</span></span>';
		echo '<span class="input-span">' . form_label('Occupation', 'occupation') ;
		echo '<span id="occupation" class="input">' . form_input('occupation', set_value( 'occupation', $mem['occupation']), $inclass ) . '</span></span>';
		echo '<span class="input-span">' . form_label('Cell Phone', 'cphone') ;
		echo '<span id="cphone" class="input">' . form_input('cphone', set_value( 'cphone', $mem['cphone']), $inclass ) . '</span></span>';
		echo '<span class="input-span">' . form_label('Work Phone', 'wphone') ;
		echo '<span id="wphone" class="input">' . form_input('wphone', set_value( 'wphone', $mem['wphone']), $inclass ) . '</span></span>';
		echo '<span class="input-span">' . form_label('Email', 'email' ) ;
		echo '<span id="email" class="input">' . form_input('email', set_value( 'email', $mem['email']), $inclass ) . '</span></span>';
		echo '<span class="input-span">' . form_label('Baptism', 'baptism') ;
		echo '<span id="baptism" class="input">' . form_input('baptism', set_value( 'baptism', $mem['baptism']), $inclassdate ) . '</span></span>';
		echo '<span class="input-span">' . form_label('Confirmation', 'confirmation') ;
		echo '<span id="confirmation" class="input">' . form_input('confirmation', set_value( 'confirmation', $mem['confirmation']), $inclassdate ) . '</span></span>';
		echo '<span class="input-span">' . form_label('Anniversary', 'anniversary') ;
		echo '<span id="anniversary" class="input">' . form_input('anniversary', set_value( 'anniversary', $mem['anniversary']), $inclassdate ) . '</span></span>';
		echo '<span class="input-span">' . form_label('Death', 'death') ;
		echo '<span id="death" class="input">' . form_input('death', set_value( 'death', $mem['death']), $inclassdate ) . '</span></span>';
		echo '<span class="input-span">' . form_label('Status', 'status') ;
		echo '<span id="status" class="input">' . form_input('status', set_value( 'status', $mem['status']), $inclassstatus ) . '</span></span>';
		echo '<span class="input-span"><span class="input"><button id="notes_button">Write Notes</button></span></span>';
		echo '<div id="notes_div" title="Write Notes">' . form_textarea('notes', set_value('notes', $mem['notes']), 'class=" input " id="notes"' ) . '</div>';
		echo '<div id="notes_holder" style="display:none;" ></div>';
	?>
		<span class="label"></span><span class="input"><?php echo form_submit('mysubmit', 'Save', 'class="button"'); ?></span>
		</form>
	</div>
	
	<div id="list" class="data">
	<table id="results">

	<?php


	if( !empty( $member_ar )):

		$ct=0;
		foreach( $member_ar as $meb ){
			foreach( $familytomember_ar as $fam ){
				if( $fam->member == $meb->key ){ $fnum = $fam->family ; }
				if( !empty($fnum )){ break; }
			}
			if( isset( $fnum ) ) {
				foreach( $family_ar as $fam ){
					if( $fam->key == $fnum ){ $fname = $fam->family_name ; }
					if( !empty( $fname )){ break; }
				} 
			}
			if( empty($fname)){ $fname = ''; }
			$meb_array[$meb->lname . $meb->fname] = array( 
							 'lname' => $meb->lname,
							 'fname' => $meb->fname,
							 'fgroup' => $fname,
							 'dob' => $meb->dob,
							 'occupation' => $meb->occupation,
							 'cphone' => $meb->cphone,
							 'wphone' => $meb->wphone,
							 'email' => $meb->email,
							 'baptism' => $meb->baptism,
							 'confirmation' => $meb->confirmation,
							 'anniversary' => $meb->anniversary,
							 'death' => $meb->death,
							 'status' => $meb->status,
							 'notes' => $meb->notes,
							 'key' => $meb->key,
							 );
			 $fnum = NULL;  $fname = NULL;
		$ct++;
		}
		?>
		<tr >
		<th></th>
		<th class="  "> Last Name </th>
		<th class="  "> First Name </th>
		<th class="  "> Family Group </th>    
		<th class="  "> DOB </th>
		<th class="  "> Occupation </th>
		<th class="  "> Cell Phone </th>
		<th class="  "> Work Phone </th>
		<th class="  "> Email </th>
		<th class="  "> Baptism </th>
		<th class="  "> Confirmation </th>
		<th class="  "> Anniversary </th>
		<th class="  "> Death </th>
		<th class="  "> Status </th>
		<th class="  "> Notes </th>
		</tr>  
		<?php
		asort( $meb_array );
		foreach( $meb_array as $meb ):
		?>
		<tr>
		<td><?php echo anchor( 'members/member/' . $meb['key'] , 'Edit', 'class="editbutton" title="edit"'); ?></td>   
		<td><?php echo $meb['lname']; ?></td>
		<td><?php echo $meb['fname']; ?></td>
		<td><?php echo $meb['fgroup']; ?></td>
		<td><?php echo $meb['dob']; ?></td>
		<td><?php echo $meb['occupation']; ?></td>
		<td><?php echo $meb['cphone']; ?></td>
		<td><?php echo $meb['wphone']; ?></td>
		<td><?php echo $meb['email']; ?></td>
		<td><?php echo $meb['baptism']; ?></td>
		<td><?php echo $meb['confirmation']; ?></td>
		<td><?php echo $meb['anniversary']; ?></td>
		<td><?php echo $meb['death']; ?></td>
		<td><?php echo $meb['status']; ?></td>
		<td><?php echo $meb['notes']; ?></td>
		</tr>    
		<?php
		endforeach;
		?>
		<?php
	endif;
	?>
	</table>
	</div>

</div>
<?php $status_ar = $statuslist->result(); ?>
<script type="text/javascript">
	$(function() {
		$(".datepicker").datepicker({
		changeYear: true,
		changeMonth: true,
		yearRange: '-100:+1',
		dateFormat: 'mm/dd/yy'
		});
	});
	$(function() {
		var availableTags = [<?php foreach( $family_ar as $fam ){ echo '"' . $fam->family_name . '", '; } ?>];
		$(".fam").autocomplete({
			source: availableTags,
			minLength: 0
		});
	});
		$(function() {
		var availableStatus = [<?php foreach( $status_ar as $status ){ echo '"' . $status->status . '", '; } ?>];
		$(".status").autocomplete({
			source: availableStatus,
			minLength: 0
		});
	});
	$(function() {
		var notes  = $( "#notes" ),
			allFields = $( [] ).add( notes );		
		$('#notes_div').dialog({
			autoOpen: false,
				width: 610,
				buttons: {
					"Ok": function() { 
							$( "#notes_holder" ).append( " <input type='hidden' name='notes' value='" + notes.val() + "'> " ); 
							$( this ).dialog( "close" );
						 
					}, 
					"Cancel": function() { 
							$( this ).dialog( "close" );
						 
					}
				}
		});
		$('#notes_button').click(function(){
			$('#notes_div').dialog('open');
			return false;
		});
	});
	$(function () {
		$(".editbutton").button({
				icons: {
					primary: 'ui-icon-pencil'
				},
				text: false
		});
	});
	$(function() {
	 <?php  foreach( $_POST as $k => $v ): 
				$set = form_error($k);
				if( !empty( $set ) ): ?>
				$('#<?php echo $k; ?> input').switchClass('ui-state-default', 'ui-state-error' );
				<?php endif;
			endforeach;
	?>
			return false;
	});
</script>
