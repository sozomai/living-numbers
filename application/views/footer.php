</div><!-- End content div   -->
<div id="dialog">
<?php echo $this->session->flashdata('notice');  ?>
<?php if( isset($flashdata) ){ echo $flashdata; }?>
</div>
<div id="footer"></div>
</div><!-- End body div   -->
</body>
<script type="text/javascript">

	$(function () {
		$("#dialog").dialog({ 
		autoOpen: <?php if( $this->session->flashdata('notice') || isset($flashdata) ){ echo 'true'; } else { echo 'false'; } ?>, 
		buttons: { "Ok": function() { 
			$(this).dialog("close"); 
			} 
		} 
		});
	});
	$(function () {
		$(".visited").button({
			icons: {primary:'ui-icon-check'},
			text: false,
		});
	});
	
    $(function() {
        $("#page_help").dialog({
            height: 400,
            autoOpen: false,
        });
        $("#help_button").button({
	        icons:{
	            primary: 'ui-icon-help',
	            },
            text:false
        });
        $('#help_button').click(function() {
            $('#page_help').dialog('open');
            return false;
        });
        
    });
</script>
</html>