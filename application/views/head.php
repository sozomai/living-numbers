<?php
$hed = $header->result();
foreach( $hed as $d ){
	$harry[$d->name] =  $d->value;
}
$this->load->helper('array');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> <?php echo element('site_title', $harry); ?> </title>
<link type="text/css" href="<?php echo base_url();  ?>files/default.css" rel="Stylesheet" />	
<link type="text/css" href="<?php echo base_url();  ?>jquery-ui/css/<?php echo element('site_theme', $harry); ?>" rel="Stylesheet" />	
<script type="text/javascript" src="<?php echo base_url();  ?>jquery-ui/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url();  ?>jquery-ui/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();  ?>files/site.js"></script>

<?php if( element('help_menu', $harry) ): ?>
<style title="text/css" >

#body {
	height:97%;
	overflow:auto;
	position:fixed;
	width:69%;
	}
.body_narrow {
	height:auto!important;
	overflow:inherit!important;
	position:relative!important;
	width:auto!important;
	}
.page_help {
	display:block!important;
	float:right!important;
	height:90%!important;
	overflow:auto!important;
	padding:20px!important;
	position:fixed!important;
	right:7px!important;
	top:7px!important;
	width:26%!important;
	}
.page_help_on {
	display: none;
	float:right;
	background:#fff;
	}
	
</style>
<?php endif; ?>
</head>

<body>
<div id="body">

<div id="header" class="">
    <img id="head_img" src="<?php echo base_url() ?>files/img/IMG_9573.JPG" title="flower" >
    <h1><a href="<?php echo base_url() ?>"><?php echo element('site_name', $harry); ?></a>: <small><?php echo element('site_plug', $harry); ?></small></h1>
</div>

<div id="menu" ><ul>
        <?php
        
        $this->load->helper('url');
        $this->load->helper('inflector');
        
        $qlinks = $query_links->result() ; 
        $count = 0;
        foreach( $qlinks as $link ){
            
            $links[$link->cat][$count] = array(
                        'name' => $link->name,
                        'desc' => $link->desc,
                        'url' => $link->url,
                        'cat' => $link->cat,
                        );
            $count++;
            }
        if( empty( $links ) ){ $links[''][0] = array(  'name' => 'Please install this program by clicking here.', 'desc' => 'Installer', 'url' => 'main/installer' ); }     
        // anchor()
        $cat_count = 1;
        foreach ( $links as $category => $alinks ) {
            if( $count > 0 ){ $col_width = 100/($count - 1); } else { $col_width = 100; }
            ?> 
            <li class="menu-item">
            <a href="<?php echo site_url(); ?>" ><?php echo $category; ?></a>
            <ul class="children">
            <?php
            $ncount = 0;
            foreach ( $alinks as $link ){ ?>
            
            <li class="menu-item">
            <a href="<?php echo site_url(underscore($category).'/'.$link['url']); ?>" rel="<?php echo $link['desc']; ?>" > <?php echo $link['name'] ; ?></a></li>
            
            <?php
            } 
            ?></ul></li><?php
        $cat_count++;
        } ?>
</ul></div>

<div id="content">