<?php
/*	NOTES

To Do yet

	Add lines for visitors so that it can be indicated whether they attended Service, Lord's Supper, Bible Class etc. Rather than clicking on LS or BC, allow numbres to be entered so that if it is a family that attended they do not need to all be listed seperately, but how many were in each service can still be calculated.
	
	If visitors attended Lord's Supper include them in year end report so that the pastor can contact their pastor.


*/

class Main extends CI_Controller {
		
	function main()
	{
		parent::__construct();
				
		$this->load->library('session');
		$this->load->helper('cookie');
		$this->load->helper('url');
		$this->load->model('Forms');
		$this->load->model('Site');
		$this->load->database();
		include( 'var.php' );
		
		// check to see if tables are installed and up to date
		
		if( !$this->db->table_exists('site_options')) {
				redirect('install');
		}
		
		$this->db->where('name', 'version');
		$check_table = $this->db->get('site_options');
		$vn = $check_table->result();
		
		if(empty($vn)){
			redirect('install/install');
		}
			
		if($vn[0]->value < $version_num ) {
			redirect('install/upgrade');
		}
		
		$data = $this->Site->site_options();		
		$data['query_links'] = $this->db->get('links');
		$data['header'] = $this->db->get('site_options');
		
		date_default_timezone_set($data['timezone']);

		$this->load->view('head', $data);		
		
	}
		
	
	function index ()
	{	
			
	
	
	$data['cur_date'] = time();	
	$data['neg_3_mo'] = time()-7776000;
	$data['neg_1_yr'] = time()-31556926;

	
	$data['members'] = $this->Site->return_member_attendance();
	
	$this->db->order_by('date');
	$visitorso = $this->db->get('service_visitors');
	$data['visitors'] = $visitorso->result_array();
	
	$data['mstatus'] = $this->db->get('member_status');
	
	$this->db->order_by('date', 'desc');
	$this->db->limit(1);
	$this->db->select('type, date, name, member_attendance, visitor_attendance, total_attendance, ss_attendance, bc_attendance, communion_attendance');
	$last_serviceo = $this->db->get('service');
	$data['last_service'] = $last_serviceo->row_array();
	
	$this->db->where( 'type', 'Worship Service');
	$lyear_serviceso = $this->db->get( 'service' );
	$lyear_services = $lyear_serviceso->result_array();
	
		$ma = 0; 
		$va = 0; 
		$ta = 0; 
		$sa = 0; 
		$ba = 0; 
		$ca = 0; 
		$tca = 0;
		$ts=0;
		$tm=0;
		$tcm=0;
		$ywed=0;
		$ybap=0; 
		$ypm=0; 
		$yconf=0; 
		$yfun=0; 
		$ynm=0;
	
	foreach( $lyear_services as $serv ):
		if( strtotime($serv['date']) > $data['neg_1_yr'] ):
			$ma = $ma + $serv['member_attendance']; 
			$va = $va + $serv['visitor_attendance']; 
			$ta = $ta + $serv['total_attendance']; 
			$sa = $sa + $serv['ss_attendance']; 
			$ba = $ba + $serv['bc_attendance']; 
			$ca = $ca + $serv['communion_attendance'];
			if( $serv['communion_attendance'] > 0 ){ $tca++; }
			$ts++;
		endif;
	endforeach;
	
	$ct=0;
	$names_array = array();
	$tuv=0;
	if(!empty($data['visitors'])): foreach( $data['visitors'] as $vis ):
		$x=true;
		if( strtotime($vis['date']) > $data['neg_1_yr'] ):
			foreach( $names_array as $name ):
				if( $name == $vis['names'] ){ $x == false; break; }
			endforeach;
			if( $x ){ $tuv++; }
			$names_array[$ct]=$vis['names'];
		endif;
		$ct++;
	endforeach; endif;
	
	if(!empty($data['members']['by_name']) ):foreach( $data['members']['by_name'] as $mem ):
		if( $mem['member']){ $tm++; }
		if( $mem['communicate']){ $tcm++; }
	endforeach; endif;	
	
	$recordso = $this->db->get('records');
	$records = $recordso->result();
	foreach( $records as $rec ):
		if( strtotime($rec->date) > $data['neg_1_yr'] ):
			if( $rec->type == 'wedding' ){ $ywed++; }
			elseif( $rec->type == 'baptism' ){ $ybap++; $ypm++; }
			elseif( $rec->type == 'confirmation' ){ $yconf++;}
			elseif( $rec->type == 'funeral' ){ $yfun++; $ynm++;}
			elseif( $rec->type == 'transfer' ){ 
				if( $rec->direction == 'to' ){ $ypm++;}
				elseif( $rec->direction == 'from' ){ $ynm++; }
			}
			elseif( $rec->type == 'termination' ){ $ynm++; }
			elseif( $rec->type == 'adult_conf' ){ $yconf++; $ynm++; }
		endif;
	endforeach;
			
	$data['year_stats'] = array();
	if( $ts > 0 ):
	$data['year_stats']['members'] = $tm;
	$data['year_stats']['communicate_members'] = $tcm;
	$data['year_stats']['total_services'] = $ts;
	$data['year_stats']['avg_att'] = $ta/$ts;
	$data['year_stats']['avg_member_att'] = $ma/$ts;
	$data['year_stats']['communion_services'] = $tca;
	if( $tca > 0 ){ $data['year_stats']['avg_communion_attendance'] = $ca/$tca; }
	$data['year_stats']['number_of_visitors'] = $tuv;
	$data['year_stats']['new_members'] = $ypm; 
	$data['year_stats']['members_lost'] = $ynm;
	$data['year_stats']['weddings'] = $ywed;
	$data['year_stats']['baptisms'] = $ybap; 
	$data['year_stats']['members_confirmed'] = $yconf; 
	$data['year_stats']['funerals'] = $yfun; 
	
	endif;
	
	$this->db->order_by('order');
	$fpso = $this->db->get('frontpage_stats');
	$data['functions'] = $fpso->result_array();
	
	$this->load->view('index', $data);
	$this->load->view('footer');
	
	}
	
	

	
	function visited_visitor () {
		
		$visitorso = $this->db->get( 'service_visitors' );
		$visitors = $visitorso->result();
		if(!empty($visitors)): foreach( $visitors as $visitor ):
			$x = $this->input->post('visited'.$visitor->key);
			if($x):
				$data = array( 'visited' => 0 );
				$this->db->where( 'key', $visitor->key );
				$this->db->update( 'service_visitors', $data );
			endif;
		endforeach; endif;
		
		redirect();
		
	}	
	
	function current_version () {
	
		include( 'var.php' );
		echo $version_num;
		die();
	}	
	
	
}