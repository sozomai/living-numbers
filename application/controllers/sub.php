<?php

class Sub extends CI_Controller {
	
	
	function sub()
	{
		parent::__construct();
		
		$this->load->library('session');
		
		$this->load->helper('cookie');
		
		$this->load->helper('url');
		
		$this->load->model('Forms');

		$this->load->model('Site');
		
		$this->load->database();
		$data = $this->Site->site_options();

		date_default_timezone_set($data['timezone']);
		
	}



	function edit_this_service () {
			
	if( $this->uri->segment(3) ){

		
		
		$serv_num = $this->uri->segment(3);
		
		$chk = $this->db->get_where('service', array('key' => $serv_num) );
		$chk_results = $chk->result();
		
		if( empty( $chk_results) ){  
				echo ' NO SERVICE SELECTED! PLESE SELECT A SERVICE TO EDIT FROM THE "VIEW SERVICE" PAGE!';
				exit;
		}
		
		$data = $this->Site->return_family_groups ();
		
		$this->db->where('key', $serv_num );
		$data['this_service_obj'] = $this->db->get('service');
					
		$data['service_type'] = $this->db->get('service_type');
		$data['offering_type'] = $this->db->get('offering_type');
		$this->db->where( 'service', $serv_num );
		$data['service_visitors'] = $this->db->get('service_visitors');
		$this->db->order_by( 'member' );
		$this->db->where( 'service', $serv_num );
		$data['offerings'] = $this->db->get('offerings');
		$this->db->where( 'service_key', $serv_num );
		$data['mem_att'] = $this->db->get( 'membertoservice' );
		
		$this->db->select( 'names' );
		$visitor_names_o = $this->db->get('service_visitors');
		$data['visitor_names'] = $visitor_names_o->result();	
		
		
		$this->load->view('services/edit_this_service', $data);
		
	} else {
	
		echo ' NO SERVICE SELECTED! PLESE SELECT A SERVICE TO EDIT FROM THE "VIEW SERVICE" PAGE!';
	}
	
	}
	
	
	
	function edit_edit_this_service () {
	
	
		$service_num = $this->input->post( 'key' );
		
		$data = $this->Site->site_options ();
		
		date_default_timezone_set( $data['timezone'] );

		$mem_data = $this->db->get('members');
		$members_ar = $mem_data->result() ;

		
		$this->load->helper('form');
		
		$this->load->library('form_validation');
		
		// set validation rules
		$this->form_validation->set_rules('type', 'Type', 'trim|required|xss_clean');	
		$this->form_validation->set_rules('date', 'Date', 'required|xss_clean');	
		$this->form_validation->set_rules('name', 'Name', 'xss_clean');	
		$this->form_validation->set_rules('offering', 'Totals: Offerings', 'numeric');	
		$this->form_validation->set_rules('total_attendance', 'Totals: Attendance -> Service ', 'is_natural');	
		$this->form_validation->set_rules('communion_attendance', 'Totals: Attendance -> Lord\'s Supper ', 'is_natural');	
		$this->form_validation->set_rules('bc_attendance', 'Totals: Attendance -> Bible Class ', 'is_natural');	
		$this->form_validation->set_rules('ss_attendance', 'Totals: Attendance -> Sunday School ', 'is_natural');	
		$this->form_validation->set_rules('member_attendance', 'Totals: Members', 'is_natural');	
		$this->form_validation->set_rules('visitor_attendance', 'Totals: Visitors', 'is_natural');	
		foreach( $members_ar as $mem ):
			$this->form_validation->set_rules($mem->key . '_service', 'Member: '.$mem->lname.', '.$mem->fname.' Attendance WS', 'integer');	
			$this->form_validation->set_rules($mem->key . '_communion', 'Member: '.$mem->lname.', '.$mem->fname.' Attendance LS', 'integer');	
			$this->form_validation->set_rules($mem->key . '_bc', 'Member: '.$mem->lname.', '.$mem->fname.' Attendance BC', 'integer');	
			$this->form_validation->set_rules($mem->key . '_ss', 'Member: '.$mem->lname.', '.$mem->fname.' Attendance SS', 'integer');	
			$this->form_validation->set_rules($mem->key . '_gen_offering', 'Member: '.$mem->lname.', '.$mem->fname.' Gen Offering', 'numeric');	
			if( $this->input->post( $mem->key . '_num_sp_offerings' ) ):
				$this->form_validation->set_rules($mem->key . '_num_sp_offerings', 'Member: '.$mem->lname.', '.$mem->fname.' Number of SP Offerings ', 'numeric');	
				$x = $this->input->post( $mem->key . '_num_sp_offerings' );
				while( $x > 0 ):
					$this->form_validation->set_rules($mem->key . '_sp_offering_' . $x . '_am', 'Member: '.$mem->lname.', '.$mem->fname.' Sp Offering '.$x.' Amount ', 'numeric');	
					$this->form_validation->set_rules($mem->key . '_sp_offering_' . $x . '_ty', 'Member: '.$mem->lname.', '.$mem->fname.' Sp Offering '.$x.' Type ', 'xss_clean');	
					$x--;
				endwhile;
			endif;
		endforeach;
		$this->form_validation->set_rules('other_gen_offering', 'Member: Other Gen Offering', 'numeric');	
		if( $this->input->post( 'other_num_sp_offerings' ) ):
			$this->form_validation->set_rules( 'other_num_sp_offerings', 'Member: Other Number of SP Offerings ', 'integer');	
			$x = $this->input->post( 'other_num_sp_offerings' );
			while( $x > 0 ):
				$this->form_validation->set_rules('other_sp_offering_' . $x . '_am', 'Member: Other Sp '.$x.' Offering Amount', 'numeric');	
				$this->form_validation->set_rules('other_sp_offering_' . $x . '_ty', 'Member: Other Sp '.$x.' Offering Type', 'xss_clean');	
				$x--;
			endwhile;
		endif;
		$this->form_validation->set_rules('visitor_form_num' , 'Number of Visitor Forms', 'is_natural');	
		$num_of_visitors_offering_specified = $this->input->post('visitor_form_num');
		$nvos = 0;
		if( $nvos > $num_of_visitors_offering_specified ){ $num_of_visitors_offering_specified = 3; }
		while( $nvos <= $num_of_visitors_offering_specified ): 
			$this->form_validation->set_rules('visitors_names_' . $nvos , 'Visitors: Visitor '.$nvos.' Names', 'xss_clean');	
			$this->form_validation->set_rules('visitor_service_' . $nvos , 'Visitors: Visitor '.$nvos.' WS', 'is_natural');	
			$this->form_validation->set_rules('visitor_communion_' . $nvos , 'Visitors: Visitor '.$nvos.' LS', 'is_natural');	
			$this->form_validation->set_rules('visitor_bc_' . $nvos , 'Visitors: Visitor '.$nvos.' BC', 'is_natural');	
			$this->form_validation->set_rules('visitor_ss_' . $nvos , 'Visitors: Visitor '.$nvos.' SS', 'is_natural');	
			$this->form_validation->set_rules('visitors_home_church_' . $nvos , 'Visitors: Visitor '.$nvos.' Home Church', 'xss_clean');	
			$this->form_validation->set_rules('visitors_contact_info_' . $nvos , 'Visitors: Visitor '.$nvos.' Contact Info', 'xss_clean');	
			$nvos++;
		endwhile;
		$this->form_validation->set_rules('notes', 'Notes', 'xss_clean');	
				
		if ( $this->form_validation->run() == FALSE ){
		
			$dataf['flashdata'] = validation_errors();
			
			$data = $this->Site->site_options ();
		
			$this->db->select( 'key, type, date, name' );
			$this->db->order_by("date", "desc"); 
			$this->db->order_by("type", "desc"); 
			$this->db->order_by("name", "desc"); 
			$data['services'] = $this->db->get('service');
			
			if( $data['services']->num_rows() > 0 ):
			
			$service_ar = $data['services']->result();
			foreach( $service_ar as $service ):
				$service_date = explode( '-', $service->date );
				$services[$service_date[0]][$service_date[1]][$service_date[2]][$service->key] = array( 
							'key' => $service->key,
							'date' => $service->date,
							'type' => $service->type,
							'name' => $service->name, 
							);
			endforeach; endif;
			

			$serv_num = $this->input->post('key');
		
			$datap = $this->Site->return_family_groups ();
			
			$this->db->where('key', $serv_num );
			$datap['this_service_obj'] = $this->db->get('service');
						
			$datap['service_type'] = $this->db->get('service_type');
			$datap['offering_type'] = $this->db->get('offering_type');
			$this->db->where( 'service', $serv_num );
			$datap['service_visitors'] = $this->db->get('service_visitors');
			$this->db->order_by( 'member' );
			$this->db->where( 'service', $serv_num );
			$datap['offerings'] = $this->db->get('offerings');
			$this->db->where( 'service_key', $serv_num );
			$datap['mem_att'] = $this->db->get( 'membertoservice' );
			
			$data['edit_data'] = $this->load->view('edit_this_service', $datap, true);
			
			$data['service_date_array']=$services;
			
			$datah['query_links'] = $this->db->get('links');
			$datah['header'] = $this->db->get('site_options');
			$this->load->view('head', $datah);		
			$this->load->view('services/view_service', $data );
			$this->load->view('footer', $dataf );	
			
		} else {
		
			$mem_status_data = $this->db->get('member_status');
			$mem_status_ar = $mem_status_data->result();
			
			if( $this->input->post('offering') ) {
				$total_offering = $this->input->post('offering') ;
			} else {
				$total_offering = 0;
				foreach( $members_ar as $mem ){
					$total_offering = $total_offering + $this->input->post( $mem->key . '_gen_offering' );
					if( $this->input->post( $mem->key . '_num_sp_offerings' ) ):
						$x = $this->input->post( $mem->key . '_num_sp_offerings' );
						while( $x > 0 ):
							$total_offering = $total_offering + $this->input->post( $mem->key . '_sp_offering_' . $x . '_am' );
							$x--;
						endwhile;
					endif;
				}
				
				$total_offering = $total_offering + $this->input->post( 'other_gen_offering' );
				if( $this->input->post( 'other_num_sp_offerings' ) ):
					$x = $this->input->post( 'other_num_sp_offerings' );
					while( $x > 0 ):
						$total_offering = $total_offering + $this->input->post( 'other_sp_offering_' . $x . '_am' );
						$x--;
					endwhile;
				endif;
			}
			
				$attendance['members'] = 0;
				$attendance['visitors'] = 0; 				
				$attendance['communion'] = 0;
				$attendance['bc'] = 0;
				$attendance['ss'] = 0;
				$attendance[''] = 0; 				
			
			foreach( $members_ar as $mem ){
				if( $this->input->post( $mem->key . '_service' ) ){
					if( $mem->status ){ 
						$mem_obj = $this->db->get_where( 'member_status' , array( 'status' => $mem->status ) );
						$mem_arr = $mem_obj->result();
						if( $mem_arr[0]->member ){ $attendance['members'] = $attendance['members'] + 1 ; }
						else { $attendance['visitors'] = $attendance['visitors'] + 1 ; }
					} else { 
					 	$attendance['visitors'] = $attendance['visitors'] + 1 ;
					}
				}
					
				if( $this->input->post( $mem->key . '_communion' ) ){
					$attendance['communion'] = $attendance['communion'] + 1 ;
				}
				if( $this->input->post( $mem->key . '_bc' ) ){
					$attendance['bc'] = $attendance['bc'] + 1 ;
				}
				if( $this->input->post( $mem->key . '_ss' ) ){
					$attendance['ss'] = $attendance['ss'] + 1 ;
				}
			}
			
			$num_of_visitors_count = $this->input->post('visitor_form_num');

			while( $num_of_visitors_count >= 0 ){
				if( $this->input->post( 'visitor_service_' . $num_of_visitors_count ) ){
				 	$attendance['visitors'] = $attendance['visitors'] + $num_of_visitors_count ;
				}	
				if( $this->input->post( 'visitor_communion_' . $num_of_visitors_count  ) ){
					$attendance['communion'] = $attendance['communion'] + $num_of_visitors_count ;
				}
				if( $this->input->post( 'visitor_bc_' . $num_of_visitors_count  ) ){
					$attendance['bc'] = $attendance['bc'] + $num_of_visitors_count ;
				}
				if( $this->input->post( 'visitor_ss_' . $num_of_visitors_count  ) ){
					$attendance['ss'] = $attendance['ss'] + $num_of_visitors_count ;
				}
				$num_of_visitors_count--;
			}
			
			$attendance['total'] = $attendance['visitors'] + $attendance['members'];
			
			$totals_array = array( 
							'total_attendance' => 'total', 
							'communion_attendance' => 'communion', 
							'bc_attendance' => 'bc', 
							'ss_attendance' => 'ss', 
							'visitor_attendance' => 'visitors', 
							'member_attendance' => 'members',
							);

			if( $this->input->post( 'offering' ) ) { 
				$which_totals = 'offering' . ':' ;
				$total_offering = $this->input->post( 'offering' );
			} else {
				$which_totals = '' ;
			}					
							
			foreach( $totals_array as $k => $v ){
				if( $this->input->post( $k ) ) { 
					$attendance[$v] = $this->input->post( $k );
					$which_totals .= $k . ':' ;
				}
			}
			
			// RECORD GENERAL SERVICE INFORMATION
		
			//create proper date input
			$date = date( 'Y-m-d', strtotime( $this->input->post('date') ) );

			$dataa = array (
							'type' => $this->input->post('type'),
							'date' => $date,
							'name' => $this->input->post('name'),
							'offering' => $total_offering,
							'attendance' => $which_totals,
							'member_attendance' => $attendance['members'],
							'visitor_attendance' => $attendance['visitors'],
							'total_attendance' => $attendance['total'],
							'communion_attendance' => $attendance['communion'],
							'bc_attendance' => $attendance['bc'],
							'ss_attendance' => $attendance['ss'],
							'notes' => $this->input->post('notes'),
							);
			
			$this->db->where( 'key', $service_num );
			$this->db->update( 'service', $dataa ); 
			
			
			// RECORD INDIVIDUL MEMBER ATTENDANCE
			$this->db->where( 'service_key', $service_num );
			$this->db->delete(  'membertoservice' );
			foreach( $members_ar as $mem ){
				if( $this->input->post( $mem->key . '_service') || $this->input->post( $mem->key . '_bc') || $this->input->post( $mem->key . '_ss') || $this->input->post( $mem->key . '_communion') ){
					$data = array ( 
									'service_key' => $service_num,
									'member' => $mem->key,
									'date' => $this->input->post( 'date' ),
									'service' => $this->input->post( $mem->key . '_service'),
									'communion' => $this->input->post( $mem->key . '_communion'),
									'bibleclass' => $this->input->post( $mem->key . '_bc'),
									'sundayschool' => $this->input->post( $mem->key . '_ss'),
									);
									
					$this->db->insert( 'membertoservice', $data );
				} 
			}
						
			
			// RECORD INDIVIDUAL MEMBER OFFERING INFORMATION
				$this->db->where( 'service', $service_num );
				$this->db->delete( 'offerings' );

			foreach( $members_ar as $mem ){
						
				if( $this->input->post( $mem->key . '_gen_offering') ){
					$data = array(
									'service' => $service_num,
									'date' => $date,
									'member' => $mem->key,
									'fund' => 'Gen',
									'amount' => $this->input->post( $mem->key . '_gen_offering'),
									);
					$this->db->insert( 'offerings', $data );
					
				}
				if( $this->input->post( $mem->key . '_num_sp_offerings' ) ):
					$x = $this->input->post( $mem->key . '_num_sp_offerings' );
					while( $x > 0 ):
						if( $this->input->post( $mem->key . '_sp_offering_' . $x . '_am') ){
							$data = array(
											'service' => $service_num,
											'date' => $date,
											'member' => $mem->key,
											'fund' => $this->input->post( $mem->key . '_sp_offering_' . $x . '_ty') ,
											'amount' => $this->input->post( $mem->key . '_sp_offering_' . $x . '_am') ,
											);
							$this->db->insert( 'offerings', $data );
							
							$this->db->where( 'type', $this->input->post( $mem->key . '_sp_offering_' . $x . '_ty') );
							$offering_type = $this->db->get('offering_type' );
							$chk_of_ty = $offering_type->num_rows();
							if( $chk_of_ty < 1 ){ 
								$data = array( 'type' => $this->input->post( $mem->key . '_sp_offering_' . $x . '_ty'));
								$this->db->insert( 'offering_type', $data );
							}
						}
						$x--;
					endwhile;
				endif;
				
			}
			
			
			// RECORD INDIVIDUAL VISITOR ATTENDANCE 
			
			$num_of_visitors_offering_specified = $this->input->post('visitor_form_num');
			$nvos = 0;
			if( $nvos > $num_of_visitors_offering_specified ){ $num_of_visitors_offering_specified = 3; }
			while( $nvos <= $num_of_visitors_offering_specified ): 
			if( $this->input->post( 'visitor_service_' . $nvos ) || $this->input->post( 'visitor_communion_' . $nvos ) || $this->input->post( 'visitor_bc_' . $nvos ) || $this->input->post( 'visitor_ss_' . $nvos ) || $this->input->post( 'visitors_names_' . $nvos ) ):

				if( $this->input->post( 'visitors_names_' . $nvos ) ) {
					$visitor_name = $this->input->post( 'visitors_names_' . $nvos );
				} else {
					$visitor_name = 'Visitor_' . $nvos ;
				}
				$data = array(
								'service' => $service_num,
								'date' => $date,
								'names' => $visitor_name,
								'home_church' => $this->input->post('visitors_home_church_' . $nvos ),
								'contact_info' => $this->input->post('visitors_contact_info_' . $nvos ),
								'ws' => $this->input->post( 'visitor_service_' . $nvos ),
								'ls' => $this->input->post( 'visitor_communion_' . $nvos ),
								'bc' => $this->input->post( 'visitor_bc_' . $nvos ),
								'ss' => $this->input->post( 'visitor_ss_' . $nvos ),
							);
				if( $this->input->post( 'visitors_key_' . $nvos) ){
					$this->db->where( 'key', $this->input->post( 'visitors_key_' . $nvos) );
					$this->db->update( 'service_visitors', $data );
				} else {
					$this->db->insert( 'service_visitors', $data );				
				}

			endif;
				$nvos++;
 			endwhile;
			

			// INSERT OTHER OFFERING INFORMATION - old other offering information already deleted above
			if( $this->input->post( 'other_gen_offering') ){
				$data = array(
								'service' => $service_num,
								'date' => $date,
								'member' => 0,
								'fund' => 'Gen',
								'amount' => $this->input->post( 'other_gen_offering'),
								);
				$this->db->insert( 'offerings', $data );
				
			}
			if( $this->input->post( 'other_num_sp_offerings' ) ):
				$x = $this->input->post( 'other_num_sp_offerings' );
				while( $x > 0 ):
					if( $this->input->post( 'other_sp_offering_' . $x . '_am') ){
						$data = array(
										'service' => $service_num,
										'date' => $date,
										'member' => 0,
										'fund' => $this->input->post( 'other_sp_offering_' . $x . '_ty') ,
										'amount' => $this->input->post( 'other_sp_offering_' . $x . '_am') ,
										);
						$this->db->insert( 'offerings', $data );
						
						$this->db->where( 'type', $this->input->post( 'other_sp_offering_' . $x . '_ty') );
						$offering_type = $this->db->get('offering_type' );
						$chk_of_ty = $offering_type->num_rows();
						if( $chk_of_ty < 1 ){ 
							$data = array( 'type' => $this->input->post( 'other_sp_offering_' . $x . '_ty'));
							$this->db->insert( 'offering_type', $data );
						}
					}
					$x--;
				endwhile;
			endif;			
			
			// IF SERVICE TYPE IS NEW INSERT INTO SERVICE_TYPE TABLE
			$this->db->where( 'type', $this->input->post( 'type') );
			$service_type = $this->db->get('service_type' );
			$chk_of_ty = $service_type->num_rows();
			if( $chk_of_ty < 1 ){ 
				$data = array( 'type' => $this->input->post( 'type'));
				$this->db->insert( 'service_type', $data );
			}
			
			
										
		$dataf['code'] = $this->Forms->view_this_service( $service_num );		
		$this->session->set_flashdata('notice', $dataf['code']);	
		
		redirect( 'services/view_service/'.$service_num );	
		
		}	
				
	}
	



}
?>