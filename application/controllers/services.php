<?php
/*	NOTES

To Do yet

	Add lines for visitors so that it can be indicated whether they attended Service, Lord's Supper, Bible Class etc. Rather than clicking on LS or BC, allow numbres to be entered so that if it is a family that attended they do not need to all be listed seperately, but how many were in each service can still be calculated.
	
	If visitors attended Lord's Supper include them in year end report so that the pastor can contact their pastor.


*/

class Services extends CI_Controller {
	
	
	function services()
	{
		parent::__construct();
		
		$this->load->library('session');
		$this->load->helper('cookie');
		$this->load->helper('url');
		$this->load->model('Forms');
		$this->load->model('Site');
		$this->load->database();
		$data = $this->Site->site_options();
		
		$data['query_links'] = $this->db->get('links');
		$data['header'] = $this->db->get('site_options');
		
		date_default_timezone_set($data['timezone']);

		$this->load->view('head', $data);		
		
	}
		
	
	function index ()
	{	
			
	
	$this->load->view('services/index');
	$this->load->view('footer');
	
	}
		
	
	function service () {
	
	
	$data = $this->Site->return_family_groups ();
	$data['service_type'] = $this->db->get('service_type');
	$data['offering_type'] = $this->db->get('offering_type');
	$sixdays = date( 'Y-m-d', time()-604800 );
	$this->db->order_by( 'date desc, key  desc' );
	$this->db->where( 'date >', $sixdays );
	$this->db->select( 'key, type, date, name' );
	$serviceo = $this->db->get('service');
	$data['services'] = $serviceo->result_array();
	$data['serv_opt'][''] = '--Select a service--';
	foreach( $data['services'] as $serv ):
		$data['serv_opt'][$serv['key']] = $serv['type'] . ' ' . date( 'M, jS Y', strtotime( $serv['date'] )) . ' ' . $serv['name'] ;
	endforeach;	
	
	$this->db->select( 'names' );
	$visitor_names_o = $this->db->get('service_visitors');
	$data['visitor_names'] = $visitor_names_o->result();	
		
	$this->load->view('services/service', $data);
	$this->load->view('footer');
	
	}
	
	
	
	
	
	
	
	
	function add_service () {
		$this->load->helper('form');
			
		$merge = $this->input->post('merge');
				
		// set validation rules
		if( $merge ) {
			$serv_num =	$this->input->post( 'merge_with' );
			$this->merge_services_post( $serv_num );
		
		} else { 
						
			$mem_data = $this->db->get('members');
			$members_ar = $mem_data->result() ;
	
			
			
			$this->load->library('form_validation');
			
				
			$this->form_validation->set_rules('type', 'Type', 'trim|required|xss_clean');	
			$this->form_validation->set_rules('date', 'Date', 'required|xss_clean');	
			$this->form_validation->set_rules('name', 'Name', 'xss_clean');	
			$this->form_validation->set_rules('offering', 'Totals: Offerings', 'numeric');	
			$this->form_validation->set_rules('total_attendance', 'Totals: Attendance -> Service ', 'is_natural');	
			$this->form_validation->set_rules('communion_attendance', 'Totals: Attendance -> Lord\'s Supper ', 'is_natural');	
			$this->form_validation->set_rules('bc_attendance', 'Totals: Attendance -> Bible Class ', 'is_natural');	
			$this->form_validation->set_rules('ss_attendance', 'Totals: Attendance -> Sunday School ', 'is_natural');	
			$this->form_validation->set_rules('member_attendance', 'Totals: Members', 'is_natural');	
			$this->form_validation->set_rules('visitor_attendance', 'Totals: Visitors', 'is_natural');	
			foreach( $members_ar as $mem ):
				$this->form_validation->set_rules($mem->key . '_service', 'Member: '.$mem->lname.', '.$mem->fname.' Attendance WS', 'integer');	
				$this->form_validation->set_rules($mem->key . '_communion', 'Member: '.$mem->lname.', '.$mem->fname.' Attendance LS', 'integer');	
				$this->form_validation->set_rules($mem->key . '_bc', 'Member: '.$mem->lname.', '.$mem->fname.' Attendance BC', 'integer');	
				$this->form_validation->set_rules($mem->key . '_ss', 'Member: '.$mem->lname.', '.$mem->fname.' Attendance SS', 'integer');	
				$this->form_validation->set_rules($mem->key . '_gen_offering', 'Member: '.$mem->lname.', '.$mem->fname.' Gen Offering', 'numeric');	
				if( $this->input->post( $mem->key . '_num_sp_offerings' ) ):
					$this->form_validation->set_rules($mem->key . '_num_sp_offerings', 'Member: '.$mem->lname.', '.$mem->fname.' Number of SP Offerings ', 'numeric');	
					$x = $this->input->post( $mem->key . '_num_sp_offerings' );
					while( $x > 0 ):
						$this->form_validation->set_rules($mem->key . '_sp_offering_' . $x . '_am', 'Member: '.$mem->lname.', '.$mem->fname.' Sp Offering '.$x.' Amount ', 'numeric');	
						$this->form_validation->set_rules($mem->key . '_sp_offering_' . $x . '_ty', 'Member: '.$mem->lname.', '.$mem->fname.' Sp Offering '.$x.' Type ', 'xss_clean');	
						$x--;
					endwhile;
				endif;
			endforeach;
			$this->form_validation->set_rules('other_gen_offering', 'Member: Other Gen Offering', 'numeric');	
			if( $this->input->post( 'other_num_sp_offerings' ) ):
				$this->form_validation->set_rules( 'other_num_sp_offerings', 'Member: Other Number of SP Offerings ', 'integer');	
				$x = $this->input->post( 'other_num_sp_offerings' );
				while( $x > 0 ):
					$this->form_validation->set_rules('other_sp_offering_' . $x . '_am', 'Member: Other Sp '.$x.' Offering Amount', 'numeric');	
					$this->form_validation->set_rules('other_sp_offering_' . $x . '_ty', 'Member: Other Sp '.$x.' Offering Type', 'xss_clean');	
					$x--;
				endwhile;
			endif;
			$this->form_validation->set_rules('visitor_form_num' , 'Number of Visitor Forms', 'is_natural');	
			$num_of_visitors_offering_specified = $this->input->post('visitor_form_num');
			$nvos = 0;
			if( $nvos > $num_of_visitors_offering_specified ){ $num_of_visitors_offering_specified = 3; }
			while( $nvos <= $num_of_visitors_offering_specified ): 
				$this->form_validation->set_rules('visitors_names_' . $nvos , 'Visitors: Visitor '.$nvos.' Names', 'xss_clean');	
				$this->form_validation->set_rules('visitor_service_' . $nvos , 'Visitors: Visitor '.$nvos.' WS', 'is_natural');	
				$this->form_validation->set_rules('visitor_communion_' . $nvos , 'Visitors: Visitor '.$nvos.' LS', 'is_natural');	
				$this->form_validation->set_rules('visitor_bc_' . $nvos , 'Visitors: Visitor '.$nvos.' BC', 'is_natural');	
				$this->form_validation->set_rules('visitor_ss_' . $nvos , 'Visitors: Visitor '.$nvos.' SS', 'is_natural');	
				$this->form_validation->set_rules('visitors_home_church_' . $nvos , 'Visitors: Visitor '.$nvos.' Home Church', 'xss_clean');	
				$this->form_validation->set_rules('visitors_contact_info_' . $nvos , 'Visitors: Visitor '.$nvos.' Contact Info', 'xss_clean');	
				$nvos++;
			endwhile;
			$this->form_validation->set_rules('notes', 'Notes', 'xss_clean');	
					
			if ( $this->form_validation->run() == FALSE ){
			
				$this->service();
							
			} else {
						
				$mem_status_data = $this->db->get('member_status');
				$mem_status_ar = $mem_status_data->result();

	
				//create proper date input
				$date = date( 'Y-m-d', strtotime( $this->input->post('date') ) );
				
				
				// insert basic data and retrieve key number
				
				$dataa = array (
								'type' => $this->input->post('type'),
								'date' => $date,
								'name' => $this->input->post('name'),
								'notes' => $this->input->post('notes'),
								);
				
				$this->db->insert( 'service', $dataa ); 
				
				$serv_infoo = $this->db->get_where( 'service', $dataa );
				$serv_info = $serv_infoo->row();
				$service_num = $serv_info->key;
				
				// RECORD INDIVIDUAL MEMBER ATTENDANCE
				foreach( $members_ar as $mem ):
				
					$itaaftm = $this->input->post( $mem->key . '_service') . $this->input->post( $mem->key . '_bc') . $this->input->post( $mem->key . '_ss') . $this->input->post( $mem->key . '_communion');
									
					if( !empty($itaaftm)){
					
						
							$postws = $this->input->post( $mem->key . '_service');
							$postls = $this->input->post( $mem->key . '_communion');
							$postbc = $this->input->post( $mem->key . '_bc');
							$postss = $this->input->post( $mem->key . '_ss');
						
						
						$data = array ( 
										'service_key' => $service_num,
										'date' => $date,
										'member' => $mem->key,
										'service' => $postws,
										'communion' => $postls,
										'bibleclass' => $postbc,
										'sundayschool' => $postss,
										);
						$this->db->insert( 'membertoservice', $data );
						
						unset( $postws, $postls, $postbc, $postss );
					} 
	
				// RECORD INDIVIDUAL MEMBER OFFERING INFORMATION
							
					if( $this->input->post( $mem->key . '_gen_offering') ):

						$fund_amount = $this->input->post( $mem->key . '_gen_offering');
						
						$data = array(
										'service' => $service_num,
										'date' => $date,
										'member' => $mem->key,
										'fund' => 'Gen',
										'amount' => $fund_amount,
										);
						$this->db->insert( 'offerings', $data );
						
						unset( $fund_amount );
					endif;
					if( $this->input->post( $mem->key . '_num_sp_offerings' ) ):
						$x = $this->input->post( $mem->key . '_num_sp_offerings' );
						while( $x > 0 ):
							if( $this->input->post( $mem->key . '_sp_offering_' . $x . '_am') ){
								$data = array(
												'service' => $service_num,
												'date' => $date,
												'member' => $mem->key,
												'fund' => $this->input->post( $mem->key . '_sp_offering_' . $x . '_ty') ,
												'amount' => $this->input->post( $mem->key . '_sp_offering_' . $x . '_am') ,
												);
								$this->db->insert( 'offerings', $data );
								
								$this->db->where( 'type', $this->input->post( $mem->key . '_sp_offering_' . $x . '_ty') );
								$offering_type = $this->db->get('offering_type' );
								$chk_of_ty = $offering_type->num_rows();
								if( $chk_of_ty < 1 ){ 
									$data = array( 'type' => $this->input->post( $mem->key . '_sp_offering_' . $x . '_ty'));
									$this->db->insert( 'offering_type', $data );
								}
							}
							$x--;
						endwhile;
					endif;
				endforeach;  // end member loop
				
				
				
				// INSERT OTHER OFFERING INFORMATION 
				
				if( $this->input->post( 'other_gen_offering') ){
						$fund_amount = $this->input->post( 'other_gen_offering');
						
						$data = array(
										'service' => $service_num,
										'date' => $date,
										'member' => 0,
										'fund' => 'Gen',
										'amount' => $fund_amount,
										);
						$this->db->insert( 'offerings', $data );
				}
				
				if( $this->input->post( 'other_num_sp_offerings' ) ):
					$x = $this->input->post( 'other_num_sp_offerings' );
					while( $x > 0 ):
						if( $this->input->post( 'other_sp_offering_' . $x . '_am') ){
							$data = array(
											'service' => $service_num,
											'date' => $date,
											'member' => 0,
											'fund' => $this->input->post( 'other_sp_offering_' . $x . '_ty') ,
											'amount' => $this->input->post( 'other_sp_offering_' . $x . '_am') ,
											);
							$this->db->insert( 'offerings', $data );
							
								$this->db->where( 'type', $this->input->post( $mem->key . 'other_sp_offering_' . $x . '_ty') );
								$offering_type = $this->db->get('offering_type' );
								$chk_of_ty = $offering_type->num_rows();
								if( $chk_of_ty < 1 ){ 
									$data = array( 'type' => $this->input->post( $mem->key . 'other_sp_offering_' . $x . '_ty'));
									$this->db->insert( 'offering_type', $data );
								}
						}
						$x--;
					endwhile;
				endif;
				// finished entering other offering information
				
				
				// RECORD INDIVIDUAL VISITOR ATTENDANCE AND OFFERINGS
				
				$num_of_visitors_offering_specified = $this->input->post('visitor_form_num');
				$nvos = 0;
				if( $nvos > $num_of_visitors_offering_specified ){ $num_of_visitors_offering_specified = 3; }
				while( $nvos <= $num_of_visitors_offering_specified ): 
				if( $this->input->post( 'visitor_service_' . $nvos ) || $this->input->post( 'visitor_communion_' . $nvos ) || $this->input->post( 'visitor_bc_' . $nvos ) || $this->input->post( 'visitor_ss_' . $nvos ) || $this->input->post( 'visitors_names_' . $nvos ) ):
	
					if( $this->input->post( 'visitors_names_' . $nvos ) ) {
						$visitor_name = $this->input->post( 'visitors_names_' . $nvos );
					} else {
						$visitor_name = 'Visitor_' . $nvos ;
					}
					$data = array(
									'service' => $service_num,
									'date' => $date,
									'names' => $visitor_name,
									'home_church' => $this->input->post('visitors_home_church_' . $nvos ),
									'contact_info' => $this->input->post('visitors_contact_info_' . $nvos ),
									'ws' => $this->input->post( 'visitor_service_' . $nvos ),
									'ls' => $this->input->post( 'visitor_communion_' . $nvos ),
									'bc' => $this->input->post( 'visitor_bc_' . $nvos ),
									'ss' => $this->input->post( 'visitor_ss_' . $nvos ),
								);
					$this->db->insert( 'service_visitors', $data );
	
				endif;
					$nvos++;
				endwhile;
				
				//define variables for attendance and offering
				$attendance['member'] = 0;
				$attendance['visitor'] = 0;
				$attendance['communion'] = 0;
				$attendance['bibleclass'] = 0;
				$attendance['sundayschool'] = 0;
				$offerings = array();
				
				// Calculate Totals
				foreach( $members_ar as $mem ):
					
					$this->db->where( 'status', $mem->status );
					$mso = $this->db->get( 'member_status' );
					$ms = $mso->row();
					if( !empty( $ms ) ){
						if( $ms->member ){ $status = 'member'; } else { $status = 'visitor' ; } 
					} else {
						 $status = 'visitor' ; 
					}
					
					$this->db->where( 'service_key', $service_num );
					$this->db->where( 'member', $mem->key );
					$mematto = $this->db->get( 'membertoservice' );
					$mematt = $mematto->row();
					if( !empty($mematt) ):
						if( $mematt->service ){ $attendance[$status]++; }
						if( $mematt->communion ){ $attendance['communion']++; }
						if( $mematt->bibleclass ){ $attendance['bibleclass']++; }
						if( $mematt->sundayschool ){ $attendance['sundayschool']++; }
					endif;
					
					$this->db->where( 'service', $service_num );
					$this->db->where( 'member', $mem->key );
					$memoffo = $this->db->get( 'offerings' );
					$memoffs = $memoffo->result();
					if( !empty($memoffs )):foreach( $memoffs as $memoff ):
						if( isset( $offerings[$memoff->fund] ) ){
							$offerings[$memoff->fund] = $offerings[$memoff->fund] + $memoff->amount ;
						} else {
							$offerings[$memoff->fund] = $memoff->amount ;
						}
					endforeach; endif;
				endforeach;
				
				// count "other offerings" offerings
	
				$this->db->where( 'service', $service_num );
				$this->db->where( 'member', 0 );
				$memoffo = $this->db->get( 'offerings' );
				$memoffs = $memoffo->result();
				if( !empty($memoffs )):foreach( $memoffs as $memoff ):
					if( isset( $offerings[$memoff->fund] ) ){
						$offerings[$memoff->fund] = $offerings[$memoff->fund] + $memoff->amount ;
					} else {
						$offerings[$memoff->fund] = $memoff->amount ;
					}
				endforeach; endif;
	
				$this->db->where( 'service', $service_num );
				$svao = $this->db->get( 'service_visitors' );
				$sva = $svao->result();
				if( !empty( $sva ) ) : foreach( $sva as $svisit ):
					$attendance['visitor'] = $attendance['visitor']+$svisit->ws ; 
					$attendance['communion'] = $attendance['communion']+$svisit->ls ; 
					$attendance['bibleclass'] = $attendance['bibleclass']+$svisit->bc ; 
					$attendance['sundayschool'] = $attendance['sundayschool']+$svisit->ss ; 
				endforeach; endif;
				
				$attendance['total'] = $attendance['visitor'] + $attendance['member'] ;
				$offering_total = 0;
				foreach( $offerings as $offering ):
					$offering_total = $offering_total + $offering ;
				endforeach;
				
				$offerings['total'] = $offering_total;
				
				// CREATE ARRAY OF OFFERINGS BY FUND TO BE STORED 
				
					$offerings_funds = 'offerings: <br>';
					foreach( $offerings as $k => $v ):
						$offerings_funds .= ' ' .$k. ': $'.$v.' <br>';
					endforeach;
				
				// OVERRIDE NUMBERS IF TOTALS ARE SUPPLIED
				
				$total_offering = $this->input->post( 'offering' );
				$total_attendance = $this->input->post( 'total_attendance' );
				$communion_attendance = $this->input->post( 'communion_attendance' );
				$bc_attendance = $this->input->post( 'bc_attendance' );
				$ss_attendance = $this->input->post( 'ss_attendance' );
				$member_attendance = $this->input->post( 'member_attendance' );
				$visitor_attendance = $this->input->post( 'visitor_attendance' );
				
				if( !empty( $total_offering ) ){ $offerings['total'] =  $total_offering; }
				if( !empty( $total_attendance ) ){ $attendance['total'] =  $total_attendance; }
				if( !empty( $communion_attendance ) ){ $attendance['communion'] =  $communion_attendance; }
				if( !empty( $bc_attendance ) ){ $attendance['bibleclass'] =  $bc_attendance; }
				if( !empty( $ss_attendance ) ){ $attendance['sundayschool'] =  $ss_attendance; }
				if( !empty( $member_attendance ) ){ $attendance['member'] =  $member_attendance; }
				if( !empty( $visitor_attendance ) ){ $attendance['visitor'] =  $visitor_attendance; }
				
				// RECORD GENERAL SERVICE INFORMATION
				
			
				$dataa = array (
								'date' => $date,
								'offering_details' => $offerings_funds,
								'offering' => $offerings['total'],
								'member_attendance' => $attendance['member'],
								'visitor_attendance' => $attendance['visitor'],
								'total_attendance' => $attendance['total'],
								'communion_attendance' => $attendance['communion'],
								'bc_attendance' => $attendance['bibleclass'],
								'ss_attendance' => $attendance['sundayschool'],
								);
				
				$this->db->where( 'key', $service_num );
				$this->db->update( 'service', $dataa ); 
				
				$flashdata = $this->Forms->view_this_service( $service_num );		
				$this->session->set_flashdata('notice', $flashdata);	
				
				redirect( 'services/view_service/'.$service_num );		
	
		
			}
		}	
	}
	

	
	
	function merge_services_post ( $service_num ) {
	
		$mem_data = $this->db->get('members');
		$members_ar = $mem_data->result() ;

		$this->load->helper('form');
		
		$this->load->library('form_validation');
			
		$this->form_validation->set_rules('merge', 'Merge', 'required|numeric');	
		$this->form_validation->set_rules('merge_with', 'Merge With', 'required|numeric|callback_ check_table');	
			$this->form_validation->set_rules('type', 'Type', 'xss_clean');	
			$this->form_validation->set_rules('date', 'Date', 'xss_clean');	
			$this->form_validation->set_rules('name', 'Name', 'xss_clean');	
		$this->form_validation->set_rules('offering', 'Totals: Offerings', 'numeric');	
		$this->form_validation->set_rules('total_attendance', 'Totals: Attendance -> Service ', 'is_natural');	
		$this->form_validation->set_rules('communion_attendance', 'Totals: Attendance -> Lord\'s Supper ', 'is_natural');	
		$this->form_validation->set_rules('bc_attendance', 'Totals: Attendance -> Bible Class ', 'is_natural');	
		$this->form_validation->set_rules('ss_attendance', 'Totals: Attendance -> Sunday School ', 'is_natural');	
		$this->form_validation->set_rules('member_attendance', 'Totals: Members', 'is_natural');	
		$this->form_validation->set_rules('visitor_attendance', 'Totals: Visitors', 'is_natural');	
		foreach( $members_ar as $mem ):
			$this->form_validation->set_rules($mem->key . '_service', 'Member: '.$mem->lname.', '.$mem->fname.' Attendance WS', 'integer');	
			$this->form_validation->set_rules($mem->key . '_communion', 'Member: '.$mem->lname.', '.$mem->fname.' Attendance LS', 'integer');	
			$this->form_validation->set_rules($mem->key . '_bc', 'Member: '.$mem->lname.', '.$mem->fname.' Attendance BC', 'integer');	
			$this->form_validation->set_rules($mem->key . '_ss', 'Member: '.$mem->lname.', '.$mem->fname.' Attendance SS', 'integer');	
			$this->form_validation->set_rules($mem->key . '_gen_offering', 'Member: '.$mem->lname.', '.$mem->fname.' Gen Offering', 'numeric');	
			if( $this->input->post( $mem->key . '_num_sp_offerings' ) ):
				$this->form_validation->set_rules($mem->key . '_num_sp_offerings', 'Member: '.$mem->lname.', '.$mem->fname.' Number of SP Offerings ', 'numeric');	
				$x = $this->input->post( $mem->key . '_num_sp_offerings' );
				while( $x > 0 ):
					$this->form_validation->set_rules($mem->key . '_sp_offering_' . $x . '_am', 'Member: '.$mem->lname.', '.$mem->fname.' Sp Offering '.$x.' Amount ', 'numeric');	
					$this->form_validation->set_rules($mem->key . '_sp_offering_' . $x . '_ty', 'Member: '.$mem->lname.', '.$mem->fname.' Sp Offering '.$x.' Type ', 'xss_clean');	
					$x--;
				endwhile;
			endif;
		endforeach;
		$this->form_validation->set_rules('other_gen_offering', 'Member: Other Gen Offering', 'numeric');	
		if( $this->input->post( 'other_num_sp_offerings' ) ):
			$this->form_validation->set_rules( 'other_num_sp_offerings', 'Member: Other Number of SP Offerings ', 'integer');	
			$x = $this->input->post( 'other_num_sp_offerings' );
			while( $x > 0 ):
				$this->form_validation->set_rules('other_sp_offering_' . $x . '_am', 'Member: Other Sp '.$x.' Offering Amount', 'numeric');	
				$this->form_validation->set_rules('other_sp_offering_' . $x . '_ty', 'Member: Other Sp '.$x.' Offering Type', 'xss_clean');	
				$x--;
			endwhile;
		endif;
		$this->form_validation->set_rules('visitor_form_num' , 'Number of Visitor Forms', 'is_natural');	
		$num_of_visitors_offering_specified = $this->input->post('visitor_form_num');
		$nvos = 0;
		if( $nvos > $num_of_visitors_offering_specified ){ $num_of_visitors_offering_specified = 3; }
		while( $nvos <= $num_of_visitors_offering_specified ): 
			$this->form_validation->set_rules('visitors_names_' . $nvos , 'Visitors: Visitor '.$nvos.' Names', 'xss_clean');	
			$this->form_validation->set_rules('visitor_service_' . $nvos , 'Visitors: Visitor '.$nvos.' WS', 'is_natural');	
			$this->form_validation->set_rules('visitor_communion_' . $nvos , 'Visitors: Visitor '.$nvos.' LS', 'is_natural');	
			$this->form_validation->set_rules('visitor_bc_' . $nvos , 'Visitors: Visitor '.$nvos.' BC', 'is_natural');	
			$this->form_validation->set_rules('visitor_ss_' . $nvos , 'Visitors: Visitor '.$nvos.' SS', 'is_natural');	
			$this->form_validation->set_rules('visitors_home_church_' . $nvos , 'Visitors: Visitor '.$nvos.' Home Church', 'xss_clean');	
			$this->form_validation->set_rules('visitors_contact_info_' . $nvos , 'Visitors: Visitor '.$nvos.' Contact Info', 'xss_clean');	
			$nvos++;
		endwhile;
		$this->form_validation->set_rules('notes', 'Notes', 'xss_clean');	
		$this->form_validation->set_message('check_table', 'There seems to be something wrong with the service you are trying to merge with. Try again or enter the data as a different service and merge later.');				
				
		if ( $this->form_validation->run() == FALSE ){
		
			$this->service();
						
		} else {
					
			$mem_status_data = $this->db->get('member_status');
			$mem_status_ar = $mem_status_data->result();
			
			$this->db->where( 'key', $service_num );
			$serv_info_obj = $this->db->get( 'service' );
			$serv_info = $serv_info_obj->row_array();

			//create proper date input
			$date = $serv_info['date'];

			// RECORD INDIVIDUL MEMBER ATTENDANCE
			foreach( $members_ar as $mem ):
			
				if( $this->input->post( $mem->key . '_service') || $this->input->post( $mem->key . '_bc') || $this->input->post( $mem->key . '_ss') || $this->input->post( $mem->key . '_communion') ){
				
					$this->db->where( 'service_key', $service_num );
					$this->db->where( 'member', $mem->key );
					$mem_ser_info_ob = $this->db->get('membertoservice');
					$mem_ser_info = $mem_ser_info_ob->result();
					
					foreach( $mem_ser_info as $mems ):
						if( $mems->service == 1 ){ $sadata['service']=1; };
						if( $mems->communion == 1 ){ $sadata['communion']=1; };
						if( $mems->bibleclass == 1 ){ $sadata['bibleclass']=1; };
						if( $mems->sundayschool == 1 ){ $sadata['sundayschool']=1; };
					endforeach;
					
						$postws = $this->input->post( $mem->key . '_service');
						$postls = $this->input->post( $mem->key . '_communion');
						$postbc = $this->input->post( $mem->key . '_bc');
						$postss = $this->input->post( $mem->key . '_ss');
					
						if( $postws == 1 ){ $sadata['service']=1; };
						if( $postls == 1 ){ $sadata['communion']=1; };
						if( $postbc == 1 ){ $sadata['bibleclass']=1; };
						if( $postss == 1 ){ $sadata['sundayschool']=1; };
						
					$this->db->where( 'service_key', $service_num );
					$this->db->where( 'member', $mem->key );
					$this->db->delete( 'membertoservice' );					
					
					$sadata['service_key']=$service_num;
					$sadata['date']=$date;
					$sadata['member']=$mem->key;
					
					$this->db->insert( 'membertoservice', $sadata );
					
					unset( $sadata, $postws, $postls, $postbc, $postss );
				} 

			// RECORD INDIVIDUAL MEMBER OFFERING INFORMATION
						
				if( $this->input->post( $mem->key . '_gen_offering') ):
					$this->db->where( 'service', $service_num );
					$this->db->where( 'member', $mem->key );
					$this->db->where( 'fund', 'Gen' );
					$omgfo = $this->db->get( 'offerings' );
					$omgf = $omgfo->result();
					$fund_amount = 0;
					if( !empty($omgf)){
						foreach( $omgf as $x ){
							$fund_amount = $fund_amount + $x->amount;
							$this->db->where( 'key', $x->key );
							$this->db->delete( 'offerings' ); 
						}
					} 
					$fund_amount = $fund_amount + $this->input->post( $mem->key . '_gen_offering');
					
					$data = array(
									'service' => $service_num,
									'date' => $date,
									'member' => $mem->key,
									'fund' => 'Gen',
									'amount' => $fund_amount,
									);
					$this->db->insert( 'offerings', $data );
					unset( $fund_amount );
				endif;
				if( $this->input->post( $mem->key . '_num_sp_offerings' ) ):
					$x = $this->input->post( $mem->key . '_num_sp_offerings' );
					while( $x > 0 ):
						if( $this->input->post( $mem->key . '_sp_offering_' . $x . '_am') ){
							$data = array(
											'service' => $service_num,
											'date' => $date,
											'member' => $mem->key,
											'fund' => $this->input->post( $mem->key . '_sp_offering_' . $x . '_ty') ,
											'amount' => $this->input->post( $mem->key . '_sp_offering_' . $x . '_am') ,
											);
							$this->db->insert( 'offerings', $data );
							
							$this->db->where( 'type', $this->input->post( $mem->key . '_sp_offering_' . $x . '_ty') );
							$offering_type = $this->db->get('offering_type' );
							$chk_of_ty = $offering_type->num_rows();
							if( $chk_of_ty < 1 ){ 
								$data = array( 'type' => $this->input->post( $mem->key . '_sp_offering_' . $x . '_ty'));
								$this->db->insert( 'offering_type', $data );
							}
						}
						$x--;
					endwhile;
				endif;
			endforeach;  // end member loop
			
			
			
			// INSERT OTHER OFFERING INFORMATION 
			
			if( $this->input->post( 'other_gen_offering') ){
					$this->db->where( 'service', $service_num );
					$this->db->where( 'member', 0 );
					$this->db->where( 'fund', 'Gen' );
					$omgfo = $this->db->get( 'offerings' );
					$omgf = $omgfo->result();
					$fund_amount = 0;
					if( !empty($omgf)){
						foreach( $omgf as $x ){
							$fund_amount = $fund_amount + $x->amount;
							$this->db->where( 'key', $x->key );
							$this->db->delete( 'offerings' ); 
						}
					} 
					$fund_amount = $fund_amount + $this->input->post( $mem->key . '_gen_offering');
					
					$data = array(
									'service' => $service_num,
									'date' => $date,
									'member' => 0,
									'fund' => 'Gen',
									'amount' => $fund_amount,
									);
					$this->db->insert( 'offerings', $data );
			}
			
			if( $this->input->post( 'other_num_sp_offerings' ) ):
				$x = $this->input->post( 'other_num_sp_offerings' );
				while( $x > 0 ):
					if( $this->input->post( 'other_sp_offering_' . $x . '_am') ){
						$data = array(
										'service' => $service_num,
										'date' => $date,
										'member' => 0,
										'fund' => $this->input->post( 'other_sp_offering_' . $x . '_ty') ,
										'amount' => $this->input->post( 'other_sp_offering_' . $x . '_am') ,
										);
						$this->db->insert( 'offerings', $data );
						
							$this->db->where( 'type', $this->input->post( $mem->key . 'other_sp_offering_' . $x . '_ty') );
							$offering_type = $this->db->get('offering_type' );
							$chk_of_ty = $offering_type->num_rows();
							if( $chk_of_ty < 1 ){ 
								$data = array( 'type' => $this->input->post( $mem->key . 'other_sp_offering_' . $x . '_ty'));
								$this->db->insert( 'offering_type', $data );
							}
					}
					$x--;
				endwhile;
			endif;
			// finished entering other offering information
			
			
			// RECORD INDIVIDUAL VISITOR ATTENDANCE AND OFFERINGS
			
			$num_of_visitors_offering_specified = $this->input->post('visitor_form_num');
			$nvos = 0;
			if( $nvos > $num_of_visitors_offering_specified ){ $num_of_visitors_offering_specified = 3; }
			while( $nvos <= $num_of_visitors_offering_specified ): 
			if( $this->input->post( 'visitor_service_' . $nvos ) || $this->input->post( 'visitor_communion_' . $nvos ) || $this->input->post( 'visitor_bc_' . $nvos ) || $this->input->post( 'visitor_ss_' . $nvos ) || $this->input->post( 'visitors_names_' . $nvos ) ):

				if( $this->input->post( 'visitors_names_' . $nvos ) ) {
					$visitor_name = $this->input->post( 'visitors_names_' . $nvos );
				} else {
					$visitor_name = 'Visitor_' . $nvos ;
				}
				$data = array(
								'service' => $service_num,
								'date' => $date,
								'names' => $visitor_name,
								'home_church' => $this->input->post('visitors_home_church_' . $nvos ),
								'contact_info' => $this->input->post('visitors_contact_info_' . $nvos ),
								'ws' => $this->input->post( 'visitor_service_' . $nvos ),
								'ls' => $this->input->post( 'visitor_communion_' . $nvos ),
								'bc' => $this->input->post( 'visitor_bc_' . $nvos ),
								'ss' => $this->input->post( 'visitor_ss_' . $nvos ),
							);
				$this->db->insert( 'service_visitors', $data );

			endif;
				$nvos++;
			endwhile;
			
			//define variables for attendance and offering
			$attendance['member'] = 0;
			$attendance['visitor'] = 0;
			$attendance['communion'] = 0;
			$attendance['bibleclass'] = 0;
			$attendance['sundayschool'] = 0;
			$offerings = array();
			
			
			// Calculate Totals
			foreach( $members_ar as $mem ):
				
				$this->db->where( 'status', $mem->status );
				$mso = $this->db->get( 'member_status' );
				$ms = $mso->row();
				if( !empty( $ms ) ){
					if( $ms->member ){ $status = 'member'; } else { $status = 'visitor' ; } 
				} else {
					 $status = 'visitor' ; 
				}
				
				$this->db->where( 'service_key', $service_num );
				$this->db->where( 'member', $mem->key );
				$mematto = $this->db->get( 'membertoservice' );
				$mematt = $mematto->row();
				if( !empty($mematt) ):
					if( $mematt->service ){ $attendance[$status]++; }
					if( $mematt->communion ){ $attendance['communion']++; }
					if( $mematt->bibleclass ){ $attendance['bibleclass']++; }
					if( $mematt->sundayschool ){ $attendance['sundayschool']++; }
				endif;
				
				$this->db->where( 'service', $service_num );
				$this->db->where( 'member', $mem->key );
				$memoffo = $this->db->get( 'offerings' );
				$memoffs = $memoffo->result();
				if( !empty($memoffs )):foreach( $memoffs as $memoff ):
					if( isset( $offerings[$memoff->fund] ) ){
						$offerings[$memoff->fund] = $offerings[$memoff->fund] + $memoff->amount ;
					} else {
						$offerings[$memoff->fund] = $memoff->amount ;
					}
				endforeach; endif;
			endforeach;
			
			// count "other offerings" offerings

			$this->db->where( 'service', $service_num );
			$this->db->where( 'member', 0 );
			$memoffo = $this->db->get( 'offerings' );
			$memoffs = $memoffo->result();
			if( !empty($memoffs )):foreach( $memoffs as $memoff ):
				if( isset( $offerings[$memoff->fund] ) ){
					$offerings[$memoff->fund] = $offerings[$memoff->fund] + $memoff->amount ;
				} else {
					$offerings[$memoff->fund] = $memoff->amount ;
				}
			endforeach; endif;

			$this->db->where( 'service', $service_num );
			$svao = $this->db->get( 'service_visitors' );
			$sva = $svao->result();
			if( !empty( $sva ) ) : foreach( $sva as $svisit ):
				if( $svisit->ws ){ $attendance['visitor']++; }
				if( $svisit->ls ){ $attendance['communion']++; }
				if( $svisit->bc ){ $attendance['bibleclass']++; }
				if( $svisit->ss ){ $attendance['sundayschool']++; }
			endforeach; endif;
			
			$attendance['total'] = $attendance['visitor'] + $attendance['member'] ;
			$offering_total = 0;
			foreach( $offerings as $offering ):
				$offering_total = $offering_total + $offering ;
			endforeach;
			
			$offerings['total'] = $offering_total;
			
			// CREATE ARRAY OF OFFERINGS BY FUND TO BE STORED 
			
				$offerings_funds = 'offerings: <br>';
				foreach( $offerings as $k => $v ):
					$offerings_funds .= ' ' .$k. ': $'.$v.' <br>';
				endforeach;
			
			// OVERRIDE NUMBERS IF TOTALS ARE SUPPLIED
			
			$total_offering = $this->input->post( 'offering' );
			$total_attendance = $this->input->post( 'total_attendance' );
			$communion_attendance = $this->input->post( 'communion_attendance' );
			$bc_attendance = $this->input->post( 'bc_attendance' );
			$ss_attendance = $this->input->post( 'ss_attendance' );
			$member_attendance = $this->input->post( 'member_attendance' );
			$visitor_attendance = $this->input->post( 'visitor_attendance' );
			
			if( !empty( $total_offering ) ){ $offerings['total'] =  $total_offering + $serv_info['offering'] ; }
			if( !empty( $total_attendance ) ){ $attendance['total'] =  $total_attendance + $serv_info['total_attendance'] ; }
			if( !empty( $communion_attendance ) ){ $attendance['communion'] =  $communion_attendance + $serv_info['total_communion'] ; }
			if( !empty( $bc_attendance ) ){ $attendance['bibleclass'] =  $bc_attendance + $serv_info['total_ bc'] ; }
			if( !empty( $ss_attendance ) ){ $attendance['sundayschool'] =  $ss_attendance + $serv_info['total_ss'] ; }
			if( !empty( $member_attendance ) ){ $attendance['member'] =  $member_attendance + $serv_info['total_member'] ; }
			if( !empty( $visitor_attendance ) ){ $attendance['visitor'] =  $visitor_attendance + $serv_info['total_visitor'] ; }
			
			// RECORD GENERAL SERVICE INFORMATION
			
		
			$dataa = array (
							'date' => $date,
							'offering_details' => $offerings_funds,
							'offering' => $offerings['total'],
							'member_attendance' => $attendance['member'],
							'visitor_attendance' => $attendance['visitor'],
							'total_attendance' => $attendance['total'],
							'communion_attendance' => $attendance['communion'],
							'bc_attendance' => $attendance['bibleclass'],
							'ss_attendance' => $attendance['sundayschool'],
							'notes' => $this->input->post('notes') . ' ' .  $serv_info['notes'],
							);
			
			$this->db->where( 'key', $service_num );
			$this->db->update( 'service', $dataa ); 
			
			$flashdata = $this->Forms->view_this_service( $service_num );		
			$this->session->set_flashdata('notice', $flashdata);	
			
			redirect( 'services/service' );		
	
		}
	}
	
	
	function chk_service_number( $data ){ 
	
		return $this->Site->check_table ( 'service', "key = ".$data."" );
	}
	
	
	function view_service () {
	
	$data = $this->Site->site_options ();

	$this->db->select( 'key, type, date, name' );
	$this->db->order_by("date", "desc"); 
	$this->db->order_by("type", "desc"); 
	$this->db->order_by("name", "desc"); 
	$data['services'] = $this->db->get('service');
	
	if( $data['services']->num_rows() > 0 ):
	
	$service_ar = $data['services']->result();
	
	foreach( $service_ar as $service ):
		$service_date = explode( '-', $service->date );
		$services[$service_date[0]][$service_date[1]][$service_date[2]][$service->key] = array( 
					'key' => $service->key,
					'date' => $service->date,
					'type' => $service->type,
					'name' => $service->name, 
					);
	endforeach;
		
	if( $this->uri->segment(3) && $this->chk_service_number( $this->uri->segment(3)) ){
		
		$serv_num = $this->uri->segment(3);
				
		$data = $this->Site->return_family_groups ();
		
		$this->db->where('key', $serv_num );
		$data['this_service_obj'] = $this->db->get('service');
					
		$data['service_type'] = $this->db->get('service_type');
		$data['offering_type'] = $this->db->get('offering_type');
		$this->db->where( 'service', $serv_num );
		$data['service_visitors'] = $this->db->get('service_visitors');
		$this->db->order_by( 'member' );
		$this->db->where( 'service', $serv_num );
		$data['offerings'] = $this->db->get('offerings');
		$this->db->where( 'service_key', $serv_num );
		$data['mem_att'] = $this->db->get( 'membertoservice' );
		
		$this->db->order_by( 'date desc, key  desc' );
		$this->db->select( 'key, type, date, name' );
		$serviceo = $this->db->get('service');
		$data['services'] = $serviceo->result_array();
		$data['serv_opt'][''] = '--Select a service--';
		foreach( $data['services'] as $serv ):
			$data['serv_opt'][$serv['key']] = $serv['type'] . ' ' . date( 'M, jS Y', strtotime( $serv['date'] )) . ' ' . $serv['name'] ;
		endforeach;	

		
		$this->db->select( 'names' );
		$visitor_names_o = $this->db->get('service_visitors');
		$data['visitor_names'] = $visitor_names_o->result();
		$data['service_exists'] = true ;
				
	} else {
	
		$data = $this->Site->return_family_groups ();
		$data['service_exists'] = false ;
	}
	
	
	$data['service_date_array']=$services;
	
	$this->load->view('services/view_service', $data);
	$this->load->view('footer');
	
	endif;
	
	}
	
	
	
	
	
	
	
	function view_this_service () {
	

	$serv_num = $this->uri->segment(3);

	
	$this->db->where( 'key', $serv_num );
	$data['service'] = $this->db->get('service');
	
	$ct = 0;
	$this->db->where( 'service', $serv_num );
	$data['offerings'] = $this->db->get('offerings');
		$off_members = $data['offerings']->result();
		foreach( $off_members as $mem ){
			if( $mem->member == 0 ){
						$offerings[ $mem->member ][$ct] = array (
										'lname' => 'other',
										'fname' => '',
										'fund' => $mem->fund,
										'amount' => $mem->amount,
										'key' => $mem->key
										);
			}
			else {
				$this->db->where( 'key', $mem->member );
				$this_member_ob = $this->db->get( 'members' );
				$this_member_ar = $this_member_ob->result();
				$this_member = $this_member_ar[0];
			
			$offerings[ $mem->member ][$ct] = array (
										'lname' => $this_member->lname,
										'fname' => $this_member->fname,
										'fund' => $mem->fund,
										'amount' => $mem->amount,
										'key' => $mem->key
										);
			}
			$ct++;
		}
		
		if( isset( $offerings )){ $data['offer_mem'] = $offerings; }
		
		
	$this->db->where( 'service_key', $serv_num );
	$attendance_obj = $this->db->get( 'membertoservice' );
			$attendance_array = $attendance_obj->result();
			if( !empty($attendance_array)){
				$ct=0;
				foreach( $attendance_array as $attender ){
					$this->db->where( 'key', $attender->member );
					$mem_obj = $this->db->get( 'members');
					$mem_array = $mem_obj->result();
					$mem = $mem_array[0];
					
					$attenders[$attender->member] = array(
														'lname' => $mem->lname,
														'fname' => $mem->fname,
														'mem_key' => $mem->key,
														'service' => $attender->service,
														'communion' => $attender->communion,
														'bibleclass' => $attender->bibleclass,
														'sundayschool' => $attender->sundayschool,
														);
					
					if( $attender->service ){ 
						$service['service'][$ct] = array( 
														'lname' => $mem->lname,
														'fname' => $mem->fname,
														'mem_key' => $mem->key
														);
					}
					if( $attender->communion ){ 
						$service['communion'][$ct] = array( 
														'lname' => $mem->lname,
														'fname' => $mem->fname,
														'mem_key' => $mem->key
														);
					}
					if( $attender->bibleclass ){ 
						$service['bibleclass'][$ct] = array( 
														'lname' => $mem->lname,
														'fname' => $mem->fname,
														'mem_key' => $mem->key
														);
					}
					if( $attender->sundayschool ){ 
						$service['sundayschool'][$ct] = array( 
														'lname' => $mem->lname,
														'fname' => $mem->fname,
														'mem_key' => $mem->key
														);
					}
					
					$ct++;
				}
				$data['attendersva']=$attenders;
				$data['attendersvs']=$service;
			}
									
	$this->load->view('services/view_this_service', $data);
	
	}
	
	
	function edit_edit_this_service () {
		$this->load->helper('form');
			
		$merge = $this->input->post('merge');
				
		// set validation rules
		if( $merge ) {
			$serv_num =	$this->input->post( 'merge_with' );
			$this->merge_services_post( $serv_num );
		
		} else { 
						
			$mem_data = $this->db->get('members');
			$members_ar = $mem_data->result() ;
	
			
			
			$this->load->library('form_validation');
			
				
			$this->form_validation->set_rules('type', 'Type', 'trim|required|xss_clean');	
			$this->form_validation->set_rules('date', 'Date', 'required|xss_clean');	
			$this->form_validation->set_rules('name', 'Name', 'xss_clean');	
			$this->form_validation->set_rules('offering', 'Totals: Offerings', 'numeric');	
			$this->form_validation->set_rules('total_attendance', 'Totals: Attendance -> Service ', 'is_natural');	
			$this->form_validation->set_rules('communion_attendance', 'Totals: Attendance -> Lord\'s Supper ', 'is_natural');	
			$this->form_validation->set_rules('bc_attendance', 'Totals: Attendance -> Bible Class ', 'is_natural');	
			$this->form_validation->set_rules('ss_attendance', 'Totals: Attendance -> Sunday School ', 'is_natural');	
			$this->form_validation->set_rules('member_attendance', 'Totals: Members', 'is_natural');	
			$this->form_validation->set_rules('visitor_attendance', 'Totals: Visitors', 'is_natural');	
			foreach( $members_ar as $mem ):
				$this->form_validation->set_rules($mem->key . '_service', 'Member: '.$mem->lname.', '.$mem->fname.' Attendance WS', 'integer');	
				$this->form_validation->set_rules($mem->key . '_communion', 'Member: '.$mem->lname.', '.$mem->fname.' Attendance LS', 'integer');	
				$this->form_validation->set_rules($mem->key . '_bc', 'Member: '.$mem->lname.', '.$mem->fname.' Attendance BC', 'integer');	
				$this->form_validation->set_rules($mem->key . '_ss', 'Member: '.$mem->lname.', '.$mem->fname.' Attendance SS', 'integer');	
				$this->form_validation->set_rules($mem->key . '_gen_offering', 'Member: '.$mem->lname.', '.$mem->fname.' Gen Offering', 'numeric');	
				if( $this->input->post( $mem->key . '_num_sp_offerings' ) ):
					$this->form_validation->set_rules($mem->key . '_num_sp_offerings', 'Member: '.$mem->lname.', '.$mem->fname.' Number of SP Offerings ', 'numeric');	
					$x = $this->input->post( $mem->key . '_num_sp_offerings' );
					while( $x > 0 ):
						$this->form_validation->set_rules($mem->key . '_sp_offering_' . $x . '_am', 'Member: '.$mem->lname.', '.$mem->fname.' Sp Offering '.$x.' Amount ', 'numeric');	
						$this->form_validation->set_rules($mem->key . '_sp_offering_' . $x . '_ty', 'Member: '.$mem->lname.', '.$mem->fname.' Sp Offering '.$x.' Type ', 'xss_clean');	
						$x--;
					endwhile;
				endif;
			endforeach;
			$this->form_validation->set_rules('other_gen_offering', 'Member: Other Gen Offering', 'numeric');	
			if( $this->input->post( 'other_num_sp_offerings' ) ):
				$this->form_validation->set_rules( 'other_num_sp_offerings', 'Member: Other Number of SP Offerings ', 'integer');	
				$x = $this->input->post( 'other_num_sp_offerings' );
				while( $x > 0 ):
					$this->form_validation->set_rules('other_sp_offering_' . $x . '_am', 'Member: Other Sp '.$x.' Offering Amount', 'numeric');	
					$this->form_validation->set_rules('other_sp_offering_' . $x . '_ty', 'Member: Other Sp '.$x.' Offering Type', 'xss_clean');	
					$x--;
				endwhile;
			endif;
			$this->form_validation->set_rules('visitor_form_num' , 'Number of Visitor Forms', 'is_natural');	
			$num_of_visitors_offering_specified = $this->input->post('visitor_form_num');
			$nvos = 0;
			if( $nvos > $num_of_visitors_offering_specified ){ $num_of_visitors_offering_specified = 3; }
			while( $nvos <= $num_of_visitors_offering_specified ): 
				$this->form_validation->set_rules('visitors_names_' . $nvos , 'Visitors: Visitor '.$nvos.' Names', 'xss_clean');	
				$this->form_validation->set_rules('visitor_service_' . $nvos , 'Visitors: Visitor '.$nvos.' WS', 'is_natural');	
				$this->form_validation->set_rules('visitor_communion_' . $nvos , 'Visitors: Visitor '.$nvos.' LS', 'is_natural');	
				$this->form_validation->set_rules('visitor_bc_' . $nvos , 'Visitors: Visitor '.$nvos.' BC', 'is_natural');	
				$this->form_validation->set_rules('visitor_ss_' . $nvos , 'Visitors: Visitor '.$nvos.' SS', 'is_natural');	
				$this->form_validation->set_rules('visitors_home_church_' . $nvos , 'Visitors: Visitor '.$nvos.' Home Church', 'xss_clean');	
				$this->form_validation->set_rules('visitors_contact_info_' . $nvos , 'Visitors: Visitor '.$nvos.' Contact Info', 'xss_clean');	
				$nvos++;
			endwhile;
			$this->form_validation->set_rules('notes', 'Notes', 'xss_clean');	
					
			if ( $this->form_validation->run() == FALSE ){
			
				$this->view_this_service();
							
			} else {
			
				$service_num = $this->input->post('key');						
				$mem_status_data = $this->db->get('member_status');
				$mem_status_ar = $mem_status_data->result();

	
				//create proper date input
				$date = date( 'Y-m-d', strtotime( $this->input->post('date') ) );
				
				
				// insert basic data and retrieve key number
				
				$dataa = array (
								'type' => $this->input->post('type'),
								'date' => $date,
								'name' => $this->input->post('name'),
								'notes' => $this->input->post('notes'),
								);
				
				$this->db->where( 'key', $service_num );
				$this->db->update( 'service', $dataa ); 
				
				$serv_infoo = $this->db->get_where( 'service', $dataa );
				$serv_info = $serv_infoo->row();
				
				// RECORD INDIVIDUAL MEMBER ATTENDANCE
					$this->db->where( 'service_key', $service_num );
					$this->db->delete( 'membertoservice' );
					
				foreach( $members_ar as $mem ):
				
					$itaaftm = $this->input->post( $mem->key . '_service') . $this->input->post( $mem->key . '_bc') . $this->input->post( $mem->key . '_ss') . $this->input->post( $mem->key . '_communion');
									
					if( !empty($itaaftm)){
					
						
							$postws = $this->input->post( $mem->key . '_service');
							$postls = $this->input->post( $mem->key . '_communion');
							$postbc = $this->input->post( $mem->key . '_bc');
							$postss = $this->input->post( $mem->key . '_ss');
						
						
						$data = array ( 
										'service_key' => $service_num,
										'date' => $date,
										'member' => $mem->key,
										'service' => $postws,
										'communion' => $postls,
										'bibleclass' => $postbc,
										'sundayschool' => $postss,
										);
						$this->db->insert( 'membertoservice', $data );
						
						unset( $postws, $postls, $postbc, $postss );
					} 
	
				// RECORD INDIVIDUAL MEMBER OFFERING INFORMATION
				
					$this->db->where( 'service', $service_num );
					$this->db->delete( 'offerings' );
					
							
					if( $this->input->post( $mem->key . '_gen_offering') ):

						$fund_amount = $this->input->post( $mem->key . '_gen_offering');
						
						$data = array(
										'service' => $service_num,
										'date' => $date,
										'member' => $mem->key,
										'fund' => 'Gen',
										'amount' => $fund_amount,
										);
						$this->db->insert( 'offerings', $data );
						
						unset( $fund_amount );
					endif;
					if( $this->input->post( $mem->key . '_num_sp_offerings' ) ):
						$x = $this->input->post( $mem->key . '_num_sp_offerings' );
						while( $x > 0 ):
							if( $this->input->post( $mem->key . '_sp_offering_' . $x . '_am') ){
								$data = array(
												'service' => $service_num,
												'date' => $date,
												'member' => $mem->key,
												'fund' => $this->input->post( $mem->key . '_sp_offering_' . $x . '_ty') ,
												'amount' => $this->input->post( $mem->key . '_sp_offering_' . $x . '_am') ,
												);
								$this->db->insert( 'offerings', $data );
								
								$this->db->where( 'type', $this->input->post( $mem->key . '_sp_offering_' . $x . '_ty') );
								$offering_type = $this->db->get('offering_type' );
								$chk_of_ty = $offering_type->num_rows();
								if( $chk_of_ty < 1 ){ 
									$data = array( 'type' => $this->input->post( $mem->key . '_sp_offering_' . $x . '_ty'));
									$this->db->insert( 'offering_type', $data );
								}
							}
							$x--;
						endwhile;
					endif;
				endforeach;  // end member loop
				
				
				
				// INSERT OTHER OFFERING INFORMATION 
				
				if( $this->input->post( 'other_gen_offering') ){
						$fund_amount = $this->input->post( 'other_gen_offering');
						
						$data = array(
										'service' => $service_num,
										'date' => $date,
										'member' => 0,
										'fund' => 'Gen',
										'amount' => $fund_amount,
										);
						$this->db->insert( 'offerings', $data );
				}
				
				if( $this->input->post( 'other_num_sp_offerings' ) ):
					$x = $this->input->post( 'other_num_sp_offerings' );
					while( $x > 0 ):
						if( $this->input->post( 'other_sp_offering_' . $x . '_am') ){
							$data = array(
											'service' => $service_num,
											'date' => $date,
											'member' => 0,
											'fund' => $this->input->post( 'other_sp_offering_' . $x . '_ty') ,
											'amount' => $this->input->post( 'other_sp_offering_' . $x . '_am') ,
											);
							$this->db->insert( 'offerings', $data );
							
								$this->db->where( 'type', $this->input->post( $mem->key . 'other_sp_offering_' . $x . '_ty') );
								$offering_type = $this->db->get('offering_type' );
								$chk_of_ty = $offering_type->num_rows();
								if( $chk_of_ty < 1 ){ 
									$data = array( 'type' => $this->input->post( $mem->key . 'other_sp_offering_' . $x . '_ty'));
									$this->db->insert( 'offering_type', $data );
								}
						}
						$x--;
					endwhile;
				endif;
				// finished entering other offering information
				
				
				// RECORD INDIVIDUAL VISITOR ATTENDANCE AND OFFERINGS
				
					$this->db->where( 'service', $service_num );
					$this->db->delete( 'service_visitors' );
				
				
				$num_of_visitors_offering_specified = $this->input->post('visitor_form_num');
				$nvos = 0;
				if( $nvos > $num_of_visitors_offering_specified ){ $num_of_visitors_offering_specified = 3; }
				while( $nvos <= $num_of_visitors_offering_specified ): 
				if( $this->input->post( 'visitor_service_' . $nvos ) || $this->input->post( 'visitor_communion_' . $nvos ) || $this->input->post( 'visitor_bc_' . $nvos ) || $this->input->post( 'visitor_ss_' . $nvos ) || $this->input->post( 'visitors_names_' . $nvos ) ):
	
					if( $this->input->post( 'visitors_names_' . $nvos ) ) {
						$visitor_name = $this->input->post( 'visitors_names_' . $nvos );
					} else {
						$visitor_name = 'Visitor_' . $nvos ;
					}
					$data = array(
									'service' => $service_num,
									'date' => $date,
									'names' => $visitor_name,
									'home_church' => $this->input->post('visitors_home_church_' . $nvos ),
									'contact_info' => $this->input->post('visitors_contact_info_' . $nvos ),
									'ws' => $this->input->post( 'visitor_service_' . $nvos ),
									'ls' => $this->input->post( 'visitor_communion_' . $nvos ),
									'bc' => $this->input->post( 'visitor_bc_' . $nvos ),
									'ss' => $this->input->post( 'visitor_ss_' . $nvos ),
								);
					$this->db->insert( 'service_visitors', $data );
	
				endif;
					$nvos++;
				endwhile;
				
				//define variables for attendance and offering
				$attendance['member'] = 0;
				$attendance['visitor'] = 0;
				$attendance['communion'] = 0;
				$attendance['bibleclass'] = 0;
				$attendance['sundayschool'] = 0;
				$offerings = array();
				
				// Calculate Totals
				foreach( $members_ar as $mem ):
					
					$this->db->where( 'status', $mem->status );
					$mso = $this->db->get( 'member_status' );
					$ms = $mso->row();
					if( !empty( $ms ) ){
						if( $ms->member ){ $status = 'member'; } else { $status = 'visitor' ; } 
					} else {
						 $status = 'visitor' ; 
					}
					
					$this->db->where( 'service_key', $service_num );
					$this->db->where( 'member', $mem->key );
					$mematto = $this->db->get( 'membertoservice' );
					$mematt = $mematto->row();
					if( !empty($mematt) ):
						if( $mematt->service ){ $attendance[$status]++; }
						if( $mematt->communion ){ $attendance['communion']++; }
						if( $mematt->bibleclass ){ $attendance['bibleclass']++; }
						if( $mematt->sundayschool ){ $attendance['sundayschool']++; }
					endif;
					
					$this->db->where( 'service', $service_num );
					$this->db->where( 'member', $mem->key );
					$memoffo = $this->db->get( 'offerings' );
					$memoffs = $memoffo->result();
					if( !empty($memoffs )):foreach( $memoffs as $memoff ):
						if( isset( $offerings[$memoff->fund] ) ){
							$offerings[$memoff->fund] = $offerings[$memoff->fund] + $memoff->amount ;
						} else {
							$offerings[$memoff->fund] = $memoff->amount ;
						}
					endforeach; endif;
				endforeach;
				
				// count "other offerings" offerings
	
				$this->db->where( 'service', $service_num );
				$this->db->where( 'member', 0 );
				$memoffo = $this->db->get( 'offerings' );
				$memoffs = $memoffo->result();
				if( !empty($memoffs )):foreach( $memoffs as $memoff ):
					if( isset( $offerings[$memoff->fund] ) ){
						$offerings[$memoff->fund] = $offerings[$memoff->fund] + $memoff->amount ;
					} else {
						$offerings[$memoff->fund] = $memoff->amount ;
					}
				endforeach; endif;
	
				$this->db->where( 'service', $service_num );
				$svao = $this->db->get( 'service_visitors' );
				$sva = $svao->result();
				if( !empty( $sva ) ) : foreach( $sva as $svisit ):
					$attendance['visitor'] = $attendance['visitor']+$svisit->ws ; 
					$attendance['communion'] = $attendance['communion']+$svisit->ls ; 
					$attendance['bibleclass'] = $attendance['bibleclass']+$svisit->bc ; 
					$attendance['sundayschool'] = $attendance['sundayschool']+$svisit->ss ; 
				endforeach; endif;
				
				$attendance['total'] = $attendance['visitor'] + $attendance['member'] ;
				$offering_total = 0;
				foreach( $offerings as $offering ):
					$offering_total = $offering_total + $offering ;
				endforeach;
				
				$offerings['total'] = $offering_total;
				
				// CREATE ARRAY OF OFFERINGS BY FUND TO BE STORED 
				
					$offerings_funds = 'offerings: <br>';
					foreach( $offerings as $k => $v ):
						$offerings_funds .= ' ' .$k. ': $'.$v.' <br>';
					endforeach;
				
				// OVERRIDE NUMBERS IF TOTALS ARE SUPPLIED
				
				$total_offering = $this->input->post( 'offering' );
				$total_attendance = $this->input->post( 'total_attendance' );
				$communion_attendance = $this->input->post( 'communion_attendance' );
				$bc_attendance = $this->input->post( 'bc_attendance' );
				$ss_attendance = $this->input->post( 'ss_attendance' );
				$member_attendance = $this->input->post( 'member_attendance' );
				$visitor_attendance = $this->input->post( 'visitor_attendance' );
				
				if( !empty( $total_offering ) ){ $offerings['total'] =  $total_offering; }
				if( !empty( $total_attendance ) ){ $attendance['total'] =  $total_attendance; }
				if( !empty( $communion_attendance ) ){ $attendance['communion'] =  $communion_attendance; }
				if( !empty( $bc_attendance ) ){ $attendance['bibleclass'] =  $bc_attendance; }
				if( !empty( $ss_attendance ) ){ $attendance['sundayschool'] =  $ss_attendance; }
				if( !empty( $member_attendance ) ){ $attendance['member'] =  $member_attendance; }
				if( !empty( $visitor_attendance ) ){ $attendance['visitor'] =  $visitor_attendance; }
				
				// RECORD GENERAL SERVICE INFORMATION
				
			
				$dataa = array (
								'date' => $date,
								'offering_details' => $offerings_funds,
								'offering' => $offerings['total'],
								'member_attendance' => $attendance['member'],
								'visitor_attendance' => $attendance['visitor'],
								'total_attendance' => $attendance['total'],
								'communion_attendance' => $attendance['communion'],
								'bc_attendance' => $attendance['bibleclass'],
								'ss_attendance' => $attendance['sundayschool'],
								);
				
				$this->db->where( 'key', $service_num );
				$this->db->update( 'service', $dataa ); 
				
				$flashdata = $this->Forms->view_this_service( $service_num );		
				$this->session->set_flashdata('notice', $flashdata);	
				
				redirect( 'services/view_service/'.$service_num );		
	
		
			}
		}	
	}

	
	
	
	
	function delete_services () {

	$data = $this->Site->site_options ();

	$this->db->order_by("date", "desc"); 
	$data['services_obj'] = $this->db->get('service');	
		
	$this->load->view('services/delete_services', $data);
	$this->load->view('footer');

	}
	
	
	
	
	
	
	function delete_delete_services () {
		
	$ser_num = $this->uri->segment(3);
	
 
 	$this->db->where( 'key', $ser_num );
	$this->db->delete('service');
	
 	$this->db->where( 'service', $ser_num );
	$this->db->delete('offerings');
		
 	$this->db->where( 'service_key', $ser_num );
	$this->db->delete('membertoservice');

 	$this->db->where( 'service_key', $ser_num );
	$this->db->delete('membertoservice');
	
 	$this->db->where( 'service', $ser_num );
	$this->db->delete('service_visitors');


		$flashdata = 'This servcie has been successfully wiped from the system. ALL data pertaining to this service is now gone.';		
		$this->session->set_flashdata('notice', $flashdata);	
		
		redirect( 'services/delete_services' );		

	}
	
	
	
}