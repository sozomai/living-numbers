<?php
/*	NOTES

This is a test

To Do yet

	Add lines for visitors so that it can be indicated whether they attended Service, Lord's Supper, Bible Class etc. Rather than clicking on LS or BC, allow numbres to be entered so that if it is a family that attended they do not need to all be listed seperately, but how many were in each service can still be calculated.
	
	If visitors attended Lord's Supper include them in year end report so that the pastor can contact their pastor.


*/

class Options extends CI_Controller {
	
	
	function options()
	{
		parent::__construct();
		
		$this->load->library('session');
		$this->load->helper('cookie');
		$this->load->helper('url');
		$this->load->model('Forms');
		$this->load->model('Site');
		$this->load->database();
		$data = $this->Site->site_options();
		
		$data['query_links'] = $this->db->get('links');
		$data['header'] = $this->db->get('site_options');
		
		$this->load->view('head', $data);		
		
	}
		
	
	function index ()
	{	
			
	
	$this->load->view('options/index', $data);
	$this->load->view('footer');
	
	}
	
	
	
	
	function site_options () {
		
	$data['options_obj'] = $this->db->get('site_options');	
	$this->db->where( 'name', 'chk_timezone' );
	$usd_obj = $this->db->get('site_options');
	$usd_array = $usd_obj->result();
	$data['chk_timezone'] = $usd_array[0];	
	
	$this->load->helper('file');
	$data['css_files'] = get_dir_file_info('./jquery-ui/css', false);
	
	$this->db->where( 'name', 'timezone' );
	$optobj = $this->db->get('site_options');
	$optarr = $optobj->result();

	   $tz_array['Quick List'] = array ( 1 => '---------------',
	   									 'America/Anchorage' => 'America/Anchorage',
	   									 'America/Boise' => 'America/Boise',
										 'America/Cayenne' => 'America/Cayenne', 
										 'America/Chicago' => 'America/Chicago', 
										 'America/Denver' => 'America/Denver', 
										 'America/Detroit' => 'America/Detroit', 
										 'America/Edmonton' => 'America/Edmonton', 
										 'America/Indiana/Indianapolis' => 'America/Indiana/Indianapolis', 
										 'America/Kentucky/Louisville' => 'America/Kentucky/Louisville', 
										 'America/Kentucky/Monticello' => 'America/Kentucky/Monticello', 
										 'America/Los_Angeles' => 'America/Los_Angeles', 
										 'America/Montreal' => 'America/Montreal', 
										 'America/New_York' => 'America/New_York', 
										 'America/North_Dakota/Center' => 'America/North_Dakota/Cente', 
										 'America/North_Dakota/New_Salem' => 'America/North_Dakota/New_Salem', 
										 'America/Phoenix' => 'America/Phoenix', 
										 'America/Vancouver' => 'America/Vancouver', 
										 'America/Winnipeg'  => 'America/Winnipeg',
										 2 => '---------------',
										 );
		$tz_array['Exhaustive List'] = array( 3=>'---------------', 4=>'' );	
		
	$ct=0;
	$tz = timezone_identifiers_list();
	foreach( $tz as $zone ){
		$zone_ar = explode( '/', $zone );
		$tz_array[$zone_ar[0]][$zone] = $zone;
		$ct++;
	}
	
	$data['timezone_list'] = $tz_array;	
	
	$this->db->order_by( 'key' );
	$mstatus_obj = $this->db->get('member_status');
	$data['mstatus'] = $mstatus_obj->result();
	
	$fpso = $this->db->get('frontpage_stats');
	$data['functions'] = $fpso->result_array();

	
	$this->load->view('options/site_options', $data);
	$this->load->view('footer');
	
	}
	
	
	
	
	
	
	
	function edit_site_options () {
		
	$data_ar = $this->Site->site_options();
	
	$mstatus_obj = $this->db->get('member_status');
	$mstatuses = $mstatus_obj->result();
	
	$fpso = $this->db->get('frontpage_stats');
	$functions = $fpso->result_array();

	
		$this->load->helper('form');
		
		$this->load->library('form_validation');
		
		// set validation rules
		$this->form_validation->set_rules('site_name', 'Site Name', 'required|xss-clean');	
		$this->form_validation->set_rules('site_title', 'Site Title', 'required|xss-clean');	
		$this->form_validation->set_rules('site_plug', 'Site Plug', 'xss-clean');	
		$this->form_validation->set_rules('site_theme', 'Site Theme', 'required|xss-clean');	
		$this->form_validation->set_rules('help_menu', 'Help Menus', 'required|xss-clean');	
		$this->form_validation->set_rules('last_month', 'Last Month', 'required|is_natural');	
		$this->form_validation->set_rules('timezone', 'Your Timezone', 'required|xss-clean');	
		$this->form_validation->set_rules('server_timezone', 'Server Timezone', 'required|xss-clean');	
		foreach( $functions as $function ):
		$this->form_validation->set_rules($function['name'].'_visible', 'xss-clean');	
		$this->form_validation->set_rules($function['name'].'_order', 'integer');	
		$this->form_validation->set_rules($function['name'].'_other_data', 'alpha_numeric');				
		endforeach;

		foreach( $mstatuses as $status ):
			$this->form_validation->set_rules( 'member_status' . $status->key, 'Member Status:'.$status->status, 'xss-clean');	
			$this->form_validation->set_rules( 'member_member' . $status->key, 'Member Status:'.$status->member, 'xss-clean');	
			$this->form_validation->set_rules( 'member_communicate' . $status->key, 'Member Status:'.$status->member, 'xss-clean');	
			$this->form_validation->set_rules( 'member_list' . $status->key, 'Member List:'.$status->member, 'xss-clean');	
		endforeach;
		$x = $this->input->post( 'num_new_statuses' );
		if( $x > 0 ){ 
			$this->form_validation->set_rules( 'num_new_statuses' , 'New Statuses', 'integer');		
			while( $x > 0 ){
				$this->form_validation->set_rules( 'member_status_new_'.$x , 'New Status '.$x, 'xss-clean');	
				$this->form_validation->set_rules( 'member_member_new_'.$x , 'New Status '.$x, 'integer');	
				$this->form_validation->set_rules( 'member_communicate_new_'.$x , 'New Status '.$x, 'integer');				$x--;
				$this->form_validation->set_rules( 'member_list'.$x , 'Member List:'.$x , 'xss-clean');	
			}
		}
		$this->form_validation->set_rules('sp_view_totals', 'View Totals', 'xss-clean');
		$this->form_validation->set_rules('sp_view_members' , 'View Members', 'xss-clean');	
		$this->form_validation->set_rules('sp_view_visitors', 'View Visitors', 'xss-clean');	
		$this->form_validation->set_rules('sp_view_attendance', 'View Attendance', 'xss-clean');	
		$this->form_validation->set_rules('sp_view_offerings', 'View Offerings', 'xss-clean');	
		$this->form_validation->set_rules('sp_view_notes', 'View Notes', 'xss-clean');	
		$this->form_validation->set_rules('sp_view_home_church', 'View Home Church', 'xss-clean');	
		$this->form_validation->set_rules('sp_view_contact_info', 'View Contact Info', 'xss-clean');	
		$this->form_validation->set_rules('sort_members', 'Sort Members', 'alpha_numeric');	
		$this->form_validation->set_rules('visitor_fields', 'Num of Visitor Fields', 'is_natural');	
		$this->form_validation->set_rules('up_user_name', 'FTP Username', 'xss-clean');	
		$this->form_validation->set_rules('up_password', 'FTP Password', 'matches[up_password2]');	
		$this->form_validation->set_rules('up_password2', 'FTP Password', 'matches[up_password]');	
		$this->form_validation->set_rules('up_servername', 'Server Name', 'xss-clean');	
				
		if ( $this->form_validation->run() == FALSE ){
		
			$this->site_options();
						
		} else {
					
			foreach( $data_ar as $k => $v ){
			 if( $k != 'version' ) {
				
				$this->db->where( 'name', $k );
				
				$data = array ( 'value' => $this->input->post( $k ) );
				
				$this->db->update( 'site_options', $data );	
			 }	
			}
			
			date_default_timezone_set( $this->input->post( 'server_timezone') );
			$this->db->where( 'name', 'chk_timezone');
			$this->db->delete( 'site_options' ); 
			$data = array( 'name'=>'chk_timezone', 'value'=> date( 'Y-m-d H:i:s', time()) );
			$this->db->insert( 'site_options', $data ); 
			
			foreach( $mstatuses as $status ):
				$name = $this->input->post( 'member_status' . $status->key );
				if(empty($name)){
					$this->db->where( 'key', $this->input->post( 'member_key' . $status->key ) );
					$this->db->delete( 'member_status' );				
				} else {
					$data = array( 
								'status' => $this->input->post( 'member_status' . $status->key ),
								'member' => $this->input->post( 'member_member' . $status->key ),
								'communicate' => $this->input->post( 'member_communicate' . $status->key ),
								'list' => $this->input->post( 'member_list' . $status->key ),
								);
								
					$this->db->where('key', $this->input->post( 'member_key' . $status->key ) );
					$this->db->update( 'member_status', $data );
				}
				
			endforeach;	
			$nnms = 1;
			while( $nnms <= $this->input->post( 'num_new_statuses' ) ){
				$data = array( 
							'status' => $this->input->post( 'member_status_new_' . $nnms ),
							'member' => $this->input->post( 'member_member_new_' . $nnms ),
							'communicate' => $this->input->post( 'member_communicate_new_' . $nnms ),
							'list' => $this->input->post( 'member_list_new_' . $nnms ),
							);
							
				$this->db->insert( 'member_status', $data );
				$nnms++;
			}
			
			
			foreach( $functions as $function ):
				$data = array( 
								'visible' => $this->input->post($function['name'].'_visible'),	
								'order' => $this->input->post($function['name'].'_order'),
								'other_data' => $this->input->post($function['name'].'_other_data'),
							);
				
				$this->db->where('key', $function['key'] );
				$this->db->update('frontpage_stats', $data);
			endforeach;
			
			
			$flashdata = 'Site Options successfully updated.'; 				
			$this->session->set_flashdata('notice', $flashdata);	
			
			redirect('options/site_options');
			
		}
	}
	
	
	
	
	
	
	
	
	function edit_database() {
	
	$data = $this->Site->site_options();
	
	$this->load->helper('file');
	$filez = get_filenames('./files/data/');
	$ct=0;
	$file_data = array();
	foreach( $filez as $file ) {
		$fdata = get_file_info('./files/data/'.$file);
		$fdata['mime'] = get_mime_by_extension($file);
		
		$file_data[$fdata['mime']][$ct] = array(
							'date' => $fdata['date'],
							'mime' => $fdata['mime'],							
							'name' => $file,
							'size' => $fdata['size'],
							'path' => $fdata['server_path'],
							);
		$ct++;
		asort( $file_data[$fdata['mime']] );	
	}
	
	$data['file_array']	= $file_data;
	$this->load->view('options/edit_database', $data);
	$this->load->view('footer');
	
	}
	
	
	
	
	
	
	
	function backup_database() {
	
	
	$this->db->where('name', 'timezone' );
	$timeza = $this->db->get('site_options');
	$timez = $timeza->result();
	$timezone = $timez[0]->value;
	
	date_default_timezone_set($timezone);
	
	$filename = date('Y-j-m_his') . 'fulldatabackup.sql';
		
	$prefs = array(
                'tables'      => array(),  // Array of tables to backup.
                'ignore'      => array('site_options'),           // List of tables to omit from the backup
                'format'      => 'txt',        // gzip, zip, txt
                'filename'    => $filename,    // File name - NEEDED ONLY WITH ZIP FILES
                'add_drop'    => TRUE,         // Whether to add DROP TABLE statements to backup file
                'add_insert'  => TRUE,         // Whether to add INSERT data to backup file
              );

	
	// Load the DB utility class
	$this->load->dbutil();
	
	// Backup your entire database and assign it to a variable
	$backup = $this->dbutil->backup($prefs);
	$backup .= '# END OF BACKUP!';
	
	// Load the file helper and write the file to your server
	$this->load->helper('file');
	write_file('./files/data/' . $filename, $backup );
	
	// Load the download helper and send the file to your desktop
	$this->load->helper('download');
	force_download($filename, $backup); 
	
	}


	
	
	function restore_database () {
		
	$config['upload_path'] = './files/data/';
		
	$this->load->library('upload', $config);
	
	$this->load->dbforge();
	
	$this->load->helper('file');
	
		if ( ! $this->upload->do_upload() )
		{
			$flashdata = array('error' => $this->upload->display_errors());
						
			$this->session->set_flashdata('notice', $flashdata);	
			
			redirect( 'options/edit_database' );		
		}	
		else
		{
			$updata = array('upload_data' => $this->upload->data());
	
			$file_content = read_file($updata['full_path']);
			
			$query_list = explode(";", $file_content);
		
			foreach($query_list as $query) {
					$this->db->query($query);  
			 }
				
				$flashdata = 'Databases Restored!';		
				$this->session->set_flashdata('notice', $flashdata);	
			
				redirect( 'options/edit_database' );		
				
		}		
	}
	
	
	
	
	
	function save_data () {
	
	
	$this->db->where('name', 'up_save_data');
	$obj = $this->db->get('site_options');
	$array = $obj->result();
	$last_uploaded_data = $array[0]->value;
	
	$this->db->where('name', 'down_save_data');
	$obj = $this->db->get('site_options');
	$array = $obj->result();
	$last_downloaded_data = $array[0]->value;
	
	$tables = $this->db->list_tables();
	
	$sd = '';
	foreach( $tables as $table ):
		$this_table_ob = $this->db->get( $table );
		$this_table_ar = $this_table_ob->result();
		$de = '';
		foreach( $this_table_ar as $te => $td ):
			$dp = '';
			foreach( $td as $k => $v ):
				$dp .= '~key~' . $k . '~value~' . $v;
			endforeach;
			$de .= '~row~' . $te . $dp;
		endforeach;
		$sd .= '~table~' . $table . $de;
	endforeach;
	
	$this->db->where('name', 'timezone' );
	$timeza = $this->db->get('site_options');
	$timez = $timeza->result();
	$timezone = $timez[0]->value;
	
	date_default_timezone_set($timezone);
	
	$filename = date('Y-j-m') . '_saveddata.txt';

	
	// Load the file helper and write the file to your server
	$this->load->helper('file');
	write_file('./files/data/' . $filename, $sd );
	
	// Load the download helper and send the file to your desktop
	$this->load->helper('download');
	force_download($filename, $sd); 

	}
	
	
	
	
	
	function restore_save_data () {
		
	$config['upload_path'] = './files/data/';
	$config['allowed_types'] = 'txt';
	$this->load->helper('form');	
	$this->load->library('upload', $config);
	
	$this->load->dbforge();
	
	$this->load->helper('file');
	
		if ( ! $this->upload->do_upload() )
		{
			$flashdata = $this->upload->display_errors();
			$this->session->set_flashdata('notice', $flashdata);	
						
			redirect( 'options/edit_database' );
		}	
		else
		{
			$updata = $this->upload->data();
	
			$file_content = read_file($updata['full_path']);
			
			$query_list = explode("~table~", $file_content);
		
			foreach($query_list as $table) {
					$rows = explode("~row~", $table);
					foreach($rows as $row){
						if( $rows[0] == $row ){ continue; } else {
							$datas = explode("~key~", $row);
							foreach( $datas as $data ){
								if( $datas[0] == $data ){ continue; } else {
									$values = explode("~value~", $data);
									$d[$values[0]] = $values[1];
								}
							}
							$r[$datas[0]] = $d ;
							unset( $d );
						}
					}
					if( isset( $r ) ){
						$t[$rows[0]]=$r;
						unset( $r );
					}
			 }
			 
			 			 
			//compare new data to old database and create list of changes
			$ct=0;
			$output = '<table>';	 
			foreach( $t as $table => $rows ): if( $table != 'familytomember' && $table != 'links' && $table != 'membertoservice' && $table != 'offerings' && $table != 'service_visitors' && $table != 'site_options' ):
				if( !empty( $rows ) ):
				foreach( $rows as $data ):
					$this->db->where( 'key', $data['key'] );
					$old_data_obj = $this->db->get( $table );
					if( $old_data_obj->num_rows() > 0 ){
						$old_data = $old_data_obj->row_array();
						if( $old_data != $data ):
							if( !isset( $ltabel ) || $ltable != $table ){ $output .= '<tr><td colspan="5"><h3>'.$table.'</h3></td></tr>'; }
							$ltable = $table;
							//check to see which data set is older
							if( $data['mod_date'] > $old_data['mod_date'] ){ 
								$merge_up = true; $merge_down = false; $add = false; $skip = false; 
							} else {  
								$merge_up = false; $merge_down = true; $add = false; $skip = false; 
							}
							//check to see if any similarity between data sets other than key
							$tx=0;
							foreach( $data as $k => $v ){
								if( $k != 'key' && $k != 'notes' && $k != 'order' && !empty($v) && !empty( $old_data[$k] )){
									if( $v == $old_data[$k] ){ $tx++;  }
								}
							}
							if( $tx == 0 ){ $merge_up = false; $merge_down = false; $add = true; $skip = false;  }
							$output .= '<tr><td rowspan="2" class="update_data_buttonset">';
							$output .= form_hidden( 'table_row_'.$ct, $table );
							$output .= form_hidden( 'od_key_row_'.$ct, $old_data['key'] );
							$output .= form_hidden( 'nd_key_row_'.$ct, $data['key'] );
							$output .= form_radio('row_'.$ct, 'merge_down', $merge_down, 'id="merge_down_'.$ct.'"');
							$output .= '<label for="merge_down_'.$ct.'"><span class="ui-button-icon-primary ui-icon ui-icon-arrowthick-1-s"></span></label>';
							$output .= form_radio('row_'.$ct, 'merge_up', $merge_up, 'id="merge_up_'.$ct.'"');
							$output .= '<label for="merge_up_'.$ct.'"><span class="ui-button-icon-primary ui-icon ui-icon-arrowthick-1-n"></span></label>';
							$output .= form_radio('row_'.$ct, 'add', $add, 'id="add_'.$ct.'"');
							$output .= '<label for="add_'.$ct.'"><span class="ui-button-icon-primary ui-icon ui-icon-plusthick"></span></label>';
							$output .= form_radio('row_'.$ct, 'skip', $skip, 'id="skip_'.$ct.'"');
							$output .= '<label for="skip_'.$ct.'"><span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span></label></td>';
							foreach( $data as $k => $v ):
								$output .= '<td>'.$k.'</td>';
							endforeach;
								$output .= '</tr><tr>';
							foreach( $data as $k => $v ):
								$output .= '<td>';
								$output .= form_hidden( 'od_'.$k.'_row_'.$ct, $old_data[$k] );
								$output .= form_hidden( 'nd_'.$k.'_row_'.$ct, $v );								
								$output .= $old_data[$k];
								if( $v != $old_data[$k] ):
									$output .= '<br><span class="ui-state-default ui-corner-all ui-button"><span class="ui-button-icon-primary ui-icon ui-icon-arrowthick-2-n-s"></span></span><br><span class="ui-state-highlight ui-corner-all" style="padding:5px;">';
									$output .= $v;
									$output .= '</span>';
								endif;
								$output .= '</td>';
							endforeach;
							$output .= '</tr>';	
							$ct++;					 
						endif;
					} else {

							if( !isset( $ltabel ) || $ltable != $table ){ $output .= '<tr><td colspan="5"><h3>'.$table.'</h3></td></tr>'; }
							$this->db->order_by( 'key' );
							$all_data = $this->db->get($table);
							$end_row = $all_data->last_row();
							$last_key = $end_row->key;
							$all_data->free_result();
							if( $data['key'] < $last_key ){ $add = false; $skip = true; } else { $add = true; $skip = false; }
							
							$ltable = $table;			
							$output .= '<tr><td rowspan="2" class="update_data_buttonset">';
							$output .= form_hidden( 'table_row_'.$ct, $table );							
							$output .= form_hidden( 'nd_key_row_'.$ct, $data['key'] );
							$output .= form_radio('row_'.$ct, 'add', $add, 'id="add_'.$ct.'"');
							$output .= '<label for="add_'.$ct.'"><span class="ui-button-icon-primary ui-icon ui-icon-plusthick"></span></label>';
							$output .= form_radio('row_'.$ct, 'skip', $skip, 'id="skip_'.$ct.'"');
							$output .= '<label for="skip_'.$ct.'"><span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span></label></td>';
							foreach( $data as $k => $v ):
								$output .= '<td>'.$k.'</td>';
							endforeach;
								$output .= '</tr><tr>';
							foreach( $data as $k => $v ):
								$output .= '<td>';
								$output .= form_hidden( 'nd_'.$k.'_row_'.$ct, $v );								
								$output .= $v.'</td>';
							endforeach;
							$output .= '</tr>';						 
							$ct++;
					}
					
						
				endforeach; endif;
			endif; endforeach;	
			$output .= form_hidden( 'num_rows', $ct );
			$output .= form_hidden( 'file', $updata['full_path'] );
			$output .= '</table>' ;
			
			$data['output'] = $output;

			$this->load->view('options/upload_data', $data);
			$this->load->view('footer');	
		}		
	}
	
	
	
	function restore_save_data_step_2 () {
	
		$this->Site->backup_all_data();
	
		$this->load->helper('form');	
		$this->load->dbforge();
		$this->load->helper('file');
	
	
		$file_content = read_file($this->input->post('file'));
		
		$query_list = explode("~table~", $file_content);
	
		foreach($query_list as $table) {
				$rows = explode("~row~", $table);
				foreach($rows as $row){
					if( $rows[0] == $row ){ continue; } else {
						$datas = explode("~key~", $row);
						foreach( $datas as $data ){
							if( $datas[0] == $data ){ continue; } else {
								$values = explode("~value~", $data);
								$d[$values[0]] = $values[1];
							}
						}
						$r[$datas[0]] = $d ;
						unset( $d );
					}
				}
				if( isset( $r ) ){
					$t[$rows[0]]=$r;
					unset( $r );
				}
		 }
		 
		 //Enter Data from previous step into Database
		
		$ct=0;
		$row = $this->input->post( 'num_rows' )-1;
		if( $row > 0 ):	
		while( $row > 0 ):
			if( $this->input->post( 'row_' . $row ) == 'merge_up' ){ 
			
				$table = $this->input->post( 'table_row_'.$row );
			
				$fields = $this->db->list_fields( $table );
				unset( $data );
				foreach( $fields as $field ):
					if( $field != 'key' && $field != 'mod_date' && $field != 'order' && $field != 'notes' ){
						$data[$field] = $this->input->post( 'nd_'.$field.'_row_'.$row );
					} elseif ( $field == 'notes' ){
						$data[$field] = $this->input->post( 'od_'.$field.'_row_'.$row ) . $this->input->post( 'nd_'.$field.'_row_'.$row );
					}
				endforeach;
									
				$this->db->where( 'key', $this->input->post( 'od_key_row_'.$row ) );
				$this->db->update( $table, $data );
				
				if( $table == 'service' ){
					$this->db->where( 'service', $this->input->post( 'od_key_row_'.$row ) );
					$this->db->delete( 'offerings' );
					$this->db->where( 'service', $this->input->post( 'od_key_row_'.$row ) );
					$this->db->delete( 'service_visitors' );
				};		 
						 
				$change_data[$table]['edit'][$ct] = array( 
												'table' => $table, 
												'key' => $this->input->post( 'nd_key_row_'.$row ), 
												'to' => $this->input->post( 'od_key_row_'.$row ),
												'mod' => 'merge_up',
												'data' => $data,
												);
												
			} elseif( $this->input->post( 'row_' . $ct ) == 'merge_down' ){ 
			
				$table = $this->input->post( 'table_row_'.$row );
			
				$fields = $this->db->list_fields( $table );
				foreach( $fields as $field ):
					if( $field != 'key' && $field != 'mod_date' && $field != 'order' && $field != 'notes' ){
						$data[$field] = $this->input->post( 'od_'.$field.'_row_'.$row );
					} elseif ( $field == 'notes' ){
						$data[$field] = $this->input->post( 'od_'.$field.'_row_'.$row ) . $this->input->post( 'nd_'.$field.'_row_'.$row );
					}
				endforeach;
															 
				$change_data[$table]['edit'][$ct] = array( 
												'table' => $table, 
												'key' => $this->input->post( 'nd_key_row_'.$row ), 
												'to' => $this->input->post( 'od_key_row_'.$row ),
												'mod' => 'merge_down',
												'data' => $data,
												);
												
			} elseif( $this->input->post( 'row_' . $ct ) == 'add' ){ 
			
				$table = $this->input->post( 'table_row_'.$row );
			
				$fields = $this->db->list_fields( $table );
				unset( $data );
				foreach( $fields as $field ):
					if( $field != 'key' && $field != 'mod_date' && $field != 'order' ){
						$post_info = 'nd_'.$field.'_row_'.$row;
						$data[$field] = $this->input->post( $post_info );
					}
				endforeach;
				$this->db->insert( $table, $data );
									
				$new_data_ob = $this->db->get_where( $table, $data );
				$new_data = $new_data_ob->row();
						 
				$change_data[$table]['edit'][$ct] = array( 
												'table' => $table, 
												'key' => $this->input->post( 'nd_key_row_'.$row ), 
												'to' => $new_data->key,
												'mod' => 'add',
												'data' => $data,
												);
				
			
			} elseif( $this->input->post( 'row_' . $ct ) == 'skip' ){ 
			
				$fields = $this->db->list_fields( $table );
				unset( $data );
				foreach( $fields as $field ):
					if( $field != 'key' && $field != 'mod_date' && $field != 'order' ){
						$data[$field] = $this->input->post( 'nd_'.$field.'_row_'.$row );
					}
				endforeach;

			
				$change_data[$table]['skip'][$ct] = array( 
					'table' => $table, 
					'key' => $this->input->post( 'nd_key_row_'.$row ), 
					'data' => $data,
					);
				

			}
			
			$row--;
			$ct++;
		endwhile;
		endif;
		
		
		foreach( $t as $table => $rows ): 
			if( $table == 'familytomember' ){
				if( !empty( $rows ) ):
				foreach( $rows as $data ):
					$skip = false;
					if( isset( $change_data['families']['skip'] )):
					foreach( $change_data['families']['skip'] as $change ):
						if( $data['family'] == $change['key'] ) { 
							$skip = true; 
						} 
					endforeach;
					endif;
					if( isset( $change_data['members']['skip'] )):
					foreach( $change_data['members']['skip'] as $change ):
						if( $data['member'] == $change['key'] ) { 
							$skip = true; 
						} 
					endforeach;
					endif;
				
					if( $skip ) { continue; }
					else { 
						$family_key = $data['family'];
						if( isset( $change_data['families']['edit'] )):
						foreach( $change_data['families']['edit'] as $change ):
							if( $data['family'] == $change['key'] ) { 
								$family_key = $change['to']; 
							}
						endforeach;
						endif;
						$member_key = $data['member'];
						if( isset( $change_data['members']['edit'] )):
						foreach( $change_data['members']['edit'] as $change ):
							if( $data['member'] == $change['key'] ) { 
								$member_key = $change['to']; 
							}
						endforeach;
						endif;
						
						unset( $data );
						$this->db->where( 'family', $family_key );
						$this->db->where( 'member', $member_key );
						$query = $this->db->get( 'familytomember' );
						$num_rows = $query->num_rows();
						if( $num_rows == 0 ){
							$data = array( 'family' => $family_key, 'member' => $member_key );
							$this->db->insert( 'familytomember', $data ); 
						}
					}
				
				endforeach;
				endif;
			} elseif ( $table == 'membertoservice' ) {
				if( !empty( $rows ) ):
				foreach( $rows as $data ):
				
					$skip = false;
					if( isset( $change_data['service']['skip']) ):
					foreach( $change_data['service']['skip'] as $change ):
						if( $data['service_key'] == $change['key'] ) { 
							$skip = true; 
						} 
					endforeach;
					endif;
					if( isset( $change_data['members']['skip']) ):
					foreach( $change_data['members']['skip'] as $change ):
						if( $data['member'] == $change['key'] ) { 
							$skip = true; 
						} 
					endforeach;
					endif;
				
					if( $skip ) { continue; }
					else { 
						$service_key = $data['service_key'];
						if( isset( $change_data['service']['edit']) ):
						foreach( $change_data['service']['edit'] as $change ):
							if( $data['service_key'] == $change['key'] ) { 
								$service_key = $change['to'];
								$mod = $change['mod']; 
							}
						endforeach;
						endif;
						$member_key = $data['member'];
						if( isset( $change_data['members']['edit']) ):
						foreach( $change_data['members']['edit'] as $change ):
							if( $data['member'] == $change['key'] ) { 
								$member_key = $change['to']; 
							}
						endforeach;
						endif;
						
						unset( $data );
						$data['service_key'] = $service_key; 
						$data['member'] = $member_key;
						foreach( $data as $k => $v ):
						if( $k != 'key' && $k != 'service_key' && $k != 'member' && $k != 'mod_date' ):
							$data[$k] = $v;
						endif;
						endforeach;

						
						$this->db->where( 'service_key', $service_key );
						$this->db->where( 'member', $member_key );
						$query = $this->db->get( 'membertoservice' );
						$num_rows = $query->num_rows();
						if( $num_rows == 0 ){
							$this->db->insert( 'membertoservice', $data ); 
						} elseif( $mod == 'merge_up' ) {
							$this->db->where( 'service_key', $service_key );
							$this->db->where( 'member', $member_key );
							$this->db->update( 'membertoservice', $data );
						}
					}
				
				
				endforeach;
				endif;
			} elseif ( $table == 'offerings' ) {
				if( !empty( $rows ) ):
				foreach( $rows as $data ):

					$skip = false;
					if( isset( $change_data['service']['skip']) ):
					foreach( $change_data['service']['skip'] as $change ):
						if( $data['service_key'] == $change['key'] ) { 
							$skip = true; 
						} 
					endforeach;
					endif;
					if( isset( $change_data['members']['skip']) ):
					foreach( $change_data['members']['skip'] as $change ):
						if( $data['member'] == $change['key'] ) { 
							$skip = true; 
						} 
					endforeach;
					endif;
				
					if( $skip ) { continue; }
					else { 
						$service_key = $data['service'];
						if( isset( $change_data['service']['edit']) ):
						foreach( $change_data['service']['edit'] as $change ):
							if( $data['service'] == $change['key'] ) { 
								$service_key = $change['to'];
								$mod = $change['mod']; 
							}
						endforeach;
						endif;
						$member_key = $data['member'];
						if( isset( $change_data['members']['edit']) ):
						foreach( $change_data['members']['edit'] as $change ):
							if( $data['member'] == $change['key'] ) { 
								$member_key = $change['to']; 
							}
						endforeach;
						endif;
						
						unset( $data );
						$data['service'] = $service_key; 
						$data['member'] = $member_key;
						foreach( $data as $k => $v ):
						if( $k != 'key' && $k != 'service' && $k != 'member' && $k != 'mod_date' ):
							$data[$k] = $v;
						endif;
						endforeach;

						if( $mod == 'merge_up' || $mod == 'add' ){
							$this->db->insert( 'membertoservice', $data ); 
						} 
					}
				
				
				endforeach;
				endif;
			} elseif ( $table == 'service_visitors' ) {
				if( !empty( $rows ) ):
				foreach( $rows as $data ):
				

					$skip = false;
					if( isset( $change_data['service']['skip']) ):
					foreach( $change_data['service']['skip'] as $change ):
						if( $data['service'] == $change['key'] ) { 
							$skip = true; 
						} 
					endforeach;
					endif;
				
					if( $skip ) { continue; }
					else { 
						$service_key = $data['service'];
						if( $change_data['service']['edit'] ):
						foreach( $change_data['service']['edit'] as $change ):
							if( $data['service'] == $change['key'] ) { 
								$service_key = $change['to'];
								$mod = $change['mod']; 
							}
						endforeach;
						endif;
						
						$data['service'] = $service_key; 
						foreach( $data as $k => $v ):
						if( $k != 'key' && $k != 'service' && $k != 'mod_date' ):
							$data[$k] = $v;
						endif;
						endforeach;

						
						if( $mod == 'merge_up' || $mod == 'add' ){
							$this->db->insert( 'membertoservice', $data ); 
						} 
					}

				
				endforeach;
				endif;
			}
		endforeach;		
		
		$flashdata = '<p>The uploaded data has been saved to the database. You may want to check to make sure that all data was correctly entered. Especially the families and services listed below. If you wish to maintain integrity between this system and the system(s) whence came the entered data, it is EXTREMLY ADVISABLE that once you are down here you save a backup of this system and restore it on your other system(s).</p><ul> ';		
		
		foreach( $change_data[$table]['edit'] as $change ):
			if( $change['table'] == 'families' ){
				$flashdata .= '<li>Family: '.$change['data']['family_name'].'</li>';
			} elseif ( $change['table'] == 'service' ){
				$flashdata .= '<li>Service: '.$change['data']['type'].' '.$change['data']['date'].' '.$change['data']['name'].'</li>';			
			}
		endforeach; 	
		$flashdata .= '</ul>';
		$this->session->set_flashdata('notice', $flashdata);	
		
		redirect( 'options/edit_database' );		


	}
	
	
	
		
	
	
	function download_data_file () {
	$this->load->helper('file');
	$this->load->helper('download');
	$name =  $this->uri->segment(3);
	$sFile =  str_replace( '.9.', '(', $name );
	$sFile =  str_replace('.0.', ')', $sFile );
	$data = file_get_contents( './files/data/' . $sFile ); // Read the file's contents
	$name = $sFile;
	force_download($name, $data); 
	}
	
	
	
	
	function download_report_file () {
	$this->load->helper('file');
	$this->load->helper('download');
	$name =  $this->uri->segment(3);
	$data = file_get_contents( './files/report/'.$name  ); // Read the file's contents
	force_download($name, $data); 
	}
	
	
	function database_files () {
	
	$this->load->helper('file');
	$this->load->helper('download');

	if( $this->input->post( 'num_rows' ) > 0 ):
		$ct = $this->input->post( 'num_rows' );
		while( $ct > 0 ):
			if( $this->input->post( 's_'.$ct ) ): 
				$name = $this->input->post( 's_'.$ct );
				$data = file_get_contents( './files/data/'.$name ); 
				force_download($name, $data); 
			endif;
			if( $this->input->post( 'd_'.$ct ) ): 
				$name = $this->input->post( 's_'.$ct );
				$path = './files/data/'.$name ; 
				delete_files($path);
			endif;
		
		$ct--;
		endwhile;	
	endif;	
	
	
	}
	
	
	function check_update () {
		$this->load->helper('file');
		$this->load->helper('url');
		$this->load->view('options/check_update');
		$this->load->view('footer');
	
	}

	
}