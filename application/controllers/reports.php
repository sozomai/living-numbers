<?php
/*	NOTES

To Do yet

	Add lines for visitors so that it can be indicated whether they attended Service, Lord's Supper, Bible Class etc. Rather than clicking on LS or BC, allow numbres to be entered so that if it is a family that attended they do not need to all be listed seperately, but how many were in each service can still be calculated.
	
	If visitors attended Lord's Supper include them in year end report so that the pastor can contact their pastor.


*/

class Reports extends CI_Controller {
	
	
	function reports() {
		parent::__construct();
		
		$this->load->library('session');
		
		$this->load->helper('cookie');
		
		$this->load->helper('url');
		
		$this->load->model('Forms');

		$this->load->model('Site');

		$this->load->model('mReports');

		
		$this->load->database();
		
		$data['query_links'] = $this->db->get('links');
		$data['header'] = $this->db->get('site_options');
		$this->load->view('head', $data);		
		
	}
		
	
	function index () {	
	
	$data = $this->Site->return_family_groups();
	$this->load->library('table');	
	
	if( $this->input->post( 'print_reports' ) ){
	
		//calculate total number of services so I don't have to redo it for each member
		$beg_date_str = strtotime ( $this->input->post( 'beg_date' ) );
		$end_date_str = strtotime( $this->input->post( 'end_date' ) );
					
		$mtCount = 0; $mwtCount = 0; $tMcount = 0; $tVcount = 0; $tCcount = 0; $tScount = 0; $tBcount = 0;

		$res = $this->db->get('service');
		$tServices = $res->result();
		foreach( $tServices as $serv ){
			$sDate = strtotime( $serv->date ); 
			if( $sDate >=  $beg_date_str && $sDate <= $end_date_str ){
				$mtCount++;
				if( $serv->type == 'Worship Service' ){
					$mwtCount++;
					$tMcount = $tMcount + $serv->member_attendance;
					$tVcount = $tVcount + $serv->visitor_attendance;
					$tCcount = $tCcount + $serv->communion_attendance;
					$tScount = $tScount + $serv->ss_attendance;
					$tBcount = $tBcount + $serv->bc_attendance;
				}
			}
		}
		$tServices = array( 'service' => $mtCount, 'worship' => $mwtCount );

		if ( $this->input->post( 'inc_service' ) ){ $inc_service = 1; } else { $inc_service = 0; }
		if ( $this->input->post( 'inc_offering' ) ){ $inc_offering = 1; } else { $inc_offering = 0; }
		if ( $this->input->post( 'inc_stats' ) ){ $inc_stats = 1; } else { $inc_stats = 0; }
				
		foreach( $data['grp_by_fam'] as $gp){
			if( !empty($gp['members'][0]['key']) ){
				foreach( $gp['members'] as $mem ){
					if( $this->input->post( 'rev_checks' ) ){
						if( !$this->input->post( $mem['key'] ) ){ 

							$data['print_data'][$mem['key']] = $this->mReports->member_report( $mem['key'] , $this->input->post( 'beg_date' ), $this->input->post( 'end_date' ), $inc_service, $inc_offering, $inc_stats, $tServices  )  ; 
						}					
					} else {
						if( $this->input->post( $mem['key'] ) ){ 

							$data['print_data'][$mem['key']] = $this->mReports->member_report( $mem['key'] , $this->input->post( 'beg_date' ), $this->input->post( 'end_date' ), $inc_service, $inc_offering, $inc_stats, $tServices  )  ; 
						}					
					}
				}
			}
		}
		$this->load->helper('inflector');
		$fileST = '';$fileSE = '';$fileO = '';
		
		
		
		if( isset($data['print_data']) ){ 
			foreach( $data['print_data'] as $gFam ){
				
				$name =  '"'.$gFam['lname'].', '.$gFam['fname'].'",';

				if( !empty( $gFam['stats'] )){
					$fileSTh = ',';
					foreach( $gFam['stats']  as $k => $v ){ $fileSTh .= humanize($k).',' ; }
					$fileSTh .= "\n";
					$fileST .= $name;
					foreach( $gFam['stats']  as $k => $v ){ $fileST .= $v.',' ; }
					$fileST .= "\n";
				}

				
				if( !empty( $gFam['services'] )){	
					$count = 0;
					$fileSEh = ',Date,Type,Name,Service,Communion,Bible Class,Sunday School'."\n";
					$fileSE .= $name."\n";
					foreach( $gFam['services'] as $serv ){ $fileSE .= ','.$serv['date'].','.$serv['type'].','.$serv['name'].','.$serv['service'].','.$serv['communion'].','.$serv['bibleclass'].','.$serv['sundayschool'].','."\n"; }
				}
				
				if( !empty( $gFam['offerings'] ) ){
					$count = 0;
					$fileOh = 'Name,Date,Fund,Amount'."\n";
					
					foreach( $gFam['offerings'] as $k => $serv ){ if( $k!='totals' ){ $fileO .= $name.$serv->date.','.$serv->fund.','.$serv->amount."\n"; }}
					$fileO .= "\n".$name.'TOTALS'."\n".',';
					foreach( $gFam['offerings']['totals'] as $k => $v ){ if( $k != 'total_offerings'){ $fileO .= $k.','; }}
					$fileO .= 'Total Offerings Given'."\n".',';
					foreach( $gFam['offerings']['totals'] as $k => $v ){ if( $k != 'total_offerings'){ $fileO .= $v.','; }}
					$fileO .= $gFam['offerings']['totals']['total_offerings']."\n";
				}
			
			}
		}
				
		$file = $data['site_title'].': '.$data['site_plug']."\n";
		$file .= 'Detailed Member Report'."\n".'From,'.date( '"M jS, Y"', $beg_date_str).',To,'.date( '"M jS, Y"',$end_date_str)."\n"."\n"."\n";
		if( $this->input->post('inc_chstats') && $mtCount > 0 && $mwtCount > 0 ){
			$file .= 'CHURCH STATISTICS'."\n"."\n";
			$file .= 'Total Services, Total Worship Services, Average Attendance, Average Member Attendance, Total Sunday School Attendance, Total Bible Class Attendance'."\n";
			$file .= $mtCount .','. $mwtCount .','. ($tVcount+$tMcount)/$mwtCount .','. $tMcount/$mwtCount .','. $tScount .','. $tBcount .','."\n";
			
			//retrieve list of visitors who recieved lords supper
			$this->db->where('ls', 1);
			$res1 = $this->db->get('service_visitors');
			$lsServs = $res1->result_array();
			if( $res1->num_rows() > 0 ){ 
				foreach( $lsServs as $serv ){
					$sDate = strtotime( $serv['date'] ); 
					if( $sDate >=  $beg_date_str && $sDate <= $end_date_str ){
						$lsVisitors[$serv['names']][$serv['key']] = $serv ; 
					}
				}
				
				if( isset( $lsVisitors )){
				
					$file .= "\n".'VISITORS WHO RECIEVED COMMUNION'."\n";
					$file .= "\n".'Name(s),Date,Home Church,Contact Info'."\n";
					foreach( $lsVisitors as $k => $visitor ){ 
						$file .= $k.',';
						$ct=0;
						foreach( $visitor as $serv ){
							if( $ct==0){ $file .= $serv['date'].','.$serv['home_church'].','.$serv['contact_info']."\n"; }
							else{ $file .= ','.$serv['date'].','.$serv['home_church'].','.$serv['contact_info']."\n"; }
							$ct++;
						}
					}
				}
			}
		}
		if( isset( $fileSTh ) ){
			$file .= "\n".'STATISTICS'."\n"."\n";
			$file .= $fileSTh;
			$file .= $fileST;
			$file .= "\n";
		}
		if( isset( $fileOh ) ){
			$file .= 'OFFERING INFORMATION'."\n"."\n";
			$file .= $fileOh;
			$file .= $fileO;
			$file .= "\n";
		}
		if( isset( $fileSEh ) ){
			$file .= 'SERVICES ATTENDED'."\n"."\n";
			$file .= $fileSEh;
			$file .= $fileSE;
			$file .= "\n";
		}
		
		$this->load->helper('download');
		force_download('myfile.csv', $file);
	
	}
	
	$this->load->view('reports/index', $data);
	$this->load->view('footer');	
	}
	
	
	function member_reports () {
	
		
	
	}
			
	
	
	
}