<?php
/*	NOTES

To Do yet

	Add lines for visitors so that it can be indicated whether they attended Service, Lord's Supper, Bible Class etc. Rather than clicking on LS or BC, allow numbres to be entered so that if it is a family that attended they do not need to all be listed seperately, but how many were in each service can still be calculated.
	
	If visitors attended Lord's Supper include them in year end report so that the pastor can contact their pastor.


*/

class Records extends CI_Controller {
	
	
	function records()
	{
		parent::__construct();
		
		$this->load->library('session');
		$this->load->helper('cookie');
		$this->load->helper('url');
		$this->load->model('Forms');
		$this->load->model('Site');
		$this->load->database();
		$data = $this->Site->site_options();
		
		$data['query_links'] = $this->db->get('links');
		$data['header'] = $this->db->get('site_options');
		
		date_default_timezone_set($data['timezone']);
		
		$this->db->select('family_name');
		$this->db->order_by('family_name', 'asc');
		$family_names = $this->db->get('families');
		$data['families'] = $family_names->result();
		
		$this->db->select('key, lname, fname');
		$this->db->order_by('lname');
		$this->db->order_by('fname');
		$members = $this->db->get('members');
		$data['members'] = $members->result();
		
		$this->db->select('status');
		$mstatus = $this->db->get('member_status');
		$data['mstatus'] = $mstatus->result();		

		$this->load->view('head', $data);		
	}
		
	
	function view ()
	{	
	
	$year = $this->uri->segment(3, date('Y') );
	if( $year == 'type' ){
		$type = $this->uri->segment(4);
		$this->db->where( 'type', $type);
		$this->db->order_by('date', 'desc');
		$records = $this->db->get('records');
		$data['records'] = $records->result_array();	
	} else {
		$cur_date = date('Y-m-d', strtotime('01-01-'.$year) );
		$end_date = date('Y-m-d', strtotime($cur_date)+31556926 );
		$this->db->where( 'date >', $cur_date);
		$this->db->where( 'date <', $end_date);
		$this->db->order_by('type', 'asc');
		$this->db->order_by('date', 'desc');
		$records = $this->db->get('records');
		$data['records'] = $records->result_array();
	}
	
	$this->db->order_by('date');
	$this->db->select('date, type');
	$yearso = $this->db->get( 'records' );
	$yeara = $yearso->result();
	foreach( $yeara as $yea ):
		$y = date( 'Y', strtotime( $yea->date ));
		$data['years'][$y] = $y;
		$data['types'][$yea->type] = $yea->type;
	endforeach;
	
	$this->load->view('records/view', $data);
	$this->load->view('footer');
	
	}
	
	

	function other () {
	
		$this->load->view('records/other');
		$this->load->view('footer');
	}

	function baptism () {
	
		$this->load->view('records/baptism');
		$this->load->view('footer');
	}

	function funeral () {
	
		$this->load->view('records/funeral');
		$this->load->view('footer');
	}

	function confirmation () {
	
		$this->load->view('records/confirmation');
		$this->load->view('footer');
	}
	
	function adult_conf () {
	
		$this->load->view('records/adult_conf');
		$this->load->view('footer');
	}

	function termination () {
	
		$this->load->view('records/termination');
		$this->load->view('footer');
	}

	function transfer () {
	
		$this->load->view('records/transfer');
		$this->load->view('footer');
	}

	function wedding () {
	
		$this->load->view('records/wedding');
		$this->load->view('footer');
	}
	
	
	function add_record () {
	
		$config['upload_path'] = './files/records/';
		$this->load->helper('form');	
		$this->load->library('upload', $config);
		$this->load->helper('file');
		$this->load->library('form_validation');
		$page = $this->input->post('page');
		$type = $this->input->post('type');
		$status = $this->input->post('status');
		
		// set validation rules
		$this->form_validation->set_rules('church', 'Church', 'trim|xss-clean');	
		$this->form_validation->set_rules('type', 'Type', 'trim|required|xss-clean');	
		$this->form_validation->set_rules('date', 'Date', 'required|xss-clean');	
		$this->form_validation->set_rules('born', 'Born', 'xss-clean');	
		if( $type == 'wedding' ){
			$this->form_validation->set_rules('name', 'Name', 'trim|xss-clean|callback_chk_name_format');
			$this->form_validation->set_rules('bride', 'Bride', 'trim|required|xss-clean|callback_chk_name_format');
			$this->form_validation->set_rules('groom', 'Groom', 'trim|required|xss-clean|callback_chk_name_format');
		} else {
			$this->form_validation->set_rules('name', 'Name', 'trim|required|xss-clean|callback_chk_name_format');
			$this->form_validation->set_rules('bride', 'Bride', 'trim|xss-clean|callback_chk_name_format');
			$this->form_validation->set_rules('groom', 'Groom', 'trim|xss-clean|callback_chk_name_format');
		}
		if( $status == 'add_to' ){
			$this->form_validation->set_rules('fgroup', 'Family Group', 'trim|required|xss-clean|callback_chk_names');	
			$this->form_validation->set_rules('mstatus', 'Member Status', 'trim|required|xss-clean');
		} else {
			$this->form_validation->set_rules('fgroup', 'Family Group', 'trim|xss-clean');	
			$this->form_validation->set_rules('mstatus', 'Member Status', 'trim|xss-clean');		
		}
		$this->form_validation->set_rules('parents', 'Parents', 'trim|xss-clean');	
		$this->form_validation->set_rules('brides_parents', 'Parents of the Bride', 'trim|xss-clean');	
		$this->form_validation->set_rules('grooms_parents', 'Parents of the Groom', 'trim|xss-clean');	
		$this->form_validation->set_rules('witness_one', 'Witness One', 'trim|xss-clean');	
		$this->form_validation->set_rules('witness_two', 'Witness Two', 'trim|xss-clean');	
		$this->form_validation->set_rules('official', 'Official', 'trim|xss-clean');	
		$this->form_validation->set_rules('passage', 'Passage', 'trim|xss-clean');	
		$this->form_validation->set_rules('sermon', 'Written Sermon', 'xss-clean');	
		$this->form_validation->set_rules('asermon', 'Audio Sermon', 'xss-clean');	
		$this->form_validation->set_rules('reason', 'Reason', 'xss-clean');	
		$this->form_validation->set_rules('notes', 'Notes', 'xss-clean');	

		$this->form_validation->set_message('chk_names', 'There is already another member with this same last AND first name. Either change the name or choose to "Change" the members status and/or family group rather than to add a new one.');
		$this->form_validation->set_message('chk_name_format', 'Names must be in the following format: Last Name, First Name(s) eg. Smith, John Frederick . DO NOT use commas other than between the last and first names.');
		
		
		if ( $this->form_validation->run() == FALSE ){
		
			$this->$page();
					
		} else {
		

			$file_data = array('upload_data' => $this->upload->data());
			
			$fields = $this->db->list_fields('records');
			foreach( $fields as $field ):
				$data[$field] = $this->input->post($field);
			endforeach;
			
			$data['date'] = date( 'Y-m-d', strtotime( $this->input->post('date')));
			$data['sermon'] = '';
			$data['asermon'] = '';
			
			$this->db->insert( 'records', $data );
			
			$name = $this->input->post('name');
			$status = $this->input->post('status');
			$mstatus =  $this->input->post('mstatus');
			$fgroup =  $this->input->post('fgroup');
			$bride = $this->input->post('bride');
			$groom = $this->input->post('groom');
			$datedata=array();
			if( $type == 'funeral' ){
				$datedata['death'] = date( 'Y-m-d', strtotime($this->input->post('died')));
			}
			if( $type == 'baptism' ){
				$datedata['baptism'] = $data['date'];
				$datedata['dob'] = date( 'Y-m-d', strtotime($this->input->post('born')));
			}
			if( $type == 'wedding' ){
				$datedata['anniversary'] = $data['date'];
			}
			
			
			if(!empty($name)):
				$this->add_or_change_mem( $status, $name, $mstatus, $fgroup, $datedata );
			endif;
			if(!empty($bride)):
				$this->add_or_change_mem( $status, $bride, $mstatus, $fgroup, $datedata );
			endif;
			if(!empty($groom)):
				$this->add_or_change_mem( $status, $groom, $mstatus, $fgroup, $datedata );
			endif;
			
			
			$upload_error = $this->upload->display_errors();

			$this->session->set_flashdata('notice', $upload_error);

			
			redirect( 'records/'.$page );			
		
		}
	}
	
	
	function chk_names () {

		$names = $this->input->post( 'name' );
		$names = explode( ',', $names );
		$lname = trim( $names[0] );
		$fname = trim( $names[1] );		
		$chk = $this->db->get_where('members', array('lname' => $lname, 'fname' => $fname ));
		if( $chk->num_rows() > 0 ) { 			
			return false; 
		} else { 
			return true;
		}				
	}
	
	function add_or_change_mem ( $status, $name, $mstatus='', $fgroup='', $dates=array() ) {
	
		if( $status == 'add_to' ){
		
			$names = $name;
			$names = explode( ',', $names );
			$lname = trim( $names[0] );
			$fname = trim( $names[1] );
			
			
			$data = array( 
						'lname' => $lname,
						'fname' => $fname,
						'status' => $mstatus,
						);
			foreach( $dates as $k =>$v ):
				$data[$k]=$v;
			endforeach;
			
			$this->db->insert('members', $data );
			
			$this->db->where('lname', $lname);
			$this->db->where('fname', $fname);
			$membero = $this->db->get('members');
			$member = $membero->row_array();
			
			$this->db->where('status', $mstatus);
			$mstatso = $this->db->get('member_status');
			if( $mstatso->num_rows() < 1 ) {
				$data['status'] = $mstatus;
				$this->db->insert('member_status', $data );
			}
				
			
			
			$this->db->where('family_name' , $fgroup );
			$familyo = $this->db->get('families');
			$family = $familyo->row_array();
			
			if( empty( $family ) ){
			
				$datafn['family_name'] = $fgroup;
				$this->db->insert('families', $datafn );
				
				$this->db->where('family_name' , $fgroup );
				$familyo = $this->db->get('families');
				$family = $familyo->row_array();
			
			} 
			
			$data = array(
						'family' => $family['key'],
						'member' => $member['key'],
						);
			$this->db->insert('familytomember', $data );
				
			
		} elseif ($status == 'change_to') {
		
			$names = $name;
			$names = explode( ',', $names );
			$lname = trim( $names[0] );
			$fname = trim( $names[1] );
			
			$this->db->where('lname', $lname);
			$this->db->where('fname', $fname);
			$membero = $this->db->get('members');
			$member = $membero->row_array();
						
			if( empty($member) ){
			
				$data = array( 
							'lname' => $lname,
							'fname' => $fname,
							'status' => $mstatus,
							);
				foreach( $dates as $k =>$v ):
					$data[$k]=$v;
				endforeach;

				$this->db->insert('members', $data );
				
				$this->db->where('lname', $lname);
				$this->db->where('fname', $fname);
				$membero = $this->db->get('members');
				$member = $membero->row_array();
									
				
			} else {
				if( !empty($mstatus) ):
				$dataa['status'] = $mstatus;
				endif;
				foreach( $dates as $k =>$v ):
					$dataa[$k]=$v;
				endforeach;
				if( isset( $dataa )):
				$this->db->where('key', $member['key']);
				$this->db->update('members', $dataa);
				endif;
			}
			
			
			if (!empty($mstatus)) {
				$this->db->where('status', $mstatus);
				$mstatso = $this->db->get('member_status');
				if( $mstatso->num_rows() < 1 ) {
					$datab['status'] = $mstatus;
					$this->db->insert('member_status', $datab );
				}
			}
						
			if(!empty($fgroup)):
			
				$this->db->where('family_name' , $fgroup );
				$familyo = $this->db->get('families');
				$family = $familyo->row_array();
				
				if(empty($family)):
					$datac['family_name'] = $fgroup;
					$this->db->insert('families',$datac);

					$this->db->where('family_name' , $fgroup );
					$familyo = $this->db->get('families');
					$family = $familyo->row_array();
					
				endif;
				
				$data = array(
							'family' => $family['key'],
							'member' => $member['key'],
							);
				$this->db->where('member', $member['key']);
				$this->db->delete('familytomember');
				$this->db->insert('familytomember', $data );
			
			endif;
			
			$new_lname = $this->input->post('new_last_name');
			if(!empty($new_lname)):
				$datad['lname']=$new_lname;
				$this->db->where('key', $member['key']);
				$this->db->update('members', $datad);			
			endif;
		
		}

	
	}


	
	function add_conf () {
	
		$config['upload_path'] = './files/records/';
		$this->load->helper('form');	
		$this->load->library('upload', $config);
		$this->load->helper('file');
		$this->load->library('form_validation');
		$page = $this->input->post('page');
		$type = $this->input->post('type');
		$status = $this->input->post('status');
		$num_conf = $this->input->post('num_conf');
		
		// set validation rules
		$this->form_validation->set_rules('type', 'Type', 'trim|required|xss-clean');	
		$this->form_validation->set_rules('date', 'Date', 'required|xss-clean');	
		$this->form_validation->set_rules('num_conf', 'Number of Conf', 'is_natural');	
		$x=0;
		while($x<$num_conf):
		$this->form_validation->set_rules('name'.$x , 'Name '.$x, 'trim|xss-clean|callback_chk_name_format');
		$this->form_validation->set_rules('passage'.$x , 'Passage '.$x, 'trim|xss-clean');
		$x++;		
		endwhile;
		$this->form_validation->set_rules('mstatus', 'Member Status', 'trim|xss-clean');		
		$this->form_validation->set_rules('official', 'Official', 'trim|xss-clean');	
		$this->form_validation->set_rules('sermon', 'Written Sermon', 'xss-clean');	
		$this->form_validation->set_rules('asermon', 'Audio Sermon', 'xss-clean');	
		$this->form_validation->set_rules('reason', 'Reason', 'xss-clean');	
		$this->form_validation->set_rules('notes', 'Notes', 'xss-clean');	

		$this->form_validation->set_message('chk_name_format', 'Names must be in the following format: Last Name, First Name(s) eg. Smith, John Frederick . DO NOT use commas other than between the last and first names.');
		
		
		if ( $this->form_validation->run() == FALSE ){
		
			$this->confirmation();
					
		} else {
		

			$file_data = array('upload_data' => $this->upload->data());
			
			$fields = $this->db->list_fields('records');
			foreach( $fields as $field ):
				$data[$field] = $this->input->post($field);
			endforeach;

			$data['date'] = date( 'Y-m-d', strtotime( $this->input->post('date') ));
			$data['sermon'] = '';
			$data['asermon'] = '';
			
			$status = $this->input->post('status');
			$mstatus =  $this->input->post('mstatus');
			$datedata['confirmation'] = $data['date'];
			
			$x=0;
			while($x<$num_conf):
				$data['name'] = $this->input->post('name'.$x);
				$data['passage'] = $this->input->post('passage'.$x);
				if( !empty($data['name']) ):
					$this->db->insert( 'records', $data );
					$this->add_or_change_mem( $status, $data['name'], $mstatus, '', $datedata );
				endif;
				$x++;
				unset( $data['name'], $data['passage'] );
			endwhile;			
			
			$upload_error = $this->upload->display_errors();

			$this->session->set_flashdata('notice', $upload_error);

			
			redirect( 'records/'.$page );			
		
		}
	}
	
	function chk_name_format ( $name ){
		if(empty($name)){
			return true;
		} else {
			$names = explode(',', $name);
			
			if( !empty($names[1]) ){ return true; } else { return false; }
		}
	}

	function transfer_members () {
	
		$this->load->helper('form');	
		$this->load->library('form_validation');
		$page = $this->input->post('page');
		$type = $this->input->post('type');
		$status = $this->input->post('status');
		$direction = $this->input->post('direction');
		$names = $this->input->post('names');
		$mstatus =  $this->input->post('mstatus');
		$fgroup =  $this->input->post('family_group');
		$date = date( 'Y-m-d', strtotime( $this->input->post('date')));
		
		// set validation rules
		$this->form_validation->set_rules('type', 'Type', 'trim|required|xss-clean');	
		$this->form_validation->set_rules('date', 'Date', 'required|xss-clean');	
		$this->form_validation->set_rules('church', 'To/From', 'required|xss-clean');	
		if( $direction == 'from' ){
			$this->form_validation->set_rules('names[]', 'Names', 'trim|required|xss-clean');
		} else {
			$this->form_validation->set_rules('names[]', 'Names', 'trim|xss-clean');
		}
		$this->form_validation->set_rules('family_group', 'Family Group', 'trim|required|xss-clean');	
		
		if( $status == 'add_to' ){
			$this->form_validation->set_rules('mstatus', 'Member Status', 'trim|required|xss-clean');
		} else {
			$this->form_validation->set_rules('mstatus', 'Member Status', 'trim|xss-clean');		
		}
		$this->form_validation->set_rules('notes', 'Notes', 'xss-clean');			
		
		if ( $this->form_validation->run() == FALSE ){
		
			$this->$page();
					
		} else {
		
			if( $direction == 'to' ){
			
				$this->db->where('family_name', $fgroup );
				$family_grpn_ob = $this->db->get('families');
				$family_grpn = $family_grpn_ob->row();
				$grpn = $family_grpn->key;
				
				$this->db->where( 'family', $grpn );
				$mem_res = $this->db->get( 'familytomember' );
				$mem_array = $mem_res->result_array();
				$ct=0;
				foreach( $mem_array as $mem ){
					$this->db->where( 'key', $mem['member'] );
					$mem_res = $this->db->get('members');
					$member = $mem_res->row_array();
					$names[$ct] = $member['lname'] .", ". $member['fname'] ;
					$ct++;
				}
				
			} 
			
			foreach( $names as $name ){
		
				$data['type'] = $this->input->post('type');
				$data['name'] = $name;
				$data['direction'] = $direction;
				$data['church'] = $this->input->post('church');				
				$data['date'] = $date;

				$this->db->insert( 'records', $data );
				
				$datedata=array();					
				
				if(!empty($name)):
					$this->add_or_change_mem( $status, $name, $mstatus, $fgroup, $datedata );
				endif;
			
			}			
									
			redirect( 'records/'.$page );			
		
		}
	}
	
}