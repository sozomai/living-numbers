<?php
class Error extends CI_Controller {
 
	function error_404()
	{
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('session');
		
		$data['query_links'] = $this->db->get('links');
		$data['header'] = $this->db->get('site_options');
		$this->load->view('head', $data);		
		$this->load->view('error');
		$this->load->view('footer');
	}
}