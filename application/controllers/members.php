<?php
/*	NOTES

To Do yet

	Add lines for visitors so that it can be indicated whether they attended Service, Lord's Supper, Bible Class etc. Rather than clicking on LS or BC, allow numbres to be entered so that if it is a family that attended they do not need to all be listed seperately, but how many were in each service can still be calculated.
	
	If visitors attended Lord's Supper include them in year end report so that the pastor can contact their pastor.


*/

class Members extends CI_Controller {
	
	
	function members()
	{
		parent::__construct();
		
		$this->load->library('session');
		$this->load->helper('cookie');
		$this->load->helper('url');
		$this->load->model('Forms');
		$this->load->model('Site');
		$this->load->database();
		$data = $this->Site->site_options();
		
		$data['query_links'] = $this->db->get('links');
		$data['header'] = $this->db->get('site_options');
		
		date_default_timezone_set($data['timezone']);

		$this->load->view('head', $data);		
		
	}
		
	
	function index ()
	{	
	
	$this->load->view('members/index', $data);
	$this->load->view('footer');
	
	}
		
	
	

	function add_new_family () {	
	
		$this->load->helper('form');
		
		$this->load->library('form_validation');
		
		// set validation rules
		$this->form_validation->set_rules('family_name', 'Family Name', 'trim|required|xss_clean|callback_chk_familyname');	
		$this->form_validation->set_rules('address', 'Address', 'trim|xss_clean');	
		$this->form_validation->set_rules('city', 'City', 'trim|xss_clean');	
		$this->form_validation->set_rules('state', 'State', 'trim|xss_clean');	
		$this->form_validation->set_rules('zip', 'Zip', 'trim|xss_clean');	
		$this->form_validation->set_rules('hphone', 'Home Phone', 'trim|xss_clean');	
		$this->form_validation->set_rules('ophone', 'Other Phone', 'trim|xss_clean');	
		$this->form_validation->set_rules('notes', 'Notes', 'xss_clean');	
		$this->form_validation->set_message('chk_data', 'This family name is already in use. Please choose a different family name.');				
		if ( $this->form_validation->run() == FALSE ){

			// $this->add_family();
			$flashdata = validation_errors();
			
			$this->session->set_flashdata('notice', $flashdata);	
			redirect( 'members/families' );
				
		} else {

				$data = array(
						   'family_name' => $this->input->post('family_name'),
						   'address' => $this->input->post('address'),
						   'city' => $this->input->post('city'),
						   'state' => $this->input->post('state'),
						   'zip' => $this->input->post('zip'),
						   'hphone' => $this->input->post('hphone'),
						   'ophone' => $this->input->post('ophone'),
						);
						
				$notes = $this->input->post('notes');	
				if( !empty( $notes ) ){
					$data['notes'] = $notes;
				}
			
				$this->db->insert('families', $data); 
				
				$flashdata = ' ';
				foreach( $data as $d ){
					$flashdata .= $d . '<br /><br />';
					}
				$flashdata .= 'Has been successfully added to the database.';
				
				$this->session->set_flashdata('notice', $flashdata);	
				redirect( 'members/families' );
		}
	}
	
	
	function chk_familyname ( $data ) {
		$chk = $this->db->get_where('families', array('family_name' => $data ));
		if( $chk->num_rows() > 0 ){ return false; } else { return true; }
	}
	
	

	function families () {
	
	$this->db->order_by( 'family_name' );		
	$data['familylist'] = $this->db->get('families');
	
	$fam_num = $this->uri->segment(3) ;
		
	if( empty($fam_num) ){  
		$fields = $this->db->list_fields('families');
		foreach( $fields as $field ){
				$data['fam'][$field] = '';
		}
	} else {
	
		$fam_res = $this->db->get_where('families', array('key' => $fam_num) );
		$chk_results = $fam_res->result();
		
		if( empty( $chk_results) ){  
			$fields = $this->db->list_fields('families');
			foreach( $fields as $field ){
					$data['fam'][$field] = '';
			}		
		} else {
			$data['fam'] = $fam_res->row_array();
		}
	}	
	
		
	$this->load->view( 'members/families', $data );
	$this->load->view('footer');
		
	}




	function edit_edit_family () {
	
		$this->load->helper('form');
		
		$this->load->library('form_validation');
		
		// set validation rules
		$this->form_validation->set_rules('family_name', 'Family Name', 'trim|required|xss_clean|callback_chk_familynameedit');	
		$this->form_validation->set_rules('address', 'Address', 'trim|xss_clean');	
		$this->form_validation->set_rules('city', 'City', 'trim|xss_clean');	
		$this->form_validation->set_rules('state', 'State', 'trim|xss_clean');	
		$this->form_validation->set_rules('zip', 'Zip', 'trim|xss_clean');	
		$this->form_validation->set_rules('hphone', 'Home Phone', 'trim|xss_clean');	
		$this->form_validation->set_rules('ophone', 'Other Phone', 'trim|xss_clean');	
		$this->form_validation->set_rules('notes', 'Notes', 'xss_clean');	
		$this->form_validation->set_message('chk_familynameedit', 'This family name is already in use. Please choose a different family name.');
						
		if ( $this->form_validation->run() == FALSE ){

			$this->edit_family();
				
		} else {
			$data = array(
					   'family_name' => $this->input->post('family_name'),
					   'address' => $this->input->post('address'),
					   'city' => $this->input->post('city'),
					   'state' => $this->input->post('state'),
					   'zip' => $this->input->post('zip'),
					   'hphone' => $this->input->post('hphone'),
					   'ophone' => $this->input->post('ophone'),
					);
					
			$notes = $this->input->post('notes');	
			if( !empty( $notes ) ){
				$data['notes'] = $notes;
			}
			
					
			$where = "key = " . $this->input->post('key') . ""; 
		
			$this->db->update('families', $data, $where); 
			
			$flashdata = ' ';
			foreach( $data as $d ){
				$flashdata .= $d . '<br /><br />';
				}
			$flashdata .= 'Has been successfully updated.';	
			
			$this->session->set_flashdata('notice', $flashdata);	
			
			redirect( 'members/families/'.$this->input->post('key') );
		}
	}
	
	function chk_familynameedit ( $data ) {
		$key = $this->input->post('key');
		$chk = $this->db->get_where('families', array('family_name' => $data));
		$res = $chk->result();
		foreach( $res as $pos ){
			if( $pos->key != $key ){ return false; break; }
		}
		return true;
	}
	
	
	
	
	
	
	
	function add_new_member () {			
	
	
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		// set validation rules
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required|xss_clean');	
		$this->form_validation->set_rules('fname', 'First Name', 'trim|required|xss_clean|callback_chk_member_names');	
		$this->form_validation->set_rules('fgroup', 'Family Group', 'trim|required|xss_clean');	
		$this->form_validation->set_rules('dob', 'Birthday', 'trim|xss_clean');	
		$this->form_validation->set_rules('occupation', 'Occupation', 'trim|xss_clean');	
		$this->form_validation->set_rules('cphone', 'Cell Phone', 'trim|xss_clean');	
		$this->form_validation->set_rules('wphone', 'Work Phone', 'trim|xss_clean');	
		$this->form_validation->set_rules('email', 'Emil', 'trim|valid_emails|xss_clean');	
		$this->form_validation->set_rules('baptism', 'Baptism', 'trim|xss_clean');	
		$this->form_validation->set_rules('confirmation', 'Confirmation', 'trim|xss_clean');	
		$this->form_validation->set_rules('anniversary', 'Anniversary', 'trim|xss_clean');	
		$this->form_validation->set_rules('death', 'Death', 'trim|xss_clean');	
		$this->form_validation->set_rules('status', 'Status', 'trim|xss_clean');	
		$this->form_validation->set_rules('notes', 'Notes', 'xss_clean');	
		$this->form_validation->set_message('chk_member_names', 'There is already another member with this same last AND first name.');
						
		if ( $this->form_validation->run() == FALSE ){

			// $this->add_member();

			$flashdata = validation_errors();
			
			$this->session->set_flashdata('notice', $flashdata);	
			redirect( 'members/member' );
				
		} else {
		
				$data = array(
							 'lname' => $this->input->post('lname'),
							 'fname' => $this->input->post('fname'),
							 'occupation' => $this->input->post('occupation'),
							 'cphone' => $this->input->post('cphone'),
							 'wphone' => $this->input->post('wphone'),
							 'email' => $this->input->post('email'),
							 'status' => $this->input->post('status'),
						);
						
				$notes = $this->input->post('notes');	
				if( !empty( $notes ) ){
					$data['notes'] = $notes;
				}
						
				$data['dob'] = $this->input->post('dob');
				$data['baptism'] = $this->input->post('baptism');
				$data['confirmation'] = $this->input->post('confirmation');
				$data['anniversary'] = $this->input->post('anniversary');
				$data['death'] = $this->input->post('death');
			
				$this->db->insert('members', $data); 
				
				if( $this->input->post('fgroup') ) {
				
					$chk = $this->db->get_where('families', array('family_name' => $this->input->post('fgroup') ));
					if( $chk->num_rows() < 1 ){ 
						$ndata = array( 'family_name' => $this->input->post('fgroup') ); 
						$this->db->insert('families', $ndata );
					}

				
					$this->db->select('key');
					$this->db->where('lname', $this->input->post('lname'));
					$this->db->where('fname', $this->input->post('fname'));
					$member_keyr = $this->db->get('members');
					$member_key = $member_keyr->row();
					$mem_key = $member_key->key;
		
					$this->db->select('key');
					$this->db->where('family_name', $this->input->post('fgroup'));
					$family_keyr = $this->db->get('families');
					$family_key = $family_keyr->row();
					$fam_key = $family_key->key;
					
					$gdata = array(
					   'family' => $fam_key ,
					   'member' => $mem_key 
					);
		
					$this->db->insert('familytomember', $gdata); 
				}
				
				if( $this->input->post('status') ) {
					$chk = $this->db->get_where('member_status', array('status' => $this->input->post('status') ));
					if( $chk->num_rows() < 1 ){ 
						$sdata = array( 'status' => $this->input->post('status') ); 
						$this->db->insert('member_status', $sdata );
					}
				}
							
				$flashdata = ' ';
				foreach( $data as $d ){
					$flashdata .= $d . '<br /><br />';
					}
				$flashdata .= 'Has been successfully added to the database.';	

			$this->session->set_flashdata('notice', $flashdata);	
			redirect( 'members/member' );
			
		} 
	}

	function chk_member_names () {
			
		$chk = $this->db->get_where('members', array('lname' => $this->input->post('lname'), 'fname' => $this->input->post('fname')));
		if( $chk->num_rows() > 0 ) { 			
			return false; 
		} else { 
			return true;
		}				
	}
	
	
	
	
	function member () {
	
	$this->db->order_by( 'lname' );
	$this->db->order_by( 'fname' );
	$data['memberlist'] = $this->db->get('members');
	$data['familytomember'] = $this->db->get('familytomember');
	$data['familylist'] = $this->db->get('families');
	$data['statuslist'] = $this->db->get('member_status');
	
	$mem_num = $this->uri->segment(3) ;
	
		
	if( empty($mem_num) ){  
			$fields = $this->db->list_fields('members');
			foreach( $fields as $field ){
					$data['memberinfo'][$field] = '';
			}		
	} else {
	
		$chk = $this->db->get_where('members', array('key' => $mem_num) );
		$chk_results = $chk->result();
		
		if( empty( $chk_results) ){  
				$fields = $this->db->list_fields('members');
				foreach( $fields as $field ){
						$data['memberinfo'][$field] = '';
				}		
		} else {	
		
			$data['memberinfo'] = $chk->row_array();
		}
	}
	
	$this->load->view( 'members/members', $data );
	$this->load->view('footer');
		
	}
	
	
	
	
	
	
	
	function edit_edit_member () {
	
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		// set validation rules
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required|xss_clean');	
		$this->form_validation->set_rules('fname', 'First Name', 'trim|required|xss_clean|callback_chk_member_namesed');	
		$this->form_validation->set_rules('fgroup', 'Family Group', 'trim|required|xss_clean');	
		$this->form_validation->set_rules('dob', 'Birthday', 'trim|xss_clean');	
		$this->form_validation->set_rules('occupation', 'Occupation', 'trim|xss_clean');	
		$this->form_validation->set_rules('cphone', 'Cell Phone', 'trim|xss_clean');	
		$this->form_validation->set_rules('wphone', 'Work Phone', 'trim|xss_clean');	
		$this->form_validation->set_rules('email', 'Emil', 'trim|valid_emails|xss_clean');	
		$this->form_validation->set_rules('baptism', 'Baptism', 'trim|xss_clean');	
		$this->form_validation->set_rules('anniversary', 'Anniversary', 'trim|xss_clean');	
		$this->form_validation->set_rules('death', 'Death', 'trim|xss_clean');	
		$this->form_validation->set_rules('status', 'Status', 'trim|xss_clean');	
		$this->form_validation->set_rules('notes', 'Notes', 'xss_clean');	
		$this->form_validation->set_message('chk_member_namesed', 'There is already another member with this same last AND first name.');
						
		if ( $this->form_validation->run() == FALSE ){

			$this->edit_member();
				
		} else {

			$data = array(
						 'lname' => $this->input->post('lname'),
						 'fname' => $this->input->post('fname'),
						 'occupation' => $this->input->post('occupation'),
						 'cphone' => $this->input->post('cphone'),
						 'wphone' => $this->input->post('wphone'),
						 'email' => $this->input->post('email'),
						 'status' => $this->input->post('status'),
					);
					
			$notes = $this->input->post('notes');	
			if( !empty( $notes ) ){
				$data['notes'] = $notes;
			}
					
					
			$data['dob'] = $this->input->post('dob');
			$data['baptism'] =$this->input->post('baptism');
			$data['confirmation'] =$this->input->post('confirmation');
			$data['anniversary'] =$this->input->post('anniversary');
			$data['death'] =$this->input->post('death');
					
			$this->db->where('key', $this->input->post('key'));
			$this->db->update('members', $data); 
			
			if( $this->input->post('fgroup') ) {
			
				$chk = $this->db->get_where('families', array('family_name' => $this->input->post('fgroup') ));
				if( $chk->num_rows() < 1 ){ 
					$ndata = array( 'family_name' => $this->input->post('fgroup') ); 
					$this->db->insert('families', $ndata );
				}

				$mem_key = $this->input->post('key');
	
				$this->db->select('key');
				$this->db->where('family_name', $this->input->post('fgroup'));
				$family_keyr = $this->db->get('families');
				$family_key = $family_keyr->row();
				$fam_key = $family_key->key;
				
				$gdata = array(
				   'family' => $fam_key,
				   'member' => $mem_key 
				);
				
				$this->db->delete( 'familytomember', array( 'member' => $mem_key ));
				$this->db->insert('familytomember', $gdata); 
			}
			
			if( $this->input->post('status') ) {
				$chk = $this->db->get_where('member_status', array('status' => $this->input->post('status') ));
				if( $chk->num_rows() < 1 ){ 
					$sdata = array( 'status' => $this->input->post('status') ); 
					$this->db->insert('member_status', $sdata );
				}
			}
						
			$flashdata = ' ';
			foreach( $data as $d ){
				$flashdata .= $d . '<br /><br />';
				}
			$flashdata .= 'Has been successfully updated.';	
			$this->session->set_flashdata('notice', $flashdata);	
			redirect( 'members/member/'.$this->input->post('key') );
		}		
	}
	
	
	function chk_member_namesed () {
			
		$chk = $this->db->get_where('members', array('lname' => $this->input->post('lname'), 'fname' => $this->input->post('fname')));
		$res = $chk->result();
		if(!empty($res)): foreach( $res as $result ):
			if( $result->key != $this->input->post('key') ) { 
				return false; 
				break;
			} 
		endforeach; endif;
		return true;
	}
	
	
	
	function familygroups () {
	
	$data = $this->Site->return_family_groups ();
	
	$this->load->view('members/familygroups', $data);
	$this->load->view('footer');
	}
	

	
	
	
	
	
	function edit_familygroups () {
		
	$familygroups_ar_a =  get_cookie('familygroups');
	$familygroups_ar_b = strchr($familygroups_ar_a,'fam');
	$familygroups_ar_c = explode('fam[]=', $familygroups_ar_b);
	$ct = 0;
//	print_r( get_cookie('familygroups') );
	foreach( $familygroups_ar_c as $TP ){
		$TPA = str_replace( ',', '', $TP );
		$TPAN = strpos( $TPA, '&' ) ;
		$fam_num = substr($TPA,0,$TPAN) ;
		$TPF = substr( $TPA, $TPAN );
		$TPB = strchr($TPF,'&mem;[]=');
		$TPC = explode( '&mem;[]=', $TPB );
		foreach( $TPC as $xyz ){
				$familygroups_ar[$ct] = array( 'fn' => $fam_num, 'mn' => $xyz, 'order' => $ct, );
				$ct++;
		}
		
	}
	
	$familyorder_ar_a =  get_cookie('familyorder');
	$familyorder_ar_b = explode('ul[]=', $familyorder_ar_a);
	$ct=0;
	foreach( $familyorder_ar_b as $str ){
		$foc[$ct] = explode( '&ul;[]=', $str );
		foreach( $foc[$ct] as $var ){
			$fod[ $ct ] = $var;
		$ct++;
		}
		$ct++;
	}
			
	
	
		foreach( $familygroups_ar as $fam ){
			$this->db->where( 'member', $fam['mn'] );
			$this->db->delete( 'familytomember' );
			if( $fam['fn'] > 0 && $fam['mn'] > 0 ) {
				$data = array( 'family'	=> $fam['fn'], 'member' => $fam['mn'], 'mem_order' => $fam['order'] );
				$this->db->insert( 'familytomember', $data );
				$data = array( 'order' => $fam['order'] );
				$this->db->where( 'key', $fam['mn'] );
				$this->db->update( 'members', $data ); 
			}
		
		}
		$ct = 0;		
		foreach( $fod as $fami ){
			if( $fami != 0 ){
				$this->db->where( 'key', $fami );
				$this->db->update( 'families', array( 'order' => $ct ) );
				
				$this->db->where( 'family', $fami );
				$this->db->update( 'familytomember', array( 'fam_order' => $ct ) );
			}
			$ct++;
		}
		

	
	redirect( 'members/familygroups' ); 

	}
	
	
	
	function delete_members () {
		
 
	$this->db->order_by("lname", "asc"); 
	$this->db->order_by("fname", "asc"); 
	$data['members_obj'] = $this->db->get('members');	

	$this->db->order_by("family_name", "asc"); 
	$data['families_obj'] = $this->db->get('families');	
		
	$this->load->view('members/delete_members', $data);
	$this->load->view('footer');

	}
	
	
	
	
	
	
	
	function delete_delete_member () {
	
	
	$mem_num = $this->uri->segment(3);
	
	
 
 	$this->db->where( 'key', $mem_num );
	$this->db->delete('members');
	
 	$this->db->where( 'member', $mem_num );
	$this->db->delete('familytomember');

 	$this->db->where( 'member', $mem_num );
	$this->db->delete('membertoservice');
	
 	$this->db->where( 'member', $mem_num );
	$this->db->delete('offerings');
		
		$flashdata = 'This member has been successfully wiped from the system. ALL data pertaining to this individual is now gone.';		
		$this->session->set_flashdata('notice', $flashdata);	
		
		redirect( 'members/delete_members' );		

	}
	
	
	
	
	
	
	function delete_delete_family () {
	
	
	$mem_num = $this->uri->segment(3);
	
	
 
 	$this->db->where( 'key', $mem_num );
	$this->db->delete('families');
	
 	$this->db->where( 'family', $mem_num );
	$this->db->delete('familytomember');
		
		$flashdata = 'This member has been successfully wiped from the system. ALL data pertaining to this individual is now gone.';		
		$this->session->set_flashdata('notice', $flashdata);	
		
		redirect( 'members/delete_members' );		

	}
	
	
}