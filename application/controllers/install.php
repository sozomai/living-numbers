<?php
class Install extends CI_Controller {
	
	function install() {
		parent::__construct();
			
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->database();
	}		

	function index()
	{	
		$this->load->view('install/index');
	}

	function upgrade()
	{	
		$this->load->helper('file');
		$this->load->model('Site');
		$this->load->helper('path');
	
		$site_options = $this->Site->site_options();
		
			//update fileupgrade.php file and close ftp sesssion

		if( $site_options['up_use_ftp'] ){
						
			$this->load->library('ftp');
			$config['hostname'] = 'ftp.'.$site_options['up_hostname'];
			$config['username'] = $site_options['up_user_name'];
			$config['password'] = $site_options['up_password'];
			$config['debug'] = TRUE;
			$this->ftp->connect($config);
			
			$server_path = $site_options['up_servername'].'/system/';
			
			$files_get = file_get_contents('http://members.clclutheran.net/downloadable/controllers/fileupgrade.php.txt');
			write_file('./system/application/tmp/fileupgrade.php', $files_get );
			$this->ftp->move($server_path.'application/tmp/fileupgrade.php', $server_path.'application/fileupgrade.php' );
			
			$this->ftp->close();
		} else {		
			$files_get = file_get_contents('http://members.clclutheran.net/downloadable/controllers/fileupgrade.php.txt');
			write_file('./system/application/controllers/fileupgrade.php', $files_get );
		}	
	
		$delete_files = file_get_contents('http://members.clclutheran.net/downloadable/delete_files.txt');
		$delete_files = explode(';', $delete_files);
		foreach( $delete_files as $dfile ){
			$dfile = trim( $dfile );
			if( $dfile == 'DONE' ){
				break;
			} else {
				$contents = read_file('./system/application/'.$dfile);
				if( $contents != FALSE ){ unlink('./system/application/'.$dfile); }
			}
		}
		
		
		$dir = './system/application/tmp/';
		$tmp_path = get_dir_file_info( $dir );
		if( !empty($tmp_path) ):
			delete_files($dir);
		endif;		
	
		$this->load->view('install/upgrade');
	}
	
	function installer()
	{
	
	$this->load->dbforge();
	include( 'var.php' );
	
	$tables = $this->db->list_tables();
	
	foreach( $tables as $table ):
		$tableo = $this->db->get( $table );
		$backup[$table] = $tableo->result();
	endforeach;

	
	
	$this->dbforge->add_field("`key` int(11) NOT NULL AUTO_INCREMENT");
	$this->dbforge->add_field("`order` int(5) NOT NULL");
	$this->dbforge->add_field("`family_name` varchar(100) NOT NULL");
	$this->dbforge->add_field("`address` varchar(255) NOT NULL");
	$this->dbforge->add_field("`city` varchar(100) NOT NULL");
	$this->dbforge->add_field("`state` varchar(100) NOT NULL");
	$this->dbforge->add_field("`zip` varchar(25) NOT NULL");	
	$this->dbforge->add_field("`hphone` varchar(25) NOT NULL");
	$this->dbforge->add_field("`ophone` varchar(25) NOT NULL");
	$this->dbforge->add_field("`notes` longtext NOT NULL");
	$this->dbforge->add_field("`mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");
	$this->dbforge->add_key('key', TRUE);

	$this->dbforge->drop_table('families');
	$this->dbforge->create_table('families', TRUE);
	
	$this->dbforge->add_field("`key` int(11) NOT NULL AUTO_INCREMENT");	
	$this->dbforge->add_field("`family` int(11) NOT NULL");
	$this->dbforge->add_field("`member` int(11) NOT NULL");
	$this->dbforge->add_field("`fam_order` int(5) NOT NULL");
	$this->dbforge->add_field("`mem_order` int(5) NOT NULL");
	$this->dbforge->add_field("`mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");
	$this->dbforge->add_key('key', TRUE);

	$this->dbforge->drop_table('familytomember');
	$this->dbforge->create_table('familytomember', TRUE);

	$this->dbforge->add_field("`key` int(11) NOT NULL AUTO_INCREMENT");	
	$this->dbforge->add_field("`type` varchar(50) NOT NULL");
	$this->dbforge->add_field("`desc` varchar(255) NOT NULL");
	$this->dbforge->add_field("`mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");
	$this->dbforge->add_key('key', TRUE);

	$this->dbforge->drop_table('service_type');
	$this->dbforge->create_table('service_type', TRUE);

	$this->dbforge->add_field("`key` int(11) NOT NULL AUTO_INCREMENT");	
	$this->dbforge->add_field("`type` varchar(50) NOT NULL");
	$this->dbforge->add_field("`desc` varchar(255) NOT NULL");
	$this->dbforge->add_field("`mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");
	$this->dbforge->add_key('key', TRUE);

	$this->dbforge->drop_table('offering_type');
	$this->dbforge->create_table('offering_type', TRUE);

	$this->dbforge->add_field("`key` int(11) NOT NULL AUTO_INCREMENT");
	$this->dbforge->add_field("`order` int(5) NOT NULL");
	$this->dbforge->add_field("`lname` varchar(100) NOT NULL");
	$this->dbforge->add_field("`fname` varchar(100) NOT NULL");
	$this->dbforge->add_field("`dob` varchar(25) NOT NULL");
	$this->dbforge->add_field("`occupation` varchar(100) NOT NULL");
	$this->dbforge->add_field("`cphone` varchar(25) NOT NULL");
	$this->dbforge->add_field("`wphone` varchar(25) NOT NULL");
	$this->dbforge->add_field("`email` varchar(50) NOT NULL");
	$this->dbforge->add_field("`baptism` varchar(25) NOT NULL");
	$this->dbforge->add_field("`confirmation` varchar(25) NOT NULL");
	$this->dbforge->add_field("`anniversary` varchar(25) NOT NULL");
	$this->dbforge->add_field("`death` varchar(25) NOT NULL");
	$this->dbforge->add_field("`status` varchar(100) NOT NULL");
	$this->dbforge->add_field("`notes` longtext NOT NULL");
	$this->dbforge->add_field("`mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");
	$this->dbforge->add_key('key', TRUE);

	$this->dbforge->drop_table('members');
	$this->dbforge->create_table('members', TRUE);
	

	$this->dbforge->add_field("`key` int(11) NOT NULL AUTO_INCREMENT");
	$this->dbforge->add_field("`service_key` int(11) NOT NULL");
	$this->dbforge->add_field("`date` date NOT NULL");
	$this->dbforge->add_field("`member` int(11) NOT NULL");
	$this->dbforge->add_field("`service` varchar(5) NOT NULL DEFAULT 'FALSE'");
	$this->dbforge->add_field("`communion` varchar(5) NOT NULL DEFAULT 'FALSE'");
	$this->dbforge->add_field("`bibleclass` varchar(5) NOT NULL DEFAULT 'FALSE'");
	$this->dbforge->add_field("`sundayschool` varchar(5) NOT NULL DEFAULT 'FALSE'");
	$this->dbforge->add_field("`notes` longtext NOT NULL");
	$this->dbforge->add_field("`mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");
	$this->dbforge->add_key('key', TRUE);

	$this->dbforge->drop_table('membertoservice');
	$this->dbforge->create_table('membertoservice', TRUE);
	

	$this->dbforge->add_field("`key` int(11) NOT NULL AUTO_INCREMENT");
	$this->dbforge->add_field("`status` varchar(100) NOT NULL");
	$this->dbforge->add_field("`description` longtext NOT NULL");
	$this->dbforge->add_field("`member` varchar(5) NOT NULL DEFAULT '1'");
	$this->dbforge->add_field("`communicate` varchar(5) NOT NULL DEFAULT '0'");
	$this->dbforge->add_field("`list` varchar(5) NOT NULL DEFAULT '1'");
	$this->dbforge->add_field("`mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");
	$this->dbforge->add_key('key', TRUE);

	$this->dbforge->drop_table('member_status');
	$this->dbforge->create_table('member_status', TRUE);

	$this->dbforge->add_field("`key` int(11) NOT NULL AUTO_INCREMENT");
	$this->dbforge->add_field("`type` varchar(100) NOT NULL");
	$this->dbforge->add_field("`date` date NOT NULL");
	$this->dbforge->add_field("`name` varchar(50) NOT NULL");
	$this->dbforge->add_field("`offering` decimal(15,2) NOT NULL");
	$this->dbforge->add_field("`attendance` varchar(500) NOT NULL");
	$this->dbforge->add_field("`offering_details` varchar(500) NOT NULL");
	$this->dbforge->add_field("`member_attendance` mediumint(9) NOT NULL");
	$this->dbforge->add_field("`visitor_attendance` mediumint(9) NOT NULL");
	$this->dbforge->add_field("`total_attendance` mediumint(9) NOT NULL");
	$this->dbforge->add_field("`ss_attendance` mediumint(9) NOT NULL");
	$this->dbforge->add_field("`bc_attendance` mediumint(9) NOT NULL");
	$this->dbforge->add_field("`communion_attendance` mediumint(9) NOT NULL");
	$this->dbforge->add_field("`visitors` longtext NOT NULL");
	$this->dbforge->add_field("`visitors_num` int(10) NOT NULL");
	$this->dbforge->add_field("`notes` longtext NOT NULL");
	$this->dbforge->add_field("`mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");
	$this->dbforge->add_key('key', TRUE);

	$this->dbforge->drop_table('service');
	$this->dbforge->create_table('service', TRUE);
	
	$this->dbforge->add_field("`key` int(11) NOT NULL AUTO_INCREMENT");
	$this->dbforge->add_field("`name` varchar(100) NOT NULL");
	$this->dbforge->add_field("`value` varchar(255) NOT NULL");	
	$this->dbforge->add_field("`notes` longtext NOT NULL");
	$this->dbforge->add_field("`mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");
	$this->dbforge->add_key('key', TRUE);

	$this->dbforge->drop_table('site_options');
	$this->dbforge->create_table('site_options', TRUE);

	$this->dbforge->add_field("`key` int(11) NOT NULL AUTO_INCREMENT");
	$this->dbforge->add_field("`name` VARCHAR( 100 ) NOT NULL");
	$this->dbforge->add_field("`desc` VARCHAR( 100 ) NOT NULL");
	$this->dbforge->add_field("`url` VARCHAR( 255 ) NOT NULL");
	$this->dbforge->add_field("`cat` VARCHAR( 100 ) NOT NULL");	
	$this->dbforge->add_field("`mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");
	$this->dbforge->add_key('key', TRUE);

	$this->dbforge->drop_table('links');
	$this->dbforge->create_table('links', TRUE);

	$this->dbforge->add_field("`key` int(11) NOT NULL AUTO_INCREMENT");
	$this->dbforge->add_field("`service` int(11) NOT NULL");
	$this->dbforge->add_field("`date` date NOT NULL");
	$this->dbforge->add_field("`member` int(11) NOT NULL");
	$this->dbforge->add_field("`fund` VARCHAR( 100 ) NOT NULL");	
	$this->dbforge->add_field("`amount` decimal(15,2) NOT NULL");	
	$this->dbforge->add_field("`mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");
	$this->dbforge->add_key('key', TRUE);

	$this->dbforge->drop_table('offerings');
	$this->dbforge->create_table('offerings', TRUE);

	$this->dbforge->add_field("`key` int(11) NOT NULL AUTO_INCREMENT");
	$this->dbforge->add_field("`service` int(11) NOT NULL");
	$this->dbforge->add_field("`date` date NOT NULL");
	$this->dbforge->add_field("`names` mediumtext NOT NULL");
	$this->dbforge->add_field("`home_church` mediumtext NOT NULL");
	$this->dbforge->add_field("`contact_info` mediumtext NOT NULL");
	$this->dbforge->add_field("`mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");
	$this->dbforge->add_field("`ws` mediumint(9) NOT NULL");
	$this->dbforge->add_field("`ls` mediumint(9) NOT NULL");
	$this->dbforge->add_field("`bc` mediumint(9) NOT NULL");
	$this->dbforge->add_field("`ss` mediumint(9) NOT NULL");	
	$this->dbforge->add_field("`visited` mediumint(9) NOT NULL");	
	$this->dbforge->add_key('key', TRUE);

	$this->dbforge->drop_table('service_visitors');
	$this->dbforge->create_table('service_visitors', TRUE);

	$this->dbforge->add_field("`key` int(11) NOT NULL AUTO_INCREMENT");
	$this->dbforge->add_field("`name` VARCHAR(100) NOT NULL");
	$this->dbforge->add_field("`data` VARCHAR(100) NOT NULL");
	$this->dbforge->add_field("`order` smallint(6) NOT NULL");
	$this->dbforge->add_field("`visible` VARCHAR(5) NOT NULL");
	$this->dbforge->add_field("`other_data` VARCHAR(100) NOT NULL");
	$this->dbforge->add_field("`mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");
	$this->dbforge->add_key('key', TRUE);

	$this->dbforge->drop_table('frontpage_stats');
	$this->dbforge->create_table('frontpage_stats', TRUE);
	
	$this->dbforge->add_field("`key` int(11) NOT NULL AUTO_INCREMENT");
	$this->dbforge->add_field("`type` VARCHAR( 25 ) NOT NULL");
	$this->dbforge->add_field("`church` VARCHAR( 255 ) NOT NULL");
	$this->dbforge->add_field("`date` DATE NOT NULL");
	$this->dbforge->add_field("`born` VARCHAR( 25 ) NOT NULL");
	$this->dbforge->add_field("`died` VARCHAR( 25 ) NOT NULL");
	$this->dbforge->add_field("`name` VARCHAR( 100 ) NOT NULL");
	$this->dbforge->add_field("`bride` VARCHAR( 100 ) NOT NULL");
	$this->dbforge->add_field("`groom` VARCHAR( 100 ) NOT NULL");
	$this->dbforge->add_field("`parents` VARCHAR( 200 ) NOT NULL");
	$this->dbforge->add_field("`brides_parents` VARCHAR( 200 ) NOT NULL");
	$this->dbforge->add_field("`grooms_parents` VARCHAR( 200 ) NOT NULL");
	$this->dbforge->add_field("`witness_one` VARCHAR( 100 ) NOT NULL");
	$this->dbforge->add_field("`witness_two` VARCHAR( 100 ) NOT NULL");
	$this->dbforge->add_field("`official` VARCHAR( 100 ) NOT NULL");
	$this->dbforge->add_field("`passage` TEXT NOT NULL");
	$this->dbforge->add_field("`text` VARCHAR( 75 ) NOT NULL");
	$this->dbforge->add_field("`sermon` LONGTEXT NOT NULL");
	$this->dbforge->add_field("`asermon` VARCHAR( 255 ) NOT NULL");
	$this->dbforge->add_field("`reason` LONGTEXT NOT NULL");
	$this->dbforge->add_field("`direction` VARCHAR( 5 ) NOT NULL");
	$this->dbforge->add_field("`notes` LONGTEXT NOT NULL");
	$this->dbforge->add_field("`mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");
	$this->dbforge->add_key('key', TRUE);

	$this->dbforge->drop_table('records');
	$this->dbforge->create_table('records', TRUE);
	if( !empty( $backup )):
		foreach( $backup as $table => $rows ):
			foreach( $rows as $row ):
				$data = array();
				foreach( $row as $k =>$v ):
					$data[$k] = $v;
				endforeach;
				$this->db->insert( $table, $data );
			endforeach;
		endforeach;
	endif;
	
	$this->db->truncate('links');
	
	$string = "INSERT INTO links (`name`, `desc`, `url`, `cat`) VALUES ('New Service', 'Record service, attendance, offerings, and communion attendance', 'service', 'Services');
INSERT INTO links (`name`, `desc`, `url`, `cat`) VALUES ('Edit/View Service', 'view details of previously enetered services', 'view_service', 'Services');
INSERT INTO links (`name`, `desc`, `url`, `cat`) VALUES ('Delete Services', 'delete a service and all data associated with it', 'delete_services', 'Services');
INSERT INTO links (`name`, `desc`, `url`, `cat`) VALUES ('Records', 'View Church Records', 'view', 'Records');
INSERT INTO links (`name`, `desc`, `url`, `cat`) VALUES ('Baptism', 'Add baptism informatoin to Church Records', 'baptism', 'Records');
INSERT INTO links (`name`, `desc`, `url`, `cat`) VALUES ('Confirmation', 'Add Confirmation informatoin to Church Records', 'confirmation', 'Records');
INSERT INTO links (`name`, `desc`, `url`, `cat`) VALUES ('Adult Confirmation', 'Add Confirmation informatoin to Church Records', 'adult_conf', 'Records');
INSERT INTO links (`name`, `desc`, `url`, `cat`) VALUES ('Wedding', 'Add Wedding informatoin to Church Records', 'wedding', 'Records');
INSERT INTO links (`name`, `desc`, `url`, `cat`) VALUES ('Funeral', 'Add Funeral informatoin to Church Records', 'funeral', 'Records');
INSERT INTO links (`name`, `desc`, `url`, `cat`) VALUES ('Transfer', 'Add Member Transfer informatoin to Church Records', 'transfer', 'Records');
INSERT INTO links (`name`, `desc`, `url`, `cat`) VALUES ('Termination', 'Add Member Termination informatoin to Church Records', 'termination', 'Records');
INSERT INTO links (`name`, `desc`, `url`, `cat`) VALUES ('Families', 'Add/Edit/View Family Groups', 'families', 'Members');
INSERT INTO links (`name`, `desc`, `url`, `cat`) VALUES ('Members', 'Add/Edit/View Members', 'member', 'Members');
INSERT INTO links (`name`, `desc`, `url`, `cat`) VALUES ('Family Groups', 'Visual interface for editing family groups and order of display.', 'familygroups', 'Members');
INSERT INTO links (`name`, `desc`, `url`, `cat`) VALUES ('Delete Members', 'delete a member or a family group', 'delete_members', 'Members');
INSERT INTO links (`name`, `desc`, `url`, `cat`) VALUES ('Reports', 'view attendance records for members', '', 'Reports');
INSERT INTO links (`name`, `desc`, `url`, `cat`) VALUES ('Update', 'check for updates to the software', 'check_update', 'Options');
INSERT INTO links (`name`, `desc`, `url`, `cat`) VALUES ('Options', 'change options and values throughout the site', 'site_options', 'Options');
INSERT INTO links (`name`, `desc`, `url`, `cat`) VALUES ('Database', 'update, download and backup the databse', 'edit_database', 'Options')";

	$data_ar = explode( ';', $string );
	foreach($data_ar as $query) {
     		$this->db->query($query);  
	 }
	 	 
	$data = array( 'status'=>'Member', 'member'=>1, 'communicate'=>1, 'list' => 1 );
	$query = $this->db->get_where('member_status', array( 'status'=>'Member')); 
	if( $query->num_rows() < 1 ){ $this->db->insert( 'member_status', $data ); }
	$data = array( 'status'=>'Voter', 'member'=>1, 'communicate'=>1, 'list' => 1 );
	$query = $this->db->get_where('member_status', array( 'status'=>'Voter' )); 
	if( $query->num_rows() < 1 ){ $this->db->insert( 'member_status', $data ); }
	$data = array( 'status'=>'Baptized', 'member'=>1, 'communicate'=>0, 'list' => 1 );
	$query = $this->db->get_where('member_status',array( 'status'=>'Baptized') ); 
	if( $query->num_rows() < 1 ){ $this->db->insert( 'member_status', $data ); }
	$data = array( 'status'=>'Visitor', 'member'=>0, 'communicate'=>0, 'list' => 1 );
	$query = $this->db->get_where('member_status', array( 'status'=>'Visitor')); 
	if( $query->num_rows() < 1 ){ $this->db->insert( 'member_status', $data ); }
	$data = array( 'status'=>'Victorious', 'member'=>0, 'communicate'=>0, 'list' => 1 );
	$query = $this->db->get_where('member_status', array( 'status'=>'Victorious')); 
	if( $query->num_rows() < 1 ){ $this->db->insert( 'member_status', $data ); }
	$data = array( 'status'=>'Ex-member', 'member'=>0, 'communicate'=>0, 'list' => 1 );
	$query = $this->db->get_where('member_status', array( 'status'=>'Ex-member')); 
	if( $query->num_rows() < 1 ){ $this->db->insert( 'member_status', $data ); }
	$data = array( 'status'=>'Transferred ', 'member'=>0, 'communicate'=>0, 'list' => 1 );
	$query = $this->db->get_where('member_status', array( 'status'=>'Transferred ')); 
	if( $query->num_rows() < 1 ){ $this->db->insert( 'member_status', $data ); }
	

		$query = $this->db->get_where('site_options', array( 'name'=>'server_timezone' )); 
		if( $query->num_rows() < 1 ){ date_default_timezone_set( 'America/Chicago' ); }
		else { 
		$query_ar = $query->result();
		date_default_timezone_set( $query_ar[0]->value );
		}


	$default_options = array(
							'site_name' => 'Living Numbers',
							'site_title' => 'Living Numbers',
							'site_plug' => 'I am not ashamed of the Gospel of Christ!',
							'site_theme' => 'dot-luv/dot-luv.css',
							'timezone' => 'America/Chicago',
							'server_timezone' => 'America/Chicago',
							'chk_timezone' => date( 'y-m-j h:i:s', time()) ,
							'up_save_data' => '',
							'down_save_data' => '',
							'visitor_fields' => 3,
							'sp_view_totals' => 0,
							'sp_view_members' => 1,
							'sp_view_visitors' => 1,
							'sp_view_attendance' => 1,
							'sp_view_offerings' => 1,
							'sp_view_notes' => 1,
							'sp_view_home_church' => 1,
							'sp_view_contact_info' => 1,
							'sp_view_merge' => 0,
							'sort_members' => 'alphabetical',
							'help_menu' => 1,
							'last_month' => '06',
							'up_use_ftp' => 0,
							'up_password' => '',
							'up_user_name' => '',
							'up_hostname' => '',
							'up_servername' => '',
							);
							
	foreach ( $default_options as $ok => $ov ){
		$data = array( 'name'=> $ok, 'value'=> $ov );
		$query = $this->db->get_where('site_options', array( 'name'=>$ok )); 
		if( $query->num_rows() < 1 ){ $this->db->insert( 'site_options', $data ); }
	}


	$this->db->delete( 'site_options', array( 'name' => 'version' )); 
	$data = array( 'name'=>'version', 'value'=> $version_num );
	$this->db->insert( 'site_options', $data ); 
	
	
	$data = array( 'type'=>'Worship Service', 'desc'=>'normal weekly worship service' );
	$query = $this->db->get_where('service_type', array( 'type'=>'Worship Service')); 
	if( $query->num_rows() < 1 ){ $this->db->insert( 'service_type', $data ); }

	
	$default_functions = array(
							array( 
									'name' => 'least_sacramental_members',
									'data' => "members",
									'order' => 3,
									'visible' => 1,
									'other_data' => 15,
								),
							array( 
									'name' => 'deliquent_members_by_last_service',
									'data' => "members",
									'order' => 1,
									'visible' => 1,
									'other_data' => 3,
								),
							array( 
									'name' => 'visitors_to_contact',
									'data' => "visitors",
									'order' => 4,
									'visible' => 1,
								),
							array( 
									'name' => 'yearly_stats',
									'data' => "year_stats",
									'order' => 6,
									'visible' => 1,
								),
							array( 
									'name' => 'last_service_stats',
									'data' => "last_service",
									'order' => 5,
									'visible' => 1,
								),
							array( 
									'name' => 'least_attendant_members',
									'data' => "members",
									'order' => 2,
									'visible' => 1,
									'other_data' => 15,
								),
							);
							
	foreach ( $default_functions as $function ){
		$data = $function;
		$query = $this->db->get_where('frontpage_stats', array( 'name'=>$function['name'] )); 
		if( $query->num_rows() < 1 ){ $this->db->insert( 'frontpage_stats', $data ); }
	}
	
		$flashdata = 'Site Installed or Updated, site ready for your use. Please check over the site options.';		
		$this->session->set_flashdata('notice', $flashdata);	
		
		redirect( 'options/site_options' );		
	}
	
}
