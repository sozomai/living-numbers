<?php

class FileUpgrade extends Controller {
		
	function fileupgrade()
	{
		parent::Controller();
		
		$this->load->helper('file');
		$this->load->model('Site');
		$this->load->model('Upgrademodel');
		
	}
	
	function index ()
	{
		$site_options = $this->Site->site_options();
		$output = "";
				
		if( $site_options['up_use_ftp'] ){

			$this->load->library('ftp');
			$config['hostname'] = 'ftp.'.$site_options['up_hostname'];
			$config['username'] = $site_options['up_user_name'];
			$config['password'] = $site_options['up_password'];
			$config['debug'] = TRUE;
			$this->ftp->connect($config);
			$server_path = $site_options['up_servername'].'/system/';
		
			$tmp_dir = file_exists('./system/application/tmp');
			
			if( $tmp_dir == 1 ){ 
				$fPerms = octal_permissions(fileperms('./system/application/controllers'));
			} else { $fPerms = '777'; }			
	
			if( $fPerms != '777'  ):

			$output .= "Setting permissions!<br>";
			
			$tmp_dir = file_exists('./system/application/tmp');
			
				if( $tmp_dir == 1 ):
					$this->ftp->chmod($server_path.'application/tmp', DIR_WRITE_MODE )	;
				endif;
			
			endif;
			
		}
		
		$files_get = read_file('./system/application/tmp/files.txt');

		
		if( $files_get == FALSE ){
			$tmp_dir = file_exists( './system/application/tmp' );
			if( $tmp_dir == 0 ):
				mkdir('./system/application/tmp');
			endif;
			$files_get = file_get_contents('http://members.clclutheran.net/downloadable/files.txt');
			write_file('./system/application/tmp/files.txt', $files_get );
			$location = '';
			$output .=  "Will continue in less than ONE minute, if and ONLY if it does not continue <a href='' >click here</a>";
		} else {
			$files = explode(';', $files_get);
			$leftovers = '';
			foreach( $files as $x => $file ){
				if( $x < 11 ){
					$file = trim( $file );
					if( $file == "DONE" ){
						
						$output .= "All Files Updated Successfully";
						$location = '../index.php/install/upgrade';
						$output .=  "Will continue in less than ONE minute, if and ONLY if it does not continue <a href='../index.php/install/upgrade' >click here</a>";
						$this->Upgrademodel->html_output ( $location, $output );		
						exit();				
					} 				
					$output .=  $file.'<br> ';

					if( $site_options['up_use_ftp'] ){
						$data = file_get_contents('http://members.clclutheran.net/downloadable/'.$file.'.txt'); 
						if( !empty ($data) ):
							$filez = explode( '/', $file );
							foreach( $filez as $f ){ $fileName = $f; } 
							write_file('./system/application/tmp/'.$fileName, $data, 'w+b');
							$this->ftp->move($server_path.'application/tmp/'.$fileName, $server_path.'application/'.$file );
						endif;	
						
						
					} else {
						$data = file_get_contents('http://members.clclutheran.net/downloadable/'.$file.'.txt'); 
						if( !empty ($data) ):
							write_file('./system/application/'.$file, $data );
						endif;
					}
				} else {
					$leftovers .= $file.';';
				}
			}
			write_file('./system/application/tmp/files.txt', $leftovers );
			$location = '';
			$output .=  "Will continue in less than ONE minute, if and ONLY if it does not continue <a href='' >click here</a>";
		}
		
		$this->Upgrademodel->html_output ( $location, $output );	
		
	}
	

	
}